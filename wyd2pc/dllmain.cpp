// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"


#if !defined(_MSC_VER)
int32_t __attribute__((__cdecl__)) CalcAddress(int32_t addr)
{
	return (int32_t)GetModuleHandleA(0) + addr;
}
#endif

BOOL APIENTRY DllMain(HMODULE hModule,
					  DWORD  ul_reason_for_call,
					  LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		{
            #if defined(_CONSOLE_)
			AllocConsole();
			freopen("CONOUT$", "w", stdout);
            #endif
			srand((uint32_t)(time(0) * clock()));
			srand((uint32_t)(time(0)));
			Packets.Start();
			break;
		}
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
	}
	return TRUE;
}

