// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
// http://anti-reversing.com/Downloads/Anti-Reversing/The_Ultimate_Anti-Reversing_Reference.pdf

#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#define _CRT_SECURE_NO_WARNINGS
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define DEFAULT_COLOR 0xFF99FF22

//#define _CONSOLE_ 1

#if defined(_DEBUG)
#define _DEBUG_
//#define KOFD

#if !defined(_CONSOLE_)
	#define _CONSOLE_ 1
#endif

#endif

//#if !defined(_DEBUG_)
//#define _DEBUG_
//#endif

//filter
#define ENABLE_AMOUNT
#define ENABLE_BAUF
#define ENABLE_DROPF
#define ENABLE_USEF
#define ENABLE_DROP
#define ENABLE_BAU
#define ENABLE_PARTY
#define ENABLE_MOVED
#define ENABLE_MOVE
#define ENABLE_GRIFO
#define _LOAD_INI_

#if defined(_DEBUG_)
//#define _LOAD_INI_

#define ENABLE_PAKF
#define ENABLE_DUMPF

//commands
//#define ENABLE_REC
#define ENABLE_DENY
#define ENABLE_SKILL
#define ENABLE_PAK
#define ENABLE_SHOWID
#define ENABLE_SAME
#define ENABLE_ITEM
#define ENABLE_SHIFT
#define ENABLE_NSHIFT
#define ENABLE_USE
#define ENABLE_NUSE
#define ENABLE_NCLICK
#define ENABLE_TROCA
#define ENABLE_BARRAS
#define ENABLE_BARRAS2
#define ENABLE_COMPRA
#define ENABLE_NMOVE
#define ENABLE_GUARDA
#define ENABLE_REC
#define ENABLE_DEL
#define ENABLE_AMG
#define ENABLE_MONT
#define ENABLE_FAKE
#define ENABLE_EHRE
#define ENABLE_REF
#define ENABLE_INV

#endif

//#define MYD 1
//#define OVER
#include <map>
#include <list>
#include <Windows.h>
#include <Psapi.h>
#include <stdint.h>
#include <stdarg.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <thread>
//#include <chrono>
#include <winsock2.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
using namespace std;

#ifdef BUILD_DLL
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT __declspec(dllimport)
#endif

#define _naked __declspec(naked)
#define asm_call_jmp __asm      \
                    {\
                        __asm nop \
                        __asm nop \
                        __asm nop \
                        __asm nop \
                        __asm nop \
                    }
#define resume_stack asm("add esp, 0x1C");

#if !defined(_MSC_VER)
#define __forceinline inline
#if defined(__GNUC__) && defined(__GNUC_MINOR__)
 #define GNUC_VERSION \
     (__GNUC__ << 16) + __GNUC_MINOR__
 #define GNUC_PREREQ(maj, min) \
     (GNUC_VERSION >= ((maj) << 16) + (min))
#else
 #define GNUC_PREREQ(maj, min) 0
#endif
#define BUILD_BUG_ON_ZERO(e) \
    (sizeof(struct { int:-!!(e)*1234; }))
#if GNUC_PREREQ(3, 1)
 #define SAME_TYPE(a, b) \
     __builtin_types_compatible_p(typeof(a), typeof(b))
 #define MUST_BE_ARRAY(a) \
     BUILD_BUG_ON_ZERO(SAME_TYPE((a), &(*a)))
#else
 #define MUST_BE_ARRAY(a) \
     BUILD_BUG_ON_ZERO(sizeof(a) % sizeof(*a))
#endif
#ifdef __cplusplus
 template <typename T, size_t N>
 char ( &ARRAY_SIZE_HELPER( T (&array)[N] ))[N];
 #define ARRAY_SIZE( array ) \
      (sizeof( ARRAY_SIZE_HELPER( array ) ))
#else
 #define ARRAY_SIZE(a) ( \
     (sizeof(a) / sizeof(*a)) \
     + MUST_BE_ARRAY(a))
#endif
#define _countof ARRAY_SIZE
#endif
#define TIMEOUT_PROXY 15

#include "itemeffect.h"
#include "data.h"
#include "struct.h"
#include "static.h"
#include "enc.h"
#include "wyd2pc.h"
