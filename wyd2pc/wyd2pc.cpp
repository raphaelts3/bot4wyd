#include "stdafx.h"

//#define MYD 1

enum e_timers
{
	WM_PACKETS = 0,
	WM_MACRO,
	WM_CHECKS
};
uint8_t send_don[0xFA];
uint8_t send_don_dir[0x910];
uint32_t timers_id[3];
HINSTANCE clientPatch;
HINSTANCE donPatch;
cPackets Packets;
MSG_MOVE packet_move;
static uint32_t client_chat_color = 0xFFFFAAAA;
int32_t patched = 0;
int32_t loginCode = 0;
int32_t eraseaddr = 0;
static int32_t addr_my_calls = 0;
//uint8_t naked_client_chat[] = { 0xFF, 0x35, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x75, 0x08 };
//uint8_t naked_hook_chat[] = { 0x8B ,0x85 ,0x68 ,0xFF ,0xFF ,0xFF ,0x8B ,0x10 ,0x8B ,0x8D ,0x68 ,0xFF ,0xFF ,0xFF ,0xFF ,0x92 ,0x88 ,0x00 ,0x00 ,0x00 ,0x50 ,0xE8 ,0xF3 ,0xA3 ,0xEA ,0x11 ,0x83 ,0xC4 ,0x04 ,0x83 ,0xF8 ,0x00 ,0x75 ,0x0A ,0x68 ,0x00 ,0x00 ,0x00 ,0x00 ,0xE9 ,0x00 ,0x00 ,0x00 ,0x00 ,0xE9 ,0x00 ,0x00 ,0x00 ,0x00 };
int32_t sChat(char *str);
void sFilterPacket(MSG_HEADER *packet, int32_t _size, int32_t send, int32_t callback);

TIMEGETTIME TimeGetTime = (TIMEGETTIME)GetProcAddress(GetModuleHandleA("Winmm.dll"), "timeGetTime");

_naked void naked_hook_chat_()
{
#if defined(_MSC_VER)
	__asm
	{
		mov eax, dword ptr ss : [ebp - 0x98]
		mov edx, dword ptr ds : [eax]
		mov ecx, dword ptr ss : [ebp - 0x98]
		call dword ptr ds : [edx + 0x88]
		push eax
		call sChat
		add esp, 4
		or eax, eax
		jnz _not_zero
	}
	addr_hook_chat_help;
	__asm { push eax }
	addr_hook_chat_bypass;
	__asm { jmp eax }
_not_zero:
	addr_hook_chat_clean;
	__asm { jmp eax }
#else
	resume_stack;
	asm("mov eax, dword ptr ss : [ebp - 0x98];\
	mov edx, dword ptr ds : [eax];\
	mov ecx, dword ptr ss : [ebp - 0x98];\
	call dword ptr ds : [edx + 0x88];\
	push eax;\
	call __Z5sChatPc;\
	add esp, 4;\
	or eax, eax;\
	jnz _not_zero;"
	);
	addr_hook_chat_help;
	asm("push eax");
	addr_hook_chat_bypass;
	asm("jmp eax;\
_not_zero:");
	addr_hook_chat_clean;
	asm("jmp eax");
#endif
}
//uint8_t naked_hook_send[] = { 0x8D ,0x44 ,0xE4 ,0x04 ,0x8B ,0xEC ,0x83 ,0xEC ,0x08 ,0x89 ,0x4D ,0xF8 ,0x50 ,0x68 ,0x01 ,0x00 ,0x00 ,0x00 ,0xFF ,0x75 ,0x0C ,0xFF ,0x75 ,0x08 ,0xE8 ,0x4B ,0xCE ,0xCF ,0xFF ,0x83 ,0xC4 ,0x10 ,0x8B ,0x4D ,0xF8 ,0xE9 ,0x00 ,0x00 ,0x00 ,0x00 };
_naked void naked_hook_send_()
{
#if defined(_MSC_VER)
	__asm
	{
		lea eax, dword ptr ss : [ebp + 4]
		mov ebp, esp
		sub esp, 8
		mov dword ptr ss : [ebp - 8], ecx
		push eax
		push 1
		push dword ptr ss : [ebp + 12]
		push dword ptr ss : [ebp + 8]
		call sFilterPacket
		add esp, 16
	}
	addr_hook_send_r;
	__asm
	{
		mov ecx, dword ptr ss : [ebp - 8]
		jmp eax
	}
#else
	resume_stack;
	asm("lea eax, dword ptr ss : [ebp + 4];\
		mov ebp, esp;\
		sub esp, 8;\
		mov dword ptr ss : [ebp - 8], ecx;\
		push eax;\
		push 1;\
		push dword ptr ss : [ebp + 12];\
		push dword ptr ss : [ebp + 8];\
		call __Z13sFilterPacketP10MSG_HEADERiii;\
		add esp, 16;"
	);
	addr_hook_send_r;
	asm("mov ecx, dword ptr ss : [ebp - 8];\
		jmp eax;");
#endif
}
//uint8_t naked_hook_send2[] = { 0x6A, 0x00, 0x6A, 0x02, 0xFF, 0x75, 0x0C, 0xFF, 0x75, 0x08, 0xE8, 0xBB, 0x6E, 0x09, 0x00, 0x83, 0xC4, 0x10, 0xC7, 0x45, 0xF8, 0x00, 0x00, 0x00, 0x00, 0xE9, 0xAC, 0x6E, 0x09, 0x00 };
_naked void naked_hook_send2_()
{
#if defined(_MSC_VER)
	__asm
	{
		push 0
		push 2
		push dword ptr ss : [ebp + 12]
		push dword ptr ss : [ebp + 8]
		call sFilterPacket
		add esp, 16
		mov dword ptr ss : [ebp - 8], 0
	}
#ifdef DON
	addr_hook_send2_r;
#endif
	__asm
	{
		jmp eax
	}
#else
	resume_stack;
	asm("push 0;\
		push 2;\
		push dword ptr ss : [ebp + 12];\
		push dword ptr ss : [ebp + 8];\
		call __Z13sFilterPacketP10MSG_HEADERiii;\
		add esp, 16;\
		mov dword ptr ss : [ebp - 8], 0;"
	);
#ifdef DON
	addr_hook_send2_r;
#endif
	asm("jmp eax;");
#endif
}
//uint8_t naked_hook_recv[] = { 0x6A ,0x00 ,0x6A ,0x00 ,0x33 ,0xD2 ,0x8B ,0x85 ,0xA0 ,0xFE ,0xFF ,0xFF ,0x66 ,0x8B ,0x10 ,0x52 ,0x50 ,0xE8 ,0xC8 ,0xFC ,0x1F ,0x00 ,0x83 ,0xC4 ,0x10 ,0x8B ,0x95 ,0xA0 ,0xFE ,0xFF ,0xFF ,0xE9 ,0xBA ,0xFC ,0x1F ,0x00 };
_naked void naked_hook_recv_()
{
#if defined(_MSC_VER)
	__asm
	{
		push 0
		push 0
		xor edx, edx
#if defined(OVER)
		mov eax, dword ptr ss : [ebp - 0x15C]
#else
		mov eax, dword ptr ss : [ebp - 0x160]
#endif
		mov dx, word ptr ds : [eax]
		push edx
		push eax
		call sFilterPacket
		add esp, 16
	}
	addr_hook_recv_r;
#if defined(OVER)
	__asm
	{
		mov edx, dword ptr ss : [ebp - 0x15C]
	}
#elif defined(MYD)
	__asm
	{
		mov ecx, dword ptr ss : [ebp - 0x160]
	}
#else
	__asm
	{
		mov edx, dword ptr ss : [ebp - 0x160]
	}
#endif
	__asm
	{
		jmp eax
	}
#else
	resume_stack;
	asm("push 0;\
		push 0;\
		xor edx, edx;\
		mov eax, dword ptr ss : [ebp - 0x160];\
		mov dx, word ptr ds : [eax];\
		push edx;\
		push eax;\
		call __Z13sFilterPacketP10MSG_HEADERiii;\
		add esp, 16;");
	addr_hook_recv_r;
#if !defined(MYD)
	asm("mov edx, dword ptr ss : [ebp - 0x160]");
#else
	asm("mov ecx, dword ptr ss : [ebp - 0x160]");
#endif
	asm("jmp eax");
#endif
}

int32_t DebuggerCheckRetn;
int32_t DebuggerCheck(void)
{
	//printf("OK\n");
	return DebuggerCheckRetn;
}

int32_t CheckSumRetn;
int32_t CheckSum(void)
{
	//printf("OK2\n");
	return CheckSumRetn;
}

void cPackets::InsertPacket(time_t t, MSG_HEADER *pak)
{
	int32_t p;
	for (p = 0; p < 64; p++)
	{
		if (TimerPacket[p].time == 0)
		{
			memcpy(TimerPacket[p].buffer, pak, pak->Size);
			TimerPacket[p].time = t;
			break;
		}
	}
}

static int64_t *crandomseed;
int32_t crandom()
{
	if (*crandomseed == 0)
	{
		*crandomseed = 2531011;
	}
	int32_t hi = (int32_t)(*crandomseed / 127773);
	int32_t lo = (int32_t)(*crandomseed % 127773);
	int32_t tmp = (16807 * lo) - (2836 * hi);
	if (tmp < 0)
	{
		tmp += 0x7FFFFFFF;
	}
	*crandomseed = tmp;
	return (((tmp >> 16) | tmp) & 0xFFFF);
}

int32_t __stdcall TunnelProxy_GetThis(const char *ip, int32_t port, int32_t unk1, int32_t unk2)
{
	return Packets.TunnelProxy(*(int32_t*)addr_send_class, ip, port, unk1, unk2);
}

int32_t mCloseSocket()
{
	char *ptr = (char*)*(int32_t*)addr_send_class;
	Packets.Macro.n = 0;
	*(int32_t*)&ptr[12] = 0;
	*(int32_t*)&ptr[16] = 0;
	*(int32_t*)&ptr[20] = 0;
	*(int32_t*)&ptr[24] = 0;
	*(int32_t*)&ptr[28] = 0;
	if (*(int32_t*)ptr != 0)
	{
		closesocket(*(int32_t*)ptr);
		*(int32_t*)ptr = 0;
		if (Packets.online)
		{
			if (Packets.proxy_mode)
			{
				
			}
			
		}
	}
	return 1;
}

int32_t cPackets::TunnelProxy(int32_t _this, const char *ip, int32_t port, int32_t bindIp, int32_t hMsg)
{
	int32_t addr_call_1 = addr_hook_connect;
	__asm
	{
		push hMsg
		push bindIp
		push port
		push ip
		mov ecx, _this
		call addr_call_1
		ret
	}
	struct timeval timeout;
	fd_set set;
	uint32_t proxy, sock;
	SOCKADDR_IN proxyInfo, serverInfo, bindInfo;
	int32_t ret;
	uint32_t err;
	char *ptr = (char*)_this;
	timeout.tv_sec = 0;
	timeout.tv_usec = TIMEOUT_PROXY * 1000 * 1000;
	FD_ZERO(&set);
	proxy_port = 0;
	*(int32_t*)&ptr[12] = 0;
	*(int32_t*)&ptr[16] = 0;
	*(int32_t*)&ptr[20] = 0;
	*(int32_t*)&ptr[24] = 0;
	if (*(int32_t*)ptr != 0)
	{
		*(int32_t*)&ptr[28] = 0;
		WSAAsyncSelect(*(int32_t*)ptr, *(HWND*)(addr_hwnd), 0, 0);
		closesocket(*(int32_t*)ptr);
		*(int32_t*)ptr = 0;
	}
	if (proxy_mode != 0)
	{
		do
		{
			
		} while (proxy_port == 0);
	}

	memset(&serverInfo, 0, sizeof(serverInfo));
	memset(&bindInfo, 0, sizeof(bindInfo));
	bindInfo.sin_family = AF_INET;
	bindInfo.sin_addr.s_addr = (unsigned long)bindIp;
	bindInfo.sin_port = 0;

	serverInfo.sin_family = AF_INET;
	serverInfo.sin_addr.s_addr = inet_addr(ip);
	serverInfo.sin_port = htons(port);

	if (proxy_port)
	{
#if defined(_DEBUG_)
		Show(DEFAULT_COLOR, "Proxy: %s:%d", proxy_ip, proxy_port);
#endif
		/* PROXY */
		proxy = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (proxy == INVALID_SOCKET)
		{
#if defined(_DEBUG_)
			Show(DEFAULT_COLOR, "Erro-1 proxy! %d", GetLastError());
#endif
			
			MessageBoxA(0, "Initialize socket fail", "ERROR", MB_OK | MB_APPLMODAL);
			return 0;
		}

		if (bind((SOCKET)proxy, (const sockaddr*)&bindInfo, (int32_t)sizeof(bindInfo)) == -1)
		{
#if defined(_DEBUG_)
			Show(DEFAULT_COLOR, "Erro-1 bind! %d", GetLastError());
#endif
			*(int32_t*)(addr_port_bind) += 10;
			bindInfo.sin_port = htons(*(int32_t*)(addr_port_bind)+5000);
			if (bind((SOCKET)proxy, (const sockaddr*)&bindInfo, (int32_t)sizeof(bindInfo)) == -1)
			{
#if defined(_DEBUG_)
				Show(DEFAULT_COLOR, "Erro-2 bind! %d", GetLastError());
#endif
				*(int32_t*)(addr_port_bind) += 10;
				bindInfo.sin_port = htons(*(int32_t*)(addr_port_bind)+5000);
				if (bind((SOCKET)proxy, (const sockaddr*)&bindInfo, (int32_t)sizeof(bindInfo)) == -1)
				{
#if defined(_DEBUG_)
					Show(DEFAULT_COLOR, "Erro-3 bind! %d", GetLastError());
#endif
					MessageBoxA(0, "Binding fail", "ERROR", MB_OK | MB_APPLMODAL);
					
					closesocket(proxy);
					TunnelProxy(_this, ip, port, bindIp, hMsg);
					return 0;
				}
			}
		}

		FD_SET(proxy, &set);
		long unsigned int iMode = 1;
		ioctlsocket(proxy, FIONBIO, &iMode);

		proxyInfo.sin_family = AF_INET;
		proxyInfo.sin_addr.s_addr = inet_addr(proxy_ip);
		proxyInfo.sin_port = htons(proxy_port);
		if (connect(proxy, (struct sockaddr *)&proxyInfo, sizeof(proxyInfo)) < 0)
		{
			err = GetLastError();
			if (err != WSAEWOULDBLOCK)
			{
#if defined(_DEBUG_)
				Show(DEFAULT_COLOR, "Erro-2 proxy! %d", err);
#endif
				closesocket(proxy);
				TunnelProxy(_this, ip, port, bindIp, hMsg);
				return 0;
			}
		}

		ret = select(proxy + 1, 0, &set, NULL, &timeout);
		if (ret == 0)
		{
#if defined(_DEBUG_)
			Show(DEFAULT_COLOR, "Erro-T proxy! %d", GetLastError());
#endif
			closesocket(proxy);
			TunnelProxy(_this, ip, port, bindIp, hMsg);
			return 0;
		}
		else if (ret == -1)
		{
#if defined(_DEBUG_)
			Show(DEFAULT_COLOR, "Erro-S proxy! %d", GetLastError());
#endif
			closesocket(proxy);
			TunnelProxy(_this, ip, port, bindIp, hMsg);
			return 0;
		}

		iMode = 0;
		ioctlsocket(proxy, FIONBIO, &iMode);

		//http://www.xoriant.com/blog/software-product-development/socks5-proxy-client-in-c.html
		//https://en.wikipedia.org/wiki/SOCKS#SOCKS5
		uint8_t proxyPacket[16] = { 5, 1, 0 };
		if (send(proxy, (char*)proxyPacket, 3, 0) == -1)
		{
#if defined(_DEBUG_)
			Show(DEFAULT_COLOR, "Proxy[send-1] Fail!");
#endif
			closesocket(proxy);
			TunnelProxy(_this, ip, port, bindIp, hMsg);
			return 0;
		}
		if (recv(proxy, (char*)proxyPacket, 2, 0) == -1)
		{
#if defined(_DEBUG_)
			Show(DEFAULT_COLOR, "Proxy[recv-1] Fail!");
#endif
			closesocket(proxy);
			TunnelProxy(_this, ip, port, bindIp, hMsg);
			return 0;
		}
		if (proxyPacket[0] == 5 && proxyPacket[1] != 0xFF)
		{
			int32_t ret;
			char log[64] = "";
#if defined(_DEBUG_)
			Show(DEFAULT_COLOR, "Proxy[1] OK");
#endif
			proxyPacket[1] = 1;//TCP
			proxyPacket[2] = 0;
			proxyPacket[3] = 1;//IPv4
			memcpy(&proxyPacket[4],
				&serverInfo.sin_addr.s_addr,
				sizeof(serverInfo.sin_addr.s_addr));
			*(u_short*)&proxyPacket[8] = serverInfo.sin_port;
			if (send(proxy, (char*)proxyPacket, 10, 0) == -1)
			{
#if defined(_DEBUG_)
				Show(DEFAULT_COLOR, "Proxy[send-2] Fail!");
#endif
				closesocket(proxy);
				TunnelProxy(_this, ip, port, bindIp, hMsg);
				return 0;
			}
			ret = recv(proxy, (char*)proxyPacket, sizeof(proxyPacket) - 1, 0);
			if (ret == -1)
			{
#if defined(_DEBUG_)
				Show(DEFAULT_COLOR, "Proxy[recv-2] Fail!");
#endif
				closesocket(proxy);
				TunnelProxy(_this, ip, port, bindIp, hMsg);
				return 0;
			}
#if defined(_DEBUG_)
			for (int32_t c = 0; c < ret; c++)
			{
				sprintf(&log[c * 3], "%02X ", proxyPacket[c]);
			}
			Show(DEFAULT_COLOR, "Proxy[-] : %s", log);
#endif
			if (proxyPacket[0] == 5 && proxyPacket[1] == 0)
			{
#if defined(_DEBUG_)
				Show(DEFAULT_COLOR, "Proxy[2] OK-%d", ret);
#endif
			}
			else
			{
#if defined(_DEBUG_)
				Show(DEFAULT_COLOR, "Erro-3 proxy(%d)!", proxyPacket[1]);
#endif
				closesocket(proxy);
				TunnelProxy(_this, ip, port, bindIp, hMsg);
				return 0;
			}
		}
		else
		{
#if defined(_DEBUG_)
			Show(DEFAULT_COLOR, "Erro-4 proxy(%d)!", proxyPacket[1]);
#endif
			closesocket(proxy);
			TunnelProxy(_this, ip, port, bindIp, hMsg);
			return 0;
		}
		sock = proxy;
	}
	else
	{
		sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (sock == INVALID_SOCKET)
		{
#if defined(_DEBUG_)
			Show(DEFAULT_COLOR, "Erro socket! %d", GetLastError());
#endif
			MessageBoxA(0, "Initialize socket fail", "ERROR", MB_OK | MB_APPLMODAL);
			return 0;
		}

		if (bind((SOCKET)sock, (const sockaddr*)&bindInfo, (int32_t)sizeof(bindInfo)) == -1)
		{
#if defined(_DEBUG_)
			Show(DEFAULT_COLOR, "Erro-1 bind! %d", GetLastError());
#endif
			*(int32_t*)(addr_port_bind) += 10;
			bindInfo.sin_port = htons(*(int32_t*)(addr_port_bind)+5000);
			if (bind((SOCKET)sock, (const sockaddr*)&bindInfo, (int32_t)sizeof(bindInfo)) == -1)
			{
#if defined(_DEBUG_)
				Show(DEFAULT_COLOR, "Erro-2 bind! %d", GetLastError());
#endif
				*(int32_t*)(addr_port_bind) += 10;
				bindInfo.sin_port = htons(*(int32_t*)(addr_port_bind)+5000);
				if (bind((SOCKET)sock, (const sockaddr*)&bindInfo, (int32_t)sizeof(bindInfo)) == -1)
				{
#if defined(_DEBUG_)
					Show(DEFAULT_COLOR, "Erro-3 bind! %d", GetLastError());
#endif
					MessageBoxA(0, "Binding fail", "ERROR", MB_OK | MB_APPLMODAL);
					closesocket(sock);
					TunnelProxy(_this, ip, port, bindIp, hMsg);
					return 0;
				}
			}
		}

		FD_SET(sock, &set);
		long unsigned int iMode = 1;
		ioctlsocket(sock, FIONBIO, &iMode);

		if (connect(sock, (struct sockaddr *)&serverInfo, sizeof(serverInfo)) < 0)
		{
			err = GetLastError();
			if (err != WSAEWOULDBLOCK)
			{
#if defined(_DEBUG_)
				Show(DEFAULT_COLOR, "Erro connect! %d", err);
#endif
				closesocket(sock);
				TunnelProxy(_this, ip, port, bindIp, hMsg);
				return 0;
			}
		}

		ret = select(sock + 1, 0, &set, NULL, &timeout);
		if (ret == 0)
		{
#if defined(_DEBUG_)
			Show(DEFAULT_COLOR, "Erro-T sock! %d", GetLastError());
#endif
			closesocket(sock);
			TunnelProxy(_this, ip, port, bindIp, hMsg);
			return 0;
		}
		else if (ret == -1)
		{
#if defined(_DEBUG_)
			Show(DEFAULT_COLOR, "Erro-S sock! %d", GetLastError());
#endif
			closesocket(sock);
			TunnelProxy(_this, ip, port, bindIp, hMsg);
			return 0;
		}

		iMode = 0;
		ioctlsocket(sock, FIONBIO, &iMode);
	}
	ret = WSAAsyncSelect(sock, *(HWND*)(addr_hwnd), hMsg, FD_READ | FD_CLOSE);
#if defined(_DEBUG_)
	Show(0xFF99FF22, "WSAAsyncSelect %d - %d", ret, WSAGetLastError());
#endif
	if (ret != 0)
	{
		if (proxy_port != 0)
		{
			//get new proxy
		}
		closesocket(sock);
		TunnelProxy(_this, ip, port, bindIp, hMsg);
		return 0;
	}
	static const uint32_t hellos[] = { 0x1F000000, 0x110000, 0xF300, 0x11 };
	uint32_t hello = 0;
	for (int32_t ih = 0; ih < 4; ih++)
	{
		hello |= hellos[ih];
	}
	send(sock, (char*)&hello, 4, 0);
	*(int32_t*)ptr = sock;
	*(int32_t*)&ptr[28] = 1;
	return 1;
}

int32_t cPackets::IsOnline()
{
	if (*(int32_t*)(addr_acc) &&
		*(int32_t*)(*(int32_t*)(addr_acc)+offset_acc) > 0 && *(int32_t*)(*(int32_t*)(addr_acc)+offset_acc) < 1000 &&
		*(int32_t*)(*(int32_t*)addr_send_class) > 0)
	{
		return 1;
	}
	return 0;
}

void cPackets::Show(uint32_t color, const char *str, ...)
{
	char msg[256];
#if defined(MYD)
	int32_t _ecx_1 = *(int32_t*)(*(int32_t*)(addr_ptrdat)+0x27B80);
#elif defined (OVER)
	int32_t _ecx_1 = *(int32_t*)(*(int32_t*)(addr_ptrdat)+0x27AC8);
#else
	int32_t _ecx_1 = *(int32_t*)(*(int32_t*)(addr_ptrdat)+0x27AB0);
#endif
	int32_t _addr_call_1 = addr_client_chat_1, _addr_call_2 = addr_client_chat_2, _addr_call_3 = addr_client_chat_3;
	va_list pArgList;
	va_start(pArgList, str);
	vsprintf(msg, str, pArgList);
	va_end(pArgList);
	if (color != -1 && myID != -1 && IsOnline())
	{
		__try
		{
#if defined(_MSC_VER)
			__asm
			{
				push 0xE54
				call _addr_call_1
				add esp, 4
				cmp eax, 0
				je __end
				mov ecx, eax
				push 0
				push 1
				push 0x77777777
				push 0
				push 0x41800000
				push 0x43960000
				push 0
				push 0
				push color
				lea eax, msg
				push eax
				call _addr_call_2
				push eax
				mov ecx, _ecx_1
				call _addr_call_3
				__end :
				nop
			}
#else
			asm("pushad;\
			 push 0xE54;\
			 call _addr_call_1;\
			 add esp, 4;\
			 mov ecx, eax;\
			 push 0;\
			 push 1;\
			 push 0x77777777;\
			 push 0;\
			 push 0x41800000;\
			 push 0x48396000;\
			 push 0;\
			 push 0;\
			 push color;\
			 lea eax, msg;\
			 push eax;\
			 call _addr_call_2;\
			 push eax;\
			 mov ecx, _ecx_1;\
			 call _addr_call_3;\
			 popad"
			);
#endif
		}
#if defined(_MSC_VER)
		__except (1)
		{
		}
#else
		catch (...)
		{
		}
#endif
	}
	WriteLog(msg);
#ifdef _SEC_
	iPushSS();
#endif
}

void cPackets::WriteLog(const char *str)
{
	time_t _t = time(0);
	struct tm *t = localtime(&_t);
	if (f_mainlog)
	{
		fprintf(f_mainlog, "[%02d %02d:%02d:%02d] > %s\n",
			t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec, str);
		fflush(f_mainlog);

	}
#if defined(_CONSOLE_)
	printf("[%02d %02d:%02d:%02d] > %s\n",
		t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec, str);
#endif
}

void cPackets::OpenCargo()
{
	int32_t addr_call1 = addr_hack_cargo;
	int32_t addr_ecx1 = addr_ptrdat;
#if defined(_MSC_VER)
	__asm
	{
		push 1
		mov ecx, dword ptr ds : [addr_ecx1]
		call addr_call1
	}
#else
	asm("push 1;\
    	 mov ecx, dword ptr ds : [addr_ptrdat];\
         call addr_call1;");
#endif
}

void cPackets::Senha2Window(uint32_t open)
{
	int32_t _ecx = *(int32_t*)(*(int32_t*)addr_ptrdat + 0x26EB0);
	int32_t _arg1 = addr_window;
	__asm
	{
		pushad
		push open
		mov ecx, _ecx
		mov edx, dword ptr ds : [ecx]
		call dword ptr ds : [edx + 0x60]
		mov ecx, _arg1
		mov ecx, dword ptr ds : [ecx]
		mov edx, dword ptr ds : [ecx]
		call dword ptr ds : [edx + 8]
		mov ecx, _ecx
		mov dword ptr ds : [ecx + 0x26EC0], eax
		popad
	}
	*(uint8_t*)addr_lock_ok = 1;
	//select char
#if defined(OVER)
	*(uint32_t*)((*(uint32_t*)addr_acc) + 0xEF4) = slot;
#else
	*(uint32_t*)((*(uint32_t*)addr_acc) + 0xF9C) = slot;
#endif
}

void cPackets::SelectChar()
{
	int32_t _ecx = *(int32_t*)addr_ptrdat;
	int32_t _arg1 = *(int32_t*)addr_window;
	__asm
	{
		pushad
		mov ecx, _arg1
		mov edx, dword ptr ds : [ecx]
		call dword ptr ds : [edx + 8]
		mov ecx, _ecx
		mov dword ptr ds : [ecx + 0x26EE0], eax
		push 0
		mov ecx, _ecx
		mov ecx, dword ptr ds : [ecx + 0x26E98]
		mov edx, dword ptr ds : [ecx]
		call dword ptr ds : [edx + 0x64]
		push 0
		mov ecx, _ecx
		mov ecx, dword ptr ds : [ecx + 0x26E9C]
		mov edx, dword ptr ds : [ecx]
		call dword ptr ds : [edx + 0x64]
		push 0
		mov ecx, _ecx
		mov ecx, dword ptr ds : [ecx + 0x26EA0]
		mov edx, dword ptr ds : [ecx]
		call dword ptr ds : [edx + 0x64]
		popad
	}
}

void cPackets::Execute(uint32_t addr)
{
#if defined(_MSC_VER)
	__asm { call addr }
#else
	asm("call addr");
#endif
}

void cPackets::ReadPacket(uint8_t *packet, int32_t op)
{
	int32_t _ecx_1 = *(int32_t*)((*(int32_t*)addr_ptrdat) + 0x4C);
	int32_t _addr_call_1 = addr_hack_recv2;
#if defined(_MSC_VER)
	__asm
	{
		push packet
		push op
		mov ecx, _ecx_1
		call _addr_call_1
	}
#else
	/*
	asm("pushl %0\n\t"
	"pushl %1\n\t"
	"movl %2, %%ecx\n\t"
	"call *%3"
	:
	: "r" (packet), "r" (op), "r" (*(int32_t*)((*(int32_t*)addr_ptrdat) + 0xF8)), "r" (addr_hack_recv2)
	: "%ecx");*/
#endif
}

void cPackets::ReadPacket2(uint8_t *packet, int32_t op)
{
	int32_t _ecx_1 = *(int32_t*)((*(int32_t*)addr_ptrdat) + 0x4C);
	int32_t _addr_call_1 = addr_hack_recv;
#if defined(_MSC_VER)
	__asm
	{
		push packet
		push op
		mov ecx, _ecx_1
		call _addr_call_1
	}
#else
	asm("push packet;\
         push op;\
         mov ecx, _ecx_1;\
         push _addr_call_1");
#endif
}

static int32_t addr_send_dirr = addr_send_dir;
static int32_t addr_sendd = addr_send;
void cPackets::SendPacket(uint8_t *packet, int32_t _size, int32_t direct)
{
	int32_t _addr_calls = addr_my_calls;
	sFilterPacket((MSG_HEADER*)packet, _size, 1, 0);
	if (direct)
	{
		int32_t _ecx_1 = addr_send_class;
		int32_t _addr_call_1 = addr_send_dirr;
#if defined(_MSC_VER)
		__asm
		{
			push _size
			push packet
			mov ecx, _ecx_1
			call _addr_call_1
		}
#else
		asm("push _size;\
             push packet;\
             mov ecx, _ecx_1;\
             call _addr_call_1");
#endif
	}
	else
	{
		int32_t _addr_call_1 = addr_sendd;
#if defined(_MSC_VER)
		__asm
		{
			push _size
			push packet
			call _addr_call_1
			add esp, 8
		}
#else
		asm("push _size;\
             push packet;\
			 call _addr_call_1;\
             add esp, 8");
#endif
	}
}

void cPackets::Party(int32_t id, char *nick)
{
	MSG_SUMMON packet(myID, 0x3AB);
	nick[15] = 0;
	packet.id = id;
	strcpy(packet.Nick, nick);
	SendPacket((uint8_t*)&packet, sizeof(packet), 0);
}

void cPackets::DeleteItem(int32_t slot, int32_t id)
{
	MSG_DELETEITEM packet(myID);
	packet.slot = slot;
	packet.itemid = id;
	if (slot >= 0 && slot < 60)
	{
		if (inv[slot].ItemID == id)
		{
			inv[slot].ItemID = 0;
		}
	}
#ifdef DON
	*(uint8_t*)eraseaddr = 1;
#endif
	SendPacket((uint8_t*)&packet, sizeof(packet), 0);
}

void cPackets::NpcCombine(int32_t npc, int32_t *pos, sItem *its)
{
	MSG_NPCCOMB packet(myID, 0x2D3);
	for (int32_t i = 0; i < 8; i++)
	{
		packet.item[i] = its[i];
		packet.slot[i] = pos[i];
	}
	SendPacket((uint8_t*)&packet, sizeof(packet), 0);
}

void cPackets::ShiftItem(int32_t slot, int32_t id, int32_t n)
{
	MSG_SPLITITEM packet(myID);
	packet.itemid = id;
	packet.num = n;
	packet.slot = slot;
	SendPacket((uint8_t*)&packet, sizeof(packet), 0);
}

void cPackets::Grifo(int32_t op, int32_t op2)
{
	MSG_SIGNALPARM2 packet(myID, 0xAD9, op, op2);
	SendPacket((uint8_t*)&packet, sizeof(packet), 0);
}

void cPackets::SignalParam(int32_t opcode, int32_t param)
{
	MSG_SIGNALPARM packet(myID, opcode, param);
	SendPacket((uint8_t*)&packet, sizeof(packet), 0);
}

int32_t cPackets::UseItem(int32_t src_type, int32_t src_pos, int32_t dst_type, int32_t dst_pos, int32_t op)
{
	MSG_USEITEM packet(myID);
	packet.dst_type = dst_type;
	packet.dst_val = dst_pos;
	packet.src_type = src_type;
	packet.src_val = src_pos;
	packet.op = op;
	packet.posx = mobs[myID].pos.x;
	packet.posy = mobs[myID].pos.y;
	if (src_type == 1)
	{
		if (src_pos >= 0 && src_pos < 60)
		{
			/*
			MSG_CREATEITEM packet2(myID);
			sItem *it = &inv[src_pos];
			it->AddAmount(-1);
			packet2._Size++;
			packet2.itemData = *it;
			packet2.invType = src_type;
			packet2.invSlot = src_pos;
			ReadPacket2((uint8_t*)&packet2, 0x182);*/
		}
	}
	SignalParam(p_Disconnect, 1);
	if (op != 0)
	{
		InsertPacket(time(0) + 5, &packet);
		return 6;
	}
	else
	{
		SendPacket((uint8_t*)&packet, sizeof(packet), 0);
		ShiftItem(src_pos, 0, 0);
	}
	return 2;
}


void cPackets::MoveItem(int32_t src_type, int32_t src_pos, int32_t dst_type, int32_t dst_pos)
{
	MSG_MOVEITEM packet(myID);
	packet.destSlot = dst_pos;
	packet.destType = dst_type;
	packet.srcSlot = src_pos;
	packet.srcType = src_type;
	SendPacket((uint8_t*)&packet, sizeof(packet), 0);
}

int32_t cPackets::PickItem(int32_t id, int32_t n)
{
	int32_t i, j = 0;
	int32_t c = 0;
	for (i = 0; i < 120; i++)
	{
		if (bau[i].ItemID == id)
		{
			while (j < 30)
			{
				if (inv[j].ItemID == 0)
				{
					MoveItem(2, i, 1, j);
					c++;
					j++;
					break;
				}
				j++;
			}
			if (j == 30 || c == n)
			{
				break;
			}
		}
	}
	if (n == 1)
	{
		return c ? j - 1 : -1;
	}
	return c;
}

int32_t cPackets::FindItem(int32_t id, int32_t i, int32_t type)
{
	if (type == 1)
	{
		if (i < 0 || i > 30)
			i = 0;
		for (; i < 30; i++)
		{
			if (inv[i].ItemID == id)
			{
				return i;
			}
		}
	}
	else if (type == 2)
	{
		if (i < 0 || i > 120)
			i = 0;
		for (; i < 120; i++)
		{
			if (bau[i].ItemID == id)
			{
				return i;
			}
		}
	}
	return -1;
}

void cPackets::SellItem(int32_t npc, int32_t src_type, int32_t src_pos)
{
	MSG_SELLITEMS packet(myID);
	packet.mobID = npc;
	packet.invSlot = src_pos;
	packet.invType = src_type;
	SendPacket((uint8_t*)&packet, sizeof(packet), 0);
}

void cPackets::BuyItem(int32_t npc, int32_t src, int32_t dst)
{
	MSG_BUYITEMS packet(myID);
	packet.sellSlot = src;
	packet.invSlot = dst;
	packet.mobID = npc;
	SendPacket((uint8_t*)&packet, sizeof(packet), 0);
}

void cPackets::Click(int32_t npc)
{
	MSG_SIGNALPARM2 packet(myID, p_RequestNpcClick, npc);
	SendPacket((uint8_t*)&packet, sizeof(packet), 0);

}

void cPackets::Revive()
{
	MSG_HEADER packet(sizeof(MSG_HEADER), 0x289, myID);
	Macro.ptr = Macro.buffer;
	Macro.n = 0;
	InsertPacket(time(0) + 4, &packet);
}

void cPackets::Move(int32_t x, int32_t y, int32_t real)
{
#ifdef OVER
	MSG_ACTION packet(myID, 0x366);
#else // OVER
	MSG_MOVE packet(myID);
#endif
	Show(DEFAULT_COLOR, "Move: %d %d -> %d %d", mobs[myID].pos.x, mobs[myID].pos.y, x, y);
	packet.mType = real;
	packet.mSpeed = 6;
	packet.xSrc = mobs[myID].pos.x;
	packet.ySrc = mobs[myID].pos.y;
	packet.xDst = x;
	packet.yDst = y;
	if (real)
	{
		SendPacket((uint8_t*)&packet, sizeof(packet), 0);
	}
	ReadPacket((uint8_t*)&packet, packet.Code);
	mobs[myID].pos.x = x;
	mobs[myID].pos.y = y;
}

void cPackets::Dump(uint8_t *packet, int32_t _size, int32_t send, int32_t callback, int32_t doit)
{
	int32_t i, j, i_max;
	int32_t don = 0;
	char temp[196] = "";
	int32_t _code = (int32_t)(*(uint16_t*)&packet[4]);
	if (send == 2)
	{
		send = 1;
		don = 1;
	}
	if ((dump[_code][0] & (send + 1)) != 0)
	{
		char file_name[64];
		FILE *f;
		sprintf(file_name, "./logs/%04X_%d", _code, dump[_code][1]);
		f = fopen(file_name, "wb");
		if (f)
		{
			fwrite(packet, _size, 1, f);
			fclose(f);
		}
		dump[_code][1]++;
	}
	if ((opcodes[_code] & 1) != 0 || doit)
	{
		/*Show(DEFAULT_COLOR, "%s%s %d:%04X %u/%u",
			(don ? "DON" : ""), (send ? "Send" : "Recv"), _size, _code,
			 *(uint32_t*)&packet[8], *(uint32_t*)0x96F210);*/
		sprintf(temp, "%s%s %d:%04X %u/%u",
			(don ? "DON" : ""), (send ? "Send" : "Recv"), _size, _code,
			*(uint32_t*)&packet[8], *(uint32_t*)0x96F210);
		WriteLog(temp);
		if (callback > 0)
		{
			/*for (i = -1; i < 12; i++)
			{
			Show(DEFAULT_COLOR, "[%d] %08X", i, *(int32_t*)(callback + (i * 4)));
			}*/
		}
		i_max = _size / 16;
		for (i = 0; i < i_max; i++)
		{
			for (j = 0; j < 4; j++)
				sprintf(&temp[j * 9], "%02X%02X%02X%02X ",
					packet[i * 16 + (j * 4) + 0],
					packet[i * 16 + (j * 4) + 1],
					packet[i * 16 + (j * 4) + 2],
					packet[i * 16 + (j * 4) + 3]);
			for (j = 0; j < 4; j++)
				sprintf(&temp[j * 5 + 36], "%c%c%c%c ",
					is_print(packet[i * 16 + (j * 4) + 0]),
					is_print(packet[i * 16 + (j * 4) + 1]),
					is_print(packet[i * 16 + (j * 4) + 2]),
					is_print(packet[i * 16 + (j * 4) + 3]));
			//Show(DEFAULT_COLOR, "%s", temp);
			WriteLog(temp);
		}
		memset(temp, 0, sizeof(temp));
		for (j = 0; j < _size % 16; j++)
			sprintf(&temp[j * 3], "%02X ", packet[i * 16 + j]);
		if (j)
			//Show(DEFAULT_COLOR, "%s", temp);
			WriteLog(temp);
	}
}

void cPackets::ReadIni(char *line)
{
	static int32_t tipo = _nil;
	static int32_t n = 0;
#ifdef _LOAD_INI_
	ifstream fp("./filter.txt", ifstream::in);
	if (fp.is_open())
	{
		char line[256];
		while (fp.good())
		{
			fp.getline(line, sizeof(line));
			cout << line << endl;
#else
	if (line == 0)
	{
		tipo = _nil;
		n = 0;
		return;
	}
#endif
	if (line[0] == '%')
	{
		for (int32_t ii = 0, jj = 0; ii < 16; ii++)
		{
			nickChecker[cnickChecker][ii] = line[1 + jj];
			if (line[1 + jj] != 0)
			{
				jj++;
			}
		}
		if ((++cnickChecker) == 8)
		{
			cnickChecker = 0;
		}
	}
	if (line[0] == '#' && tipo != _commands)
	{
		tipo = _nil;
		if (!strcmp(line, "#plano"))
		{
			tipo = _plano;
		}
		else if (!strcmp(line, "#proxy"))
		{
			tipo = _proxy;
		}
		else if (!strcmp(line, "#pcdata"))
		{
			tipo = _pcdata;
		}
		else if (!strcmp(line, "#mac"))
		{
			tipo = _mac;
		}
		else if (!strcmp(line, "#rede"))
		{
			tipo = _rede;
		}
		else if (!strcmp(line, "#commands"))
		{
			tipo = _commands;
		}
		else if (!strcmp(line, "#servidor"))
		{
			tipo = _servidor;
		}
		else if (!strcmp(line, "#chave"))
		{
			tipo = _chave;
		}
#ifdef ENABLE_AMOUNT
		else if (!strcmp(line, "#amount"))
		{
			tipo = _amount;
		}
#endif // ENABLE_AMOUNT
#ifdef ENABLE_BAUF
		else if (!strcmp(line, "#bau"))
		{
			tipo = _bau;
		}
#endif // ENABLE_BAUF
#ifdef ENABLE_DROPF
		else if (!strcmp(line, "#drop"))
		{
			tipo = _drop;
		}
#endif // ENABLE_DROPF
#ifdef ENABLE_USEF
		else if (!strcmp(line, "#use"))
		{
			tipo = _use;
		}
#endif // ENABLE_USEF
#ifdef ENABLE_PAKF
		else if (!strcmp(line, "#packets"))
		{
			tipo = _packet;
		}
#endif // ENABLE_PAKF
#ifdef ENABLE_DUMPF
		else if (!strcmp(line, "#dump"))
		{
			tipo = _dump;
		}
#endif // ENABLE_DUMPF
	}
	else if (strlen(line) > 0 && (line[0] != '/' || tipo == _commands) && isspace(line[0]) == 0)
	{
		if (tipo == _amount)
		{
			int32_t x = atoi(line);
			if (x >= 0 && x < 9000)
			{
				drop_list[x] |= (1 << tipo);
				amount_list[x] = -1;
				n++;
			}
		}
		else if (tipo == _drop || tipo == _bau || tipo == _use)
		{
			int32_t id;
			int32_t c = 0;
			int32_t k = 0;
			char *tok = strtok(line, "|");
			while (tok)
			{
				if (c == 0)
				{
					id = atoi(tok);
					if (id >= 0 && id < 9000)
					{
						drop_list[id] |= (1 << tipo);
						n++;
					}
					else
					{
						break;
					}
					c++;
				}
				else
				{
					int32_t v[3][2] = { {0, 0}, {0, 0}, {0, 0} };
					sscanf(tok, "%d %d %d %d %d %d",
						&v[0][0], &v[0][1], &v[1][0], &v[1][1], &v[2][0], &v[2][1]);
					drop_efs[id][k][0][0] = v[0][0];
					drop_efs[id][k][0][1] = v[0][1];
					drop_efs[id][k][1][0] = v[1][0];
					drop_efs[id][k][1][1] = v[1][1];
					drop_efs[id][k][2][0] = v[2][0];
					drop_efs[id][k][2][1] = v[2][1];
					k++;
				}
				tok = strtok(0, "|");
			}
		}
		else if (tipo == _packet)
		{
			int32_t x = 0;
			sscanf(line, "%x", &x);
			if (x >= 0 && x < 0xFFFF)
				opcodes[x] |= 1;
			else if (x == 0x10000)
			{
				for (int32_t k = 0; k < 0xFFFF; k++)
				{
					opcodes[k] |= 1;
				}
			}
		}
		else if (tipo == _dump)
		{
			int32_t x = 0, type = 0;
			sscanf(line, "%x %d", &x, &type);
			if (x >= 0 && x < 0xFFFF)
			{
				opcodes[x] |= 2;
				dump[x][0] = type;
				dump[x][1] = 0;
			}
			else if (x == 0x10000)
			{
				for (int32_t k = 0; k < 0xFFFF; k++)
				{
					opcodes[k] |= 2;
					dump[k][0] = type;
					dump[k][1] = 0;
				}
			}
		}
		else if (tipo == _mac)
		{
			uint32_t macv[6] = { 0, 0, 0, 0, 0, 0 };
			if (sscanf(line, "%02X:%02X:%02X:%02X:%02X:%02X", &macv[0], &macv[1], &macv[2], &macv[3], &macv[4], &macv[5]) == 6)
			{
				for (int32_t ti = 0; ti < 6; ti++)
				{
					pBullshitDon.mac[ti] = macv[ti];
				}
			}
		}
		else if (tipo == _rede)
		{
			uint32_t macv[16];
			if (sscanf(line, "%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
				&macv[0], &macv[1], &macv[2], &macv[3], &macv[4], &macv[5], &macv[6],
				&macv[7], &macv[8], &macv[9], &macv[10], &macv[11], &macv[12], &macv[13],
				&macv[14], &macv[15]) == 16)
			{
				for (int32_t ti = 0; ti < 16; ti++)
				{
					client_mac[ti] = macv[ti];
				}
			}
		}
		else if (tipo == _pcdata)
		{
			char *tokpc = strtok(line, ";");
			int32_t ti = 0;
			while (tokpc && ti < 5)
			{
				pBullshitDon.data[ti][0] = 0;
				strcpy(pBullshitDon.data[ti], tokpc);
				ti++;
				tokpc = strtok(0, ";");
			}
		}
		else if (tipo == _proxy)
		{
			char *tmp;
			tmp = strstr(line, ":");
			*tmp = '\0';
			tmp++;
			strcpy(proxy_ip, line);
			proxy_port = atoi(tmp);
		}
		else if (tipo == _chave)
		{
			strcpy(chave, line);
		}
		else if (tipo == _plano)
		{
			if (!strcmp(line, "free"))
			{
				planos = free_;
				__asm
				{
					ret 0x40
				}
			}
#if defined(_DEBUG_)
			else if (!strcmp(line, "test"))
			{
				planos = test_;
			}
#endif
			else
			{
				planos = money_;
			}
		}
		else if (tipo == _servidor)
		{
			strcpy(servidor, line);
			if (!strcmp(servidor, "don") || !strcmp(servidor, "don2"))
			{
				server = _don;
			}
			else if (!strcmp(servidor, "wyd2"))
			{
				server = _wyd2;
			}
			else if (!strcmp(servidor, "kofd"))
			{
				server = _kofd;
			}
			else if (!strcmp(servidor, "over"))
			{
				server = _over;
			}
			else if (!strcmp(servidor, "brazuc"))
			{
				server = _brazuc;
			}
			else if (!strcmp(servidor, "legacy"))
			{
				server = _legacy;
			}
			else if (!strcmp(servidor, "uow"))
			{
				server = _uow;
			}
			else if (!strcmp(servidor, "aon"))
			{
				server = _aon;
			}
			else if (!strcmp(servidor, "gd"))
			{
				server = _gd;
			}
			else if (!strcmp(servidor, "final"))
			{
				server = _final;
			}
			else if (!strcmp(servidor, "hero"))
			{
				server = _hero;
			}
			else
			{
				server = _no_server;
				__asm
				{
				_give_to_me_now:
					jmp _give_to_me_now
				}
			}
		}
		else if (tipo == _commands)
		{
			if (!strcmp(line, "#loop"))
			{
				hacks.commands.loop = 1;
			}
			else if (!strcmp(line, "#repeat"))
			{
				/*if(!strncmp(hacks.commands.cmd[0], "#macro", 6))
				{
				memcpy(hacks.commands.cmd[hacks.commands.n], hacks.commands.cmd[0], 128);
				memset(hacks.commands.cmd[0], 0, 128);
				hacks.commands.n++;
				}*/
				hacks.commands.repeat = hacks.commands.n;
			}
			else if (!strcmp(line, "#novato"))
			{
				novato = 1;
			}
			else
			{
				Chat(line);
				strcpy(hacks.commands.cmd[hacks.commands.n], line);
				hacks.commands.n++;
			}
		}
	}
#ifdef _LOAD_INI_
		}
//Show(DEFAULT_COLOR, "%d drops lidos.", n);
fp.close();
	}
#endif
}

int32_t sChat(char *str)
{
	if (str != 0)
		return Packets.Chat(str);
	return 0;
}

void RWindowEvent(uint32_t msg, uint32_t v, uint32_t _ecx)
{
	int32_t __ecx = *(int32_t*)((*(int32_t*)addr_ptrdat) + 0x28) + 0x24;
	__asm
	{
		push v
		push msg
		mov ecx, __ecx
		mov edx, dword ptr ds : [ecx]
		call dword ptr ds : [edx]
	}
}

_naked void HWindowEvent()
{
	__asm
	{
		mov edx, dword ptr ss : [ebp - 4]
		mov ecx, dword ptr ss : [ebp + 0xC]
		mov eax, dword ptr ss : [ebp + 0x8]
		push edx
		push ecx
		push eax
		lea ecx, Packets
		call cPackets::FilterMessage
		mov ecx, dword ptr ss : [ebp + 0xC]
		mov eax, dword ptr ss : [ebp + 0x8]
		push ecx
		push eax
	}
	addr_hook_window_ret;
	__asm
	{
		jmp eax
	}
}

void sFilterPacket(MSG_HEADER *packet, int32_t _size, int32_t send, int32_t callback)
{
	if (packet != 0 && _size > 0)
		Packets.FilterPacket(packet, _size, send, callback);
}

static uint32_t lastCheckNick = -1;

void cPackets::FilterMessage(uint32_t uMsg, uint32_t v, uint32_t _ebp_4)
{
#if defined(RECORD_MESSAGE)
	if (Record.on == 1 && Record.n < NUM_PACKETS)
	{
		Record.t[Record.n] = TimeGetTime() - Record.t[NUM_PACKETS];// - (Record.n > 0 ? Record.t[Record.n - 1] : Record.t[NUM_PACKETS]);
		Record.buffer[Record.n][0] = uMsg;
		Record.buffer[Record.n][1] = v;
		Record.n++;
	}
	Show(DEFAULT_COLOR, "#-> %p %p %p %d %d",
		*(int32_t*)((*(int32_t*)addr_ptrdat) + 0x28), _ebp_4, _ebp_4 - *(int32_t*)((*(int32_t*)addr_ptrdat) + 0x28),
		uMsg, v);
#endif
}

void cPackets::FilterPacket(MSG_HEADER *packet, int32_t _size, int32_t send, int32_t callback)
{
	uint32_t pTime = packet->Time;
	if (loginCode == 0)
	{
		loginCode = packet->Code;
#ifdef _SEC_
		iPushSS();
#endif
	}
	if (packet->Code == loginCode)
	{
		MSG_REQLOGIN *pak = (MSG_REQLOGIN*)packet;
		Reset();
		if (pak->Pwd[0] == pak->Pwd[1] && pak->Pwd[1] == pak->Pwd[2] && pak->Pwd[2] == pak->Pwd[3] && pak->Pwd[3] == '#')
		{
			sprintf(filter_name, "./%s.ini", pak->Name);
			char _apikey[256] = "";
			Record.t[NUM_PACKETS] = TimeGetTime();
			GetPrivateProfileStringA("WYD2BOT", "Apikey", "", _apikey, sizeof(_apikey) - 2, filter_name);
			if (_apikey[0] != 0)
			{
				char *ptr;
				for (ptr = _apikey; *ptr; ptr++)
				{
					apikey[ptr - _apikey] = *ptr;
					*ptr = 0;
				}
				memset(nickChecker, 0, sizeof(nickChecker));
				apikey[ptr - _apikey] = *ptr;
				GetPrivateProfileStringA("WYD2BOT", "Login", "", login, sizeof(login) - 1, filter_name);
				GetPrivateProfileStringA("WYD2BOT", "Senha", "", senha, sizeof(senha) - 1, filter_name);
				GetPrivateProfileStringA("WYD2BOT", "Senha2", "", senha2, sizeof(senha2) - 1, filter_name);
				slot = GetPrivateProfileIntA("WYD2BOT", "Slot", 0, filter_name);
				sv = GetPrivateProfileIntA("WYD2BOT", "Servidor", -1, filter_name);
				ch = GetPrivateProfileIntA("WYD2BOT", "Canal", -1, filter_name);
				//proxy_mode = GetPrivateProfileIntA("WYD2BOT", "Proxy", 0, filter_name);
				proxy_mode = 0;
				opcodes[0x10A] = 1;
				online = 1;
			}
			if (planos <= free_ ||
#if defined(_DEBUG_)
				planos > 4)
#else
				planos > 3)
#endif
			{
				int32_t r = rand();
				__asm
				{
					jmp r
				}
			}
			memset(pak->Name, 0, 12);
			memset(pak->Pwd, 0, 12);
			for (int32_t i = 0, r = rand() % 4; i < 6 + r; i++)
			{
				pak->Name[i] = rand() % 25 + 'A';
				pak->Pwd[i] = rand() % 25 + 'A';
			}
			if (planos <= free_ ||
#if defined(_DEBUG_)
				planos > 4)
#else
				planos > 3)
#endif
			{
				__asm
				{
					retn
				}
			}
		}
		cnickChecker = 0;
		lastCheckNick = -1;
		if (login[0] != 0 && senha[0] != 0)
		{
			memset(pak->Name, 0, 12);
			memset(pak->Pwd, 0, 12);
			strcpy(pak->Name, login);
			strcpy(pak->Pwd, senha);
		}
		if (proxy_mode == 1)
		{
			if (sv != -1)
			{
				char *svlist = (char*)(addr_svlist);
#if defined(MYD)
				*(int32_t*)(*(int32_t*)(addr_sv_ch)+0x2179C) = sv;
				*(int32_t*)(*(int32_t*)(addr_sv_ch)+0x217A0) = ch;
#else
				*(int32_t*)(*(int32_t*)(addr_sv_ch)+0x217F4) = sv;
				*(int32_t*)(*(int32_t*)(addr_sv_ch)+0x217F8) = ch;
#endif
				strcpy((char*)((*(int32_t*)addr_ip) + 0x60),
					&svlist[(sv * 64 * 11) + (ch * 64)]);
			}
			TunnelProxy(
				*(int32_t*)addr_send_class,
				(const char*)((*(int32_t*)addr_ip) + 0x60),
				*(int32_t*)addr_port,
				0,
				0x464);
		}
		switch (server)
		{
		case _wyd2:
		{
#if !defined(OVER)
			//pak->Code = loginID;
			pak->Name[11] = 0;
			pak->Name[12] = rand();
			pak->Name[13] = rand();
			pak->Name[14] = rand();
			pak->Name[15] = rand();
			for (int32_t i = 0, r = rand() % 4; i < 8 + r; i++)
			{
				pak->Change[i] = rand() % 25 + 'A';
			}
			//*(uint16_t*)&pak->Change[14] = 0x1020;
			break;
#endif
		}
		case _aon:
		{
			pak->Name[11] = 0;
			for (int32_t i = 0; i < 6; i++)
			{
				uint8_t byte = pBullshitDon.mac[i];
				uint8_t bytehi = (byte >> 4) & 0x0F;
				uint8_t bytelo = byte & 0x0F;
				if (bytehi > 9)
				{
					pak->Name[12 + (i * 3)] = bytehi + 'A' - 0x0A;
				}
				else
				{
					pak->Name[12 + (i * 3)] = bytehi + '0';
				}
				if (bytelo > 9)
				{
					pak->Name[12 + (i * 3) + 1] = bytelo + 'A' - 0x0A;
				}
				else
				{
					pak->Name[12 + (i * 3) + 1] = bytelo + '0';
				}
				pak->Name[12 + (i * 3) + 2] = '.';
			}
			//*(uint32_t*)&packet.Change[44] = loginID;
			break;
		}
		case _hero:
		{
			for (int32_t i = 0; i < 14; i++)
			{
				pak->hash[21] = rand() % 25 + 'A';
			}
			break;
		}
		case _over:
		{
			/*for(int32_t i = 0; i < 6; i++)
			{
				((uint8_t*)&packet.flag)[i] = rand() % 255;
			}*/
			break;
		}
		}
		if (*(int32_t*)&client_mac[0] != 0 ||
			*(int32_t*)&client_mac[4] != 0 ||
			*(int32_t*)&client_mac[8] != 0 ||
			*(int32_t*)&client_mac[12] != 0)
		{
			memcpy(pak->Keys, client_mac, 16);
		}
	}
	if (opcodes[packet->Code])
	{
		Dump((uint8_t*)packet, _size, send, callback);
	}
	if (server <= _no_server ||
		server > _aon)
	{
		__asm
		{
			xor eax, edx
			jmp eax
		}
	}
	if (hacks.refn.on && hacks.refn.buy != -1 && hacks.refn.buy < packet->Time)
	{
		hacks.refn.buy = -1;
		DeleteItem(0, hacks.refn.id);
		BuyItem(hacks.refn.npc, hacks.refn.slot, 0);
	}
	if (hacks.mont.on && hacks.mont.buy != -1 && hacks.mont.buy < packet->Time)
	{
		hacks.mont.buy = -1;
		DeleteItem(0, hacks.mont.id);
		BuyItem(hacks.mont.npc, hacks.mont.slot, 0);
	}
	if (hacks.showid.on)
	{
		if (send != 0)
		{
			if (hacks.fake.id != 0)
			{
				packet->Index = hacks.fake.id;
			}
		}
		else
		{/*
		 if(hacks.pick.c > 0)
		 {
		 hacks.pick.c--;
		 }else if(hacks.pick.c == 0)
		 {
		 if(hacks.pick.i < hacks.pick.n)
		 {
		 if(hacks.pick.stage == 0)
		 {
		 Move(hacks.pick.data[hacks.pick.i][1],
		 hacks.pick.data[hacks.pick.i][2],
		 1);
		 }else if(hacks.pick.stage == 1)
		 {
		 MSG_PICKITEM pak_get(myID);
		 for(int32_t i = 0; i < 60; i++)
		 {
		 if(inv[i].ItemID == 0)
		 {
		 pak_get.initID = hacks.pick.data[hacks.pick.i][0];
		 pak_get.invType = 1;
		 pak_get.InvSlot = i;
		 pak_get.posX = hacks.pick.data[hacks.pick.i][1];
		 pak_get.posY = hacks.pick.data[hacks.pick.i][2];
		 pak_get._Size = 0;
		 SendPacket((uint8_t*)&pak_get,
		 sizeof(pak_get),
		 0);
		 Show(DEFAULT_COLOR, "Pick: %d-%d/%d -> %d",
		 hacks.pick.data[hacks.pick.i][0],
		 hacks.pick.data[hacks.pick.i][1],
		 hacks.pick.data[hacks.pick.i][2], i);
		 break;
		 }
		 }
		 }else if(hacks.pick.stage == 2)
		 {
		 Move(hacks.pick.data[hacks.pick.i][3],
		 hacks.pick.data[hacks.pick.i][4],
		 1);
		 hacks.pick.i = (hacks.pick.i + 1) % 64;
		 }
		 hacks.pick.c = 5;
		 hacks.pick.stage = (hacks.pick.stage + 1) % 3;
		 }else
		 {
		 hacks.pick.i = 0;
		 hacks.pick.n = 0;
		 }
		 }
				*/
		}
	}
#if defined(_SEC_) && defined(_CHECKER_)
	if (send == 1 &&
		packet->Index > 0 && packet->Index < 1000 &&
		online &&
		packet->Time >(lastCheckNick + 100 * 1000)
		)
	{
		MSG_COMMAND pak_cmd(myID, 4);
		lastCheckNick = packet->Time;
		for (int32_t ii = 0; ii < 16; ii++)
		{
			pak_cmd.cmd[ii] = (char)nickChecker[cnickChecker][ii];
		}
		for (int32_t ii = 0; ii < 96; ii++)
		{
			pak_cmd.val[ii] = (char)apikey[ii];
		}
		pak_cmd.Code += 0x330;
		SendPacket((uint8_t*)&pak_cmd, sizeof(pak_cmd), 0);
		cnickChecker = (cnickChecker + 1) % 8;
		//memset(pak, 0, _size);
	}
#endif // defined
	switch (packet->Code)
	{
	case 0xF200:
	{
		if (online)
		{
			MSG_BULLSHITDON *pak = (MSG_BULLSHITDON *)packet;
			if (*(int32_t*)pBullshitDon.mac != 0)
			{
				memcpy(pak->mac, pBullshitDon.mac, 6);
			}
			for (int32_t pi = 0; pi < 5; pi++)
			{
				if (pBullshitDon.data[pi][0] != 0)
				{
					memcpy(pak->data[pi], pBullshitDon.data[pi], sizeof(pak->data[pi]));
				}
			}
		}
		break;
	}
	case 0x26E:
	{
		MSG_CREATEOBJECT *pak = (MSG_CREATEOBJECT *)packet;/*
		if (hacks.showid.on)
		{
			hacks.pick.data[hacks.pick.n][0] = pak->InitID;
			hacks.pick.data[hacks.pick.n][1] = pak->Pos.x;
			hacks.pick.data[hacks.pick.n][2] = pak->Pos.y;
			hacks.pick.data[hacks.pick.n][3] = mobs[myID].pos.x;
			hacks.pick.data[hacks.pick.n][4] = mobs[myID].pos.y;
			hacks.pick.n = (hacks.pick.n + 1) % 64;
		}*/
		break;
	}
	case 0x10A: case 0x10E:
	{
		sItem *store = 0;
#ifdef _SEC_
		iCIsDebuggerPresent();
#endif
#ifdef _SEC_
		iInterrupt41();
#endif
		if (packet->Code == 0x10A)
		{
			MSG_LOGIN *pak = (MSG_LOGIN*)packet;
			store = pak->Storage;
		}
		else
		{
			_MSG_LOGIN_754 *pak = (_MSG_LOGIN_754*)packet;
			store = pak->Storage;
		}
		for (int32_t i = 0; i < 120; i++)
		{
			bau[i] = store[i];
		}
		//#if defined(RECORD_MESSAGE)
		if (Macro.on == 1)
			//if(planos != free_)
		{
			MSG_LOCKPASS packet_lock(0);
			map<int32_t, list<int32_t>> mapa_ecx;
			packet_lock.Size = sizeof(packet_lock);
			if (senha2[0] == 'x')
			{
				uint8_t tmp_senha2[32] = "";
				for (int32_t c = 0; c < 20; c++)
				{
					char tmp_v[3] = { senha2[c * 2 + 1], senha2[c * 2 + 2], 0 };
					uint32_t tmp_h = 0;
					sscanf(tmp_v, "%X", &tmp_h);
					packet_lock.password[c] = tmp_h;
				}
				sscanf(senha2, "x%hhX%hhX%hhX%hhX%hhX%hhX%hhX%hhX%hhX%hhX%hhX%hhX%hhX%hhX%hhX%hhX%hhX%hhX%hhX%hhX",
					&tmp_senha2[0], &tmp_senha2[1], &tmp_senha2[2], &tmp_senha2[3],
					&tmp_senha2[4], &tmp_senha2[5], &tmp_senha2[6], &tmp_senha2[7],
					&tmp_senha2[8], &tmp_senha2[9], &tmp_senha2[10], &tmp_senha2[11],
					&tmp_senha2[12], &tmp_senha2[13], &tmp_senha2[14], &tmp_senha2[15],
					&tmp_senha2[16], &tmp_senha2[17], &tmp_senha2[18], &tmp_senha2[19]);
				memcpy(packet_lock.password, tmp_senha2, 20);
			}
			else
			{
				strcpy(packet_lock.password, senha2);
			}
			/*
			for (uint32_t i = 0; i < NUM_PACKETS; i++)
			{
				if (Macro.t[i] != 0)
				{
					mapa_ecx[Macro.buffer[i][0]].push_back(i);
				}
			}
			for (uint32_t addr = *(uint32_t*)addr_ptrdat, c = 0; c < mapa_ecx.size(); addr++)
			{
				uint32_t v = *(uint32_t*)addr;
				if (mapa_ecx.find(v) != mapa_ecx.end())
				{
					for (list<int32_t>::iterator it = mapa_ecx[v].begin(); it != mapa_ecx[v].end(); ++it)
					{
						Macro.buffer[*it][2] = addr - 0x44;
					}
					c++;
				}
			}*/
			InsertPacket(time(0) + 1, &packet_lock);
		}
		/*#else
		Macro.n = 0;
		#endif*/
		break;
	}
	case 0x338:
	{
		MSG_CNFMOBKILL *pak = (MSG_CNFMOBKILL*)packet;
		if (Macro.on == 1 && pak->KilledMob == myID)
		{
			Macro.n = -1;
			Show(DEFAULT_COLOR, "Revivendo");
			/*
			WriteLog("Morto por: %s(Pos: %04d/%04d) em %04d/%04d",
								mobs[pak->Killer].PlayerName, mobs[pak->Killer].Current.x, mobs[pak->Killer].Current.y,
								mobs[cID].Current.x, mobs[cID].Current.y);*/
			Revive();
		}
		break;
	}
	case 0x20D:
	{
		MSG_REQLOGIN *pak = (MSG_REQLOGIN*)packet;
		strcpy(login_s, pak->Name);
		strcpy(senha_s, pak->Pwd);
		break;
	}
	case 0x213:
	{
		MSG_SIGNALPARM *pak = (MSG_SIGNALPARM*)packet;
		slot_s = pak->signal;
		break;
	}
	case 0xFDE:
	{
		if (send == 0)
		{
			if (Record.on == 1)
			{
				if (Record.t[NUM_PACKETS] == 0)
				{
					Record.t[NUM_PACKETS] = TimeGetTime();
				}
				Record.n = 0;
				Record.ptr = Record.buffer;
			}
#if !defined(RECORD_MESSAGE)
			if (Macro.on == 1)
			{
				Macro.n = -2;
			}
#endif
		}
		else
		{
			sprintf(senha2_s, "x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
				((uint8_t*)packet)[12], ((uint8_t*)packet)[13], ((uint8_t*)packet)[14], ((uint8_t*)packet)[15],
				((uint8_t*)packet)[16], ((uint8_t*)packet)[17], ((uint8_t*)packet)[18], ((uint8_t*)packet)[19],
				((uint8_t*)packet)[20], ((uint8_t*)packet)[21], ((uint8_t*)packet)[22], ((uint8_t*)packet)[23],
				((uint8_t*)packet)[24], ((uint8_t*)packet)[25], ((uint8_t*)packet)[26], ((uint8_t*)packet)[27],
				((uint8_t*)packet)[28], ((uint8_t*)packet)[29], ((uint8_t*)packet)[30], ((uint8_t*)packet)[31]);
		}
		break;
	}
	case 0x3B2:
	{
		MSG_SUMMON *pak = (MSG_SUMMON *)packet;
		if (hacks.showid.on)
		{
			/*MSG_COMMAND pak2(myID);
			strcpy(pak2.cmd, "relo");
			strcpy(pak2.val, pak->Nick);
			SendPacket((uint8_t*)&pak2, sizeof(pak2), 0);*/
		}
		break;
	}
	case 0x334:
	{
		MSG_COMMAND *pak = (MSG_COMMAND *)packet;
		if (hacks.showid.on)
		{
			if (pak->val[0] == '=')
			{
				if (pak->val[1] == '#')
				{
					//Chat(&pak->val[1]);
				}
			}
		}
#if defined(_SEC_) && defined(_CHECKER_)
		for (int32_t ii = 0; ii < 8; ii++)
		{
			int32_t jj;
			for (jj = 15; jj >= 0; jj--)
			{
				if (nickChecker[ii][jj] != pak->cmd[jj])
				{
					break;
				}
			}
			if (jj == -1)
			{
				memset(pak->cmd, 0, 16);
				if (pak->val[0] == '#')
				{
#if defined(_MSC_VER)
					__asm { nop }
#else
					asm("nop")
#endif
				}
				else if (pak->val[0] == '!')
				{
					do
					{
					__unlucky__:
						char explode[2];
#if defined(_MSC_VER)
						__asm
						{
						__FUCK_YOU_BRO__:
							shl eax, 16
								shl edx, 4
								imul edx, eax
								mov eax, edx
								xor edx, eax
								mov ecx, edx
								div ecx
								ret 0x11
								jmp __FUCK_YOU_BRO__
						}
#endif
						ii++;
						explode[ii] = 0;
					} while (ii != 0);
				}
				else
				{
					goto __unlucky__;
				}
				memset(pak->cmd, 0, _size);
			}
		}
#endif // defined
		break;
	}
	case 0x3AF:
	{
		MSG_SIGNALPARM *pak = (MSG_SIGNALPARM*)packet;
		if (hacks.npcbuy.on)
		{
			hacks.npcbuy.pTime = pak->Time + 5000;
			if (Gold > pak->signal)
			{
				ShiftItem(hacks.npcbuy.invslot, 0, 0);
			}
		}
		Gold = pak->signal;
		break;
	}
	case 0x114:
	{
		MSG_CHARINFO *pak = (MSG_CHARINFO*)packet;
		myID = -1;
		Gold = pak->Character.Gold;
		memcpy(equip, pak->Character.Equip, sizeof(equip));
		for (int32_t i = 0; i < 60; i++)
		{
			int32_t id = pak->Character.Inventory[i].ItemID;
			inv[i] = pak->Character.Inventory[i];
			if ((drop_list[id] & (1 << _amount)) != 0)
			{
				sItem *it = &pak->Character.Inventory[i];
				int32_t j;
				for (j = 0; j < 3; j++)
				{
					if (it->Effect[j].Index == EF_AMOUNT)
					{
						if (it->Effect[j].Value < 120)
						{
							amount_list[it->ItemID] = i;
						}
						break;
					}
				}
				if (j == 3)
				{
					amount_list[it->ItemID] = i;
				}
			}
		}
#ifdef _SEC_
		CheckALL();
#endif
		FILE *f_ini = fopen("./atual.ini", "w");
		if (f_ini)
		{
			fprintf(f_ini, "[WYD2BOT]\nApikey=\nLogin=%s\nSenha=%s\nSenha2=%s\nSlot=%d\n",
				login_s, senha_s, senha2_s, slot_s);
			fprintf(f_ini, "Servidor=%d\nCanal=%d",
#if defined(MYD)
				*(int32_t*)(*(int32_t*)(addr_sv_ch)+0x2179C),
				*(int32_t*)(*(int32_t*)(addr_sv_ch)+0x217A0)
#else
				*(int32_t*)(*(int32_t*)(addr_sv_ch)+0x217F4),
				*(int32_t*)(*(int32_t*)(addr_sv_ch)+0x217F8)
#endif
			);
			fclose(f_ini);
		}
		break;
	}
	case 0x364:
	{
		MSG_SPAWN *pak = (MSG_SPAWN*)packet;
		if (myID == -1)
		{
			myID = pak->Idx;
			//Dump((uint8_t*)pak, pak->Header._Size, 0);
			if (planos != free_)
			{
				Show(DEFAULT_COLOR, "Oi, %s!", pak->PlayerName);
				Show(DEFAULT_COLOR, "Eu sou Hookinho, seu amiguinho!");
				Show(DEFAULT_COLOR, "%s", VERSION);
#ifdef DON
				eraseaddr = *(int32_t*)(*(int32_t*)CalcAddress(0x417C71 - 0x400000 + 1) + CalcAddress(0x417C71 - 0x400000) + 7);
#endif
			}
		}
		if (hacks.showid.on)
		{
			sprintf(pak->pTab, "Index: %d", pak->Idx);
			if ((pak->Score._dir & 1) != 0 && pak->Idx < 1000)
			{
				pak->Score._dir &= 254;
				sprintf(pak->pTab, "Index(INV): %d", pak->Idx);
			}
		}
		strcpy(mobs[pak->Idx].nick, pak->PlayerName);
		if (pak->Idx == myID && hacks.deny.on)
		{
			pak->Current.x = mobs[pak->Idx].pos.x;
			pak->Current.y = mobs[pak->Idx].pos.y;
		}
		mobs[pak->Idx].pos.x = pak->Current.x;
		mobs[pak->Idx].pos.y = pak->Current.y;
		mobs[pak->Idx].send_atk = 0;
		mobs[pak->Idx].recv_atk = 0;
		mobs[pak->Idx].removeTime = -1;
		break;
	}
	case 0x376:
	{
		MSG_MOVEITEM *pak = (MSG_MOVEITEM *)packet;
		if (send == 0)
		{
			sItem *src = 0, *dst = 0;
			int32_t z = 0;
			if (hacks.npctrade.on)
			{
				if ((pak->destSlot == 14 && pak->destType == 1) || (pak->srcSlot == 14 && pak->srcType == 1))
				{
					z = 1;
				}
			}
			if (z == 0)
			{
				if (pak->destType == 1)
					dst = inv;
				else if (pak->destType == 2)
					dst = bau;
				else
					dst = equip;
				if (pak->srcType == 1)
					src = inv;
				else if (pak->srcType == 2)
					src = bau;
				else
					src = equip;
				dst[pak->destSlot].swap(&src[pak->srcSlot]);
			}
			if (hacks.drop.on)
			{
				if (hacks.ehre.on)
				{
					int32_t i = FindItem(0);
					if (i != -1)
					{
						BuyItem(hacks.ehre.npcBardes, 7, i);
					}
					else
					{
						hacks.ehre.on = 0;
						Show(DEFAULT_COLOR, "Ehre: inv cheio");
					}
				}
				else if (z == 1)
				{
					if (inv[14].GetAmount() > 1)
					{
						Show(DEFAULT_COLOR, "Troca: shift");
						ShiftItem(14, hacks.npctrade.itemId, 1);
					}
					else
					{
						Show(DEFAULT_COLOR, "Troca: click");
						Click(hacks.npctrade.npcId);
					}
				}
				else if (hacks.npcbuy.on)
				{
					if (dst[pak->destSlot].ItemID == hacks.npcbuy.itemId)
					{
						hacks.npcbuy.invslot = pak->destSlot;
					}
					else if (src[pak->srcSlot].ItemID == hacks.npcbuy.itemId)
					{
						hacks.npcbuy.invslot = pak->srcSlot;
					}
					else
					{
						break;
					}
					hacks.npcbuy.pTime = pak->Time + 5000;
					//Show(DEFAULT_COLOR, "Compra: use %d", hacks.npcbuy.invslot);
					//UseItem(1, hacks.npcbuy.invslot, 0, 0, 0);
				}
			}
		}
		break;
	}
	case 0x379:
	{
		MSG_BUYITEMS *pak = (MSG_BUYITEMS*)packet;
		if (send == 1 && hacks.showid.on == 1)
		{
			Show(DEFAULT_COLOR, "CompraNpc: Id(%d) Slot(%d)", pak->mobID, pak->sellSlot);
		}
		break;
	}
	case 0x101:
	{
		MSG_CLIENTMSG *pak = (MSG_CLIENTMSG*)packet;
		if (hacks.drop.on)
		{
			if (!strcmp(pak->strMessage, "Desculpe."))
			{
				memset(pak, 0, _size);
			}
		}
#if defined(_CHECKER_)
		if (lastCheckNick != -1 && (lastCheckNick + 5000) < pak->Time)
		{
			memset(pak, 0, _size);
		}
#endif
		break;
	}
#ifdef OVER
	case 0x367: case 0x366:
#else
	case 0x366: case 0x36C:
#endif
	{
		MSG_MOVE *pak = (MSG_MOVE*)packet;
		if (pak->Index == myID)
		{
			if (hacks.rec.on)
			{
				if (pak->xDst != hacks.rec.pos.x || pak->yDst != hacks.rec.pos.y)
				{
					Move(hacks.rec.pos.x, hacks.rec.pos.y, 1);
				}
			}
			if (hacks.deny.on)
			{
				pak->xDst = pak->xSrc;
				pak->yDst = pak->ySrc;
			}
		}
		mobs[pak->Index].pos.x = pak->xDst;
		mobs[pak->Index].pos.y = pak->yDst;
		break;
	}
#ifdef OVER
	case 0x39D: case 0x39E: case 0x36C:
#else
	case 0x39D: case 0x39E: case 0x367:
#endif
	{
		MSG_ATTACKONE *pak = (MSG_ATTACKONE*)packet;
		if (send == 1 && pak->AttackerID == myID)
		{
			if (hacks.skill.id != -1)
			{
				Show(DEFAULT_COLOR, "SkillIndex: %d->%d", pak->SkillIndex, hacks.skill.id);
				pak->SkillIndex = hacks.skill.id;
			}
			if (hacks.same.on)
			{
				if (pak->Code == 0x39E)
				{
					pak->TargetID2 = pak->TargetID;
					pak->Damage2 = pak->Damage;
				}
				else if (pak->Code == 0x367)
				{
					MSG_ATTACKMULT *pak2 = (MSG_ATTACKMULT*)packet;
					for (int32_t i = 1; i < hacks.same.on; i++)//SkillData[pak2->SkillIndex][2]; i++)
					{
						pak2->Target[i].x = pak2->Target[0].x;
						pak2->Target[i].y = pak2->Target[0].y;
					}
				}
				if (hacks.showid.on)
				{
					if (opcodes[packet->Code])
					{
						Dump((uint8_t*)packet, _size, send, callback);
					}
				}
			}
		}
		break;
	}
	case 0x185:
	{
		MSG_ATTINV *pak = (MSG_ATTINV*)packet;
		int32_t i, slot;
		ReadPacket2((uint8_t*)pak, pak->Code);
		memcpy(inv, pak->inv, sizeof(inv));
		if (hacks.drop.on)
		{
			for (slot = 0; slot < 30; slot++)
			{
				int32_t id = inv[slot].ItemID;
				if (id != 0)
				{
					if ((drop_list[id] & (1 << _bau)) != 0)
					{
						for (i = 0; i < 120; i++)
						{
							if (bau[i].ItemID == 0)
							{
								MoveItem(1, slot, 2, i);
								Show(DEFAULT_COLOR, "Item guardado: %d(%d->%d)", id, slot, i);
								break;
							}
						}
					}
					else if ((drop_list[id] & (1 << _use)) != 0)
					{
						UseItem(1, slot, 0, 0, 0);
						Show(DEFAULT_COLOR, "Item usado: %d/%d", id, slot);
					}
					else if ((drop_list[id] & (1 << _amount)) != 0)
					{
						if (amount_list[id] != slot && inv[slot].GetAmount() != 120)
						{
							if (amount_list[id] == 255)
							{
								amount_list[id] = slot;
							}
							else
							{
								sItem *it = &inv[amount_list[id]];
								if (it->GetAmount() < 120)
								{
									MoveItem(1, slot, 1, amount_list[id]);
								}
								else
								{
									amount_list[id] = slot;
								}
							}
							Show(DEFAULT_COLOR, "Item amontoado: %d(%d->%d)", id, slot, amount_list[id]);
						}
					}
					else
					{
						Show(DEFAULT_COLOR, "Item mantido: %d(%d)", id, slot);
					}
				}
			}
		}
		if (hacks.npctrade.on)
		{
			if (FindItem(hacks.npctrade.itemId) != -1)
			{
				Click(hacks.npctrade.npcId);
				Show(DEFAULT_COLOR, "Troca : trade-1");
			}
			else
			{
				int32_t k = FindItem(hacks.npctrade.itemId, 0, 2);
				if (k == -1)
				{
					hacks.npctrade.on = 0;
					hacks.drop.on = 0;
					Show(DEFAULT_COLOR, "Troca : terminou");
				}
				else
				{
					int32_t j = FindItem(0);
					if (j == -1)
					{
						hacks.npctrade.on = 0;
						hacks.drop.on = 0;
						Show(DEFAULT_COLOR, "Troca : inv cheio");
					}
					else
					{
						MoveItem(2, k, 1, j);
						Show(DEFAULT_COLOR, "Troca : (%d) -> (%d)", k, j);
						Click(hacks.npctrade.npcId);
						Show(DEFAULT_COLOR, "Troca : trade-2");
					}
				}
			}
		}
		memset(pak, 0, packet->Size);
		break;
	}
	case 0x182:
	{
		MSG_CREATEITEM *pak = (MSG_CREATEITEM*)packet;
		int32_t slot = pak->invSlot;
		int32_t old = 0;
		if (pak->Size != sizeof(MSG_CREATEITEM))
			break;
		hacks.npctrade.pTime = -1;
		if (pak->invType == 1)
		{
			old = inv[slot].ItemID;
			inv[slot] = pak->itemData;
		}
		else if (pak->invType == 2)
		{
			old = bau[slot].ItemID;
			bau[slot] = pak->itemData;
		}
		if (hacks.drop.on)
		{
			if (pak->invType == 0)
			{
				if (slot == 14 && hacks.mont.on)
				{
					if (pak->itemData.Mount.Grow_Level != 120)
					{
						if (inv[0].ItemID != 0)
						{
							UseItem(1, 0, 0, 14, 0);
						}
						else if (hacks.mont.buy == -1)
						{
							hacks.mont.buy = pak->Time + 5000;
						}
					}
					else
					{
						hacks.mont.on = 0;
						DeleteItem(0, hacks.mont.id);
						Show(DEFAULT_COLOR, "Montaria 120");
					}
				}
				else if (slot == hacks.refn.eqp && hacks.refn.on)
				{
					if (pak->itemData.GetRefin() != hacks.refn.sanc)
					{
						if (inv[0].ItemID != 0)
						{
							UseItem(1, 0, 0, hacks.refn.eqp, 0);
						}
						else if (hacks.refn.buy == -1)
						{
							hacks.refn.buy = pak->Time + 5000;
						}
					}
					else
					{
						hacks.refn.on = 0;
						DeleteItem(0, hacks.refn.id);
						Show(DEFAULT_COLOR, "Refinado!");
					}
				}
			}
			else if (pak->invType == 1)
			{
				int32_t i;
				int32_t id = pak->itemData.ItemID;
				if (hacks.ehre.on)
				{
					ReadPacket2((uint8_t*)pak, pak->Code);
					if (id == 697)
					{
						int32_t x = FindItem(3338);
						if (x != -1)
						{
							int32_t s[2] = { -1, -1 };
							s[0] = FindItem(697);
							if (s[0] != -1)
							{
								s[1] = FindItem(697, s[0] + 1);
								if (s[1] != -1)
								{
									int32_t _v[8];
									sItem _it[8];
									memset(_v, 0, sizeof(_v));
									memset(_it, 0, sizeof(_it));
									_v[0] = s[0];
									_v[1] = s[1];
									_v[2] = FindItem(3338);
									_it[0].ItemID = 697;
									_it[1].ItemID = 697;
									_it[2] = inv[_v[2]];
									NpcCombine(hacks.ehre.npcEhre, _v, _it);
								}
								else
								{
									int32_t i = FindItem(0);
									if (i != -1)
									{
										BuyItem(hacks.ehre.npcBardes, 7, i);
									}
									else
									{
										hacks.ehre.on = 0;
										Show(DEFAULT_COLOR, "Ehre: inv cheio");
									}
								}
							}
						}
						else
						{
							hacks.ehre.on = 0;
							Show(DEFAULT_COLOR, "Ehre: nenhuma ref encontrar");
						}
					}
					else if (id == 3338)
					{
						if (pak->itemData.GetRefin() == 9)
						{
							int32_t i;
							i = 0;
							while (i != -1)
							{
								i = FindItem(3338, i, 2);
								if (i != -1)
								{
									if (bau[i].GetRefin() != 9)
									{
										MoveItem(1, pak->invSlot, 2, i);
										break;
									}
									else
									{
										i++;
									}
								}
							}
							if (i == -1)
							{
								i = FindItem(0, 0, 2);
								hacks.ehre.on = 0;
								if (i != -1)
								{
									MoveItem(1, pak->invSlot, 2, i);
								}
								Show(DEFAULT_COLOR, "Ehre: terminou");
							}
						}
						else
						{
							int32_t i = FindItem(0);
							if (i != -1)
							{
								BuyItem(hacks.ehre.npcBardes, 7, i);
							}
							else
							{
								hacks.ehre.on = 0;
								Show(DEFAULT_COLOR, "Ehre: inv cheio");
							}
						}
					}
					memset(pak, 0, sizeof(MSG_CREATEITEM));
					break;
				}
				else if (hacks.npcbuy.on == 1 && slot == hacks.npcbuy.invslot)
				{
					hacks.npcbuy.pTime = pak->Time + 5000;
					if (Gold >= 1200000000)
					{
						int32_t x = FindItem(0);
						if (x != -1)
						{
							Show(DEFAULT_COLOR, "Compra: buy");
							BuyItem(hacks.npcbuy.npcId, hacks.npcbuy.slot, x);
						}
						else
						{
							hacks.npcbuy.on = 0;
							Show(DEFAULT_COLOR, "Compra: inv cheio");
						}
					}
					else
					{
#if defined(_DEBUG_)
						Show(DEFAULT_COLOR, "Compra: check %d-%d", hacks.npcbuy.invslot, inv[hacks.npcbuy.invslot].GetAmount());
#endif
						if (inv[hacks.npcbuy.invslot].ItemID != 0)
						{
							Show(DEFAULT_COLOR, "Compra: use %d", hacks.npcbuy.invslot);
							UseItem(1, hacks.npcbuy.invslot, 0, 0, 0);
						}
						else
						{
							int32_t x = FindItem(hacks.npcbuy.itemId, 0, 2);
							if (x != -1)
							{
								Show(DEFAULT_COLOR, "Compra: %d->%d", x, hacks.npcbuy.invslot);
								MoveItem(2, x, 1, hacks.npcbuy.invslot);
								Show(DEFAULT_COLOR, "Compra: use %d", hacks.npcbuy.invslot);
								UseItem(1, hacks.npcbuy.invslot, 0, 0, 0);
							}
							else
							{
								if (hacks.npcbuy.itemId == 4010)
								{
									hacks.npcbuy.itemId = 4029;
									Show(DEFAULT_COLOR, "Compra: 4010 to 4029(%d)", hacks.npcbuy.itemId);
									ShiftItem(hacks.npcbuy.invslot, 0, 0);
								}
								else
								{
									hacks.npcbuy.on = 0;
									Show(DEFAULT_COLOR, "Compra: terminou");
								}
							}
						}
					}
				}
				else if (hacks.mont.on)
				{
					if (slot == 0)
					{
						if (hacks.mont.id == -1)
						{
							hacks.mont.id = id;
						}
						UseItem(1, 0, 0, 14, 0);
					}
				}
				else if (hacks.refn.on)
				{
					if (slot == 0)
					{
						if (hacks.refn.id == -1)
						{
							hacks.refn.id = id;
						}
						UseItem(1, 0, 0, hacks.refn.eqp, 0);
					}
				}
				else if (id != 0 && (hacks.npctrade.on == 0 || id != hacks.npctrade.itemId))
				{
					uint8_t ef[3][2];
					memset(ef, 0, sizeof(ef));
					if (!drop_list[id])
					{
						DeleteItem(slot, id);
						Show(DEFAULT_COLOR, "Item deletado: %d(%d)", id, slot);
						memset(pak, 0, sizeof(MSG_CREATEITEM));
						break;
					}
					if (drop_efs[id][0][0][0] != 0)
					{
						int32_t i = 0;
						for (int32_t e = 0; e < 3; e++)
						{
							if (pak->itemData.Effect[e].Index != 0 &&
								pak->itemData.Effect[e].Index != EF_SANC)
							{
								int32_t j;
								for (j = 0; j < 3; j++)
								{
									if (ef[j][0] == pak->itemData.Effect[e].Index)
									{
										ef[j][1] += pak->itemData.Effect[e].Value;
										break;
									}
								}
								if (j == 3)
								{
									for (j = 0; j < 3; j++)
									{
										if (ef[j][0] == 0)
										{
											ef[j][0] = pak->itemData.Effect[e].Index;
											ef[j][1] = pak->itemData.Effect[e].Value;
											break;
										}
									}
								}
								i++;
							}
						}
						if (i == 0)
						{
							i = 3;
						}
						if (i < 3)
						{
							int32_t e;
							for (e = 0; e < 5; e++)
							{
								if (drop_efs[id][e][0][0] != 0)
								{
									int32_t j;
									for (j = 0; j < 3; j++)
									{
										int32_t k;
										if (drop_efs[id][e][j][0] == 0)
											continue;
										for (k = 0; k < 3; k++)
										{
											if (drop_efs[id][e][j][0] == ef[k][0])
											{
												if (ef[k][1] >= drop_efs[id][e][j][1])
												{
													break;
												}
											}
										}
										if (k == 3)
										{
											break;
										}
									}
									if (j == 3)
									{
										break;
									}
								}
							}
							if (e == 5)
							{
								i = 3;
							}
						}
						if (i == 3)
						{
							Show(DEFAULT_COLOR, "Item deletado: %d %d %d %d %d %d %d",
								id, pak->itemData.efs[0],
								pak->itemData.efs[1],
								pak->itemData.efs[2],
								pak->itemData.efs[3],
								pak->itemData.efs[4],
								pak->itemData.efs[5]);
							DeleteItem(slot, id);
							memset(pak, 0, sizeof(MSG_CREATEITEM));
							break;
						}
					}
					if ((drop_list[id] & (1 << _bau)) != 0)
					{
						for (i = 0; i < 120; i++)
						{
							if (bau[i].ItemID == 0)
							{
								ReadPacket2((uint8_t*)pak, pak->Code);
								MoveItem(1, slot, 2, i);
								Show(DEFAULT_COLOR, "Item guardado: %d(%d->%d)", id, slot, i);
								memset(pak, 0, sizeof(MSG_CREATEITEM));
								break;
							}
						}
					}
					else if ((drop_list[id] & (1 << _use)) != 0)
					{
						UseItem(1, slot, 0, 0, 0);
						Show(DEFAULT_COLOR, "Item usado: %d/%d", id, slot);
						memset(pak, 0, sizeof(MSG_CREATEITEM));
					}
					else if ((drop_list[id] & (1 << _amount)) != 0)
					{
						if (amount_list[id] != slot)
						{
							if (amount_list[id] == 255)
							{
								amount_list[id] = slot;
							}
							else
							{
								sItem *it = &inv[amount_list[id]];
								if (it->GetAmount() < 120)
								{
									ReadPacket2((uint8_t*)pak, pak->Code);
									MoveItem(1, slot, 1, amount_list[id]);
									memset(pak, 0, sizeof(MSG_CREATEITEM));
								}
								else
								{
									if (hacks.npctrade.on || hacks.barras.on)
									{
										int32_t _b = FindItem(0, 0, 2);
										if (_b != -1)
										{
											MoveItem(1, amount_list[id], 2, _b);
										}
									}
									amount_list[id] = slot;
								}
							}
							Show(DEFAULT_COLOR, "Item amontoado: %d(%d->%d)", id, slot, amount_list[id]);
							if (hacks.barras.on)
							{
								if (hacks.barras.itemId == 0)
								{
									int32_t x = FindItem(0);
									hacks.barras.itemId = id;
									if (x != -1)
									{
										Show(DEFAULT_COLOR, "Barras: compra %d", x);
										BuyItem(hacks.barras.npcId, hacks.barras.slotId, x);
									}
									else
									{
										hacks.barras.on = 0;
										Show(DEFAULT_COLOR, "Barras: invent�rio cheio");
									}
								}
								else if (id == hacks.barras.itemId)
								{
									int32_t x = FindItem(0);
									if (x != -1)
									{
										Show(DEFAULT_COLOR, "Barras: compra %d", x);
										BuyItem(hacks.barras.npcId, hacks.barras.slotId, x);
									}
									else
									{
										hacks.barras.on = 0;
										Show(DEFAULT_COLOR, "Barras: invent�rio cheio");
									}
								}
							}
						}
					}
					else
					{
						Show(DEFAULT_COLOR, "Item mantido: %d(%d)", id, slot);
					}
				}
				else if (hacks.npctrade.on)
				{
					ReadPacket2((uint8_t*)pak, pak->Code);
					if (slot != 14)
					{
						if (id == hacks.npctrade.itemId)
						{
							hacks.npctrade.slot = slot;
							Show(DEFAULT_COLOR, "Troca: trade");
							Click(hacks.npctrade.npcId);
							hacks.npctrade.pTime = pTime + 5000;
						}
						else if (slot == hacks.npctrade.slot)
						{
							if (inv[14].GetAmount() > 1)
							{
								Show(DEFAULT_COLOR, "Troca: shift");
								ShiftItem(14, hacks.npctrade.itemId, 1);
							}
							else
							{
								Show(DEFAULT_COLOR, "Troca: click");
								Click(hacks.npctrade.npcId);
							}
						}
					}
					else if (id == 0)
					{
						int32_t _b = FindItem(hacks.npctrade.itemId, 0, 2);
						if (_b != -1)
						{
							sItem aux;
							Show(DEFAULT_COLOR, "Troca: (%d)->14", _b);
							MoveItem(2, _b, 1, 14);
							aux = bau[_b];
							bau[_b] = inv[14];
							inv[14] = aux;
							hacks.npctrade.slot = -1;
						}
						else
						{
							hacks.drop.on = 0;
							hacks.npctrade.on = 0;
							Show(DEFAULT_COLOR, "Troca: terminou");
						}
					}
					memset(pak, 0, pak->Size);
				}
			}
		}
		else if (hacks.showid.on)
		{
			Show(DEFAULT_COLOR, "Item: %d %d %d %d %d %d %d -> %d",
				pak->itemData.ItemID,
				pak->itemData.EF1,
				pak->itemData.EFV1,
				pak->itemData.EF2,
				pak->itemData.EFV2,
				pak->itemData.EF3,
				pak->itemData.EFV3,
				slot);
		}
		break;
	}
	default:;
	}
	if (hacks.npctrade.on && hacks.npctrade.pTime < packet->Time)
	{
		Show(DEFAULT_COLOR, "Troca: trade-1");
		Click(hacks.npctrade.npcId);
		hacks.npctrade.pTime = packet->Time + 5000;
	}
	if (hacks.npcbuy.on && hacks.npcbuy.pTime < packet->Time)
	{
		Show(DEFAULT_COLOR, "Compra: recheck");
		ShiftItem(hacks.npcbuy.invslot, 0, 0);
	}
#if !defined(RECORD_MESSAGE)
	if (Record.on == 1)
	{
		if (send != 0)
		{
			if (Packets.hacks.macro.macro_data[0] != *(uint8_t*)addr_macro_type ||
				Packets.hacks.macro.macro_data[1] != *(uint8_t*)addr_macro_racao ||
				Packets.hacks.macro.macro_data[2] != *(uint8_t*)addr_macro_hp ||
				Packets.hacks.macro.macro_data[3] != *(uint8_t*)(*(uint32_t*)addr_ptrdat + 0x27D28) ||
#if defined(OVER)
				Packets.hacks.macro.macro_data[4] != *(uint8_t*)(*(uint32_t*)addr_ptrdat + 0x27AC4))
#else
				Packets.hacks.macro.macro_data[4] != *(uint8_t*)(*(uint32_t*)addr_ptrdat + 0x27AAC))
#endif
			{
				Packets.hacks.macro.macro_data[0] = *(uint8_t*)addr_macro_type;
				Packets.hacks.macro.macro_data[1] = *(uint8_t*)addr_macro_racao;
				Packets.hacks.macro.macro_data[2] = *(uint8_t*)addr_macro_hp;
				Packets.hacks.macro.macro_data[3] = *(uint8_t*)(*(uint32_t*)addr_ptrdat + 0x27D28);
#if defined(OVER)
				Packets.hacks.macro.macro_data[4] = *(uint8_t*)(*(uint32_t*)addr_ptrdat + 0x27AC4);
#else
				Packets.hacks.macro.macro_data[4] = *(uint8_t*)(*(uint32_t*)addr_ptrdat + 0x27AAC);
#endif
				Show(DEFAULT_COLOR, "%d %d %d %d %d", Packets.hacks.macro.macro_data[0],
					Packets.hacks.macro.macro_data[1],
					Packets.hacks.macro.macro_data[2],
					Packets.hacks.macro.macro_data[3],
					Packets.hacks.macro.macro_data[4]);
				if ((int32_t)((int32_t)Record.ptr + sizeof(MSG_SIGNALPARM2)) < (int32_t)(Record.buffer + (NUM_PACKETS * SIZE_BUFFER_RECORD)))
				{
					MSG_SIGNALPARM2 s2(0, 1);
					s2.c_signal[0] = Packets.hacks.macro.macro_data[0];
					s2.c_signal[1] = Packets.hacks.macro.macro_data[1];
					s2.c_signal[2] = Packets.hacks.macro.macro_data[2];
					s2.c_signal[3] = Packets.hacks.macro.macro_data[3];
					s2.c_signal2[0] = Packets.hacks.macro.macro_data[4];
					if (Record.t[NUM_PACKETS] == 0)
					{
						Record.t[NUM_PACKETS] = TimeGetTime();
					}
					Record.t[Record.n] = TimeGetTime() - Record.t[NUM_PACKETS];// - (Record.n > 0 ? Record.t[Record.n - 1] : Record.t[NUM_PACKETS]);
					memcpy(Record.ptr, &s2, sizeof(MSG_SIGNALPARM2));
					*(uint16_t*)Record.ptr = sizeof(MSG_SIGNALPARM2);
					Record.ptr += sizeof(MSG_SIGNALPARM2);
					Record.n++;
				}
			}
			switch (packet->Code)
			{
				/*case 0x333: case 0x334: case 0x39D: case 0x39E: case 0x000: case 0x3A0: case 0x2E5: case 0xF200: case 0x376: case 0x378: case 0x2E4:
				case 0x277: case 0x39A:
		#ifdef OVER
				case 0x36C:
		#else
				case 0x367:
		#endif*/
			default:
			{
				break;
			}
			//default:
			case 0x20D: case 0xFDE: case 0x213: case 0x373: case 0x379: case 0x290: case 0x28B: case 0x3AE:// case 0x291:
#ifdef OVER
			case 0x366:
#else
			case 0x36C:
#endif
			{
				if (((int32_t)Record.ptr + _size) < ((int32_t)Record.buffer + (NUM_PACKETS * SIZE_BUFFER_RECORD)))
				{
					Record.t[Record.n] = TimeGetTime() - Record.t[NUM_PACKETS];// - (Record.n > 0 ? Record.t[Record.n - 1] : Record.t[NUM_PACKETS]);
					memcpy(Record.ptr, packet, _size);
					*(uint16_t*)Record.ptr = _size;
					Record.ptr += _size;
					Record.n++;
				}
				break;
			}
			}
		}
	}
#endif
	if (send != 0 && Macro.on != 0)
	{
		switch (packet->Code)
		{
		case 0xFDE:
		{
			Senha2Window(0);
			break;
		}/*
		 case 0x213:
		 {
		 SelectChar();
		 break;
		 }*/
		}
	}
}

int32_t cPackets::Chat(char *str)
{
	int32_t op = 0;
#ifdef _SEC_
	iCheckHWBP();
#endif
	if (str[0] == '#')
	{
		if (!strcmp(str, "#reload"))
		{
			return 1;
		}
#ifdef ENABLE_REC
		if (!strncmp(str, "#rec ", 5))
		{
			sscanf(str, "#rec %d", &op);
			if (op == 1)
			{
				Record.t[Record.n] = TimeGetTime() - Record.t[NUM_PACKETS];
				Record.on = 1;
				Show(DEFAULT_COLOR, "Iniciando grava��o");
			}
			else if (op == 0)
			{
				if (Record.on == 1)
				{
					Record.t[Record.n] = TimeGetTime() - Record.t[NUM_PACKETS];// - (Record.n > 0 ? Record.t[Record.n - 1] : 0);
					*(uint16_t*)Record.ptr = 2;
					Record.ptr += 2;
					Record.n++;
				}
				Record.on = 0;
				Show(DEFAULT_COLOR, "Pausando grava��o");
			}
			else if (op == -1)
			{
				memset(Record.t, 0, sizeof(Record.t));
				memset(Record.buffer, 0, sizeof(Record.buffer));
				Record.ptr = Record.buffer;
				Record.n = 0;
				Record.on = 0;
				Show(DEFAULT_COLOR, "Reiniciando a grava��o");
			}
			return 1;
		}
		if (!strncmp(str, "#save ", 6))
		{
			char rec_name[48] = "./";
			sscanf(str, "#save %[^\n]s", &rec_name[2]);
			if (rec_name[2] != 0)
			{
				FILE *f = fopen(rec_name, "wb");
				if (f)
				{
					fwrite(&Record, sizeof(Record), 1, f);
					fclose(f);
					SetFileAttributes(rec_name, FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_READONLY);
					Show(DEFAULT_COLOR, "Macro '%s' salvo", &rec_name[2]);
				}
				else
				{
					Show(DEFAULT_COLOR, "Tente outro nome.");
				}
			}
			return 1;
		}
		if (!strncmp(str, "#play ", 6))
		{
			char rec_name[48] = "./";
			sscanf(str, "#play %d %[^\n]s", &op, &rec_name[2]);
			if (rec_name[2] != 0)
			{
				FILE *f = fopen(rec_name, "rb");
				if (f)
				{
					fread(&Macro, sizeof(Macro), 1, f);
					fclose(f);
					Record.on = 0;
#if !defined(RECORD_MESSAGE)
					Macro.ptr = Macro.buffer;
#endif
					Macro.n = -1;
					Macro.runs = op;
					Macro.on = 1;
					Show(DEFAULT_COLOR, "Macro '%s' ser� executado %u vezes", rec_name, Macro.runs);
				}
			}
			return 1;
		}
#endif
#ifdef ENABLE_MOVED
		if (!strncmp(str, "#moved ", 7))
		{
			int32_t x = -1, y = -1;
			sscanf(str, "#moved %d %d", &x, &y);
			if (x != -1 && y != -1)
			{
				Move(x, y, 1);
			}
			return 1;
		}
#endif // ENABLE_MOVED
#ifdef ENABLE_MOVE
		if (!strncmp(str, "#move ", 6))
		{
			int32_t x = -1, y = -1;
			sscanf(str, "#move %d %d", &x, &y);
			if (x != -1 && y != -1)
			{
				Move(x, y, 0);
			}
			return 1;
		}
#endif // ENABLE_MOVE
#ifdef ENABLE_SKILL
		if (!strncmp(str, "#skill ", 7))
		{
			int32_t sk = -1;
			sscanf(str, "#skill %d", &sk);
			if (sk != -1)
			{
				hacks.skill.id = sk;
			}
			return 1;
		}
#endif // ENABLE_SKILL
#ifdef ENABLE_PAK
		if (!strncmp(str, "#pak ", 5))
		{
			int32_t x = 0;
			sscanf(str, "#pak %x", &x);
			if (x >= 0 && x < 0xFFFF)
				opcodes[x] = 1;
			else if (x == 0x10000)
			{
				memset(opcodes, 1, sizeof(opcodes));
			}
			Show(DEFAULT_COLOR, "Packet: 0x%04X", x);
			return 1;
		}
#endif // ENABLE_PAK
#ifdef ENABLE_SHOWID
		if (!strncmp(str, "#showids ", 9))
		{
			sscanf(str, "#showids %d", &op);
			hacks.pick.n = 0;
			hacks.pick.stage = 0;
			hacks.pick.c = 0;
			hacks.pick.i = 0;
			if (op == 0)
			{
				hacks.showid.on = 0;
				Show(DEFAULT_COLOR, "ShowIds: OFF");
			}
			else
			{
				hacks.showid.on = 1;
				Show(DEFAULT_COLOR, "ShowIds: ON");
			}
			return 1;
		}
#endif // ENABLE_SHOWID
#ifdef ENABLE_DROP
		if (!strncmp(str, "#drop ", 6))
		{
			sscanf(str, "#drop %d", &op);
			if (op == 0)
			{
				hacks.drop.on = 0;
				Show(DEFAULT_COLOR, "Drop: OFF");
			}
			else
			{
				hacks.drop.on = 1;
				Show(DEFAULT_COLOR, "Drop: ON");
			}
			return 1;
		}
#endif // ENABLE_DROP
#ifdef ENABLE_INV

		if (!strncmp(str, "#inv ", 5))
		{
			int32_t op2 = 2, op3 = 0;
			sscanf(str, "#inv %d %d %d", &op, &op2, &op3);
			if (op == 0)
			{
				hacks.inv.on = 0;
				Show(DEFAULT_COLOR, "Inv: OFF");
			}
			else
			{
				hacks.inv.on = 1;
				Grifo(op2, op3);
				Show(DEFAULT_COLOR, "Inv: ON(%d/%d)", op2, op3);
			}
			return 1;
		}
#endif
#ifdef ENABLE_DENY
		if (!strncmp(str, "#deny ", 6))
		{
			sscanf(str, "#deny %d", &op);
			if (op == 0)
			{
				hacks.deny.on = 0;
				Show(DEFAULT_COLOR, "Deny: OFF");
			}
			else
			{
				hacks.deny.on = 1;
				Show(DEFAULT_COLOR, "Deny: ON");
			}
			return 1;
		}
#endif // ENABLE_DENY
#ifdef ENABLE_SAME
		if (!strncmp(str, "#same ", 6))
		{
			sscanf(str, "#same %d", &op);
			if (op == 0)
			{
				hacks.same.on = 0;
				Show(DEFAULT_COLOR, "Same: OFF");
			}
			else
			{
				sscanf(str, "#same %d", &hacks.same.on);
				Show(DEFAULT_COLOR, "Same: ON");
			}
			return 1;
		}
#endif // ENABLE_SAME
#ifdef ENABLE_BAU
		if (!strncmp(str, "#bau", 4))
		{
			Show(DEFAULT_COLOR, "Bau aberto");
			OpenCargo();
			return 1;
		}
#endif // ENABLE_BAU
#ifdef ENABLE_GRIFO
		if (!strncmp(str, "#grifo ", 7))
		{
			int32_t op = -1;
			sscanf(str, "#grifo %d", &op);
			if (op != -1)
			{
				Show(DEFAULT_COLOR, "Grifo: %d", op);
				Grifo(op);
			}
			return 1;
		}
#endif // ENABLE_GRIFO
#ifdef ENABLE_ITEM
		if (!strncmp(str, "#item ", 6))
		{
			int32_t v[4] = { -1, -1, -1, -1 };
			sscanf(str, "#item %d %d %d %d", &v[0], &v[1], &v[2], &v[3]);
			if (v[0] != -1 && v[1] != -1 && v[2] != -1 && v[3] != -1)
			{
				MoveItem(v[0], v[1], v[2], v[3]);
				Show(DEFAULT_COLOR, "MoveItem: %d %d -> %d %d", v[0], v[1], v[2], v[3]);
			}
			return 1;
		}
#endif // ENABLE_ITEM
#ifdef ENABLE_PARTY
		if (!strncmp(str, "#party ", 7))
		{
			char nick[16] = "";
			int32_t id = -1;
			sscanf(str, "#party %d %[^\n]s", &id, nick);
			if (id != -1 && nick[0])
			{
				Party(id, nick);
				Show(DEFAULT_COLOR, "Party: %s(%d)", nick, id);
			}
			return 1;
		}
#endif // ENABLE_PARTY
#ifdef ENABLE_SHIFT
		if (!strncmp(str, "#shift ", 7))
		{
			int32_t slot = -1, id = -1, n = -5;
			sscanf(str, "#shift %d %d %d", &slot, &id, &n);
			if (slot != -1 && id != -1 && n != -5)
			{
				ShiftItem(slot, id, n);
				Show(DEFAULT_COLOR, "ShiftItem: %d %d %d", slot, id, n);
			}
			return 1;
		}
#endif // ENABLE_SHIFT
#ifdef ENABLE_NSHIFT
		if (!strncmp(str, "#nshift ", 8))
		{
			int32_t slot = -1, id = -1, n = 1;
			sscanf(str, "#nshift %d %d", &slot, &n);
			if (slot >= 0 && slot < 60 && n > 0)
			{
				id = inv[slot].ItemID;
				for (int32_t i = 0; i < n; i++)
				{
					ShiftItem(slot, id, 1);
				}
				Show(DEFAULT_COLOR, "nShiftItem: %d %d %d", slot, id, n);
			}
			return 1;
		}
#endif // ENABLE_NSHIFT
#ifdef ENABLE_USE
		if (!strncmp(str, "#use ", 5))
		{
			int32_t slot = -1, id = -1, op = 0;
			sscanf(str, "#use %d %d", &slot, &op);
			if (slot >= 0 && slot < 60)
			{
				id = inv[slot].ItemID;
				UseItem(1, slot, 0, 0, op);
				Show(DEFAULT_COLOR, "UseItem: %d %d %d", slot, id, op);
			}
			return 1;
		}
#endif // ENABLE_USE
#ifdef ENABLE_NUSE
		if (!strncmp(str, "#nuse ", 6))
		{
			int32_t slot = -1, id = -1, n = 1;
			sscanf(str, "#nuse %d %d", &slot, &n);
			if (slot >= 0 && slot < 60 && n > 0)
			{
				id = inv[slot].ItemID;
				for (int32_t i = 0; i < n; i++)
				{
					UseItem(1, slot, 0, 0, 0);
				}
				Show(DEFAULT_COLOR, "nUseItem: %d %d %d", slot, id, n);
			}
			return 1;
		}
#endif // ENABLE_NUSE
#ifdef ENABLE_NCLICK
		if (!strncmp(str, "#nclick ", 8))
		{
			int32_t id = -1, n = -1;
			sscanf(str, "#nclick %d %d", &id, &n);
			for (int32_t i = 0; i < n; i++)
			{
				Click(id);
			}
			Show(DEFAULT_COLOR, "nClick: %d %d", id, n);
			return 1;
		}
#endif // ENABLE_NCLICK
#ifdef ENABLE_TROCA
		if (!strncmp(str, "#troca ", 7))
		{
			int32_t id = -1, n = -1;
			sscanf(str, "#troca %d %d", &id, &n);
			if (id == -1 || n == -1)
			{
				hacks.npctrade.on = 0;
			}
			else
			{
				int32_t _b = FindItem(n, 0, 2);
				int32_t _s = 0;
				hacks.npctrade.on = 0;
				if (_b == -1 && inv[14].ItemID != n)
				{
					Show(DEFAULT_COLOR, "Troca: nenhum (%d(%d)) encontrado", n, inv[14].ItemID);
				}
				else if (_b != -1)
				{
					int32_t amm;
					hacks.npctrade.npcId = id;
					hacks.npctrade.itemId = n;
					hacks.npctrade.on = 1;
					hacks.npctrade.pTime = 0xFFFFFFFF;
					hacks.npctrade.slot = -1;
					if (inv[14].ItemID == 0)
					{
						Show(DEFAULT_COLOR, "Troca: (%d)->(%d)", _b, _s);
						MoveItem(2, _b, 1, 14);
						bau[_b].swap(&inv[14]);
					}
					else
					{
						amm = inv[14].GetAmount();
						if (amm > 1)
						{
							ShiftItem(14, n, 1);
						}
						else
						{
							Click(hacks.npctrade.npcId);
						}
					}
					Show(DEFAULT_COLOR, "Troca: %d %d", id, n);
				}
				else
				{
					Show(DEFAULT_COLOR, "Troca: nenhum (%d) encontrado", n);
				}
			}
			return 1;
		}
#endif // ENABLE_TROCA
#ifdef ENABLE_BARRAS
		if (!strncmp(str, "#barras ", 8))
		{
			int32_t npc = -1, slot = -1;
			sscanf(str, "#barras %d %d", &npc, &slot);
			if (npc == -1 || slot == -1)
			{
				hacks.barras.on = 0;
			}
			else
			{
				int32_t x;
				hacks.barras.npcId = npc;
				hacks.barras.slotId = slot;
				x = FindItem(0);
				if (x == -1)
				{
					Show(DEFAULT_COLOR, "Barras: invent�rio cheio");
				}
				else
				{
					hacks.barras.on = 1;
					Show(DEFAULT_COLOR, "Barras: compra %d", x);
					BuyItem(npc, slot, x);
				}
			}
			return 1;
		}
#endif // ENABLE_BARRAS
#ifdef ENABLE_BARRAS2
		if (!strncmp(str, "#barras2 ", 9))
		{
			int32_t npc = -1, slot = -1;
			sscanf(str, "#barras2 %d %d", &npc, &slot);
			if (npc == -1 || slot == -1)
			{
				hacks.barras2.on = 0;
			}
			else
			{
				hacks.barras2.npcId = npc;
				hacks.barras2.slotId = slot;
				hacks.barras2.on = 1;
			}
			return 1;
		}
#endif // ENABLE_BARRAS2
#ifdef ENABLE_COMPRA
		if (!strncmp(str, "#compra ", 8))
		{
			int32_t npc = -1, slot = -1;
			sscanf(str, "#compra %d %d", &npc, &slot);
			if (npc == -1 || slot == -1)
			{
				hacks.npcbuy.on = 0;
			}
			else
			{
				int32_t ids[2] = { 4010, 4029 };
				hacks.npcbuy.slot = slot;
				hacks.npcbuy.npcId = npc;
				hacks.npcbuy.pTime = 0xFFFFFFFF;
				for (int32_t i = 0; i < 2; i++)
				{
					int32_t id = ids[i];
					hacks.npcbuy.itemId = id;
					hacks.npcbuy.invslot = FindItem(id);
					if (hacks.npcbuy.invslot != -1)
					{
						if (Gold < 1200000000)
						{
							hacks.npcbuy.on = 1;
							Show(DEFAULT_COLOR, "Compra: use %d", hacks.npcbuy.invslot);
							UseItem(1, hacks.npcbuy.invslot, 0, 0, 0);
						}
						else
						{
							int32_t x = FindItem(0);
							if (x != -1)
							{
								hacks.npcbuy.on = 1;
								Show(DEFAULT_COLOR, "Compra: buy %d", x);
								BuyItem(npc, slot, x);
							}
							else
							{
								Show(DEFAULT_COLOR, "Compra: inv cheio");
								hacks.npcbuy.on = 0;
							}
						}
					}
					else
					{
						int32_t x = FindItem(id, 0, 2);
						if (x != -1)
						{
							hacks.npcbuy.invslot = FindItem(0);
							if (hacks.npcbuy.invslot == -1)
							{
								Show(DEFAULT_COLOR, "Compra: inv cheio");
								hacks.npcbuy.on = 0;
							}
							else
							{
								Show(DEFAULT_COLOR, "Compra: %d->%d", x, hacks.npcbuy.invslot);
								hacks.npcbuy.on = 1;
								MoveItem(2, x, 1, hacks.npcbuy.invslot);
							}
						}
						else
						{
							hacks.npcbuy.on = 0;
							Show(DEFAULT_COLOR, "Compra: nenhum (%d) encontrado", id);
							continue;
						}
					}
					break;
				}
			}
			return 1;
		}
#endif // ENABLE_COMPRA
#ifdef ENABLE_NMOVE
		if (!strncmp(str, "#nmove ", 7))
		{
			int32_t id = -1, n = -1;
			int32_t i = 0;
			sscanf(str, "#nmove %d %d", &id, &n);
			i = PickItem(id, n);
			Show(DEFAULT_COLOR, "nMove: %d %d/%d", id, n, i);
			return 1;
		}
#endif // ENABLE_NMOVE
#ifdef ENABLE_GUARDA
		if (!strncmp(str, "#guarda ", 8))
		{
			int32_t id = -1, n = -1;
			int32_t i = 0, j = 127, k = 0;
			sscanf(str, "#guarda %d", &n);
			for (k = 0; k < 60; k++)
			{
				if (inv[k].ItemID != 0)
				{
					while (j >= 0)
					{
						if (bau[j].ItemID == 0)
						{
							MoveItem(1, k, 2, j--);
							break;
						}
						j--;
					}
					if (j == -1)
					{
						break;
					}
					i++;
					if (i == n)
					{
						break;
					}
				}
			}
			Show(DEFAULT_COLOR, "Guarda: %d/%d", n, i);
			return 1;
		}
#endif // ENABLE_GUARDA
#ifdef ENABLE_REC
		if (!strncmp(str, "#rec ", 5))
		{
			int32_t x = -1, y = -1;
			if (hacks.rec.on)
			{
				hacks.rec.on = 0;
				Show(DEFAULT_COLOR, "Rec: OFF");
			}
			else
			{
				sscanf(str, "#rec %d %d", &x, &y);
				hacks.rec.pos.x = x;
				hacks.rec.pos.y = y;
				hacks.rec.on = 1;
				Show(DEFAULT_COLOR, "Rec: %d %d", x, y);
			}
			return 1;
		}
#endif // ENABLE_REC
#ifdef ENABLE_DEL
		if (!strncmp(str, "#del ", 5))
		{
			int32_t slot = -1, id = -1;
			sscanf(str, "#del %d %d", &slot, &id);
			if (slot != -1 && id != -1)
			{
				DeleteItem(slot, id);
				Show(DEFAULT_COLOR, "DeletaItem: %d %d", slot, id);
			}
			return 1;
		}
#endif // ENABLE_DEL
#ifdef ENABLE_AMG
		if (!strncmp(str, "#amg ", 5))
		{
			int32_t n = -1, id = -1;
			sscanf(str, "#amg %d %d", &id, &n);
			if (n != -1 && id != -1)
			{
				for (int32_t i = 0; i < n; i++)
				{
					UseItem(1, id, 0, 14, 0);
				}
				Show(DEFAULT_COLOR, "Amagos: %d %d", id, n);
			}
			return 1;
		}
#endif // ENABLE_AMG
#ifdef ENABLE_MONT
		if (!strncmp(str, "#mont ", 6))
		{
			if (hacks.mont.on)
			{
				hacks.mont.on = 0;
				Show(DEFAULT_COLOR, "MacroAmagos: OFF");
			}
			else
			{
				int32_t npc = -1, slot = -1;
				sscanf(str, "#mont %d %d", &npc, &slot);
				hacks.mont.npc = npc;
				hacks.mont.slot = slot;
				hacks.mont.id = -1;
				hacks.mont.buy = -1;
				hacks.mont.on = 1;
				BuyItem(npc, slot, 0);
				Show(DEFAULT_COLOR, "MacroAmagos: ON Npc(%d) Slot(%d)", npc, slot);
			}
			return 1;
		}
#endif // ENABLE_MONT
#ifdef ENABLE_FAKE
		if (!strncmp(str, "#fake ", 6))
		{
			int32_t id = 0;
			sscanf(str, "#fake %d", &id);
			hacks.fake.id = id;
			Show(DEFAULT_COLOR, "FakeID: %d", id);
			return 1;
		}
#endif // ENABLE_FAKE
#ifdef ENABLE_EHRE
		if (!strncmp(str, "#ehre ", 6))
		{
			int32_t i;
			sscanf(str, "#ehre %d %d",
				&hacks.ehre.npcEhre, &hacks.ehre.npcBardes);
			hacks.ehre.on = 1;
			i = 0;
			while (i != -1)
			{
				i = FindItem(3338, i, 2);
				if (i != -1)
				{
					if (bau[i].GetRefin() != 9)
					{
						int32_t j = FindItem(0);
						if (j != -1)
						{
							MoveItem(2, i, 1, j);
							break;
						}
						else
						{
							Show(DEFAULT_COLOR, "Ehre: inv cheio",
								hacks.ehre.npcBardes, hacks.ehre.npcEhre);
							hacks.ehre.on = 0;
							break;
						}
					}
					else
					{
						i++;
					}
				}
			}
			if (i == -1)
			{
				Show(DEFAULT_COLOR, "Ehre: nenhuma ref encontrada",
					hacks.ehre.npcBardes, hacks.ehre.npcEhre);
				hacks.ehre.on = 0;
			}
			else
			{
				Show(DEFAULT_COLOR, "Ehre: Bardes(%d) Ehre(%d)",
					hacks.ehre.npcBardes, hacks.ehre.npcEhre);
			}
			return 1;
		}
#endif // ENABLE_EHRE
#ifdef ENABLE_REF
		if (!strncmp(str, "#ref ", 5))
		{
			if (hacks.refn.on)
			{
				hacks.refn.on = 0;
				Show(DEFAULT_COLOR, "Refina��o: OFF");
			}
			else
			{
				int32_t npc = -1, slot = -1, sanc = -1, eqp = -1;
				sscanf(str, "#ref %d %d %d %d", &npc, &slot, &eqp, &sanc);
				hacks.refn.npc = npc;
				hacks.refn.slot = slot;
				hacks.refn.sanc = sanc;
				hacks.refn.id = -1;
				hacks.refn.eqp = eqp;
				hacks.refn.buy = -1;
				hacks.refn.on = 1;
				BuyItem(npc, slot, 0);
				Show(DEFAULT_COLOR, "Refina��o: ON Npc(%d) Slot(%d) Ref(%d) Equip(%d)", npc, slot, sanc, eqp);
			}
			return 1;
		}
#endif // ENABLE_REF
		return 1;
	}
#ifdef _SEC_
	iPushSS();
#endif
	return 0;
}

void hook_messagebox()
{
	MessageBoxA(0, "At� aqui foi", "", 0);
}

void cPackets::Hooks()
{
#ifdef _SEC_
	iCIsDebuggerPresent();
#endif
	//chat
	write_jmp(addr_hook_chat, (uint32_t)naked_hook_chat_);
#ifdef _SEC_
	iInterrupt41();
#endif

#ifdef _SEC_
	iPushSS();
#endif

#ifdef DON
	//send
	write_jmp(addr_hook_send2, (uint32_t)naked_hook_send2_);
	//write_jmp((uint32_t)&send_don_dir[addr_hook_send2 - CalcAddress(0x428E0C - 0x400000)], (uint32_t)naked_hook_send2_);
	//write_jmp(addr_hook_send, (uint32_t)naked_hook_send_);
#else // DON
	//send
	write_jmp(addr_hook_send, (uint32_t)naked_hook_send_);
#endif

	//write_jmp(addr_hook_connect, (uint32_t)TunnelProxy_GetThis, 1);
	//write_jmp(addr_hook_close, (uint32_t)mCloseSocket, 1);
	//recv
	write_jmp(addr_hook_recv, (uint32_t)naked_hook_recv_);
#ifdef _SEC_
	iCheckHWBP();
#endif

	//winkey
	*(uint8_t*)addr_winkey_1 = 0xEB;
	*(uint8_t*)addr_winkey_2 = 0xEB;
	*(uint8_t*)addr_winkey_3 = 0xEB;

#ifdef _SEC_
	iInterrupt41();
#endif
#if defined(RECORD_MESSAGE)
	write_jmp(addr_hook_window, (uint32_t)HWindowEvent);
#endif

	//limite chat
	*(uint16_t*)addr_lim_chat = 0xE990;

	//login failed
	*(uint8_t*)addr_login_failed = 0xEB;

	//notify
	*(uint16_t*)addr_notify = 0xE990;

	uint8_t hook_my_calls[] = { 0xE8, 0x00, 0x00, 0x00, 0x00, 0x58, 0xE8, 0x00, 0x00, 0x00, 0x00, 0x58, 0xE8, 0x00, 0x00, 0x00, 0x00, 0x58, 0xFF, 0xE2 };
	uint8_t hook_my_calls_cc[sizeof(hook_my_calls)];
	for (int32_t i = 0; i < sizeof(hook_my_calls); i++)
	{
		hook_my_calls_cc[i] = 0xCC;
	}
	for (int32_t i = 0x580000; i < 0x5B0000; i++)
	{
		if (memcmp((void*)i, hook_my_calls_cc, sizeof(hook_my_calls)) == 0)
		{
			memcpy((void*)i, hook_my_calls, sizeof(hook_my_calls));
			addr_my_calls = i;
			break;
		}
	}
	uint32_t addrs_fix[] = {
	0x42903A - 0x400000, 0x429050 - 0x400000, 0x42909A - 0x400000, 0x4290B0 - 0x400000,
	0x4290CB - 0x400000, 0x429352 - 0x400000, 0x4293BB - 0x400000, 0x429421 - 0x400000,
	0x429493 - 0x400000, 0x4294E7 - 0x400000, 0x4294FD - 0x400000, 0x42957E - 0x400000,
	0x429594 - 0x400000, 0x429625 - 0x400000, 0x429653 - 0x400000 };
	int32_t addr_base_fix = CalcAddress(0x428E0C - 0x400000);
	for (int32_t i = 0; i < _countof(addrs_fix); i++)
	{
		int32_t real_addr_fix = CalcAddress(addrs_fix[i]);
		int32_t real_addr_call = *(int32_t*)(real_addr_fix + 1) + real_addr_fix + 5;
		write_jmp((uint32_t)&send_don_dir[real_addr_fix - addr_base_fix], real_addr_call, 0);
	}
	//write_jmp((uint32_t)&send_don_dir[CalcAddress(0x4290BF - 0x400000) - addr_base_fix], (uint32_t)hook_messagebox, 0);
	write_jmp(
		(uint32_t)&send_don[CalcAddress(0x54B103 - 0x400000) - CalcAddress(0x54B02D - 0x400000)],
		(uint32_t)&send_don_dir[addr_send_dir - addr_base_fix],
		0);
	write_jmp(
		(uint32_t)&send_don[CalcAddress(0x54B11A - 0x400000) - CalcAddress(0x54B02D - 0x400000)],
		CalcAddress(0x54B126 - 0x400000),
		0);
	send_don_dir[CalcAddress(0x42918F - 0x400000) - addr_base_fix + 2] = 0x95;
	send_don_dir[CalcAddress(0x429330 - 0x400000) - addr_base_fix + 2] = 0xE4;
	addr_send_dirr = addr_send_dir;
	addr_sendd = addr_send;
}

void CALLBACK TimerProc(HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime)
{
	if (timers_id[WM_PACKETS] == idEvent)
	{
		static int64_t count_packets = 0;
		int32_t pos[15];
		int32_t i_barras = 0;
		int32_t s_barras = 0;
		memset(pos, 0, sizeof(pos));
		Packets.hacks.barras2.on = 0;
		if (patched > 1)
		{
			//if(Packets.IsOnline())
			if ((count_packets % 10) == 0)
			{
				int32_t p;
				for (p = 0; p < 64; p++)
				{
					if (Packets.TimerPacket[p].time != 0 &&
						Packets.TimerPacket[p].time < time(0))
					{
						break;
					}
				}
				if (p != 64)
				{
					MSG_HEADER *packet = (MSG_HEADER*)Packets.TimerPacket[p].buffer;
					Packets.SendPacket((uint8_t*)packet, packet->Size, 0);
					Packets.TimerPacket[p].time = 0;
				}
			}
			if (*(int32_t*)addr_send_class > 0 && *(int32_t*)(*(int32_t*)addr_send_class) > 0)
			{
				if (Packets.hacks.barras2.on)
				{
					switch (s_barras % 2)
					{
					case 0:
					{
						Packets.BuyItem(Packets.hacks.barras2.npcId,
							Packets.hacks.barras2.slotId,
							i_barras);
						i_barras++;
						if (i_barras == 7)
						{
							i_barras = 0;
							s_barras++;
						}
						break;
					}
					case 1:
					{
						if ((s_barras % 4) == 3)
							memset(pos, 0, sizeof(pos));
						sItem *it = &Packets.inv[i_barras + 7];
						if (it->ItemID && it->GetAmount() == 120)
						{
							int32_t x = 0;
							for (int32_t j = 0; j < 7; j++)
							{
								if (pos[j] > x)
								{
									x = pos[j];
								}
							}
							x = Packets.FindItem(0, x, 2);
							pos[i_barras] = x + 1;
							Packets.MoveItem(1, i_barras + 7, 2, x);
						}
						else if (Packets.inv[i_barras].ItemID)
						{
							Packets.MoveItem(1, i_barras, 1, i_barras + 7);
						}
						i_barras++;
						if (i_barras == 7)
						{
							i_barras = 0;
							s_barras++;
						}
						break;
					}
					}
				}
			}
		}
		count_packets++;
	}
	else if (timers_id[WM_MACRO] == idEvent)
	{
		static int64_t countMacro = 0;
		static int64_t count_macro = 0;
		if (patched > 1)
		{
			if (*(int32_t*)addr_send_class > 0 && *(int32_t*)(*(int32_t*)addr_send_class) > 0)
			{
				//Packets.Show(0xFF99FF22, "#1- %d %d %I64d %I64d", Packets.Macro.on, Packets.Macro.n, countMacro, count);
				if (Packets.hacks.macro.re_l > 0)
				{
					if (Packets.hacks.macro.re_l == 1)
					{
						Packets.Macro.n = 0;
						countMacro = -1;
#if !defined(RECORD_MESSAGE)
						Packets.Macro.ptr = Packets.Macro.buffer;
#endif
						Packets.Macro.runs = 1;
						Packets.Macro.on = 1;
					}
					Packets.hacks.macro.re_l--;
				}
				else
				{
					if (Packets.Macro.on == 1 && countMacro < count_macro && Packets.Macro.n != -1)
					{
						if (Packets.Macro.n == -2)
						{
							Packets.Macro.ptr = Packets.Macro.buffer;
							Packets.Macro.n = 0;
						}
						else
						{
							if (Packets.Macro.t[Packets.Macro.n] == 0 &&
#if !defined(RECORD_MESSAGE)
								*Packets.Macro.ptr == 0)
#else
								1)
#endif
							{
								Packets.Macro.runs--;
								if (Packets.Macro.runs == 0)
								{
									Packets.Macro.on = 0;
								}
								else
								{
									Packets.Macro.n = -1;
#if !defined(RECORD_MESSAGE)
									Packets.Macro.ptr = Packets.Macro.buffer;
#endif
								}
							}
							else if (Packets.Macro.on == 1)
							{
#if !defined(RECORD_MESSAGE)
								if (*(uint16_t*)&Packets.Macro.ptr[4] != loginCode)
								{
									if (Packets.myID != -1 && *(uint16_t*)&Packets.Macro.ptr[6] != 0x213)
									{
										*(uint16_t*)&Packets.Macro.ptr[6] = Packets.myID;
									}
									Packets.Dump(Packets.Macro.ptr, *(uint16_t*)Packets.Macro.ptr, 1, 0, 1);
									switch (*(uint16_t*)&Packets.Macro.ptr[4])
									{
									case 1:
									{
										MSG_SIGNALPARM2 *paks2 = (MSG_SIGNALPARM2 *)Packets.Macro.ptr;
										*(uint8_t*)addr_macro_type = (uint8_t)paks2->c_signal[0];
										*(uint8_t*)addr_macro_racao = (uint8_t)paks2->c_signal[1];
										*(uint8_t*)addr_macro_hp = (uint8_t)paks2->c_signal[2];
										*(uint8_t*)(*(uint32_t*)addr_ptrdat + 0x27D28) = (uint8_t)paks2->c_signal[3];
#if defined(OVER)
										*(uint8_t*)(*(uint32_t*)addr_ptrdat + 0x27AC4) = (uint8_t)paks2->c_signal2[0];
#else
										*(uint8_t*)(*(uint32_t*)addr_ptrdat + 0x27AAC) = (uint8_t)paks2->c_signal2[0];
#endif
										break;
									}
#ifdef OVER
									case 0x367: case 0x366:
#else
									case 0x366: case 0x36C:
#endif
									{
										//Packets.ReadPacket(Packets.Macro.ptr, *(uint16_t*)&Packets.Macro.ptr[4]);
										Packets.SendPacket(Packets.Macro.ptr, *(uint16_t*)Packets.Macro.ptr, 0);
										((MSG_MOVE*)Packets.Macro.ptr)->mType = 1;
										Packets.ReadPacket(Packets.Macro.ptr, *(uint16_t*)&Packets.Macro.ptr[4]);
										break;
									}
									default:
									{
										Packets.SendPacket(Packets.Macro.ptr, *(uint16_t*)Packets.Macro.ptr, 0);
										break;
									}
									}
								}
								Packets.Macro.ptr += *(uint16_t*)Packets.Macro.ptr;
#else
								RWindowEvent(Packets.Macro.buffer[Packets.Macro.n][0],
									Packets.Macro.buffer[Packets.Macro.n][1],
									Packets.Macro.buffer[Packets.Macro.n][2]);
								Packets.Show(0xFF99FF22, "%d: %d %d %d", Packets.Macro.n,
									Packets.Macro.buffer[Packets.Macro.n][0],
									Packets.Macro.buffer[Packets.Macro.n][1],
									Packets.Macro.buffer[Packets.Macro.n][2]);
#endif
								Packets.Macro.n++;
							}
						}
						countMacro = count_macro + (Packets.Macro.t[Packets.Macro.n] - Packets.Macro.t[Packets.Macro.n - 1]) / 110;
						Packets.Show(0xFF99FF22, "#Time: %I64d %I64d %I64d %I64d", Packets.Macro.t[Packets.Macro.n], Packets.Macro.t[Packets.Macro.n - 1], (Packets.Macro.t[Packets.Macro.n] - Packets.Macro.t[Packets.Macro.n - 1]) / 110, countMacro);
					}
				}
			}
			else if (Packets.online && (Packets.Macro.n > 0 || *Packets.Macro.buffer))
			{
				//Packets.Show(0xFF99FF22, "#3- %d %I64d %I64d", Packets.Macro.on, countMacro, count);
				if (Packets.sv != -1 && Packets.ch != -1)
				{
					char *svlist = (char*)(addr_svlist);
					strcpy((char*)((*(int32_t*)addr_ip) + 0x60),
						&svlist[(Packets.sv * 64 * 11) + (Packets.ch * 64)]);
					Packets.TunnelProxy(
						*(int32_t*)addr_send_class,
						(const char*)((*(int32_t*)addr_ip) + 0x60),
						*(int32_t*)addr_port,
						0,
						0x464);
					Packets.Macro.n = 0;
#if !defined(RECORD_MESSAGE)
					Packets.Macro.ptr = Packets.Macro.buffer;
#endif
					Packets.Macro.on = 1;
				}
			}
		}
		count_macro++;
	}
	else if (timers_id[WM_CHECKS] == idEvent)
	{
		static int64_t count_check = 0;
		if ((count_check % 100) == 0)
		{
			if (Packets.online)
			{
				if (Packets.proxy_mode)
				{
					
				}
				
			}
		}
		switch (rand() % 4)
		{
		case 0:
		{
#ifdef _SEC_
			iPushSS();
#endif
			break;
		}
		case 1:
		{
#ifdef _SEC_
			iCIsDebuggerPresent();
#endif
			break;
		}
		case 2:
		{
#ifdef _SEC_
			iInterrupt41();
#endif
			break;
		}
		case 3:
		{
#ifdef _SEC_
			iCheckHWBP();
#endif
			break;
		}
		}
		count_check++;
	}
}

void ThreadHandle()
{
	int32_t e = 0;
	int64_t count = 0;
#ifdef _SEC_
	iPushSS();
#endif
	while (1)
	{
		if (patched == 1)
		{
#ifdef _SEC_
			CheckALL();
#endif
			patched++;
		}
		e++;
		count++;
		switch (count % 10)
		{
		case 3:
		{
#ifdef _SEC_
			iCIsDebuggerPresent();
#endif
			break;
		}
		case 0:
		{
#ifdef _SEC_
			iInterrupt41();
#endif
			break;
		}
		case 2:
		{
#ifdef _SEC_
			iPushSS();
#endif
			break;
		}
		case 1:
		{
#ifdef _SEC_
			iCheckHWBP();
#endif
			break;
		}
		}
		Sleep(100);
	}
}

void cPackets::Reset()
{
	//strcpy(filter_name, "filter.txt");
	memset(amount_list, -1, sizeof(amount_list));
	memset(drop_list, 0, sizeof(drop_list));
	memset(opcodes, 0, sizeof(opcodes));
	memset(dump, 0, sizeof(dump));
	memset(mobs, 0, sizeof(mobs));
	memset(bau, 0, sizeof(bau));
	memset(inv, 0, sizeof(inv));
	memset(&hacks, 0, sizeof(hacks));
	memset(drop_efs, 0, sizeof(drop_efs));
	memset(&TimerPacket, 0, sizeof(TimerPacket));
	memset(&Macro, 0, sizeof(Macro));
	memset(&Record, 0, sizeof(Record));
#if !defined(RECORD_MESSAGE)
	Macro.ptr = Macro.buffer;
	Record.ptr = Record.buffer;
#endif
	Macro.on = 0;
	Record.on = 0;
	hacks.commands.c = -1;
	hacks.commands.n = 0;
	hacks.commands.i = 0;
	hacks.commands.loop = 0;
	hacks.commands.repeat = -1;
	proxy_ip[0] = 0;
	memset(apikey, 0, sizeof(int32_t) * 256);
	hacks.skill.id = -1;
	myID = -1;
	planos = free_;
	novato = 0;
	online = 0;
}

cPackets::cPackets()
{
	Reset();
	patched = -1;
	f_mainlog = 0;
}

void cPackets::Start()
{
	DWORD oldProtect;
	DWORD oldProtect2;
	time_t _t = time(0);
	struct tm *t = localtime(&_t);
	char tmp[128];
	CreateDirectory("logs", 0);
	sprintf(tmp, "logs/%02d_%02d_%02d_%02d_%02d_%02d.txt",
		t->tm_mday, t->tm_mon + 1, t->tm_year + 1900, t->tm_hour, t->tm_min, t->tm_sec);
	f_mainlog = fopen(tmp, "w");
	memset(&pBullshitDon, 0, sizeof(pBullshitDon));
	//mprotect((void*)addr_base, size_code, 0x1 | 0x2 | 0x4);
	loop = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)ThreadHandle, 0, 0, 0);
	timers_id[WM_MACRO] = SetTimer(0, 0, 100, TimerProc);
	timers_id[WM_PACKETS] = SetTimer(0, 0, 99, TimerProc);
	timers_id[WM_CHECKS] = SetTimer(0, 0, 1000, TimerProc);
#ifdef AON
	FILE *f = fopen("./UI/acc.bin", "wb");
	if (f)
	{
		int16_t x = 0x96;
		fwrite(&x, sizeof(x), 1, f);
		fclose(f);
	}
#endif
#ifdef _SEC_
	CheckALL();
#endif
#ifdef DON
	int32_t addr_DEBUG_gerCheck = 0;
	int32_t addr_CheckSum = 0;
	int32_t dllSize = 0;
	int32_t exeSize = 0;
	int32_t virtualAddr = 0;
	int32_t caddr = 0x400000;
	memcpy(send_don, (void*)(CalcAddress(0x54B02D - 0x400000)), sizeof(send_don));
	memcpy(send_don_dir, (void*)(CalcAddress(0x428E0C - 0x400000)), sizeof(send_don_dir));
	VirtualProtect((void*)CalcAddress(0x5CC459 - 0x400000), 32, 0x40, &oldProtect);
	strcpy((char*)CalcAddress(0x5CC459 - 0x400000), "DoNPatch.dll\0\0");
	VirtualProtect((void*)CalcAddress(0x5CC459 - 0x400000), 32, oldProtect, &oldProtect2);
	donPatch = LoadLibraryA("DoNPatch.dll");
	clientPatch = LoadLibraryA("ClientPatch.dll");
	if (donPatch == 0 || clientPatch == 0)
	{
		Show(DEFAULT_COLOR, "Falha ao carregar as dlls: d:%p c:%p", donPatch, clientPatch);
		exit(1);
	}

#ifdef _SEC_
	CheckHWBP(42);
#endif
	for (int32_t i = 0; i < 4090; i++)
	{
		if (memcmp(".text", (const char*)(i + caddr), 5) == 0)
		{
			exeSize = *(int32_t*)(i + caddr + 16);
			break;
		}
		else if (memcmp("CODE", (const char*)(i + caddr), 5) == 0)
		{
			exeSize = *(int32_t*)(i + caddr + 16);
			break;
		}
	}

	uint8_t *exe_dump = (uint8_t*)malloc(exeSize);
	memcpy(exe_dump, (void*)caddr, exeSize);
	caddr = (int32_t)clientPatch;
	for (int32_t i = 0; i < 4090; i++)
	{
		if (memcmp(".text", (const char*)(i + caddr), 5) == 0)
		{
			dllSize = *(int32_t*)(i + caddr + 16);
			virtualAddr = *(int32_t*)(i + caddr + 12);
			break;
		}
		else if (memcmp("CODE", (const char*)(i + caddr), 5) == 0)
		{
			dllSize = *(int32_t*)(i + caddr + 16);
			virtualAddr = *(int32_t*)(i + caddr + 12);
			break;
		}
	}
	DWORD OldProtect, OldProtect2;
	int32_t patcheds = 0;
	for (int32_t i = 0; dllSize - 32; i++)
	{
		static uint8_t debuggercheck[] = { 0x85, 0xC0, 0x74, 0x09, 0xB8, 0x04, 0x00, 0x00, 0x00, 0x8B, 0xE5, 0x5D, 0xC3, 0x64, 0xA1, 0x30, 0x00, 0x00, 0x00, 0x85, 0xC0 };
		static uint8_t checksum[] = { 0x74, 0x04, 0x32, 0xC0, 0xEB, 0x2B, 0x33, 0xC9, 0xE8 };
		static uint8_t checksum2[] = { 0x7C, 0xDB, 0x8D, 0x04, 0x3E, 0x03, 0xC3, 0x03, 0xD0 };
		if (memcmp((void*)(i + caddr + virtualAddr), debuggercheck, sizeof(debuggercheck)) == 0)
		{
			addr_DEBUG_gerCheck = i + caddr + virtualAddr - 12;
			patcheds++;
		}
		if (memcmp((void*)(i + caddr + virtualAddr), checksum, sizeof(checksum)) == 0)
		{

			//addr_CheckSum = i + caddr + virtualAddr;
			VirtualProtect((void*)(i + caddr + virtualAddr), sizeof(checksum), 0x40, &OldProtect);
			*(uint8_t*)(i + caddr + virtualAddr) = 0xEB;
			VirtualProtect((void*)(i + caddr + virtualAddr), sizeof(checksum), OldProtect, &OldProtect2);
			patcheds++;
		}
		if (memcmp((void*)(i + caddr + virtualAddr), checksum2, sizeof(checksum2)) == 0)
		{
			//addr_CheckSum = i + caddr + virtualAddr;
			VirtualProtect((void*)(i + caddr + virtualAddr + 15), 2, 0x40, &OldProtect);
			*(uint16_t*)(i + caddr + virtualAddr + 15) = 0x9090;
			VirtualProtect((void*)(i + caddr + virtualAddr + 15), 2, OldProtect, &OldProtect2);
			patcheds++;
		}
		if (patcheds == 3)
		{
			break;
		}
	}
	DebuggerCheckRetn = ((int32_t(*)(void))(addr_DEBUG_gerCheck))();

	//CheckSumRetn = ((int32_t(*)(void))(addr_CheckSum))();

	write_jmp(addr_DEBUG_gerCheck, (int32_t)DebuggerCheck, 1, 1);
	//write_jmp(addr_CheckSum, (int32_t)CheckSum, 1, 1);
	VirtualProtect((void*)addr_base, size_code, 0x40, &oldProtect);
	write_jmp(0x005CC47C, 0x005BB744, 1, 1);
#ifdef _LOAD_INI_
	ReadIni();
#endif
	Hooks();
	addr_sendd = (int32_t)send_don;
	addr_send_dirr = (int32_t)&send_don_dir[addr_send_dir - CalcAddress(0x428E0C - 0x400000)];
	VirtualProtect((void*)addr_base, size_code, oldProtect, &oldProtect2);
	patched = 1;
#else // DON
#ifdef _SEC_
	iCheckHWBP();
#endif
	VirtualProtect((void*)addr_base, size_code, 0x40, &oldProtect);
#if defined(WYD2)
	strcpy((char*)CalcAddress(0x5CC459 - 0x400000), "GameProtect.dll\0\0");
#endif
#if defined(MYD)
#if defined(WUOW)
	*(uint8_t*)(0x005F3814) = 'U';
#else
	*(uint8_t*)(0x005F37C0) = 'M';
#endif
#endif
	Hooks();
#ifdef _LOAD_INI_
	ReadIni();
#endif
	VirtualProtect((void*)addr_base, size_code, oldProtect, &oldProtect2);
#if defined(WYD2)
	LoadLibraryA("GameProtect.dll");
#endif
#if defined(MYD)
#if defined(WUOW)
	clientPatch = LoadLibraryA("UoWHackShield.dll");
	//printf("%p\n", clientPatch);
#endif
	LoadLibraryA("Patch MyD.dll");
	patched = 1;
#endif
#endif
#ifdef _SEC_
	iPushSS();
#endif
}

cPackets::~cPackets()
{
#if defined(_DEBUG_)
	if (f_mainlog)
		fclose(f_mainlog);
#endif // _DEBUG_
}
