#pragma once
enum e_cfg
{
	_nil = -1,
	_amount,
	_bau,
	_drop,
	_packet,
	_use,
	_dump,
	_plano,
	_pcdata,
	_mac,
	_proxy,
	_commands,
	_chave,
	_servidor,
	_rede
};

typedef struct
{
	char nick[16];
	sPoint<int16_t> pos;
	uint32_t removeTime;
	int32_t send_atk;
	int32_t recv_atk;
} object;

class cPackets
{
public:
	enum e_planos
	{
		free_ = 0,
		money_,
		test_
	} planos;
	enum e_codigo
	{
		_ms = 0,
		_url
	};
	enum e_server
	{
		_no_server = -1,
		_don,
		_kofd,
		_wyd2,
		_over,
		_myd,
		_brazuc,
		_legacy,
		_uow,
		_aon,
		_gd,
		_final,
		_hero
	} server;
	object mobs[30002];
	char filter_name[24];
	int32_t apikey[256];
	sItem bau[128];
	sItem inv[64];
	sItem equip[16];
	int32_t Gold;
	uint8_t drop_list[9000];
	uint8_t amount_list[9000];
	uint8_t drop_efs[9000][5][3][2];
	//uint8_t bau_list[9000];
	uint8_t opcodes[0xFFFF];
	int32_t dump[0xFFFF][2];
	int32_t myID;
	char proxy_ip[32];
	int32_t proxy_port;
	char servidor[16];
	uint8_t client_mac[16];
	char chave[48];
	char login[18];
	char senha[18];
	char senha2[8];
	char login_s[18];
	char senha_s[18];
	char senha2_s[64];
	int32_t slot_s;
	int32_t slot;
	int32_t sv;
	int32_t ch;
	int32_t novato;
	int32_t proxy_mode;
	int32_t nickChecker[8][16];
	int32_t cnickChecker;
	int32_t online;
	MSG_BULLSHITDON pBullshitDon;
	HANDLE loop;
	struct sTimerPacket
	{
		time_t time;
		uint8_t buffer[1024];
	} TimerPacket[64];
    #if !defined(RECORD_MESSAGE)
	struct sMacro
	{
		int8_t on;
		uint32_t runs;
		int32_t n;
		time_t t[NUM_PACKETS+1];
		uint8_t buffer[NUM_PACKETS * SIZE_BUFFER_RECORD];
		uint8_t *ptr;
	} Macro, Record;
	#else
	struct sMacro
	{
		int8_t on;
		uint32_t runs;
		int32_t n;
		time_t t[NUM_PACKETS+1];
		uint32_t buffer[NUM_PACKETS][3];
		uint32_t *ptr;
	} Macro, Record;
	#endif
	struct s_hacks
	{
		struct
		{
			int32_t id;
		} fake;
		struct
		{
			int32_t on;
		} showid;
		struct
		{
			int32_t on;
			sPoint<int32_t> pos;
		} rec;
		struct
		{
			int32_t on;
		} deny;
		struct
		{
			int32_t id;
		} skill;
		struct
		{
			int32_t on;
		} drop;
		struct
		{
			int32_t on;
		} inv;
		struct
		{
			int32_t id;
		} chat;
		struct
		{
			int32_t on;
			int32_t slot;
			int32_t npc;
			int32_t id;
			uint32_t buy;
		} mont;
		struct
		{
			int32_t on;
			int32_t npcEhre;
			int32_t npcBardes;
		} ehre;
		struct
		{
			int32_t on;
			int32_t npcId;
			int32_t slotId;
			int32_t itemId;
		} barras, barras2;
		struct
		{
			int32_t on;
		} same;
		struct
		{
			int32_t on;
			int32_t npcId;
			int32_t itemId;
			int32_t slot;
			uint32_t pTime;
		} npctrade;
		struct
		{
			int32_t on;
			int32_t npcId;
			int32_t itemId;
			int32_t slot;
			int32_t invslot;
			uint32_t pTime;
		} npcbuy;
		struct
		{
			int32_t on;
			int32_t slot;
			int32_t npc;
			int32_t sanc;
			int32_t eqp;
			int32_t id;
			uint32_t buy;
		} refn;
		struct
		{
			int32_t n;
			int32_t i;
			int32_t stage;
			int32_t c;
			int32_t data[64][3];
		} pick;
		struct
		{
			int32_t on;
			int32_t progress;
			int32_t tipo;
			int32_t buff;
			int32_t mob;
			int32_t skill;
			int32_t stage;
			int32_t re;
			int32_t re_c;
			int32_t re_l;
			char pt[16];
			sPoint<int32_t> pos;
			int32_t hp[2];
			uint32_t macro_data[5];
		} macro;
		struct
		{
			int32_t loop;
			int32_t repeat;
			char cmd[256][128];
			int32_t n;
			int32_t i;
			int32_t c;
		} commands;
	} hacks;
	FILE *f_mainlog;

	int32_t TunnelProxy(int32_t _this, const char *ip, int32_t port, int32_t bindport, int32_t hMsg);
	void GetProxy(int32_t arg = 0);
	void FilterMessage(uint32_t uMsg, uint32_t v, uint32_t _ebp_4);
	void Start();
	void Reset();
	void InsertPacket(time_t t, MSG_HEADER *pak);
	int32_t ReadHttpFile(const char *url, char *buffer, uint32_t len);
	void Show(const char *str, ...);
	void WriteLog(const char *str);
	void Show(uint32_t color, const char *str, ...);
	void Party(int32_t id, char *nick);
	void DeleteItem(int32_t slot, int32_t id);
	void Grifo(int32_t op, int32_t op2 = 2);
	int32_t IsOnline();
	void Revive();
	void SignalParam(int32_t opcode, int32_t param);
	void ShiftItem(int32_t slot, int32_t id, int32_t n);
	void NpcCombine(int32_t npc, int32_t *pos, sItem *its);
	int32_t UseItem(int32_t src_type, int32_t src_pos, int32_t dst_type, int32_t dst_pos, int32_t op);
	void MoveItem(int32_t src_type, int32_t src_pos, int32_t dst_type, int32_t dst_pos);
	int32_t PickItem(int32_t id, int32_t n = 1);
	int32_t FindItem(int32_t id, int32_t i = 0, int32_t type = 1);
	void SellItem(int32_t npc, int32_t src_type, int32_t src_pos);
	void BuyItem(int32_t npc, int32_t src, int32_t dst);
	void Click(int32_t npc);
	void DoEhre();
	void Move(int32_t x, int32_t y, int32_t real);
	void Hooks();
	int32_t Chat(char *str);
	void Dump(uint8_t *packet, int32_t _size, int32_t send, int32_t callback, int32_t doit = 0);
	void FilterPacket(MSG_HEADER *packet, int32_t _size, int32_t send, int32_t callback);
	void Execute(uint32_t addr);
	void Senha2Window(uint32_t open = 0);
	void SelectChar();
	void OpenCargo();
	void ReadPacket(uint8_t *packet, int32_t op);
	void ReadPacket2(uint8_t *packet, int32_t op);
	void SendPacket(uint8_t *packet, int32_t _size, int32_t direct);
	void ReadIni(char *line = 0);
	cPackets();
	~cPackets();
} extern Packets;
