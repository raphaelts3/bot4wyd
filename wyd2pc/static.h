#pragma once

#if defined(_MSC_VER)
static int32_t __cdecl CalcAddress(int32_t addr)
{
	return (int32_t)GetModuleHandleA(0) + addr;
}
#else
extern int32_t __attribute__((__cdecl__)) CalcAddress(int32_t addr);
#endif

#if defined(MYD)

#define MSG_LOGIN MSG_LOGIN_759
#define MSG_CHARINFO MSG_CHARINFO_759
#define MSG_SPAWN MSG_SPAWN_759
#define MSG_CNFMOBKILL MSG_CNFMOBKILL_759
#define MSG_ATTACKONE MSG_ATTACKONE_759
#define MSG_ATTACKMULT MSG_ATTACKMULT_759
#define MSG_UPDATESCORE MSG_UPDATESCORE_759
#define MSG_MOVE MSG_MOVE_757
#define MSG_REQLOGIN MSG_REQLOGIN_757
#define MAX_SLOTS_INV 60
#define MAX_SLOTS_WARE 120
#define MAX_AMOUNT 255

#define addr_window CalcAddress(0x96F20C - 0x400000)
#define size_code CalcAddress(0x1F3000 - 0x400000)
#define addr_base CalcAddress(0x00401000 - 0x400000)
#define addr_send CalcAddress(0x0054DE25 - 0x400000)
#define addr_send_dir CalcAddress(0x004259B0 - 0x400000)
#define addr_send_class CalcAddress(0x2288744 - 0x400000)
#define addr_ptrdat CalcAddress(0x6EEB60 - 0x400000)
#define addr_acc CalcAddress(0x228874C - 0x400000)
#define offset_acc 0xDFC

//internet read
#define addr_gethttp	CalcAddress(0x00542E38 - 0x400000)
#define addr_http_open	CalcAddress(0x005F43B4 - 0x400000)
#define addr_http_url	CalcAddress(0x005F43B0 - 0x400000)
#define addr_http_read	CalcAddress(0x005F43A8 - 0x400000)
#define addr_http_close	CalcAddress(0x005F43A4 - 0x400000)

//client chat
#define addr_client_chat_1 CalcAddress(0x005C6753 - 0x400000)
#define addr_client_chat_2 CalcAddress(0x0040731E - 0x400000)
#define addr_client_chat_3 CalcAddress(0x00408DD6 - 0x400000)

//hook chat
#define addr_hook_chat CalcAddress(0x00466FF4 - 0x400000)
#define addr_hook_chat_bypass CalcAddress(0x00466FF9 - 0x400000)
#define addr_hook_chat_clean CalcAddress(0x004670BD - 0x400000)
#define addr_hook_chat_help CalcAddress(0x006140E8 - 0x400000)
extern uint8_t naked_hook_chat[];// = {0x8B ,0x85 ,0x68 ,0xFF ,0xFF ,0xFF ,0x8B ,0x10 ,0x8B ,0x8D ,0x68 ,0xFF ,0xFF ,0xFF ,0xFF ,0x92 ,0x88 ,0x00 ,0x00 ,0x00 ,0x50 ,0xE8 ,0xF3 ,0xA3 ,0xEA ,0x11 ,0x83 ,0xC4 ,0x04 ,0x83 ,0xF8 ,0x00 ,0x75 ,0x0A ,0x68 ,0x00 ,0x00 ,0x00 ,0x00 ,0xE9 ,0x00 ,0x00 ,0x00 ,0x00 ,0xE9 ,0x00 ,0x00 ,0x00 ,0x00};

//hook send
#define addr_hook_send CalcAddress(0x004259B1 - 0x400000)
#define addr_hook_send_r CalcAddress(0x004259B6 - 0x400000)
extern uint8_t naked_hook_send[];// = {0x68 ,0x01 ,0x00 ,0x00 ,0x00 ,0xFF ,0x75 ,0x0C ,0xFF ,0x75 ,0x08 ,0xE8 ,0x4B ,0xCE ,0xCF ,0xFF ,0x83 ,0xC4 ,0x0C ,0xFF ,0x75 ,0x0C ,0xFF ,0x75 ,0x08 ,0xE9 ,0x2C ,0xB0 ,0xE0 ,0x50};

//hook recv
#define addr_hook_recv CalcAddress(0x0054D5A2 - 0x400000)
#define addr_hook_recv_r CalcAddress(0x0054D5A8 - 0x400000)
extern uint8_t naked_hook_recv[];// = {0x6A ,0x00 ,0x33 ,0xD2 ,0x8B ,0x85 ,0xA0 ,0xFE ,0xFF ,0xFF ,0x66 ,0x8B ,0x10 ,0x52 ,0x50 ,0xE8 ,0xC8 ,0xFC ,0x1F ,0x00 ,0x83 ,0xC4 ,0x0C ,0x8B ,0x95 ,0xA0 ,0xFE ,0xFF ,0xFF ,0xE9 ,0xBA ,0xFC ,0x1F ,0x00};

///hacks
//bau
#define addr_hack_cargo CalcAddress(0x0044C2AF - 0x400000)
extern uint8_t naked_hack_cargo[];// = {0xB8 ,0xDF ,0xFC ,0x4B ,0x00 ,0x8B ,0x10 ,0x6A ,0x01 ,0x8B ,0xCA ,0xE8 ,0xC7 ,0xE7 ,0x0B ,0x00 ,0xC3};

//recv packet
#define addr_hack_recv CalcAddress(0x0051642B - 0x400000)
#define addr_hack_recv2 CalcAddress(0x004B868B - 0x400000)
extern uint8_t naked_hack_recv[];// = {0x68 ,0xDF ,0xFC ,0x4B ,0x00 ,0x68 ,0x6C ,0x03 ,0x00 ,0x00 ,0x8B ,0x15 ,0xDF ,0xFC ,0x4B ,0x00 ,0x83 ,0xC2 ,0x4C ,0x8B ,0x0A ,0xE8 ,0xEC ,0x9C ,0xE5 ,0xFF ,0xC3};

//move
#define addr_hack_move CalcAddress(0x0051642B - 0x400000)
extern uint8_t naked_hack_move[];// = {0x8B ,0x15 ,0xDF ,0xFC ,0x4B ,0x00 ,0x83 ,0xC2 ,0x4C ,0x8B ,0x0A ,0x68 ,0xDF ,0xFC ,0x4B ,0x00 ,0x68 ,0x6C ,0x03 ,0x00 ,0x00 ,0xE8 ,0x1A ,0x77 ,0x00 ,0x00 ,0xC3};
extern MSG_MOVE packet_move;

//winkeys
#define addr_winkey_1 CalcAddress(0x00424009 - 0x400000)
#define addr_winkey_2 CalcAddress(0x00424018 - 0x400000)
#define addr_winkey_3 CalcAddress(0x00424027 - 0x400000)

//limita��o chat
#define addr_lim_chat CalcAddress(0x00466DA6 - 0x400000)

//login failed
#define addr_login_failed CalcAddress(0x0054B670 - 0x400000)

//connect
#define addr_hook_connect CalcAddress(0x00424404 - 0x400000)
#define addr_hook_close CalcAddress(0x0042421E - 0x400000)
#define addr_port_bind CalcAddress(0x00634774 - 0x400000)
#define addr_hwnd CalcAddress(0x2288774 - 0x400000)
#define addr_ip CalcAddress(0x2288740 - 0x400000)
#define addr_port CalcAddress(0x004B342F - 0x400000)
#define addr_sv_ch CalcAddress(0x228874C - 0x400000)
#define addr_svlist CalcAddress(0x11D6DE0 - 0x400000)
#define addr_hook_window CalcAddress(0x0040CF8A - 0x400000)
#define addr_hook_window_ret CalcAddress(0x0040CF92 - 0x400000)
#define addr_lock_ok CalcAddress(0x00638448 - 0x400000)
#define addr_macro_type CalcAddress(0x00638334 - 0x400000)
#define addr_macro_racao CalcAddress(0x00638338 - 0x400000)
#define addr_macro_hp CalcAddress(0x0063833C - 0x400000)
#define addr_notify CalcAddress(0x0054CE5C - 0x400000)

#elif defined(OVER)
#define MSG_LOGIN _MSG_LOGIN_754
#define MSG_CHARINFO MSG_CHARINFO_757
#define MSG_SPAWN MSG_SPAWN_754
#define MSG_CNFMOBKILL MSG_CNFMOBKILL_757
#define MSG_ATTACKONE MSG_ATTACKONE_754
#define MSG_ATTACKMULT MSG_ATTACKMULT_754
#define MSG_UPDATESCORE MSG_UPDATESCORE_757
#define MSG_MOVE MSG_ACTION
#define MSG_REQLOGIN MSG_REQLOGIN_754
#define MAX_SLOTS_INV 60
#define MAX_SLOTS_WARE 120
#define MAX_AMOUNT 120

#define addr_window CalcAddress(0x009362CC - 0x400000)
#define size_code CalcAddress(0x001AC000 - 0x400000)
#define addr_base CalcAddress(0x00401000 - 0x400000)
#define addr_send CalcAddress(0x00569B76 - 0x400000)
#define addr_send_dir CalcAddress(0x0042790E - 0x400000)
#define addr_send_class CalcAddress(0x13C1AA4 - 0x400000)
#define addr_ptrdat CalcAddress(0x00684BB0 - 0x400000)
#define addr_acc CalcAddress(0x13C1AAC - 0x400000)
#define offset_acc 0xC5C

//internet read
#define addr_gethttp	CalcAddress(0x005621F1 - 0x400000)
#define addr_http_open	CalcAddress(0x005AD344 - 0x400000)
#define addr_http_url	CalcAddress(0x005AD340 - 0x400000)
#define addr_http_read	CalcAddress(0x005AD354 - 0x400000)
#define addr_http_close	CalcAddress(0x005AD350 - 0x400000)

//client chat
#define addr_client_chat_1 CalcAddress(0x0059998A - 0x400000)
#define addr_client_chat_2 CalcAddress(0x0040688A - 0x400000)
#define addr_client_chat_3 CalcAddress(0x0040832B - 0x400000)

//hook chat
#define addr_hook_chat CalcAddress(0x0046563C - 0x400000)
#define addr_hook_chat_bypass CalcAddress(0x00465997 - 0x400000)
#define addr_hook_chat_clean CalcAddress(0x00465A5B - 0x400000)
#define addr_hook_chat_help CalcAddress(0x005C1D98 - 0x400000)
extern uint8_t naked_hook_chat[];// = {0x8B ,0x85 ,0x68 ,0xFF ,0xFF ,0xFF ,0x8B ,0x10 ,0x8B ,0x8D ,0x68 ,0xFF ,0xFF ,0xFF ,0xFF ,0x92 ,0x88 ,0x00 ,0x00 ,0x00 ,0x50 ,0xE8 ,0xF3 ,0xA3 ,0xEA ,0x11 ,0x83 ,0xC4 ,0x04 ,0x83 ,0xF8 ,0x00 ,0x75 ,0x0A ,0x68 ,0x00 ,0x00 ,0x00 ,0x00 ,0xE9 ,0x00 ,0x00 ,0x00 ,0x00 ,0xE9 ,0x00 ,0x00 ,0x00 ,0x00};

//hook send
#define addr_hook_send CalcAddress(0x0042790F - 0x400000)
#define addr_hook_send_r CalcAddress(0x00427914 - 0x400000)
extern uint8_t naked_hook_send[];// = {0x68 ,0x01 ,0x00 ,0x00 ,0x00 ,0xFF ,0x75 ,0x0C ,0xFF ,0x75 ,0x08 ,0xE8 ,0x4B ,0xCE ,0xCF ,0xFF ,0x83 ,0xC4 ,0x0C ,0xFF ,0x75 ,0x0C ,0xFF ,0x75 ,0x08 ,0xE9 ,0x2C ,0xB0 ,0xE0 ,0x50};

//hook recv
#define addr_hook_recv CalcAddress(0x005692F1 - 0x400000)
#define addr_hook_recv_r CalcAddress(0x005692F7 - 0x400000)
extern uint8_t naked_hook_recv[];// = {0x6A ,0x00 ,0x33 ,0xD2 ,0x8B ,0x85 ,0xA0 ,0xFE ,0xFF ,0xFF ,0x66 ,0x8B ,0x10 ,0x52 ,0x50 ,0xE8 ,0xC8 ,0xFC ,0x1F ,0x00 ,0x83 ,0xC4 ,0x0C ,0x8B ,0x95 ,0xA0 ,0xFE ,0xFF ,0xFF ,0xE9 ,0xBA ,0xFC ,0x1F ,0x00};

///hacks
//bau
#define addr_hack_cargo CalcAddress(0x0044760C - 0x400000)

//recv packet
#define addr_hack_recv CalcAddress(0x005384DF - 0x400000)
#define addr_hack_recv2 CalcAddress(0x004B2CE0 - 0x400000)

//move
#define addr_hack_move CalcAddress(0x005384DF - 0x400000)
extern MSG_MOVE packet_move;

//winkeys
#define addr_winkey_1 CalcAddress(0x00425FD9 - 0x400000)
#define addr_winkey_2 CalcAddress(0x00425FE8 - 0x400000)
#define addr_winkey_3 CalcAddress(0x00425FF7 - 0x400000)

//limita��o chat
#define addr_lim_chat CalcAddress(0x00465744 - 0x400000)

//login failed
#define addr_login_failed CalcAddress(0x0056A1C0 - 0x400000)

//connect
#define addr_hook_connect CalcAddress(0x00426337 - 0x400000)
#define addr_hook_close CalcAddress(0x0042617A - 0x400000)
#define addr_port_bind CalcAddress(0x5DBFC - 0x400000)
#define addr_hwnd CalcAddress(0x13C1AD4 - 0x400000)
#define addr_ip CalcAddress(0x13C1AA0 - 0x400000)
#define addr_port CalcAddress(0x004ADF51 - 0x400000)
#define addr_sv_ch CalcAddress(0x13C1AAC - 0x400000)
#define addr_svlist CalcAddress(0x0093A0D0 - 0x400000)
#define addr_hook_window CalcAddress(0x0040C40F - 0x400000)
#define addr_hook_window_ret CalcAddress(0x0040C417 - 0x400000)
#define addr_lock_ok CalcAddress(0x005D8144 - 0x400000)
#define addr_macro_type CalcAddress(0x005D805C - 0x400000)
#define addr_macro_racao CalcAddress(0x005D8060 - 0x400000)
#define addr_macro_hp CalcAddress(0x005D8064 - 0x400000)
#define addr_notify CalcAddress(0x00568BDD - 0x400000)
/*
#elif defined(DON)
#define MSG_LOGIN _MSG_LOGIN_757
#define MSG_CHARINFO MSG_CHARINFO_757
#define MSG_SPAWN MSG_SPAWN_757
#define MSG_CNFMOBKILL MSG_CNFMOBKILL_757
#define MSG_ATTACKONE MSG_ATTACKONE_756
#define MSG_ATTACKMULT MSG_ATTACKMULT_756
#define MSG_UPDATESCORE MSG_UPDATESCORE_757
#define MSG_MOVE MSG_MOVE_757
#define MSG_REQLOGIN MSG_REQLOGIN_757
#define MAX_SLOTS_INV 60
#define MAX_SLOTS_WARE 120
#define MAX_AMOUNT 120

#define addr_window CalcAddress(0x96F20C - 0x400000)
#define size_code CalcAddress(0x1CC000 - 0x400000)
#define addr_base CalcAddress(0x00401000 - 0x400000)
#define addr_send CalcAddress(0x0054B02D - 0x400000)
#define addr_send_dir CalcAddress(0x004296EE - 0x400000)
#define addr_send_class CalcAddress(0x13FA9CC - 0x400000)
#define addr_ptrdat CalcAddress(0x6BDAF0 - 0x400000)
#define addr_acc CalcAddress(0x13FA9D4 - 0x400000)
#define offset_acc 0xC5C

//internet read
#define addr_gethttp	CalcAddress(0x0054191D - 0x400000)
#define addr_http_open	CalcAddress(0x005CD358 - 0x400000)
#define addr_http_url	CalcAddress(0x005CD354 - 0x400000)
#define addr_http_read	CalcAddress(0x005CD350 - 0x400000)
#define addr_http_close	CalcAddress(0x005CD34C - 0x400000)

//client chat
#define addr_client_chat_1 CalcAddress(0x005B8113 - 0x400000)
#define addr_client_chat_2 CalcAddress(0x00406AC8 - 0x400000)
#define addr_client_chat_3 CalcAddress(0x00408569 - 0x400000)

//hook chat
#define addr_hook_chat CalcAddress(0x0046EC32 - 0x400000)
#define addr_hook_chat_bypass CalcAddress(0x0046EC37 - 0x400000)
#define addr_hook_chat_clean CalcAddress(0x0046ECFB - 0x400000)
#define addr_hook_chat_help CalcAddress(0x005EC424 - 0x400000)
extern uint8_t naked_hook_chat[];// = {0x8B ,0x85 ,0x68 ,0xFF ,0xFF ,0xFF ,0x8B ,0x10 ,0x8B ,0x8D ,0x68 ,0xFF ,0xFF ,0xFF ,0xFF ,0x92 ,0x88 ,0x00 ,0x00 ,0x00 ,0x50 ,0xE8 ,0xF3 ,0xA3 ,0xEA ,0x11 ,0x83 ,0xC4 ,0x04 ,0x83 ,0xF8 ,0x00 ,0x75 ,0x0A ,0x68 ,0x00 ,0x00 ,0x00 ,0x00 ,0xE9 ,0x00 ,0x00 ,0x00 ,0x00 ,0xE9 ,0x00 ,0x00 ,0x00 ,0x00};

//hook send
#define addr_hook_send CalcAddress(0x004296EF - 0x400000)
#define addr_hook_send_r CalcAddress(0x004296F4 - 0x400000)
extern uint8_t naked_hook_send[];
#define addr_hook_send2 CalcAddress(0x00428E15 - 0x400000)
#define addr_hook_send2_r CalcAddress(0x00428E1C - 0x400000)
extern uint8_t naked_hook_send2[];// = {0x68 ,0x01 ,0x00 ,0x00 ,0x00 ,0xFF ,0x75 ,0x0C ,0xFF ,0x75 ,0x08 ,0xE8 ,0x4B ,0xCE ,0xCF ,0xFF ,0x83 ,0xC4 ,0x0C ,0xFF ,0x75 ,0x0C ,0xFF ,0x75 ,0x08 ,0xE9 ,0x2C ,0xB0 ,0xE0 ,0x50};

//hook recv
#define addr_hook_recv CalcAddress(0x0054A7A8 - 0x400000)
#define addr_hook_recv_r CalcAddress(0x0054A7AE - 0x400000)
extern uint8_t naked_hook_recv[];// = {0x6A ,0x00 ,0x33 ,0xD2 ,0x8B ,0x85 ,0xA0 ,0xFE ,0xFF ,0xFF ,0x66 ,0x8B ,0x10 ,0x52 ,0x50 ,0xE8 ,0xC8 ,0xFC ,0x1F ,0x00 ,0x83 ,0xC4 ,0x0C ,0x8B ,0x95 ,0xA0 ,0xFE ,0xFF ,0xFF ,0xE9 ,0xBA ,0xFC ,0x1F ,0x00};

///hacks
//bau
#define addr_hack_cargo CalcAddress(0x0044FA55 - 0x400000)


//recv packet
#define addr_hack_recv CalcAddress(0x0051836E - 0x400000)
#define addr_hack_recv2 CalcAddress(0x004BE0BA - 0x400000)

//move
#define addr_hack_move CalcAddress(0x0051836E - 0x400000)
extern MSG_MOVE packet_move;

//winkeys
#define addr_winkey_1 CalcAddress(0x00427DB9 - 0x400000)
#define addr_winkey_2 CalcAddress(0x00427DC8 - 0x400000)
#define addr_winkey_3 CalcAddress(0x00427DD7 - 0x400000)

//limita��o chat
#define addr_lim_chat CalcAddress(0x0046E9E4 - 0x400000)

//login failed
#define addr_login_failed CalcAddress(0x0054B670 - 0x400000)

//connect
#define addr_hook_connect CalcAddress(0x004B9315 - 0x400000)
#define addr_hook_close CalcAddress(0x00427F5A - 0x400000)
#define addr_port_bind CalcAddress(0x606E3C - 0x400000)
#define addr_hwnd CalcAddress(0x13FA9FC - 0x400000)
#define addr_ip CalcAddress(0x13FA9C8 - 0x400000)
#define addr_port CalcAddress(0x4B9302 - 0x400000)
#define addr_sv_ch CalcAddress(0x13FA9D4 - 0x400000)
#define addr_svlist CalcAddress(0x972FF8 - 0x400000)
*/
#else
#define MSG_LOGIN _MSG_LOGIN_757
#define MSG_CHARINFO MSG_CHARINFO_757
#define MSG_SPAWN MSG_SPAWN_757
#define MSG_CNFMOBKILL MSG_CNFMOBKILL_757
#define MSG_ATTACKONE MSG_ATTACKONE_756
#define MSG_ATTACKMULT MSG_ATTACKMULT_756
#define MSG_UPDATESCORE MSG_UPDATESCORE_757
#define MSG_MOVE MSG_MOVE_757
#define MSG_REQLOGIN MSG_REQLOGIN_757
#define MAX_SLOTS_INV 60
#define MAX_SLOTS_WARE 120
#define MAX_AMOUNT 120

#define size_code CalcAddress(0x1CC000 - 0x400000)
#define addr_base CalcAddress(0x00401000 - 0x400000)
#define addr_send CalcAddress(0x0054B02D - 0x400000)
#define addr_send_dir CalcAddress(0x004296EE - 0x400000)
#define addr_send_class CalcAddress(0x13FA9CC - 0x400000)
#define addr_ptrdat CalcAddress(0x6BDAF0 - 0x400000)
#define addr_window CalcAddress(0x96F20C - 0x400000)
#define addr_acc CalcAddress(0x13FA9D4 - 0x400000)
#define offset_acc 0xC5C

//internet read
#define addr_gethttp	CalcAddress(0x0054191D - 0x400000)
#define addr_http_open	CalcAddress(0x005CD358 - 0x400000)
#define addr_http_url	CalcAddress(0x005CD354 - 0x400000)
#define addr_http_read	CalcAddress(0x005CD350 - 0x400000)
#define addr_http_close	CalcAddress(0x005CD34C - 0x400000)

//client chat
#define addr_client_chat_1 CalcAddress(0x005B8113 - 0x400000)
#define addr_client_chat_2 CalcAddress(0x00406AC8 - 0x400000)
#define addr_client_chat_3 CalcAddress(0x00408569 - 0x400000)

//hook chat
#define addr_hook_chat CalcAddress(0x0046EC32 - 0x400000)
#define addr_hook_chat_bypass CalcAddress(0x0046EC37 - 0x400000)
#define addr_hook_chat_clean CalcAddress(0x0046ECFB - 0x400000)
#define addr_hook_chat_help CalcAddress(0x005EC424 - 0x400000)
extern uint8_t naked_hook_chat[];// = {0x8B ,0x85 ,0x68 ,0xFF ,0xFF ,0xFF ,0x8B ,0x10 ,0x8B ,0x8D ,0x68 ,0xFF ,0xFF ,0xFF ,0xFF ,0x92 ,0x88 ,0x00 ,0x00 ,0x00 ,0x50 ,0xE8 ,0xF3 ,0xA3 ,0xEA ,0x11 ,0x83 ,0xC4 ,0x04 ,0x83 ,0xF8 ,0x00 ,0x75 ,0x0A ,0x68 ,0x00 ,0x00 ,0x00 ,0x00 ,0xE9 ,0x00 ,0x00 ,0x00 ,0x00 ,0xE9 ,0x00 ,0x00 ,0x00 ,0x00};

//hook send
#define addr_hook_send CalcAddress(0x004296EF - 0x400000)
#define addr_hook_send_r CalcAddress(0x004296F7 - 0x400000)
extern uint8_t naked_hook_send[];// = {0x68 ,0x01 ,0x00 ,0x00 ,0x00 ,0xFF ,0x75 ,0x0C ,0xFF ,0x75 ,0x08 ,0xE8 ,0x4B ,0xCE ,0xCF ,0xFF ,0x83 ,0xC4 ,0x0C ,0xFF ,0x75 ,0x0C ,0xFF ,0x75 ,0x08 ,0xE9 ,0x2C ,0xB0 ,0xE0 ,0x50};

#define addr_hook_send2 CalcAddress(0x00428E15 - 0x400000)
#define addr_hook_send2_r CalcAddress(0x00428E1C - 0x400000)
extern uint8_t naked_hook_send2[];// = {0x68 ,0x01 ,0x00 ,0x00 ,0x00 ,0xFF ,0x75 ,0x0C ,0xFF ,0x75 ,0x08 ,0xE8 ,0x4B ,0xCE ,0xCF ,0xFF ,0x83 ,0xC4 ,0x0C ,0xFF ,0x75 ,0x0C ,0xFF ,0x75 ,0x08 ,0xE9 ,0x2C ,0xB0 ,0xE0 ,0x50};

//hook recv
#define addr_hook_recv CalcAddress(0x0054A7A8 - 0x400000)
#define addr_hook_recv_r CalcAddress(0x0054A7AE - 0x400000)
extern uint8_t naked_hook_recv[];// = {0x6A ,0x00 ,0x33 ,0xD2 ,0x8B ,0x85 ,0xA0 ,0xFE ,0xFF ,0xFF ,0x66 ,0x8B ,0x10 ,0x52 ,0x50 ,0xE8 ,0xC8 ,0xFC ,0x1F ,0x00 ,0x83 ,0xC4 ,0x0C ,0x8B ,0x95 ,0xA0 ,0xFE ,0xFF ,0xFF ,0xE9 ,0xBA ,0xFC ,0x1F ,0x00};

///hacks
//bau
#define addr_hack_cargo CalcAddress(0x0044FA55 - 0x400000)

//recv packet
#define addr_hack_recv CalcAddress(0x0051836E - 0x400000)
#define addr_hack_recv2 CalcAddress(0x004BE0BA - 0x400000)

//move
#define addr_hack_move CalcAddress(0x0051836E - 0x400000)
extern MSG_MOVE packet_move;

//winkeys
#define addr_winkey_1 CalcAddress(0x00427DB9 - 0x400000)
#define addr_winkey_2 CalcAddress(0x00427DC8 - 0x400000)
#define addr_winkey_3 CalcAddress(0x00427DD7 - 0x400000)

//limita��o chat
#define addr_lim_chat CalcAddress(0x0046E9E4 - 0x400000)

//login failed
#define addr_login_failed CalcAddress(0x0054B670 - 0x400000)

//connect
#define addr_hook_connect CalcAddress(0x00428117 - 0x400000)
#define addr_hook_close CalcAddress(0x00427F5A - 0x400000)
#define addr_port_bind CalcAddress(0x606E3C - 0x400000)
#define addr_hwnd CalcAddress(0x13FA9FC - 0x400000)
#define addr_ip CalcAddress(0x13FA9C8 - 0x400000)
#define addr_port CalcAddress(0x4B9302 - 0x400000)
#define addr_sv_ch CalcAddress(0x13FA9D4 - 0x400000)
#define addr_svlist CalcAddress(0x972FF8 - 0x400000)
#define addr_hook_window CalcAddress(0x40C65B - 0x400000)
#define addr_hook_window_ret CalcAddress(0x40C663 - 0x400000)
#define addr_lock_ok CalcAddress(0x60AAF8 - 0x400000)
#define addr_macro_type CalcAddress(0x60A9FC - 0x400000)
#define addr_macro_racao CalcAddress(0x60AA00 - 0x400000)
#define addr_macro_hp CalcAddress(0x60AA04 - 0x400000)
#define addr_notify CalcAddress(0x54A07F - 0x400000)
#endif

void inline write_jmp(uint32_t from, uint32_t to, uint8_t jmp = 1, uint8_t protect = 0)
{
	DWORD p1, p2;
	if(protect)
	{
		VirtualProtect((PVOID)from, 5, 0x40, &p1);
	}
	*(uint8_t*)from = 0xE8 + jmp;
	*(uint32_t*)(from + 1) = (int32_t)to - from - 5;
	if(protect)
	{
		VirtualProtect((PVOID)from, 5, p1, &p2);
	}
}

int32_t inline dist(sPoint<int16_t> *a, sPoint<int16_t> *b)
{
	return (int32_t)ceil(sqrt(pow(a->x - b->x, 2) + pow(a->y - b->y,2)));
}

uint8_t inline is_print(uint8_t c)
{
	if (isprint(c))
		return c;
	return '.';
}

static int32_t SkillData[][4] = {{0,0,13,2},
{1,1,13,0},
{2,3,13,4},
{3,14,1,0},
{4,0,1,2},
{5,0,1,0},
{6,0,1,8},
{7,0,8,4},
{8,0,1,1},
{9,0,0,0},
{10,0,2,1},
{11,13,1,0},
{12,0,2,1},
{13,24,1,5},
{14,0,0,0},
{15,36,1,0},
{16,0,2,3},
{17,0,5,3},
{18,0,2,3},
{19,7,1,3},
{20,0,1,3},
{21,0,1,3},
{22,0,1,3},
{23,1,8,4},
{24,0,1,6},
{25,0,1,6},
{26,0,13,0},
{27,0,1,6},
{28,0,2,5},
{29,0,13,6},
{30,0,5,1},
{31,0,1,5},
{32,0,1,5},
{33,0,1,6},
{34,1,1,6},
{35,0,13,6},
{36,1,1,6},
{37,0,1,0},
{38,0,1,6},
{39,0,13,6},
{40,0,1,6},
{41,2,13,5},
{42,0,1,0},
{43,11,1,6},
{44,9,13,5},
{45,15,1,6},
{46,18,1,0},
{47,32,1,5},
{48,0,1,6},
{49,0,1,6},
{50,0,1,6},
{51,10,13,6},
{52,0,1,4},
{53,25,1,0},
{54,0,1,0},
{55,0,5,3},
{56,0,1,0},
{57,0,1,0},
{58,0,1,0},
{59,0,1,0},
{60,0,1,0},
{61,0,1,0},
{62,0,1,0},
{63,0,1,0},
{64,16,1,0},
{65,0,0,0},
{66,16,1,0},
{67,0,0,0},
{68,16,1,0},
{69,0,0,0},
{70,16,1,0},
{71,16,1,0},
{72,0,1,2},
{73,0,1,0},
{74,0,0,0},
{75,27,1,0},
{76,19,1,0},
{77,21,1,0},
{78,0,0,0},
{79,0,6,6},
{80,0,1,2},
{81,37,1,0},
{82,29,1,0},
{83,0,1,0},
{84,0,1,0},
{85,31,1,0},
{86,0,13,6},
{87,38,1,0},
{88,0,1,3},
{89,26,1,0},
{90,30,1,0},
{91,38,0,0},
{92,36,1,0},
{93,0,1,0},
{94,0,0,0},
{95,28,1,0},
{96,4,13,4},
{97,0,13,13},
{98,0,1,5},
{99,0,1,0},
{100,20,0,0},
{101,0,0,0},
{102,29,1,0},
{103,0,0,0}};

typedef uint32_t(__stdcall *TIMEGETTIME)(void);

extern TIMEGETTIME TimeGetTime;