import os, numpy

root_dir = '.'
    
manu = {}
saida1 = open("mbs.txt", "w")
saida1.write("static char mb[][2][128] = {\n")
saida2 = open("hds.txt", "w")
saida2.write("static char hd[][128] = {\n")
saida3 = open("chars.txt", "w")
saida3.write("static char chars[] = {")
for i in range(26):
    saida3.write("'" + chr(ord('a')+i) + "', ")
for i in range(26):
    saida3.write("'" + chr(ord('A')+i) + "', ")
for i in range(9):
    saida3.write("'" + chr(ord('0')+i) + "', ")
saida3.write("'9'")
saida3.write("};")
saida3.close()


f = open("mb.txt", "r")
for line in f:
    toks = line.split("=")
    manu[toks[0]] = toks[1].replace("\r", "").replace("\n", "")
f.close()

for directory, subdirectories, files in os.walk(root_dir):
    for file in files:
        _dir = os.path.join(directory, file)
        if _dir.find(".html") != -1:
            f = open(_dir, "r")
            text = f.read()
            i = text.find("product/")
            while i != -1:
                j = text.find(">", i+1)
                k = text.find("<", j+1)
                name = text[j+1:k]
                toks = name.split(' ')
                if file[0] == 'm':
                    saida1.write("\t{\"" + manu[toks[0]] + "\",\"" + ' '.join(toks[1:]) + "\"},\n")
                elif name.find("EVO") == -1:
                    saida2.write("\t\"" + name.replace("Toshiba ", "").replace("Western Digital ", "WDC ").replace("Samsung ", "").replace("Seagate ", "").replace("Sandisk ", "").replace("Crucial ", "").replace("Hitachi ", "").replace("Mushkin ", "") + " ATA Device\",\n")
                else:
                    saida2.write("\t\"" + name + " ATA Device\",\n")
                i = text.find("product/", k)
            f.close()

saida1.write("\t{\"ASUSTeK Computer INC\", \"M5A78L-M/USB3\"}\n")
saida1.write("};")
saida1.close()
saida2.write("\t\"WDC WD5000LPVT-08G33T1 ATA Device\"\n")
saida2.write("};")
saida2.close()
