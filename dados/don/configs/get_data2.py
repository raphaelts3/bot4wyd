import os, numpy

rand = numpy.random.randint
root_dir = '.'

max_n = 2200
first = 0
manu = {}
data = []
for i in range(max_n):
    data.append(["", "", "", "", ""])

def get_empty(c):
    p = -1
    for i in range(max_n):
        r = rand(0, max_n)
        if data[r][c] == "":
            p = r
            break
    if p == -1:
        for i in range(max_n):
            if data[i][c] == "":
                p = i
                break
    return p

def get_serial():
    serial = ""
    if rand(0, 2) == 0:
        for i in range(rand(0, 5)):
            serial += chr(ord('A') + rand(0, 26))
    for i in range(16):
        serial += chr(ord('0') + rand(0, 10))
    if rand(0, 2) == 0:
        for i in range(rand(0, 2)):
            serial += chr(ord('A') + rand(0, 26))
    return serial

def get_version():
    version = ""
    for i in range(8):
        r = rand(0, 2)
        if r == 0:
            r = rand(0, 4)
            version = ""
            aux = ["", "", ""]
            if r == 0:
                if rand(0, 2) == 0:
                    aux[0] = 'x'
                else:
                    aux[0] = 'X'
                    
                if rand(0, 2) == 1:
                    aux[1] = '0'
                    
                if rand(0, 2) == 0:
                    aux[2] = 'x'
                else:
                    aux[2] = 'X'
                version = "Rev {0}.{1}{2}".format(aux[0], aux[1], aux[2])
            elif r == 1:
                if rand(0, 2) == 0:
                    aux[0] = 'x'
                else:
                    aux[0] = 'X'
                    
                if rand(0, 2) == 0:
                    aux[1] = 'x'
                else:
                    aux[1] = 'X'
                version = "{0}.{1}".format(aux[0], aux[1])
            elif r == 2:
                aux[0] = chr(ord('0') + rand(1, 4))
                aux[1] = chr(ord('0') + rand(0, 10))
                version = "Rev {0}.{1}".format(aux[0], aux[1])
            elif r == 3:
                aux[0] = chr(ord('0') + rand(1, 4))
                aux[1] = chr(ord('0') + rand(0, 10))
                version = "{0}.{1}".format(aux[0], aux[1])
    return version

def data_to_array(p, i):
    vet = [0] * 128
    info = data[p][i]
    for i in range(len(info)):
        vet[i] = ord(info[i])
        
    return bytearray(vet)

macs = {}
def create_file(p):
    pos = first + p
    f = open("configs/config_" + str(pos), "wb")
    ok = 0
    vet = [0] * 8
    while ok == 0:
        for i in range(4):
            if rand(0, 2) == 0:
                mac = ""
                tmp_vet = [0] * 8
                for j in range(6):
                    tmp_vet[j] = rand(0, 256)
                    mac += "{:02X}".format(tmp_vet[j])  
                if mac not in macs:
                    macs[mac] = 1
                    vet = tmp_vet
                    ok = 1
                    break
                    
    f.write(bytearray(vet))

    for i in range(5):
        f.write(data_to_array(pos, i))
    f.close()

f = open("mb.txt", "r")
for line in f:
    toks = line.split("=")
    manu[toks[0]] = toks[1].replace("\r", "").replace("\n", "")
f.close()

for directory, subdirectories, files in os.walk(root_dir):
    for file in files:
        _dir = os.path.join(directory, file)
        if _dir.find(".html") != -1:
            f = open(_dir, "r")
            text = f.read()
                        
            i = text.find("product/")
            while i != -1:
                j = text.find(">", i+1)
                k = text.find("<", j+1)
                name = text[j+1:k]
                toks = name.split(' ')
                if file[0] == 'm':
                    p = get_empty(0)
                    data[p][0] = manu[toks[0]]
                    data[p][2] = ' '.join(toks[1:])
                    data[p][1] = get_serial()
                    data[p][3] = get_version()
                else:
                    p = get_empty(4)
                    if name.find("EVO") == -1:
                        data[p][4] = name.replace("Toshiba ", "").replace("Western Digital ", "WDC ").replace("Samsung ", "").replace("Seagate ", "").replace("Sandisk ", "").replace("Crucial ", "").replace("Hitachi ", "").replace("Mushkin ", "") + " ATA Device"
                    else:
                        data[p][4] = name + " ATA Device"
                if (i % 2) == 0:
                    p = rand(0, 256)
                i = text.find("product/", k)
            f.close()

for i in range(max_n):
    create_file(i)
