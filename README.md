					Os Srs. Aluado, Rabicho, Almofadinha e Pontas
			Fornecedores de recursos para feiticeiros malfeitores, têm a honra de apresentar:
							O MAPA DO MAROTO

					Messrs Moony, Wormtail, Padfoot, and Prongs 
				Purveyors of Aids to Magical Mischief-Makers are proud to present:
							THE MARAUDER'S MAP

* Importante notar que pode compilado com Linux ou Windows
* Organização dos arquivos
	* Commands.xlsx
		* Uma lista (quase) total dos comandos disponíveis no bot/hook
	* wyd2pc
		* Hook para WYD até 01/03 funcionava nos servidores:
			* DON (7.57)
			* HERO (7.54)
			* KOFD (7.57)
			* MYD (7.59+)
	* wyd2proxy_vs
		* Bot para WYD até 01/03 funcionava nos servidores:
			* DON (7.57)
			* KOFD (7.57)
			* MYD (7.59+)
			* HERO (7.54)
	- Qualquer servidor que use como base essas versões podem ser adaptados facilmente

Todos os códigos aqui presentes estão sobre licensa MIT, ou seja, não sei porque,
nem onde, nem como, você vai usar, e também não garanto e nem me responsabilizo
por nada. Use como, onde e quando você quiser, por SUA conta e risco.

The MIT License (MIT)

Copyright (c) 2017 Raphael Tomé Santana

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.