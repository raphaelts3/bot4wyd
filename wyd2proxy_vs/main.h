/***

The MIT License (MIT)

Copyright (c) 2017 Raphael Tom� Santana

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
***/

#ifndef MAIN_HPP_INCLUDED
#define MAIN_HPP_INCLUDED

#if !defined(_WIN32)
#pragma GCC diagnostic ignored "-Wconversion"
#endif

#define ENABLE_PROTECT
#define ENABLE_RANGE
#define ENABLE_INV
#define ENABLE_BUFF
#define ENABLE_MOVED
#define ENABLE_PARTY
//#define PROXY
#if defined(_WIN32)
#define DIR_BASE "../dados/"
#define PROXY_BASE "../proxys/"
#define PROXY_FIND PROXY_BASE "*.*"
#if !defined(VERSION)
#define VERSION "BETA"
#endif
#define DON
#else
#define DIR_BASE "/bots/base/"
#define PROXY_BASE "/proxys/"
#define PROXY_FIND PROXY_BASE
#endif

#if !defined(VERSION)
#define VERSION "BETA"
#define DON
#endif

#if defined(_DEBUG)
	#define ENABLE_PROTECT
	#define ENABLE_RANGE
	#define ENABLE_BUFF
	#define ENABLE_MOVED
	#define ENABLE_PARTY
	#if defined(_WIN32)
	#define MAX_LINE_DUMP	12
	#endif
#endif

#if defined(CHECKER)
	#define MAX_COMMANDS 128
	#define MAX_TIMER_PACKETS 2
	#define MAX_BUFFER_TIMER 512
#else
	#define MAX_COMMANDS 2048
	#define MAX_TIMER_PACKETS 64
	#define MAX_BUFFER_TIMER 2048
#endif

#if !defined(MAX_LINE_DUMP)
#define MAX_	LINE_DUMP	12
#endif

#if defined(_WIN32)
	#define _CRT_SECURE_NO_WARNINGS
	#define _WINSOCK_DEPRECATED_NO_WARNINGS
	#define WIN32_LEAN_AND_MEAN				// Exclude rarely-used stuff from Windows headers
	#include <Windows.h>
	#include <direct.h>
	#include <winsock2.h>
	#include <mmsystem.h>
	#include <IPHlpApi.h>
	#include <Wininet.h>
#else
	#include <dirent.h>
	#include <sys/file.h>
	#include <sys/fcntl.h>
	#include <sys/socket.h>    //socket
	#include <arpa/inet.h> //inet_addr
	#include <unistd.h> //close
	#include <errno.h> //errno
	#include <sys/stat.h> //mkdir
	#include <sys/time.h> //timeGetTime
	#define SOCKADDR_IN struct sockaddr_in
	#define SOCKADDR struct sockaddr
	uint32_t inline timeGetTime()
	{
		struct timeval now;
		gettimeofday(&now, NULL);
		return (now.tv_sec * 1000) + (now.tv_usec/1000);
	}
	#define INVALID_SOCKET (uint32_t)-1
#endif

#include <atomic>
//#include <pthread.h>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <cinttypes>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <time.h>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <thread>
#include <chrono>
#include <cstdint>
#include <map>

#if !defined(_WIN32)
//#define close(x) closesocket(x)
typedef uint32_t DWORD;
#endif
//typedef uint32_t uint;
//typedef unsigned char uchar;
//typedef int16_t ushort;

using namespace std;

#define threadTimer		101
#define logTimer		102
#define h(_v) hex << _v << dec
#define strcpy_s(_d, _s) strncpy(_d, _s, sizeof(_d))

#include "itemeffect.h"
#include "data.h"
#include "struct.h"


#undef MAX_SLOTS_INV
#undef MAX_SLOTS_WARE
#undef MAX_AMOUNT
#if defined(MYD)
#define MSG_LOGIN MSG_LOGIN_759
#define MSG_CHARINFO MSG_CHARINFO_759
#define MSG_SPAWN MSG_SPAWN_759
#define MSG_CNFMOBKILL MSG_CNFMOBKILL_759
#if defined(STAR)
#define MSG_ATTACKONE MSG_ATTACKONE_759
#define MSG_ATTACKMULT MSG_ATTACKMULT_759
#else
#define MSG_ATTACKONE MSG_ATTACKONE_759
#define MSG_ATTACKMULT MSG_ATTACKMULT_759
#endif
#define MSG_UPDATESCORE MSG_UPDATESCORE_759
#define MSG_MOVE MSG_MOVE_757
#define MSG_REQLOGIN MSG_REQLOGIN_757
#define MSG_UPDATEETC MSG_UPDATEETC_759
#define MAX_SLOTS_INV 60
#define MAX_SLOTS_WARE 120
#define MAX_AMOUNT 255

#elif defined(OVER)
#define MSG_LOGIN _MSG_LOGIN_754
#define MSG_CHARINFO MSG_CHARINFO_757
#define MSG_SPAWN MSG_SPAWN_754
#define MSG_CNFMOBKILL MSG_CNFMOBKILL_757
#define MSG_ATTACKONE MSG_ATTACKONE_754
#define MSG_ATTACKMULT MSG_ATTACKMULT_754
#define MSG_UPDATESCORE MSG_UPDATESCORE_757
#define MSG_MOVE MSG_ACTION
#define MSG_REQLOGIN MSG_REQLOGIN_754
#define MSG_UPDATEETC MSG_UPDATEETC_757
#define MAX_SLOTS_INV 64
#define MAX_SLOTS_WARE 128
#define MAX_AMOUNT 128

#else
#define MSG_LOGIN _MSG_LOGIN_757
#define MSG_CHARINFO MSG_CHARINFO_757
#define MSG_SPAWN MSG_SPAWN_757
#define MSG_CNFMOBKILL MSG_CNFMOBKILL_757
#define MSG_ATTACKONE MSG_ATTACKONE_756
#define MSG_ATTACKMULT MSG_ATTACKMULT_756
#define MSG_UPDATESCORE MSG_UPDATESCORE_757
#define MSG_MOVE MSG_MOVE_757
#define MSG_REQLOGIN MSG_REQLOGIN_757
#define MSG_UPDATEETC MSG_UPDATEETC_757
#define MAX_SLOTS_INV 60
#define MAX_SLOTS_WARE 120
#define MAX_AMOUNT 120

#endif

#define PLUS_RANGE 2
//#define ENABLE_RANDOM_RANGE
#define TIMEOUT_PROXY 15
#include "enc.h"
#include "client.h"

extern Client client;
extern MD5 md5;

template <typename T>
string itoa ( T Number )
{
	ostringstream ss;
	ss << Number;
	return ss.str();
}

uint8_t inline is_print(uint8_t c)
{
	if(isprint(c))
		return c;
	return '.';
}
#if !defined(_WIN32)
static struct timespec min_ts;
static struct timespec main_ts;
static struct timespec rec_ts;
#endif

#endif // MAIN_HPP_INCLUDED
