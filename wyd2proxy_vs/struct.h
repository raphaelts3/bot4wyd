/***

The MIT License (MIT)

Copyright (c) 2017 Raphael Tom� Santana

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
***/

#ifndef _STRUCT_HPP_
#define _STRUCT_HPP_

#if !defined(MYD)
#define MAX_AFFECT 16
#else
#define MAX_AFFECT 32
#endif

#define MAXSKILLSC 24
#define MAXSKILLSF 32

struct gIndex
{
	union
	{
		int32_t v;
		struct
		{
			int32_t Guild : 12;
			int32_t Channel : 20;
		};
	};

	friend bool operator== (const gIndex &lhs, const  int32_t &rhs)
	{
		return lhs.v == rhs;
	}

	friend bool operator!= (const gIndex &lhs, const  int32_t &rhs)
	{
		return !(lhs.v == rhs);
	}

	friend bool operator== (const gIndex &lhs, const  gIndex &rhs)
	{
		return lhs.v == rhs.v;
	}

	friend bool operator!= (const gIndex &lhs, const  gIndex &rhs)
	{
		return !(lhs == rhs);
	}

	friend bool operator> (const gIndex &lhs, const  int32_t &rhs)
	{
		return lhs.v > rhs;
	}

	friend bool operator< (const gIndex &lhs, const  int32_t &rhs)
	{
		return lhs.v < rhs;
	}

	friend bool operator<= (const gIndex &lhs, const  int32_t &rhs)
	{
		return !(lhs.v > rhs);
	}

	friend bool operator>= (const gIndex &lhs, const  int32_t &rhs)
	{
		return !(lhs.v < rhs);
	}

	friend bool operator! (const gIndex &me)
	{
		return !me.v;
	}

	gIndex(int32_t _v) : v(_v)
	{

	}
};

#define BITSHIFT(pos) ((1 << pos))
#define HAVESKILL(learn, pos) ((learn & ((int64_t) 1 << (pos))))
#define RED 8
#define BLUE 7
#define WHITE 9
#define item_data sItem
#define ITEM sItem
#define EF1 Effect[0].Index
#define EFV1 Effect[0].Value
#define EF2 Effect[1].Index
#define EFV2 Effect[1].Value
#define EF3 Effect[2].Index
#define EFV3 Effect[2].Value

typedef struct
{
	uint8_t aType;//[0]
	uint8_t Master;//[1]
	int16_t aValue;//[2]
	int32_t Time;//[4]
} AFFECT;

typedef struct
{
	uint16_t Time : 8;
	uint16_t Index : 8;
} _Affect;

template <class T>
struct sPoint
{
	T x;
	T y;

	sPoint()
	{
		this->x = 0;
		this->y = 0;
	}

	template <class T2, class T3>
	sPoint(T2 x, T3 y)
	{
		this->x = x;
		this->y = y;
	}

	template <class T4>
	sPoint(const sPoint<T4> &D)
	{
		x = D.x;
		y = D.y;
	}

	template <class T1>
	void operator=(const sPoint<T1> &D)
	{
		x = D.x;
		y = D.y;
	}

	/*
	void operator=(const sPoint<int32_t> &D)
	{
		x = D.x;
		y = D.y;
	}*/

	friend bool operator== (const sPoint<T> &lhs, const sPoint<T> &rhs)
	{
		return (lhs.x == rhs.x && lhs.y == rhs.y);
	}

	friend bool operator!= (const sPoint<T> &lhs, const sPoint<T> &rhs)
	{
		return !(lhs == rhs);
	}
};

#pragma pack(push, 1)

typedef struct
{
	int16_t Level;//[0]
	int16_t Defense;//[2]
	int16_t Attack;//[4]

	union
	{
		uint8_t _dir;
		struct
		{
			uint8_t Merchant : 4;
			uint8_t Direction : 4;
		} Dir;//[6]
	};

	struct
	{
		uint8_t Move : 4;
		uint8_t Attack : 4;
	} Speed;//[7]

	int16_t MaxHP, MaxMP;//[8]
	int16_t CurHP, CurMP;//[12]

	int16_t Str, int32_t;//[16]
	int16_t Dex, Con;//[20]

	uint8_t wMaster, fMaster;//[24]
	uint8_t sMaster, tMaster;//[26]
} SCORE_757;//[28]


typedef struct
{
	int32_t Level;//[0]
	int32_t Defense;//[4]
	int32_t Attack;//[8]

	struct
	{
		uint16_t Merchant : 8;
		uint16_t Direction : 8;
	} Dir;//[12]

	struct
	{//verificar
		uint16_t Move : 8;/* 401429 */
		uint16_t Attack : 8;
	} Speed;//[14]

	int32_t MaxHP, MaxMP;//[16]
	int32_t CurHP, CurMP;//[24]

	int16_t Str, Int;//[32]
	int16_t Dex, Con;//[36]

	union
	{
		uint16_t Master[4];//[40]
		struct
		{
			uint16_t wMaster, fMaster;//[40]
			uint16_t sMaster, tMaster;//[44]
		};
	};

	int32_t GetMoveSpeed();
} SCORE;//[48]

typedef struct//[0x4B4 - base_old + (int32_t)&mobs[0] - 0x1FDF154]
{
	AFFECT Buffs[16];//[0]
	int32_t status;//[128]
	int32_t leader;//[132]
	int32_t unk10[2];//[136]
	sPoint<int32_t> oldPos;//[144]
	uint32_t lastMove;//[152]
	int32_t lastSpeed;//[156]
	sPoint<int32_t> currPos;//[160]
	char unk2[36];//[168]
	int32_t unk__2;//[204]
	char unk__3[4];//[208]
	int32_t quarto_level;//[212]
	int32_t spawnType;//[216]
	sPoint<int32_t> newPos;//[220]
	int32_t possible_x[5];//[228]
	int32_t possible_y[5];//[248]
	int32_t waitGen[5];//[268]
	int32_t unk__6;//[288]
	int32_t nWaitGen;//[292]
	int32_t gener;//[296]
	int16_t unk7;//[300]
	sItem unk17;//[302]
	int16_t party[13];//[310]
	int32_t damageBonus;//[336]
	int32_t evoker_index;//[340]
	int32_t unk15;//[344]
	int32_t show_guild;//[348]
	int32_t	dropBonus;//[352]
	int32_t expBonus;//[356]
	int32_t rangeTotal;//[360]
	int32_t agmo;//[364]
	int32_t Util;//[368]
	int32_t face;//[372]
	int32_t quest_npc_id;//[376]
	char unk8[20];//[380]
	int32_t htbuff_37;//[400]
	int32_t hpBoss;//[404]
	int32_t bonus_hp;//[408] RegenHP
	int32_t bonus_mp;//[412] RegenMP
	int32_t perfuBonus;//[416]
	int32_t absBonus;//[420]
	char unk5[4];//[424]
	int32_t idTemp;//[428]
	char unk__12[8];//[432]
	int32_t moveSpeed;//[440]
	uint32_t timeChangeMS;//[444]
	int32_t inrune;//[448]
	int32_t towerIndex;//[452]
	char tab[26];//[456] [1660]
	char unk6[22];//[482]
	int32_t buffsprop;//[504]
	int16_t ValueJoia;//[508]
	char unk__8[10];//[510]
} new_temp_757;//[520]//[0x4B4 - base_old + (int32_t)&mobs[0] - 0x1FDF154]

typedef struct//[0x4B4 - base_old + (int32_t)&mobs[0] - 0x1FDF154]
{
	AFFECT Buffs[32];//[0]+128
	int32_t type;//[128] (status)
	int32_t leader;//[132]
	int32_t Formation;//[136]
	int32_t RouteType;//[140]
	sPoint<int32_t> oldPos;//[144]
	uint32_t lastMove;//[152]
	int32_t lastSpeed;//[156]
	sPoint<int32_t> currPos;//[160]
	sPoint<int32_t> autoPos;//[168]
	char unk2[4];//[176]
	char dirs[24];//[180]
	int32_t timerGen;//[204]
	char unk__3[4];//[208]
	int32_t quarto_level;//[212]
	int32_t spawnType;//[216]
	sPoint<int32_t> newPos;//[220]
	int32_t possible_x[5];//[228]
	int32_t possible_y[5];//[248]
	int32_t waitGen[5];//[268]
	int32_t inKingdomWar;//[288]
	int32_t nWaitGen;//[292]
	int32_t gener;//[296]
	int16_t targetId;//[300]
	int16_t targetsAgro[4];//[302]
	int16_t party[13];//[310]
	int32_t damageBonus;//[336]
	int32_t evoker_index;//[340]
	int32_t unk15;//[344]
	int32_t show_guild;//[348]
	int32_t	dropBonus;//[352]
	int32_t expBonus;//[356]
	int32_t rangeTotal;//[360]
	int32_t agmo;//[364]
	int32_t Util;//[368]
	int32_t face;//[372]
	int32_t quest_npc_id;//[376]
	char unk8[4];//[380]
	int32_t kephra;//[384]401BE5
	int32_t staticGen;//[388]
	int32_t unk18;//[392]
	int32_t unk19;//[396]
	int32_t htbuff_37;//[400]
	int32_t hpBoss;//[404]
	int32_t bonus_hp;//[408] RegenHP
	int32_t bonus_mp;//[412] RegenMP
	int32_t perfuBonus;//[416]
	int32_t absBonus;//[420]
	int32_t petCount;//[424] unk5
	int32_t idTemp;//[428]
	int32_t imId;//[432] //im = itemmaster
	int32_t imCounter;//[436]
	int32_t moveSpeed;//[440]
	int32_t timeChangeMS;//[444]
	int32_t inrune;//[448]
	int32_t citizen_pvm;//[452] ?
	char tab[26];//[456] [1660]
	int32_t unk6;//[482]
	int32_t UnkAgmo;//[484]
	int32_t royalCounter;//[488]
	sPoint<int32_t> royalPos;//[492]
	int32_t unk__14;//[500]
	int32_t buffsprop;//[504]
	int16_t ValueJoia;//[508]
	int16_t unk__15;//[510]
	int32_t petDamageBonus;//[512]
	int32_t unk__17;//[516]
} new_temp;//[520]//[0x4B4 - base_old + (int32_t)&mobs[0] - 0x1FDF154]

typedef struct//base_old + (int32_t)&mobs[0] - 0x1FDEF94
{
	int32_t cpoint;//[0]
	int32_t block_chat;//[4]
	int32_t nt_limit_count;//[8]
	union {//[12]
		int32_t millage;
		struct
		{
			char in_quest;//[12]
			char evo;//[13]
			char reset;//[14]
			char other;//[15]
		};//int32_t in_quest;
	};
	int32_t nt_time_t_ban;//[16]
	int32_t fame;//[20]
	int32_t kefra_ticket_count;//[24]
	int32_t exp_2x_unk;//[28]
	AFFECT Buffs[16];//[32]
} new_mob_757;//[160]

typedef struct//base_old + (int32_t)&mobs[0] - 0x1FDEF94
{
	int32_t cpoint;//[0]
	int32_t block_chat;//[4]
	int32_t nt_limit_count;//[8]
	union {//[12]
		int32_t millage;
		struct
		{
			char in_quest;//[12]
			char evo;//[13]
			char reset;//[14]
			char other;//[15]
		};//int32_t in_quest;
	};
	int32_t nt_time_t_ban;//[16]
	int32_t fame;//[20]
	int32_t kefra_ticket_count;//[24]
	int32_t exp_2x_unk;//[28]
	AFFECT Buffs[32];//[32]
} new_mob;//[288]

typedef struct//1FDF034
{
	//1FDF0E0 lvitem
	char block_chat;					//[000]
	char quest;							//[001]
	char quest_2;						//[002]
	char armor;							//[003]
	char new_citizen;					//[004] ?
	char cele_lock;						//[005]
	int32_t new_timer;						//[006]
	char bless_cfg;						//[010]
	char bless_sephira;					//[011] mistery?
	union
	{
		int32_t other_quests;					//[012]
		struct
		{
			int16_t day_kills, kills;				//[12]
		};
	};
	struct sub							//[016]
	{
		uint32_t learn;				//[016]
		sItem face;						//[020]
		SCORE_757 bStatus;					//[028]
		uint32_t Exp;				//[056]
		uint8_t SkillBar[20];		//[060]
		uint16_t pStatus;			//[080]
		uint16_t pSkill;			//[082]
		uint32_t learn_2;			//[084]
		sItem new_face;					//[088]
		int16_t Fixed_0;					//[096]
		int16_t Fixed_4;					//[098]
		int16_t Fixed_5;					//[100]
		char Fixed_0x52;				//[102]
		char Speed;						//[103]
		uint16_t bpStatus;		//[104]
		uint16_t bpStatus_2;		//[106]
		char unk5[4];					//[108]
		int16_t Str;						//[112]
		int16_t Int;						//[114]
		int16_t Dex;						//[116]
		int16_t Con;						//[118]
		char unk6[4];					//[120]
		int32_t Fixed_0_2;					//[124]
		uint8_t SkillBar_2[16];	//[128]
		int32_t unk7;						//[144]
		uint16_t bonus_1;			//[148]
		uint16_t bonus_2;			//[150]
		char unk4[20];					//[152]
	} Sub;
	int32_t lvitem[4];						//[172]
	char unk5[100];						//[188]
} new_mob_two_757;//[288]

typedef struct//1FDF034
{
	//1FDF0E0 lvitem
	char block_chat;					//[000]
	char quest;							//[001]
	char quest_2;						//[002]
	char armor;							//[003]
	char new_citizen;					//[004] ?
	char cele_lock;						//[005]
	union
	{
		int32_t new_info;					//[006]
		char new_infoC[4];				//[006] 1FDF03D = new_infoC[3] = 1 => HardCore original, nÃ£o utilizado
	};
	char bless_cfg;						//[010]
	char bless_sephira;					//[011] mistery?
	union
	{
		int32_t other_quests;					//[012]
		struct
		{
			int16_t day_kills, kills;				//[12]
		};
	};
	struct sub							//[016]
	{
		int64_t learn;				//[016]
		sItem face;						//[020]
		SCORE bStatus;				//[028]
		int64_t Exp;			//[056]
		uint8_t SkillBar[20];		//[060]
		uint16_t pStatus;			//[080]
		uint16_t pSkill;			//[082]
		int64_t learn_2;			//[084]
		sItem new_face;					//[088]
		int16_t Fixed_0;					//[096]
		int16_t Fixed_4;					//[098]
		int16_t Fixed_5;					//[100]
		char Fixed_0x52;				//[102]
		char Speed;						//[103]
		uint16_t bpStatus;		//[104]
		uint16_t bpStatus_2;		//[106]
		char unk5[4];					//[108]
		int16_t Str;						//[112]
		int16_t Int;						//[114]
		int16_t Dex;						//[116]
		int16_t Con;						//[118]
		char unk6[4];					//[120]
		int32_t Fixed_0_2;					//[124]
		uint8_t SkillBar_2[16];	//[128]
		int32_t unk7;						//[144]
		uint16_t bonus_1;			//[148]
		uint16_t bonus_2;			//[150]
		char unk4[20];					//[152]
	} Sub;
	int32_t lvitem[4];						//[172]
	char unk5[100];						//[188]
} new_mob_two;//[288]

typedef struct//base_old + (int32_t)&mobs[0] - 0x1FDECA0
{
	char Name[16];				//[000]
	char CapeInfo;				//[016] Race
	uint8_t Merchant;		//[017]
	uint16_t GuildIndex;	//[018]
	uint8_t ClassInfo;	//[020]
	uint8_t SkillProp;	//[021]
	uint16_t QuestInfo;	//[022]
	int32_t Gold;					//[024]
	uint32_t Exp;			//[028]
	sPoint<int16_t> Last;			//[032]
	SCORE_757 bStatus;				//[036]
	SCORE_757 Status;				//[064]
	ITEM Equip[16];				//[092]
	ITEM Inventory[64];			//[220]
	uint32_t Learn;			//[732]
	uint16_t pStatus;		//[736]
	uint16_t pMaster;		//[738]
	uint16_t pSkill;		//[740]
	uint8_t Critical;		//[742]
	uint8_t SaveMana;		//[743]
	uint8_t SkillBar[4];	//[744]
	char GuildMemberType;		//[748] 7~8
	uint8_t MagicIncrement;//[749]
	uint8_t RegenHP;		//[750]
	uint8_t RegenMP;		//[751]
	char Resist1, Resist2;		//[752]
	char Resist3, Resist4;		//[754]
} MOBFX_757;//[756] - [1200(4B0)] = 524(20C)

typedef struct//base_old + (int32_t)&mobs[0] - 0x1FDECA0 //750
{
	char Name[16];				//[0000]

	/**
	CapeInfo (Valores):

	4 - ?
	7 - Para capa 543 e 545.
	Capa Azul.
	Capa do level 256 e 221.
	8 - Para capa 544 e 546.
	Capa Vermelha.
	Capa do level 256 e 221.
	**/
	char CapeInfo;				//[016] Race

	uint8_t Merchant;		//[017]

	uint16_t GuildIndex;	//[018]
	uint8_t ClassInfo;	//[020]
	uint8_t SkillProp;	//[021]
	uint16_t QuestInfo;	//[022]

	int32_t Gold;					//[024]
	int32_t unk;					//[028]
	int64_t Exp;		//[032]

	sPoint<int16_t> Last;			//[040]
	SCORE bStatus;			//[044]
	SCORE Status;			//[092]//
	ITEM Equip[16];				//[140]
	ITEM Inventory[64];			//[268]

	int64_t Learn;//[780]
	uint16_t pStatus;//[788]
	uint16_t pMaster;//[790]
	uint16_t pSkill;//[792]
	uint8_t Critical;//[794]
	uint8_t SaveMana;//[795]

	uint8_t SkillBar[4];//[796]
	char GuildMemberType;//[800]  7~8

	uint8_t MagicIncrement;//[801]
	uint8_t RegenHP;//[802]
	uint8_t RegenMP;//[803]

	union
	{
		uint8_t Resist[4];//[804]
		struct
		{
			uint8_t Resist1, Resist2;//[804]
			uint8_t Resist3, Resist4;//[806]
		};
	};
} MOBFX;//[808]

typedef struct
{
	MOBFX_757 mob;
	new_mob_757 newmob;
	new_mob_two_757 newmob2;
	new_temp_757 tempmob;
} MOB_757;

/*
typedef struct
{
	MOBFX mob;
	new_mob newmob;
	new_mob_two newmob2;
	new_temp tempmob;
} MOB;
*/

#pragma pack(pop)

typedef struct
{
	int16_t PosX[4];//[0]
	int16_t PosY[4];//[8]
	char Name[4][16];//[16]

	SCORE_757 Status[4];//[80]
	ITEM Equip[4][16];//[192]

	uint16_t Guild[4];//[704]
	int32_t Gold[4];//[712]
	uint32_t Exp[4];//[728]
} SELCHARLIST_757;//[744]

typedef struct
{
	int16_t PosX[4];//[0]
	int16_t PosY[4];//[8]
	char Name[4][16];//[16]

	SCORE Status[4];//[80]
	ITEM Equip[4][16];//[272]

	uint16_t Guild[4];//[784]
	int32_t Gold[4];//[792]
	int64_t Exp[4];//[808]
} SELCHARLIST;//[840]

struct MSG_HEADER
{
	int16_t Size;//[00]
	uint8_t Key;//[02]
	uint8_t Hash;//[03]
	uint16_t Code;//[04]
	int16_t Index;//[06]
	uint32_t Time;//[08]
	MSG_HEADER(int32_t _Size = 0, int32_t _Code = 0, int32_t _Index = 0, uint32_t _Time = 0) :
		Size(_Size), Key(0), Hash(0), Code(_Code), Index(_Index), Time(_Time)
	{
		if (_Size > 12)
			memset(&((uint8_t*)this)[12], 0, Size - 12);
	}
};

#define _CheckPacket(pak,t) \
if (pak->Header.Size != sizeof(t)) \
{ \
	Log("%s: Erro ao ler o pacote, tamanho invÃ¡lido.", __func__); \
	return false; \
}

struct MSG_LOGIN_759 : MSG_HEADER
{
	char Keys[16];//[00C]
	int32_t unk1;//[01C]
	SELCHARLIST SelList;//[020]
	ITEM Storage[128];//[368]

	int32_t Gold;//[768]
	char Name[16];

	int32_t unk2;
	MSG_LOGIN_759(int32_t Index = 0, int32_t Code = 0) : MSG_HEADER(sizeof(MSG_LOGIN_759), Code, Index)
	{

	}
};

typedef struct
{
	MSG_HEADER Header;//[000]
	char Keys[16];//[012]
	SELCHARLIST_757 SelList;//[028]
	ITEM Storage[128];//[300]

	int32_t Gold;//[6F4]
	char Name[16];//[6F8]

	int32_t unk1;//[718]
	int32_t unk2;//[71C]
} _MSG_LOGIN_757;//0x720

typedef struct
{
	MSG_HEADER Header;//[000]
	SELCHARLIST_757 SelList;//[00C]
	ITEM Storage[128];//[300]

	int32_t Gold;//[6F4]
	char Name[12];//[6F8]

	int32_t unk1;//[718]
	char Keys[16];//[012]
} _MSG_LOGIN_754;//0x720

typedef struct
{
	MSG_HEADER Header;//[0]
	char Keys[16];//[12]
	SELCHARLIST_757 SelList;//[28]
	ITEM Storage[128];//[772]

	int32_t Gold;//[1796]
	char Name[16];//[1800]

	int32_t unk1;//[1816]
	int32_t unk2;//[1820]
} MSG_LOGIN_IN_757;//[1824]

typedef struct
{
	MSG_HEADER Header;//[0]
	char Keys[16];//[12]
	SELCHARLIST SelList;//[28]
	ITEM Storage[128];//[868]

	int32_t Gold;//[1892]
	char Name[16];//[1896]

	int32_t unk1;//[1912]
	int32_t unk2;//[1916]
} MSG_LOGIN_IN_759;//[1920]

typedef struct
{
	MSG_HEADER Header;//[0]
	char Keys[16];//[12]
	SELCHARLIST_757 SelList;//[28]
	ITEM Storage[128];//[772]

	int32_t Gold;//[1796]
	char Name[16];//[1800]

	int32_t unk1;//[1816]
	int32_t unk2;//[1820]
} MSG_LOGIN_757;//[1836]

struct MSG_CREATECHAR : MSG_HEADER
{
	SELCHARLIST SelList;
	MSG_CREATECHAR(int32_t Index = 0, int32_t Code = 0) : MSG_HEADER(sizeof(MSG_CREATECHAR), Code, Index)
	{

	}
};

struct MSG_DELETECHAR : MSG_HEADER
{
	SELCHARLIST SelList;
	MSG_DELETECHAR(int32_t Index = 0, int32_t Code = 0) : MSG_HEADER(sizeof(MSG_DELETECHAR), Code, Index)
	{

	}
};

struct MSG_CHARINFO_759 : MSG_HEADER
{
	sPoint<int16_t> Point;//[00C]
	MOBFX Character;//[010]
	char unk[216];//[338]
	int16_t slotid;//[410]
	int16_t clientID;//[412]
	uint16_t weather;//[414]
	uint8_t skill_bar[16];//[416]
	uint16_t hash;//[426]
	new_mob newMob;//[428]
	new_mob_two newMob2;//[]
	MSG_CHARINFO_759(int32_t Index = 0, int32_t Code = 0) : MSG_HEADER(sizeof(MSG_CHARINFO_759), Code, Index)
	{

	}
};

struct MSG_CHARINFO_757 : MSG_HEADER
{
	sPoint<int16_t> Point;//[12]
	MOBFX_757 Character;//[16]
	int16_t slotid;//[772]
	uint16_t clientid;//[774]
	int16_t unk2;//[776]
	char skill_bar[16];//[778]
	int16_t zero;//[794]
	MSG_CHARINFO_757(int32_t Index = 0, int32_t Code = 0) : MSG_HEADER(sizeof(MSG_CHARINFO_757), Code, Index)
	{

	}
};

typedef struct
{
	MSG_HEADER Header;//[0]
	sPoint<int16_t> Point;//[12]
	MOBFX_757 Character;//[16]
	int16_t slotid;//[772]
	uint16_t hash;//[774]
	int16_t unk2;//[776]
	char skill_bar[16];//[778]
	int16_t zero;//[794]
	new_mob_757 newMob;//[796]
	new_mob_two_757 newMob2;//[956]
} MSG_CHARINFO_IN_757;

typedef struct
{
	MSG_HEADER Header;//[0]
	sPoint<int16_t> Point;//[12]
	MOBFX Character;//[16]
	int16_t slotid;//[824]
	uint16_t hash;//[826]
	int16_t unk2;//[828]
	char skill_bar[16];//[830]
	int16_t zero;//[846]
	new_mob newMob;//[848]
	new_mob_two newMob2;//[1136]
} MSG_CHARINFO_IN;

typedef struct
{
	MSG_HEADER Header;
	char Name[16];//12
	char Pwd[12];//28

	int32_t Version;//40
	int32_t unk1;//44

	char Keys[16];//48
	char unk2[52];//64
} MSG_REQLOGIN_OLD;//116

struct MSG_REQLOGIN_757 : MSG_HEADER
{
	char Pwd[12];//[12]
	char Name[16];//[24]
	union
	{
		char Change[52];//40
		struct
		{
			char flag;
			char channel;
			char space;
			char slot;
			char hash[36];
			uint32_t time;
			uint32_t clock;
			uint32_t change_index;
		};
	};

	int32_t Version;//92
	int32_t unk1;//96
	char Keys[16];//100
	MSG_REQLOGIN_757(int32_t Index = 0, int32_t Code = 0x20D) : MSG_HEADER(sizeof(MSG_REQLOGIN_757), Code, Index)
	{

	}
};//116 MSG_REQLOGIN_757

struct MSG_REQLOGIN_754 : MSG_HEADER
{
	char Name[16];//[24]
	char Pwd[12];//[12]
	//char Change[52];//40
	int32_t Version;//40
	int32_t unk1;//44
	char Keys[16];//100

	char flag;
	char channel;
	char space;
	char slot;
	char hash[36];
	uint32_t time;
	uint32_t clock;
	uint32_t change_index;

	MSG_REQLOGIN_754(int32_t Index = 0, int32_t Code = 0x20D) : MSG_HEADER(sizeof(MSG_REQLOGIN_754), Code, Index)
	{

	}
};//116 MSG_REQLOGIN_757

struct MSG_REQLOGIN_760 : MSG_HEADER
{
#if defined(_CLIENT_BR_)
	char Pwd[12];				//[012]
#else
	char Pwd[36];				//[012]
#endif
	char Name[16];				//[048]
	//char Change[52];//40

	char flag;					//[064]
	char channel;               //[065]
	char space;					//[066]
	char slot;					//[067]
	char hash[36];				//[068]
	uint32_t time;			//[104]
	uint32_t clock;			//[108]
	uint32_t change_index;	//[112]

	int32_t Version;				//[116]
	int32_t unk1;					//[120]
	char Keys[16];				//[124]
	MSG_REQLOGIN_760(int32_t Index = 0, int32_t Code = 0) : MSG_HEADER(sizeof(MSG_REQLOGIN_760), Code, Index)
	{

	}
};//[140]

struct MSG_REQLOGOUT : MSG_HEADER
{
	MSG_REQLOGOUT(int32_t Index = 0, int32_t Code = 0) : MSG_HEADER(sizeof(MSG_REQLOGOUT), Code, Index)
	{

	}
};

/*
typedef struct
{
MSG_HEADER Header;
int32_t SlotIndex;
char Name[16];
int32_t ClassIndex;
} MSG_REQCREATECHAR;
*/

struct MSG_REQCREATECHAR : MSG_HEADER
{
	int32_t SlotIndex;//[12]
	char Name[16];//[16]
	int32_t ClassIndex;//[32]
	int32_t Arch;//[36]
	int32_t Face;//[40]
	MSG_REQCREATECHAR(int32_t Index = 0, int32_t Code = 0x20F) : MSG_HEADER(sizeof(MSG_REQCREATECHAR), Code, Index)
	{

	}
};//[44]

struct MSG_REQDELETECHAR : MSG_HEADER
{
	int32_t SlotIndex;
	char Name[16];
	char Pwd[12];
	MSG_REQDELETECHAR(int32_t Index = 0, int32_t Code = 0) : MSG_HEADER(sizeof(MSG_REQDELETECHAR), Code, Index)
	{

	}
};

struct MSG_REQCHARINFO : MSG_HEADER
{
	int32_t slot;
	char unk[20];
	MSG_REQCHARINFO(int32_t Index = 0, int32_t Code = 0) : MSG_HEADER(sizeof(MSG_REQCHARINFO), Code, Index)
	{

	}
};

typedef struct
{
	MSG_HEADER Header;//[0]
	int32_t slot;//[12]
	MOBFX Character;//[16]
	ITEM Storage[128];//[772]
	int32_t Gold;//[1796]

	char skillbar[16];//[1800]
	char username[16];//[1816]
	int32_t unk;//[1832]
	new_mob newMob;//[1836]
	new_mob_two newMob2;//[1996]
} REQSAVECHAR_IN;

typedef struct
{
	MSG_HEADER Header;//[0]
	int32_t slot;//[12]
	MOBFX Character;//[16]
	ITEM Storage[128];//[772]
	int32_t Gold;//[1796]

	char skillbar[16];//[1800]
	char username[16];//[1816]
	new_mob newMob;//[1832]
	new_mob_two newMob2;//[1992]
} REQSAVECHARANDQUIT_IN;

typedef struct
{
	MSG_HEADER Header;//[0]
	int32_t slot;//[12]
	MOBFX Character;//[16]
	ITEM Storage[128];//[772]
	int32_t Gold;//[1796]
	char skillbar[8];//[1800]
	char username[16];//[1808]
	int32_t unk;//[1824]
} REQSAVECHAR;//[1828]

typedef struct
{
	MSG_HEADER Header;//[0]
	int32_t slot;//[12]
	MOBFX Character;//[16]
	ITEM Storage[128];//[772]
	int32_t Gold;//[1796]

	char skillbar[8];//[1800]
	char username[16];//[1808]
} REQSAVECHARANDQUIT;//[1824]

typedef struct
{
	MSG_HEADER Header;
	int32_t dat1;
} pCL_P1h;

struct MSG_PARTYINFO : MSG_HEADER
{
	uint8_t ClassInfo;//[12]
	uint8_t Pos;//[13]
	int32_t Level;//[14] int16_t
	int32_t MaxHP;//[18] int16_t
	int32_t CurHP;//[22] int16_t
	int16_t pIndex;//[26]
	char Nick[16];//[28]
	MSG_PARTYINFO(int32_t Index, int32_t Code = 0x037D) : MSG_HEADER(sizeof(MSG_PARTYINFO), Code, Index)
	{

	}
};

struct MSG_SIGNALPARM : MSG_HEADER//0x004015FF
{
	union
	{
		int32_t signal;//[12]
		int16_t s_signal[2];
		char c_signal[4];
	};
	MSG_SIGNALPARM(int32_t Index = 0, int32_t Code = 0, int32_t Signal = 0) : MSG_HEADER(sizeof(MSG_SIGNALPARM), Code, Index), signal(Signal)
	{

	}
};

struct MSG_EFFECTSTATICDAMAGE : MSG_HEADER
{
	sPoint<int16_t> pos[2];
	int16_t parms[2];
	MSG_EFFECTSTATICDAMAGE(int32_t Index = 0, int32_t Code = 0x3A2) : MSG_HEADER(sizeof(MSG_EFFECTSTATICDAMAGE), Code, Index)
	{

	}
};

struct MSG_SIGNALPARM2 : MSG_HEADER
{
	int32_t signal;//[12]
	int32_t signal2;//[16]
	MSG_SIGNALPARM2(int32_t Index = 0, int32_t Code = 0, int32_t Signal = 0, int32_t Signal2 = 0) : MSG_HEADER(sizeof(MSG_SIGNALPARM2), Code, Index), signal(Signal), signal2(Signal2)
	{

	}
};

struct MSG_ATTITEMPRICE : MSG_HEADER
{
	int32_t signal;//[12]
	int32_t signal2;//[16]
	MSG_ATTITEMPRICE(int32_t Index = 0, int32_t Code = 0x1C2) : MSG_HEADER(sizeof(MSG_SIGNALPARM2), Code, Index)
	{
		/*
		signal = tmControl.GameOption.itemprice[0];
		signal2 = tmControl.GameOption.itemprice[1] << 3;
		*/
	}
};

// Request Client Message
struct MSG_CLIENTMSG : MSG_HEADER
{
	char strMessage[96];
	MSG_CLIENTMSG(int32_t Index = 0, int32_t Code = 0x101) : MSG_HEADER(sizeof(MSG_CLIENTMSG), Code, Index)
	{

	}
};

// Request Remove Item
struct MSG_REMOVEITEM : MSG_HEADER
{
	int16_t initID;//[12]
	int16_t NotUsed1;//[14]
	MSG_REMOVEITEM(int32_t Index = 0, int32_t Code = 0x16F) : MSG_HEADER(sizeof(MSG_REMOVEITEM), Code, Index)
	{

	}
};

// Request Create Item
struct MSG_CREATEITEM : MSG_HEADER
{
	int16_t invType;
	int16_t invSlot;
	ITEM itemData;
	MSG_CREATEITEM(int32_t Index = 0, int32_t Code = 0x182) : MSG_HEADER(sizeof(MSG_CREATEITEM), Code, Index)
	{

	}
};

// Request Refresh Inventory
struct MSG_ATTINV : MSG_HEADER
{
	ITEM inv[64];
	int32_t gold;
	MSG_ATTINV(int32_t Index = 0, int32_t Code = 0x185) : MSG_HEADER(sizeof(MSG_ATTINV), Code, Index)
	{

	}
};

// Request Pick Item
struct MSG_PICKITEM : MSG_HEADER
{
	int32_t invType;
	int32_t InvSlot;
	int16_t initID;
	int16_t posX;
	int16_t posY;
	int16_t Unknown1;
	MSG_PICKITEM(int32_t Index = 0, int32_t Code = 0x270) : MSG_HEADER(sizeof(MSG_PICKITEM), Code, Index)
	{

	}
};

// Request Drop Item
struct MSG_DROPITEM : MSG_HEADER
{
	int32_t invType;
	int32_t InvSlot;
	int32_t Unknown1;
	int16_t posX;
	int16_t posY;
	int32_t Unknown2;
	MSG_DROPITEM(int32_t Index = 0, int32_t Code = 0x272) : MSG_HEADER(sizeof(MSG_DROPITEM), Code, Index)
	{

	}
};

// Request Add Points
struct MSG_ADDPOINTS : MSG_HEADER
{
	int16_t Mode;
	int16_t Info;
	int32_t unk;
	MSG_ADDPOINTS(int32_t Index = 0, int32_t Code = 0x277) : MSG_HEADER(sizeof(MSG_ADDPOINTS), Code, Index)
	{

	}
};

struct MSG_REQCLICKNPC : MSG_HEADER
{
	int32_t npcID;//[0C]
	int32_t option;//[10]
	MSG_REQCLICKNPC(int32_t Index = 0, int32_t Code = p_RequestNpcClick) : MSG_HEADER(sizeof(MSG_REQCLICKNPC), Code)
	{

	}
};

// Request Sell Items
struct MSG_REQNPCITEMS : MSG_HEADER
{
	int16_t npcID;
	int16_t Unknown;
	MSG_REQNPCITEMS(int32_t Index = 0, int32_t Code = p_RequestNpcItems) : MSG_HEADER(sizeof(MSG_REQNPCITEMS), Code, Index)
	{

	}
};

struct MSG_SENDNPCITEMS : MSG_HEADER
{
	int32_t merch;
	sItem Items[27];
	int32_t imposto;
	MSG_SENDNPCITEMS(int32_t Index = 0, int32_t Code = p_SendNpcItems) : MSG_HEADER(sizeof(MSG_SENDNPCITEMS), Code, Index)
	{

	}
};

// Request Change City ID
struct MSG_CHANGECITY : MSG_HEADER
{
	int32_t cityID;
	MSG_CHANGECITY(int32_t Index = 0, int32_t Code = 0x291) : MSG_HEADER(sizeof(MSG_CHANGECITY), Code, Index)
	{

	}
};

// Request NPC Combination
// 2D3, 2D2
struct MSG_NPCCOMB : MSG_HEADER
{
	ITEM item[8];
	uint8_t slot[8];
	MSG_NPCCOMB(int32_t Index = 0, int32_t Code = 0x2D2) : MSG_HEADER(sizeof(MSG_NPCCOMB), Code, Index)
	{

	}
};

// Request Spawn Say
struct MSG_SAY : MSG_HEADER
{
	char message[96];
	MSG_SAY(int32_t Index = 0, int32_t Code = 0x333) : MSG_HEADER(sizeof(MSG_SAY), Code, Index)
	{

	}
};

// Request Spawn Say Color
struct MSG_SAYCOLOR : MSG_HEADER
{
	char spawnMessage[88];
	uint32_t color;
	int32_t unk;
	MSG_SAYCOLOR(int32_t Index = 0, int32_t _color = 0xFF00CD00, int32_t Code = 0xD1D) : MSG_HEADER(sizeof(MSG_SAYCOLOR), Code, Index)
	{
		color = _color;
	}
};

struct MSG_COMMAND : MSG_HEADER
{
	char cmd[16];
	char val[96];
	int32_t gm;
	MSG_COMMAND(int32_t Index = 0, int32_t Code = 0x334) : MSG_HEADER(sizeof(MSG_COMMAND), Code, Index)
	{

	}
};

struct MSG_CREATETRADE : MSG_HEADER
{
	char name[24];//[12]
	sItem item[12];//[36]
	char slot[12];//[132]
	int32_t price[12];//[144]
	uint16_t tax;//[192]
	uint16_t idx;//[194]
	MSG_CREATETRADE(int32_t Index = 0, int32_t Code = 0x397) : MSG_HEADER(sizeof(MSG_CREATETRADE), Code, Index)
	{

	}
};

struct MSG_BULLSHITDON : MSG_HEADER
{
	uint8_t mac[8];
	char data[5][128];
	MSG_BULLSHITDON(int32_t Index = 0, int32_t Code = 0xF200) : MSG_HEADER(sizeof(MSG_COMMAND), Code, Index)
	{

	}
};

struct MSG_UPDATESCORE_757 : MSG_HEADER
{
	SCORE_757	Score;//[12]
	uint8_t	Critical;//[40]
	uint8_t	SaveMana;//[41]
	_Affect Affect[MAX_AFFECT];//[42]
	uint16_t	Guild;//[126]
	uint16_t	GuildLevel;//[128]
	char	Resist[4];//[130]
	uint16_t	RegHp;//[136]
	uint16_t	RegMp;//[140]
	uint8_t	Magic;//[144]
	uint8_t	Oitava;//[145]
	uint32_t	Hold;//[146]
	MSG_UPDATESCORE_757(int32_t Index = 0, int32_t Code = p_UpdateScore) : MSG_HEADER(sizeof(MSG_UPDATESCORE_757), Code, Index)
	{

	}
};//[150]

struct MSG_UPDATESCORE_759 : MSG_HEADER
{
	SCORE	Score;//[12]
	uint8_t	Critical;//[60]
	uint8_t	SaveMana;//[61]
	_Affect Affect[MAX_AFFECT];//[62]
	uint16_t	Guild;//[126]
	uint16_t	GuildLevel;//[128]
	char	Resist[4];//[130]
	int16_t	unk;//[134]
	int32_t		RegHp;//[136]
	int32_t		RegMp;//[140]
	uint8_t	Magic;//[144]
	uint8_t	Oitava;//[145]
	uint32_t	Hold;//[146]
	MSG_UPDATESCORE_759(int32_t Index = 0, int32_t Code = p_UpdateScore) : MSG_HEADER(sizeof(MSG_UPDATESCORE_759), Code, Index)
	{

	}
};//[150]

typedef struct
{
	MSG_HEADER	 Header;//[0]
	SCORE_757			 Score;//[12]
	uint8_t  Critical;//[40]
	uint8_t  SaveMana;//[41]
	_Affect Affect[16];//[42]
	uint16_t Guild;//[74]
	uint16_t GuildLevel;//[76]
	char           Resist[4];//[78]
	uint16_t ReqHp;//[82]
	uint16_t ReqMp;//[84]
	uint8_t Magic;//[86]
	uint8_t Unk;//[87]
	uint32_t Hold;//[88]
} MSG_UpdateScore_757;//[92]

typedef struct
{
	MSG_HEADER	 Header;//[0]
	SCORE			 Score;//[12]
	uint8_t  Critical;//[40]
	uint8_t  SaveMana;//[41]
	_Affect Affect[4];//[42]
	uint16_t Guild;//[50]
	uint16_t GuildLevel;//[52]
	char           Resist[4];//[54]
	uint16_t ReqHp;//[58]
	uint16_t ReqMp;//[60]
	uint8_t Magic;//[62]
	uint8_t Unk;//[63]
	//uint32_t Hold;
} MSG_UpdateScore_656;//[64]

struct MSG_UPDATEETC_757 : MSG_HEADER
{
	uint32_t  	Hold;//12
	uint32_t  	Exp;//16
	uint32_t	LearnedSkill;//20
	int16_t		ScoreBonus;//24
	int16_t		SpecialBonus;//26
	int16_t		SkillBonus;//28
	int16_t		snDummy;//30
	int32_t		Coin;//32
	MSG_UPDATEETC_757(int32_t Index = 0, int32_t Code = 0x337) : MSG_HEADER(sizeof(MSG_UPDATEETC_757), Code, Index)
	{

	}
};

struct MSG_UPDATEETC_759 : MSG_HEADER
{
	uint32_t	Hold;//12
	int64_t		Exp;//16
	int64_t		LearnedSkill;//24
	int16_t		ScoreBonus;//32
	int16_t		SpecialBonus;//34
	int16_t		SkillBonus;//36
	int16_t		snDummy;//38
	int32_t		Coin;//40
	MSG_UPDATEETC_759(int32_t Index = 0, int32_t Code = 0x337) : MSG_HEADER(sizeof(MSG_UPDATEETC_759), Code, Index)
	{

	}
};//48

typedef struct
{
	MSG_HEADER	Header;//12
	//uint32_t  Hold;//16
	uint32_t  Exp;//20
	uint32_t	LearnedSkill;//24
	int16_t			ScoreBonus;//26
	int16_t			SpecialBonus;//28
	int16_t			SkillBonus;//30
	int16_t			snDummy;//32
	int32_t			Coin;//36
} MSG_UpdateEtc_656;

//0x338
struct MSG_CNFMOBKILL_759 : MSG_HEADER
{
	uint32_t		Hold;//16
	uint16_t		KilledMob;//18
	uint16_t		Killer;//20
	int64_t		Exp;//24
	MSG_CNFMOBKILL_759(int32_t Index = 0, int32_t Code = p_CNFMobKill) : MSG_HEADER(sizeof(MSG_CNFMOBKILL_759), Code, Index)
	{

	}
};

struct MSG_CNFMOBKILL_757 : MSG_HEADER
{
	uint32_t	 Hold;//16
	uint16_t KilledMob;//18
	uint16_t Killer;//20
	uint32_t   Exp;//24
	MSG_CNFMOBKILL_757(int32_t Index = 0, int32_t Code = p_CNFMobKill) : MSG_HEADER(sizeof(MSG_CNFMOBKILL_757), Code, Index)
	{

	}
};

typedef struct
{
	MSG_HEADER	 Header;//12
	//uint32_t	 Hold;//16
	uint16_t KilledMob;//18
	uint16_t Killer;//20
	uint32_t   Exp;//24
} MSG_CNFMobKill_656;

typedef struct
{
	MSG_HEADER Header;//[0]

	struct {
		int16_t X, Y;
	} Current;//[12]

	int16_t Index;//[16]
	union
	{
		char MobName[16];//[18]

		struct
		{
			char PlayerName[12];//
			uint8_t ChaosPoints;
			uint8_t CurrentKill;
			int16_t TotalKill;
		};
	};

	struct {
		uint16_t ItemID : 12;
		uint16_t Sanc : 4;
	} ItemEff[16];//[34]

	_Affect Affect[16];//[66]

	uint16_t GuildIndex;//[98]
	SCORE_757 Score;//[100]

	struct sp {
		uint16_t Type : 8;
		uint16_t MemberType : 8;
	} Spawn;//[128]

	char pAnctCode[16];//[130]
	char pTab[26];//[146]
	char Trade[4];//[174]
} pCL_364h_OLD;//[176]

struct MSG_SPAWN_757 : MSG_HEADER
{
	sPoint<int16_t> Current;//[12]

	int16_t Idx;//[16]
	union
	{
		char MobName[16];//[18]

		struct
		{
			char PlayerName[12];//[18]
			uint8_t ChaosPoints;//[30]
			uint8_t CurrentKill; //[31]
			int16_t TotalKill;//[32]
		};
	};

	struct {
		uint16_t ItemID : 12;
		uint16_t Sanc : 4;
	} ItemEff[16];//[34]

	_Affect Affect[MAX_AFFECT];//[66]

	uint16_t GuildIndex;//[98]

	SCORE_757 Score;//[100]

	struct {
		uint16_t Type : 8;
		uint16_t MemberType : 8;
	} Spawn;//[128]

	char pAnctCode[16];//[130]
	char pTab[26];//[146]
	char Trade[4];//[174]
	MSG_SPAWN_757(int32_t Index = 0, int32_t Code = p_Spawn) : MSG_HEADER(sizeof(MSG_SPAWN_757), Code, Index)
	{

	}
};//[176]


struct MSG_SPAWN_754 : MSG_HEADER
{
	sPoint<int16_t> Current;//[12]

	int16_t Idx;//[16]
	union
	{
		char MobName[16];//[18]

		struct
		{
			char PlayerName[12];//[18]
			uint8_t ChaosPoints;//[30]
			uint8_t CurrentKill; //[31]
			int16_t TotalKill;//[32]
		};
	};

	struct {
		uint16_t ItemID : 12;
		uint16_t Sanc : 4;
	} ItemEff[16];//[34]

	_Affect Affect[MAX_AFFECT];//[66]

	uint16_t GuildIndex;//[98]

	SCORE_757 Score;//[100]

	struct {
		uint16_t Type : 8;
		uint16_t MemberType : 8;
	} Spawn;//[128]

	char pAnctCode[16];//[130]
	char pTab[26];//[146]
	char Trade[4];//[174]
	MSG_SPAWN_754(int32_t Index = 0, int32_t Code = p_Spawn) : MSG_HEADER(sizeof(MSG_SPAWN_754), Code, Index)
	{

	}
};//[176]

struct MSG_SPAWN_759 : MSG_HEADER
{
	//MSG_HEADER Header;					//[00]

	sPoint<int16_t>	Current;			//[12]

	int16_t Idx;						//[16]
	union
	{
		char MobName[16];				//[18]

		struct
		{
			char PlayerName[12];//
			uint8_t ChaosPoints;
			uint8_t CurrentKill;
			int16_t TotalKill;
		};
	};

	struct {
		uint16_t ItemID : 12;
		uint16_t Sanc : 4;
	} ItemEff[16];						//[34]

	_Affect Affect[MAX_AFFECT];				//[66]

	uint16_t GuildIndex;			//[130]

	struct {
		uint32_t MemberType : 16;
		uint32_t Unk : 16;
	} GuildInfo;						//[132]

	SCORE Score;						//[136]

	struct {
		uint16_t Type : 8;
		uint16_t MemberType : 8;
	} Spawn;							//[184]

	char pAnctCode[16];					//[186]
	char pTab[26];						//[202]
	char Cidadania;						//[228]
	char Trade[3];						//[229]
	MSG_SPAWN_759(int32_t Index = 0, int32_t Code = p_Spawn, int32_t Size = sizeof(MSG_SPAWN_759)) : MSG_HEADER(Size, Code, Index)
	{

	}
};//[232]

typedef struct
{
	pCL_364h_OLD spawn;
	char trade[26];
} pCL_363h_OLD;

typedef struct
{
	MSG_SPAWN_757 spawn;//[0]
	char trade[26];//[176]
} pCL_363h_757;

struct MSG_SPAWNTRADE : MSG_SPAWN_759
{
	char trade[24];//[232]
	MSG_SPAWNTRADE(int32_t Index = 0, int32_t Code = p_SpawnTrade) : MSG_SPAWN_759(Index, Code, sizeof(MSG_SPAWNTRADE))
	{

	}
};

// Request Action
struct MSG_ACTION : MSG_HEADER
{
	//MSG_HEADER Header;
	int16_t xSrc, ySrc;
	int32_t mSpeed;
	int32_t mType;
	int16_t xDst, yDst;
	char mCommand[24];
	MSG_ACTION(int32_t Index = 0, int32_t Code = 0x366) : MSG_HEADER(sizeof(MSG_ACTION), Code, Index)
	{

	}
};

// Message Quiz
struct MSG_QUIZ : MSG_HEADER
{
	//MSG_HEADER Header;//[0]
	int16_t unk1, unk2;//[12] [14]
	char messages[4][96];//[16]
	MSG_QUIZ(int32_t Index = 0, int32_t Code = 0x7B1) : MSG_HEADER(sizeof(MSG_QUIZ), Code, Index)
	{

	}
};//[52]

// Request Move
struct MSG_MOVE_757 : MSG_HEADER
{
	//MSG_HEADER Header;//[0]
	int16_t xSrc, ySrc;//[12] [14]
	int32_t mType;//[16]
	int32_t mSpeed;//[20]
	char mCommand[24];//[24]
	int16_t xDst, yDst;//[48] [50]
	MSG_MOVE_757(int32_t idx, int32_t to_x, int32_t to_y, int32_t _mType = 2);
	MSG_MOVE_757(int32_t Index = 0, int32_t Code = p_Move1) : MSG_HEADER(sizeof(MSG_MOVE_757), Code, Index)
	{

	}
};//[52]

// Refresh Itens
struct MSG_REFRESHITEMS : MSG_HEADER
{
	union
	{
		uint16_t ID;
		struct
		{
			uint16_t ItemID : 12;
			uint16_t Sanc : 4;
		};
	} ItemEff[16];//[12]

	uint8_t pAnctCode[16];//[44]
	MSG_REFRESHITEMS(int32_t Index = 0, int32_t Code = 0x36B) : MSG_HEADER(sizeof(MSG_REFRESHITEMS), Code, Index)
	{

	}
};

// Request Emotion
struct MSG_EMOTION : MSG_HEADER
{
	int16_t effType;
	int16_t effValue;
	int32_t Unknown1;
	MSG_EMOTION(int32_t Index = 0, int32_t effType = 0, int32_t effValue = 0) : MSG_HEADER(sizeof(MSG_EMOTION), 0x36A, Index)
	{

	}
};

struct MSG_USEITEM : MSG_HEADER
{
	int32_t src_type;//12
	int32_t src_val;//16
	int32_t dst_type;//20
	int32_t dst_val;//24
	int16_t posx;//28
	int16_t posy;//30
	int32_t op;//32
	MSG_USEITEM(int32_t Index = 0, int32_t Code = 0x373) : MSG_HEADER(sizeof(MSG_USEITEM), Code, Index)
	{

	}
};

// Request Move Item
struct MSG_MOVEITEM : MSG_HEADER
{
	uint8_t destType;
	uint8_t destSlot;
	uint8_t srcType;
	uint8_t srcSlot;
	int32_t Unknown;
	MSG_MOVEITEM(int32_t Index = 0, int32_t Code = 0x376) : MSG_HEADER(sizeof(MSG_MOVEITEM), Code, Index)
	{

	}
};

// Create Object On Ground
struct MSG_CREATEOBJECT : MSG_HEADER
{
	sPoint<int16_t> Pos;//[12]
	int16_t InitID;//[16] ID + 10k
	sItem Item;//[18]
	char Rotate;//[26]
	char Stage;//[27]
	char Height;//[28]
	char Unk;//[29]
	int16_t Unk2;//[30]
	MSG_CREATEOBJECT(int32_t Index = 0, int32_t Code = 0x26E) : MSG_HEADER(sizeof(MSG_CREATEOBJECT), Code, Index)
	{

	}
};//[32]

// Request Chance SkillBar
struct MSG_SKILLBAR : MSG_HEADER
{
	char SkillBar1[4];
	char SkillBar2[16];
	MSG_SKILLBAR(int32_t Index = 0, int32_t Code = 0x378) : MSG_HEADER(sizeof(MSG_SKILLBAR), Code, Index)
	{

	}
};

// Request Buy Items
struct MSG_BUYITEMS : MSG_HEADER
{
	int16_t mobID;
	uint16_t sellSlot;
	int16_t invSlot;
	int16_t Unknown1;
	int32_t Unknown2;
	MSG_BUYITEMS(int32_t Index = 0, int32_t Code = 0x379) : MSG_HEADER(sizeof(MSG_BUYITEMS), Code, Index)
	{

	}
};

// Request Sell Items
struct MSG_SELLITEMS : MSG_HEADER
{
	int16_t mobID;//[0C]
	int16_t invType;//[0E]
	int32_t invSlot;//[10]
	MSG_SELLITEMS(int32_t Index = 0, int32_t Code = 0x37A) : MSG_HEADER(sizeof(MSG_SELLITEMS), Code, Index)
	{

	}
};

struct MSG_CHANGE_CHANNEL : MSG_HEADER
{
	char login[14];
	uint16_t crc32;
	char flag;
	char channel;
	char space;
	char slot;
	char hash[36];
	uint32_t time;
	uint32_t clock;
	uint32_t change_index;
	MSG_CHANGE_CHANNEL(int32_t Index = 0, int32_t Code = 0) : MSG_HEADER(sizeof(MSG_CHANGE_CHANNEL), Code, Index)
	{

	}
};

struct MSG_TRADE : MSG_HEADER
{
	sItem item[15];//[12]
	char slot[15];//[132]
	char unk1;//[147]
	int32_t gold;//[148]
	int16_t mode;//[152]
	int16_t otherid;//[154]
	MSG_TRADE(int32_t Index = 0, int32_t Code = 0x383) : MSG_HEADER(sizeof(MSG_TRADE), Code, Index)
	{

	}
};//[156]

typedef struct
{
	MSG_HEADER Header;//[0]
	sItem item[8];//[12]
	char slot[8];//[76]
	int32_t gold;//[84]
	int16_t mode;//[88]
	int16_t otherid;//[90]
} pCL_383h_656;//[92]

// Request Buy Item Trade
struct MSG_BUYTRADE : MSG_HEADER
{
	int32_t Slot;
	int32_t Index;
	int32_t Gold;
	int32_t Unknown;
	ITEM Item;
	MSG_BUYTRADE(int32_t Index = 0, int32_t Code = 0x398) : MSG_HEADER(sizeof(MSG_BUYTRADE), Code, Index)
	{

	}
};

// Request Open Trade
struct MSG_OPENTRADE : MSG_HEADER
{
	int32_t Index;
	MSG_OPENTRADE(int32_t Index = 0, int32_t Code = 0x39A) : MSG_HEADER(sizeof(MSG_OPENTRADE), Code, Index)
	{

	}
};

typedef struct
{
	MSG_HEADER			Header;//[0]
	uint16_t		AttackerID;//[12]
	uint16_t		Progress;//[14]
	sPoint<int16_t> AttackerPOS;//[16]
	sPoint<int16_t> TargetPOS;//[20]
	int16_t 				SkillIndex;//[24] 0-98-106  (-1Ã€ÃŒÂ¸Ã© Â¹Â°Â¸Â®Ã€Ã» Â°Ã¸Â°Ã�)
	int16_t				CurrentMp;//[26]
	uint8_t		Motion;//[28]
	uint8_t		SkillParm;//[29] Â½ÂºÃ…Â³ ÃˆÂ¿Â°Ãº Ã�ÃµÃ†Ã¸Â·Ã¼ Â¶Ã‡Â´Ã‚ Â¹Â°Â¸Â®Â°Ã¸Â°Ã� Ã€ÃŒÃ†Ã¥Ã†Â®. 0, Ã€ÃŒ ÂµÃ°Ã†ÃºÃ†Â®.
	uint8_t		FlagLocal;//[30]
	uint8_t		DoubleCritical;//[31] 0ÂºÃ±Ã†Â® Â´ÃµÂºÃ­ 1ÂºÃ±Ã†Â® Ã…Â©Â¸Â®Ã†Â¼Ã„Â®
	//uint32_t		Hold;			// v701 only
	uint32_t		CurrentExp; //[32]
	int16_t				ReqMp; //[36]
	int16_t				Rsv; //[38]
	uint16_t		TargetID; //[40]
	int16_t				Damage; //[42]
} MSG_AttackOne_656;//44

struct MSG_ATTACKONE_754 : MSG_HEADER
{
	uint16_t		AttackerID;//[12] [24]
	uint16_t		Progress;//[14]
	sPoint<int16_t>			AttackerPOS;//[16]
	sPoint<int16_t>			TargetPOS;//[20]
	int16_t 				SkillIndex;//[24] 0-98-106  (-1Ã€ÃŒÂ¸Ã© Â¹Â°Â¸Â®Ã€Ã» Â°Ã¸Â°Ã�) [30]
	int16_t				CurrentMp;//[26] [28]
	uint8_t		Motion;//[28] [12]
	uint8_t		SkillParm;//[29] Â½ÂºÃ…Â³ ÃˆÂ¿Â°Ãº Ã�ÃµÃ†Ã¸Â·Ã¼ Â¶Ã‡Â´Ã‚ Â¹Â°Â¸Â®Â°Ã¸Â°Ã� Ã€ÃŒÃ†Ã¥Ã†Â®. 0, Ã€ÃŒ ÂµÃ°Ã†ÃºÃ†Â®. [15]
	uint8_t		FlagLocal;//[30] [13]
	uint8_t		DoubleCritical;//[31] 0ÂºÃ±Ã†Â® Â´ÃµÂºÃ­ 1ÂºÃ±Ã†Â® Ã…Â©Â¸Â®Ã†Â¼Ã„Â® [14]
	uint32_t		Hold;			//[32] v701 only [32]
	uint32_t		CurrentExp; //[36] [40]
	int16_t				ReqMp; //[40] [36]
	int16_t				Rsv; //[42] [38]
	uint16_t		TargetID; //[44] [46]
	int16_t				Damage; //[46] [48]
	uint16_t		TargetID2; //[44] [46]
	int16_t				Damage2; //[46] [48]
	MSG_ATTACKONE_754(int32_t Index = 0, int32_t Code = 0x39D) : MSG_HEADER(sizeof(MSG_ATTACKONE_754), Code, Index)
	{

	}
};//50


struct MSG_ATTACKMULT_754 : MSG_HEADER
{
	uint16_t		AttackerID;//[12] [24]
	uint16_t		Progress;//[14]
	sPoint<int16_t>			AttackerPOS;//[16]
	sPoint<int16_t>			TargetPOS;//[20]
	int16_t 				SkillIndex;//[24] 0-98-106  (-1Ã€ÃŒÂ¸Ã© Â¹Â°Â¸Â®Ã€Ã» Â°Ã¸Â°Ã�) [30]
	int16_t				CurrentMp;//[26] [28]
	uint8_t		Motion;//[28] [12]
	uint8_t		SkillParm;//[29] Â½ÂºÃ…Â³ ÃˆÂ¿Â°Ãº Ã�ÃµÃ†Ã¸Â·Ã¼ Â¶Ã‡Â´Ã‚ Â¹Â°Â¸Â®Â°Ã¸Â°Ã� Ã€ÃŒÃ†Ã¥Ã†Â®. 0, Ã€ÃŒ ÂµÃ°Ã†ÃºÃ†Â®. [15]
	uint8_t		FlagLocal;//[30] [13]
	uint8_t		DoubleCritical;//[31] 0ÂºÃ±Ã†Â® Â´ÃµÂºÃ­ 1ÂºÃ±Ã†Â® Ã…Â©Â¸Â®Ã†Â¼Ã„Â® [14]
	uint32_t		Hold;			//[32] v701 only [32]
	uint32_t		CurrentExp; //[36] [40]
	int16_t				ReqMp; //[40] [36]
	int16_t				Rsv; //[42] [38]
	sPoint<int16_t> Target[13];//46
	int16_t unk;//48
	MSG_ATTACKMULT_754(int32_t Index = 0, int32_t Code = 0x36C) : MSG_HEADER(sizeof(MSG_ATTACKMULT_754), Code, Index)
	{

	}
};//50

struct MSG_ATTACKONE_756 : MSG_HEADER
{
	uint8_t Motion;//12
	uint8_t FlagLocal;//13
	uint8_t DoubleCritical;//14
	uint8_t SkillParm;//15
	sPoint<int16_t> AttackerPOS;//16
	sPoint<int16_t> TargetPOS;//20
	uint16_t AttackerID;//24
	uint16_t Progress;//26
	int16_t CurrentMp;//28
	int16_t SkillIndex;//30
	uint32_t Hold;//32
	int16_t ReqMp;//36
	int16_t Rsv;//38
	uint32_t CurrentExp;//40
	uint16_t unk2;//44
	uint16_t TargetID;//46
	int16_t Damage;//48
	uint16_t TargetID2;//46
	int16_t Damage2;//48
	MSG_ATTACKONE_756(int32_t Index = 0, int32_t Code = 0x39D) : MSG_HEADER(sizeof(MSG_ATTACKONE_756), Code, Index)
	{

	}
};//50


struct MSG_ATTACKONE_756_HT : MSG_HEADER
{
	uint8_t Motion;//12
	uint8_t FlagLocal;//13
	uint8_t DoubleCritical;//14
	uint8_t SkillParm;//15
	sPoint<int16_t> AttackerPOS;//16
	sPoint<int16_t> TargetPOS;//20
	uint16_t AttackerID;//24
	uint16_t Progress;//26
	int16_t CurrentMp;//28
	int16_t SkillIndex;//30
	uint32_t Hold;//32
	int16_t ReqMp;//36
	int16_t Rsv;//38
	uint32_t CurrentExp;//40
	uint16_t unk2;//44
	uint16_t TargetID;//46
	int16_t Damage;//48
	uint16_t TargetID2;//46
	int16_t Damage2;//48
	MSG_ATTACKONE_756_HT(int32_t Index = 0, int32_t Code = 0x39D) : MSG_HEADER(sizeof(MSG_ATTACKONE_756_HT), Code, Index)
	{

	}
};//50


struct MSG_ATTACKMULT_756 : MSG_HEADER
{
	uint8_t Motion;//12
	uint8_t FlagLocal;//13
	uint8_t DoubleCritical;//14
	uint8_t SkillParm;//15
	sPoint<int16_t> AttackerPOS;//16
	sPoint<int16_t> TargetPOS;//20
	uint16_t AttackerID;//24
	uint16_t Progress;//26
	int16_t CurrentMp;//28
	int16_t SkillIndex;//30
	uint32_t Hold;//32
	int16_t ReqMp;//36
	int16_t Rsv;//38
	uint32_t CurrentExp;//40
	uint16_t unk2;//44
	sPoint<int16_t> Target[13];//46
	int16_t unk;//48
	MSG_ATTACKMULT_756(int32_t Index = 0, int32_t Code = 0x367) : MSG_HEADER(sizeof(MSG_ATTACKMULT_756), Code, Index)
	{

	}
};//50

typedef struct
{
	MSG_HEADER			Header;//[0]
	//
	uint32_t		Hold;//[12]
	int16_t				ReqMp;//[36] [16]
	int16_t				Rsv;//[38] [18]
	uint32_t		CurrentExp;//[32] [20]
	uint16_t		Progress;//[14] [24] not confirme
	sPoint<int16_t> AttackerPOS;//[16] [26]
	sPoint<int16_t> TargetPOS;//[20] [30]
	uint16_t		AttackerID;//[12] [34]
	uint8_t		unk1;//[31] 0ÂºÃ±Ã†Â® Â´ÃµÂºÃ­ 1ÂºÃ±Ã†Â® Ã…Â©Â¸Â®Ã†Â¼Ã„Â®[36]
	uint8_t		SkillParm;//[29] Â½ÂºÃ…Â³ ÃˆÂ¿Â°Ãº Ã�ÃµÃ†Ã¸Â·Ã¼ Â¶Ã‡Â´Ã‚ Â¹Â°Â¸Â®Â°Ã¸Â°Ã� Ã€ÃŒÃ†Ã¥Ã†Â®. 0, Ã€ÃŒ ÂµÃ°Ã†ÃºÃ†Â®.[37]
	uint8_t		Motion;//[28] [38]
	uint8_t		FlagLocal;//[30] [39]
	uint8_t		DoubleCritical;//[40]
	uint8_t		unk2;//[41]
	int16_t				CurrentMp;//[26] [42]
	int16_t 				SkillIndex;// 0-98-106  (-1Ã€ÃŒÂ¸Ã© Â¹Â°Â¸Â®Ã€Ã» Â°Ã¸Â°Ã�)[24] [44]
	uint16_t		TargetID;//[40] [46]
	int16_t				Damage;//[42] [48]
	int16_t				unk3;//[50]
} MSG_AttackOne_757;//[52]

typedef struct
{
	MSG_AttackOne_656 Attack;
	sPoint<int16_t> Target[13];
} pCL_36Ch;

typedef struct
{
	MSG_HEADER			Header;//[0]
	//
	uint32_t		Hold;//[12]
	int16_t				ReqMp;//[36] [16]
	int16_t				Rsv;//[38] [18]
	uint32_t		CurrentExp;//[32] [20]
	uint16_t		Progress;//[14] [24] not confirme
	//uint16_t		PosX,		PosY;//[16] [26][28]
	//uint16_t		TargetX,	TargetY;//[20] [30][32]
	sPoint<int16_t> AttackerPOS;//[16] [26]
	sPoint<int16_t> TargetPOS;//[20] [30]
	uint16_t		AttackerID;//[12] [34]
	uint8_t		DoubleCritical;//[31] 0ÂºÃ±Ã†Â® Â´ÃµÂºÃ­ 1ÂºÃ±Ã†Â® Ã…Â©Â¸Â®Ã†Â¼Ã„Â®[36]
	uint8_t		SkillParm;//[29] Â½ÂºÃ…Â³ ÃˆÂ¿Â°Ãº Ã�ÃµÃ†Ã¸Â·Ã¼ Â¶Ã‡Â´Ã‚ Â¹Â°Â¸Â®Â°Ã¸Â°Ã� Ã€ÃŒÃ†Ã¥Ã†Â®. 0, Ã€ÃŒ ÂµÃ°Ã†ÃºÃ†Â®.[37]
	uint8_t		Motion;//[28] [38]
	uint8_t		FlagLocal;//[30] [39]
	uint8_t		unk1;//[40]
	uint8_t		unk2;//[41]
	int16_t				CurrentMp;//[26] [42]
	int16_t 				SkillIndex;// 0-98-106  (-1Ã€ÃŒÂ¸Ã© Â¹Â°Â¸Â®Ã€Ã» Â°Ã¸Â°Ã�)[24] [44]
	sPoint<int16_t> Target[13];
} pCL_36Ch_757;

struct MSG_ATTACKONE_759 : MSG_HEADER
{
	//234 36
	uint32_t			Hold;//[12]
	int32_t				ReqMp;//[16] acc->CurrentMP
	int32_t				Rsv;//[20]
	int64_t			CurrentExp;//[24]
	int16_t			Unk3;//[32]
	sPoint<int16_t>	AttackerPOS;//[34]
	sPoint<int16_t>	TargetPOS;//[38]
	uint16_t			AttackerID;//[42]
	int16_t			Progress;//[44]
	uint8_t			Motion;//[46]
	uint8_t			FlagLocal;//[47]
	uint8_t			DoubleCritical;//[48]
	uint8_t			Unk2;//[49](uint8_t)
	uint8_t			Unk1;//[50]
	uint8_t			SkillParm;//[51]
	int32_t				CurrentMp;//[52]
	int16_t			SkillIndex;//[56]
	int32_t				TargetID;//[60]
	int32_t				Damage;//[64]
	int32_t				TargetID2;//[60]
	int32_t				Damage2;//[64]
	MSG_ATTACKONE_759(int32_t Index = 0, int32_t Code = p_Attack) : MSG_HEADER(sizeof(MSG_ATTACKONE_759), Code, Index)
	{

	}
};//48

struct MSG_ATTACKMULT_759 : MSG_HEADER
{
	uint32_t			Hold;//[12]
	int32_t				ReqMp;//[16] acc->CurrentMP
	int32_t				Rsv;//[20]
	int64_t			CurrentExp;//[24]
	int16_t			Unk3;//[32]
	sPoint<int16_t>	AttackerPOS;//[34]
	sPoint<int16_t>	TargetPOS;//[38]
	uint16_t			AttackerID;//[42]
	int16_t			Progress;//[44]
	uint8_t			Motion;//[46]
	uint8_t			FlagLocal;//[47]
	uint8_t			DoubleCritical;//[48]
	uint8_t			Unk2;//[49](uint8_t)
	uint8_t			Unk1;//[50]
	uint8_t			SkillParm;//[51]
	int32_t				CurrentMp;//[52]
	int16_t			SkillIndex;//[56]
	sPoint<int32_t>		Target[13];//[60]
	int32_t				Ht;//[164]
	MSG_ATTACKMULT_759(int32_t Index = 0, int32_t Code = p_AttackArea) : MSG_HEADER(sizeof(MSG_ATTACKMULT_759), Code, Index)
	{

	}
};//48

// Request Item Anct
struct MSG_ITEMANCT : MSG_HEADER
{
	ITEM items[8];
	char slot[8];
	MSG_ITEMANCT(int32_t Index = 0, int32_t Code = 0x3A6) : MSG_HEADER(sizeof(MSG_ITEMANCT), Code, Index)
	{

	}
};

enum pCL_3B5i
{
	_Item10 = 0,
	_PedraSabio = 2,
	_Joia = 3,
	_Oher = 7
};

struct MSG_ITEMCOMB : MSG_HEADER
{
	sItem Item[8];
	/*
	union {
	_sItem Item[8];
	struct {
	_sItem Item10[2];
	_sItem PedraSabio;
	_sItem Joia[4];
	_sItem Oher;
	};
	};*/

	char Slot[8];
	MSG_ITEMCOMB(int32_t Index = 0, int32_t Code = 0x3B5) : MSG_HEADER(sizeof(MSG_ITEMCOMB), Code, Index)
	{

	}
};

struct MSG_ATTBUFFS : MSG_HEADER
{
	struct
	{
		uint8_t aType;
		uint8_t Master;

		int16_t aValue;
		int32_t Time;
	} affect[MAX_AFFECT];
	MSG_ATTBUFFS(int32_t Index = 0, int32_t Code = 0x3B9) : MSG_HEADER(sizeof(MSG_ATTBUFFS), Code, Index)
	{

	}
};


struct MSG_HPMP : MSG_HEADER
{
#if defined(MYD)
	int32_t hp;//[12]
	int32_t mp;//[16]
	int32_t maxhp;//[20]
	int32_t maxmp;//[24]
#else
	int16_t hp;//[12]
	int16_t mp;//[14]
	int16_t maxhp;//[16]
	int16_t maxmp;//[18]
#endif
	MSG_HPMP(int32_t Index = 0, int32_t Code = 0x181) : MSG_HEADER(sizeof(MSG_HPMP), Code, Index)
	{

	}
};


struct MSG_SEND_LEADERS : MSG_HEADER
{
	int32_t leader[10][5];
	MSG_SEND_LEADERS(int32_t Index = 0, int32_t Code = 0x427) : MSG_HEADER(sizeof(MSG_SEND_LEADERS), Code, Index)
	{

	}
};

//Skill venda de ht
struct MSG_HTSELL : MSG_HEADER
{
	int16_t Unk;//npc
	int16_t src_type;
	int16_t src_val;
	int16_t Unk2;
	MSG_HTSELL(int32_t Index = 0, int32_t Code = 0x2D4) : MSG_HEADER(sizeof(MSG_HTSELL), Code, Index)
	{

	}
};

// Party
struct MSG_REQPARTY : MSG_HEADER
{
	uint8_t _class;//[12]
	uint8_t pos;//[13]
	uint16_t level;//[14]
	uint16_t hp[2];//[16]
	int16_t id;//20
	char Nick[16];//22
	MSG_REQPARTY(int32_t Index = 0, int32_t Code = 0x37F) : MSG_HEADER(sizeof(MSG_REQPARTY), Code, Index)
	{

	}
};

// Summon
struct MSG_SUMMON : MSG_HEADER
{
	int16_t id;//12
	char Nick[18];//16
	MSG_SUMMON(int32_t Index = 0, int32_t Code = 0x3B2) : MSG_HEADER(sizeof(MSG_SUMMON), Code, Index)
	{

	}
};

struct MSG_LCKPASS : MSG_HEADER
{
	char Senha2[16];//[12]
	int32_t Change;//[28]
	MSG_LCKPASS(int32_t Index = 0, int32_t Code = 0xFDE) : MSG_HEADER(sizeof(MSG_LCKPASS), Code, Index)
	{

	}
};

struct MSG_KEY : MSG_HEADER
{
	int32_t ID;//12
	int16_t mode;//16 1 try open / 3 open
	char unk;//18
	char nulled;//19
	MSG_KEY(int32_t idx, int32_t m, int32_t res);
	MSG_KEY(int32_t Index = 0, int32_t Code = 0x374) : MSG_HEADER(sizeof(MSG_KEY), Code, Index)
	{

	}
};

struct MSG_DELETEITEM : MSG_HEADER
{
	uint32_t slot;
	uint32_t itemid;
	MSG_DELETEITEM(int32_t Index = 0, int32_t Code = 0x2E4) : MSG_HEADER(sizeof(MSG_DELETEITEM), Code, Index)
	{

	}
};

struct MSG_EFFECT : MSG_HEADER
{
	int32_t slot;
	int32_t unk;
	MSG_EFFECT(int32_t Index = 0, int32_t Code = 0x3B4) : MSG_HEADER(sizeof(MSG_EFFECT), Code, Index)
	{

	}
};

struct MSG_SPLITITEM : MSG_HEADER
{
	int32_t slot;//[12]
	int32_t itemid;//[16]
	int32_t num;//[20]
	MSG_SPLITITEM(int32_t Index = 0, int32_t Code = 0x2E5) : MSG_HEADER(sizeof(MSG_SPLITITEM), Code, Index)
	{

	}
};

struct MSG_GUILDREC : MSG_HEADER
{
	int32_t index;
	int32_t mode;
	MSG_GUILDREC(int32_t Index = 0, int32_t Code = 0x3D5) : MSG_HEADER(sizeof(MSG_GUILDREC), Code, Index)
	{

	}
};

struct MSG_DONATEMSG : MSG_HEADER
{
	int32_t type;
	int32_t value;
	MSG_DONATEMSG(int32_t Index = 0, int32_t Code = 0x405) : MSG_HEADER(sizeof(MSG_DONATEMSG), Code, Index)
	{

	}
};

struct MSG_LOCKPASS : MSG_HEADER
{
	char password[16];
	int32_t change;
	MSG_LOCKPASS(int32_t Index = 0, int32_t Code = 0xFDE) : MSG_HEADER(sizeof(MSG_LOCKPASS), Code, Index)
	{

	}
};

struct MSG_SEAL_OUT_PUT_REQUEST : MSG_HEADER
{
	int16_t info;
	char login[16];
	char nick[18];
	MSG_SEAL_OUT_PUT_REQUEST(int32_t Index = 0, int32_t Code = p_PutSealComplete) : MSG_HEADER(sizeof(MSG_SEAL_OUT_PUT_REQUEST), Code, Index)
	{
		//p_PutSealComplete ou p_OutSealComplete
	}
};

struct MSG_SEAL_OUT_PUT : MSG_HEADER
{
	SELCHARLIST sel;
	MSG_SEAL_OUT_PUT(int32_t Index = 0, int32_t Code = p_PutSealComplete) : MSG_HEADER(sizeof(MSG_SEAL_OUT_PUT), Code, Index)
	{

	}
};

struct MSG_SEAL_INFO_REQUEST : MSG_HEADER
{
	int32_t unique;
	MSG_SEAL_INFO_REQUEST(int32_t Index = 0, int32_t Code = p_SealInfo) : MSG_HEADER(sizeof(MSG_SEAL_INFO_REQUEST), Code, Index)
	{

	}
};

typedef struct
{
	int32_t unique;
	MOBFX mob;
	new_mob newmob;
	new_mob_two newmobtwo;
} SEAL_INFO;

struct MSG_SEAL_INFO : MSG_HEADER
{
	int32_t unique;
	char face;		//int16_t face; //original
	char resets;
	int16_t _level;
	int16_t _str;
	int16_t _int;
	int16_t _dex;
	int16_t _con;
	uint8_t wm;
	uint8_t fp;
	uint8_t sp;
	uint8_t tp;
	int16_t skill[9];
	char quest;		//int16_t quest; //original
	char evo;
	MSG_SEAL_INFO(int32_t Index = 0, int32_t Code = p_SealInfo) : MSG_HEADER(sizeof(MSG_SEAL_INFO), Code, Index)
	{

	}
};

struct DB_NPC_CASH : MSG_HEADER
{
	int32_t pak_slot;
	int32_t mob_id;
	int32_t gener;
	int32_t item_id;
	DB_NPC_CASH(int32_t Index = 0, int32_t Code = 0xFDC) : MSG_HEADER(sizeof(DB_NPC_CASH), Code, Index)
	{

	}
};

struct DB_NPC_CASH_BUY : MSG_HEADER
{
	int32_t message;
	int32_t cash;
	int32_t item_id;
	sItem npc_item[15];
	DB_NPC_CASH_BUY(int32_t Index = 0, int32_t Code = 0xFDC) : MSG_HEADER(sizeof(DB_NPC_CASH_BUY), Code, Index)
	{

	}
};

struct DB_NPC_CASH_QUANT : MSG_HEADER
{
	uint8_t npc;
	uint8_t slot;
	int32_t quantidade;
	DB_NPC_CASH_QUANT(int32_t Index = 0, int32_t Code = 0xFDC) : MSG_HEADER(sizeof(DB_NPC_CASH_QUANT), Code, Index)
	{

	}
};

struct DB_LOG : MSG_HEADER
{
	char Log[240];
	int32_t type;
	DB_LOG(int32_t Index = 0, int32_t Code = 0xD1E) : MSG_HEADER(sizeof(DB_LOG), Code, Index)
	{

	}
};

struct MSG_SET_GUILD : MSG_HEADER
{
	int32_t city;
	gIndex guild;
	MSG_SET_GUILD(int32_t Index = 0, int32_t Code = 0xC18) : MSG_HEADER(sizeof(MSG_SET_GUILD), Code, Index), guild(0)
	{

	}
};

struct MSG_SET_INFO_GUILD : MSG_HEADER
{
	gIndex guild;//[12]
	int32_t type;//[16]
	int32_t info;//[20]
	MSG_SET_INFO_GUILD(int32_t Index = 0, int32_t Code = 0xC17) : MSG_HEADER(sizeof(MSG_SET_INFO_GUILD), Code, Index), guild(0)
	{

	}
};

typedef enum
{
	packet_tm_connect = 0,
	packet_client_connect = 1,
	packet_tm_chat = 2,
	packet_tm_global = 4
} type_packet;

struct MSG_INIT_CITYS : MSG_HEADER
{
	int32_t leader[5];
	MSG_INIT_CITYS(int32_t Index = 0, int32_t Code = 0x80D) : MSG_HEADER(sizeof(MSG_INIT_CITYS), Code, Index)
	{

	}
};

struct MSG_SET_FAME_MANTLE_GUILD : MSG_HEADER
{
	int32_t red_id;
	int32_t blue_id;
	int32_t red_fame;
	int32_t blue_fame;
	MSG_SET_FAME_MANTLE_GUILD(int32_t Index = 0, int32_t Code = 0xC23) : MSG_HEADER(sizeof(MSG_SET_FAME_MANTLE_GUILD), Code, Index)
	{

	}
};

struct MSG_ALLY_WAR_GUILD : MSG_HEADER
{
	int32_t send;
	int32_t recv;
	MSG_ALLY_WAR_GUILD(int32_t Index = 0, int32_t Code = 0xE0E) : MSG_HEADER(sizeof(MSG_ALLY_WAR_GUILD), Code, Index)
	{

	}
};

struct MSG_SHOW_REBUY_ITENS : MSG_HEADER
{
	//MSG_HEADER header;
	struct
	{
		int32_t id_sell;
		sItem item;
		int32_t unk2;
	} Itens[10];
	MSG_SHOW_REBUY_ITENS(int32_t Index = 0, int32_t Code = 0) : MSG_HEADER(sizeof(MSG_SHOW_REBUY_ITENS), Code, Index)
	{

	}
};

struct MSG_SEND_SAPPHIRE : MSG_HEADER
{
	int32_t db_id;
	int32_t blue_red;
	int32_t tm_id;
	MSG_SEND_SAPPHIRE(int32_t Index = 0, int32_t Code = 0x423) : MSG_HEADER(sizeof(MSG_SEND_SAPPHIRE), Code, Index)
	{

	}
};

struct pCL_38A : MSG_HEADER
{
	int32_t war;
	int32_t noatun;
	int32_t ally;
	pCL_38A(int32_t Index = 0, int32_t Code = 0x38A) : MSG_HEADER(sizeof(pCL_38A), Code, Index)
	{

	}
};

struct MSG_UPDATE_GUILD : MSG_HEADER
{
	int32_t id;//[12][0C]
	char name[12];//[16][10]
	char nulled;//[28][1C]
	char C;//[29][1D]
	char N;//[30][1E]
	char M;//[31][1F]
	MSG_UPDATE_GUILD(int32_t Index = 0, int32_t Code = 0x3C16) : MSG_HEADER(sizeof(MSG_UPDATE_GUILD), Code, Index)
	{

	}
};



struct NPKO_MESSAGE : MSG_HEADER
{
	int32_t type;
	int32_t canal;
	char login[16];
	char message[96];
	NPKO_MESSAGE(int32_t Index = 0, int32_t Code = 0x3409) : MSG_HEADER(sizeof(NPKO_MESSAGE), Code, Index)
	{

	}
};

struct MSG_CMD_SEND : MSG_HEADER
{
	int32_t level;
	char cmd[96];
	MSG_CMD_SEND(int32_t Index = 0, int32_t Code = 0xC0B) : MSG_HEADER(sizeof(MSG_CMD_SEND), Code, Index)
	{

	}
};


typedef struct
{
	type_packet type;
	int32_t tmIndex;
	char message[256];
} PacketStruct;

struct sGuilds
{//[56] 0x88B5EE8
	gIndex Index;//[0] 0x88B5EE8
	char Guild[16];//[4] 88B5EEC
	char Sub[3][16];//[16] 88B5EF8 88B5F04 88B5F10
	char N;//State [52] 88B5F1C
	char C;//Citizen [53] 88B5F1D
	char M;//Mantle [54] 88B5F1E
	char nulled;//Null [55] 88B5F1F
	int32_t F;//fame [56] 88B5F20
	char Aviso[128];
	//88B5F24
	sGuilds(int32_t idx = 0) : Index(idx) {}
};

struct MSG_INIT_GUILD : MSG_HEADER
{
	sGuilds Guild;
	MSG_INIT_GUILD(int32_t Index = 0, int32_t Code = 0xC1B) : MSG_HEADER(sizeof(MSG_INIT_GUILD), Code, Index)
	{

	}
};


struct MSG_SUB_GUILD : MSG_HEADER
{
	//MSG_HEADER header;
	int32_t member_index;//[0C]
	int32_t guild_index;//[10]
	char pos_sub;//[14]
	char nome_sub[15];//[15]
	MSG_SUB_GUILD(int32_t Index = 0, int32_t Code = p_SubGuild) : MSG_HEADER(sizeof(MSG_SUB_GUILD), Code, Index)
	{

	}
};

struct MSG_SEND_ITEM : MSG_HEADER
{
	int32_t resp;
	char login[16];
	sItem item;
	MSG_SEND_ITEM(int32_t Index = 0, int32_t Code = p_ItemSend) : MSG_HEADER(sizeof(MSG_SEND_ITEM), Code, Index)
	{

	}
};

/*
int32_t StartX;//[018]
int32_t Seg1X;//[01C]
int32_t Seg2X;//[020]
int32_t Seg3X;//[024]
int32_t DestX;//[028]
int32_t StartY;//[02C]
int32_t Seg1Y;//[030]
int32_t Seg2Y;//[034]
int32_t Seg3Y;//[038]
int32_t DestY;//[03C]
int32_t StartRange;//[040]
int32_t Seg1Range;//[044]
int32_t Seg2Range;//[048]
int32_t Seg3Range;//[04C]
int32_t DestRange;//[050]
int32_t StartWait;//[054]
int32_t Seg1Wait;//[058]
int32_t Seg2Wait;//[05C]
int32_t Seg3Wait;//[060]
int32_t DestWait;//[064]
char StartAction[80];//[068]
char Seg1Act[80];//[0B8]
char Seg2Act[80];//[108]
char Seg3Act[80];//[158]
char DestAct[80];//[1A8]
FightAction1 -> 1F8
FightAction2 -> 248
FightAction3 -> 298
FightAction4 -> 2E8
FleeAction1 -> 338
FleeAction2 -> 388
FleeAction3 -> 3D8
FleeAction4 -> 428
DieAction1 -> 478
DieAction2 -> 4C8
DieAction3 -> 518
DieAction4 -> 568
=> 5B8
*/
struct STRUCT_GENER//BDEAA0 (BB0)
{
	enum { Start, Segment1, Segment2, Segment3, Dest };
	int32_t n;
	int32_t MinuteGenerate;//[004]
	int32_t MaxNumMob;//[008]
	int32_t NumMobs;//[00C]
	int32_t MinGroup;//[010]
	int32_t MaxGroup;//[014]
	int32_t PosX[5];//[018]
	int32_t PosY[5];//[02C]
	int32_t Range[5];//[040]
	int32_t Wait[5];//[054]
	char Action[5][80];//[068] Start, Seg1, Seg2, Seg3, Dest
	char Tab[80];
	int32_t Formation;//[5B8]
	int32_t RouteType;//[5BC]
	int32_t CountGenerate;//[5C0]
	int32_t mobID;//[5C4]
	MOBFX Leader;//[5C8]
	MOBFX Follower;//[8BC]
};//1F8

typedef struct
{//BAF990
	enum q_type
	{
		Speech = 1,//Reward = 1
		Level,//Reward = 3
		Item,//Reward = 2
		Refin,
		Conname,
		Gold,
		Trans,
		Log,
		TimeCheck,
		Equip,//Reward = 5
		Bit,
		Class,
		Stats,
		NotEquip,
		Evo,
		DeleteItem,//Reward = 4
		Skill,//Reward = 6
		QItem,//Reward = 7
		GiveGold,//Reward = 8
		GiveExp,//Reward = 9
		Teleport,//Reward = 10
		EqDelete,//Reward = 11
		SetBit,//Reward = 12
		ReStat,//Reward = 13
		ReStatAll//Reward = 14
	};
	int32_t Loaded;//[000]
	char Name[16];//[004]
	sPoint<int32_t> Pos;//[014] X Y
	char BaseSpeech[8][96];//[01C]
	struct s_condition // BAFCAC
	{
		q_type Type;//[31C][00]
		int32_t Value[4];//[320][04]
		char ConName[32];//[330][14]
		char Speech[96];//[350][34]
	} Condition[8];//sizeof(148)
	struct s_reward//7BC BB05EC
	{
		q_type Type;//[7BC][00]
		int32_t Value[4];//[7C0][04]
		int32_t ID[2];//[7D0][14]
		sItem Item;//[7D8][1C]
	} Reward[8];//sizeof(36)
	char RewardSpeech[96];//[D7C]
	char Tab[32];
	int32_t mobIndex;
} STRUCT_QUEST_NPC;

typedef struct
{//0x884F930
	char Question[4][100];
	int32_t True;
} STRUCT_QUIZ;

#endif // _STRUCT_H_
