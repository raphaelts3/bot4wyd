/***

The MIT License (MIT)

Copyright (c) 2017 Raphael Tom� Santana

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
***/

#include "main.h"

uint32_t Client::class_timer[7] = {0, 0, 0, 0, 20000, 0, 0};//[50][54][58][5C][60][64][68]
uint32_t Client::static_timer[2] = {0, 0};//[96F210][96F214]
uint32_t Client::pak_timer[2] = {0, 0};
uint32_t Client::recv_timer[3] = {0, 0, 0};//[13FAA00][13FAA04][13FAA08]
sk_756 SkillData;
s__ItemList itemlist[MAX_ITEMLIST];
int32_t CriticalMatrix[] =
{
512, 384, 448, 640, 480, 896, 576, 129, 496, 383, 960, 641, 544, 895,  65, 130, 504, 382, 447, 642, 992, 894, 577, 131, 528, 381, 959, 643,  33, 893,  66, 132,
508, 380, 446, 644, 479, 892, 578, 133, 999, 379, 958, 645, 545, 891,  67, 134, 520, 378, 445, 646, 991, 890, 579, 135,  17, 377, 957, 647,  34, 889,  68, 136,
510, 376, 444, 648, 478, 888, 580, 137, 495, 375, 956, 649, 546, 887,  69, 138, 999, 374, 443, 650, 990, 886, 581, 139, 529, 373, 955, 651,  35, 885,  70, 140,
516, 372, 442, 652, 477, 884, 582, 141, 999, 371, 954, 653, 547, 883,  71, 142,   9, 370, 441, 654, 989, 882, 583, 143,  18, 369, 953, 655,  36, 881,  72, 144,
511, 368, 440, 656, 476, 880, 584, 145, 494, 367, 952, 657, 548, 879,  73, 146, 503, 366, 439, 658, 988, 878, 585, 147, 530, 365, 951, 659,  37, 877,  74, 148,
999, 364, 438, 660, 475, 876, 586, 149, 999, 363, 950, 661, 549, 875,  75, 150, 521, 362, 437, 662, 987, 874, 587, 151,  19, 361, 949, 663,  38, 873,  76, 152,
514, 360, 436, 664, 474, 872, 588, 153, 493, 359, 948, 665, 550, 871,  77, 154, 999, 358, 435, 666, 986, 870, 589, 155, 531, 357, 947, 667,  39, 869,  78, 156,
  5, 356, 434, 668, 473, 868, 590, 157, 999, 355, 946, 669, 551, 867,  79, 158,  10, 354, 433, 670, 985, 866, 591, 159,  20, 353, 945, 671,  40, 865,  80, 160,
999, 352, 432, 672, 472, 864, 592, 161, 492, 351, 944, 673, 552, 863,  81, 162, 502, 350, 431, 674, 984, 862, 593, 163, 532, 349, 943, 675,  41, 861,  82, 164,
507, 348, 430, 676, 471, 860, 594, 165, 999, 347, 942, 677, 553, 859,  83, 166, 522, 346, 429, 678, 983, 858, 595, 167,  21, 345, 941, 679,  42, 857,  84, 168,
999, 344, 428, 680, 470, 856, 596, 169, 491, 343, 940, 681, 554, 855,  85, 170, 999, 342, 427, 682, 982, 854, 597, 171, 533, 341, 939, 683,  43, 853,  86, 172,
517, 340, 426, 684, 469, 852, 598, 173, 999, 339, 938, 685, 555, 851,  87, 174,  11, 338, 425, 686, 981, 850, 599, 175,  22, 337, 937, 687,  44, 849,  88, 176,
513, 336, 424, 688, 468, 848, 600, 177, 490, 335, 936, 689, 556, 847,  89, 178, 501, 334, 423, 690, 980, 846, 601, 179, 534, 333, 935, 691,  45, 845,  90, 180,
999, 332, 422, 692, 467, 844, 602, 181, 999, 331, 934, 693, 557, 843,  91, 182, 523, 330, 421, 694, 979, 842, 603, 183,  23, 329, 933, 695,  46, 841,  92, 184,
  3, 328, 420, 696, 466, 840, 604, 185, 489, 327, 932, 697, 558, 839,  93, 186, 999, 326, 419, 698, 978, 838, 605, 187, 535, 325, 931, 699,  47, 837,  94, 188,
  6, 324, 418, 700, 465, 836, 606, 189, 999, 323, 930, 701, 559, 835,  95, 190,  12, 322, 417, 702, 977, 834, 607, 191,  24, 321, 929, 703,  48, 833,  96, 192,
512, 320, 416, 704, 464, 832, 608, 193, 488, 319, 928, 705, 560, 831,  97, 194, 500, 318, 415, 706, 976, 830, 609, 195, 536, 317, 927, 707,  49, 829,  98, 196,
506, 316, 414, 708, 463, 828, 610, 197, 999, 315, 926, 709, 561, 827,  99, 198, 524, 314, 413, 710, 975, 826, 611, 199,  25, 313, 925, 711,  50, 825, 100, 200,
509, 312, 412, 712, 462, 824, 612, 201, 487, 311, 924, 713, 562, 823, 101, 202, 999, 310, 411, 714, 974, 822, 613, 203, 537, 309, 923, 715,  51, 821, 102, 204,
518, 308, 410, 716, 461, 820, 614, 205, 999, 307, 922, 717, 563, 819, 103, 206,  13, 306, 409, 718, 973, 818, 615, 207,  26, 305, 921, 719,  52, 817, 104, 208,
999, 304, 408, 720, 460, 816, 616, 209, 486, 303, 920, 721, 564, 815, 105, 210, 499, 302, 407, 722, 972, 814, 617, 211, 538, 301, 919, 723,  53, 813, 106, 212,
999, 300, 406, 724, 459, 812, 618, 213, 998, 299, 918, 725, 565, 811, 107, 214, 525, 298, 405, 726, 971, 810, 619, 215,  27, 297, 917, 727,  54, 809, 108, 216,
515, 296, 404, 728, 458, 808, 620, 217, 485, 295, 916, 729, 566, 807, 109, 218, 999, 294, 403, 730, 970, 806, 621, 219, 539, 293, 915, 731,  55, 805, 110, 220,
  7, 292, 402, 732, 457, 804, 622, 221, 997, 291, 914, 733, 567, 803, 111, 222,  14, 290, 401, 734, 969, 802, 623, 223,  28, 289, 913, 735,  56, 801, 112, 224,
  1, 288, 400, 736, 456, 800, 624, 225, 484, 287, 912, 737, 568, 799, 113, 226, 498, 286, 399, 738, 968, 798, 625, 227, 540, 285, 911, 739,  57, 797, 114, 228,
505, 284, 398, 740, 455, 796, 626, 229, 996, 283, 910, 741, 569, 795, 115, 230, 526, 282, 397, 742, 967, 794, 627, 231,  29, 281, 909, 743,  58, 793, 116, 232,
999, 280, 396, 744, 454, 792, 628, 233, 483, 279, 908, 745, 570, 791, 117, 234, 999, 278, 395, 746, 966, 790, 629, 235, 541, 277, 907, 747,  59, 789, 118, 236,
519, 276, 394, 748, 453, 788, 630, 237, 995, 275, 906, 749, 571, 787, 119, 238,  15, 274, 393, 750, 965, 786, 631, 239,  30, 273, 905, 751,  60, 785, 120, 240,
  2, 272, 392, 752, 452, 784, 632, 241, 482, 271, 904, 753, 572, 783, 121, 242, 497, 270, 391, 754, 964, 782, 633, 243, 542, 269, 903, 755,  61, 781, 122, 244,
999, 268, 390, 756, 451, 780, 634, 245, 994, 267, 902, 757, 573, 779, 123, 246, 527, 266, 389, 758, 963, 778, 635, 247,  31, 265, 901, 759,  62, 777, 124, 248,
  4, 264, 388, 760, 450, 776, 636, 249, 481, 263, 900, 761, 574, 775, 125, 250, 999, 262, 387, 762, 962, 774, 637, 251, 543, 261, 899, 763,  63, 773, 126, 252,
  8, 260, 386, 764, 449, 772, 638, 253, 993, 259, 898, 765, 575, 771, 127, 254,  16, 258, 385, 766, 961, 770, 639, 255,  32, 257, 897, 767,  64, 769, 128, 256
};
atomic<bool> ready (false);

void Client::BullshitDon()
{
	int32_t i, r = -1;
	FILE *f;
	char dir[256];
	char dir2[256];
	memset(&pBullshitDon, 0, sizeof(MSG_HEADER));
	pBullshitDon.Code = 0xF200;
	pBullshitDon.Size = sizeof(pBullshitDon);
	srand(time(0) + clock());
	if(pBullshitDon.data[0][0] == 0)
	{
		sprintf(dir, "%s/%s/configs/%s", DIR_BASE, servidor, login);
		f = fopen(dir, "rb");
		if(f)
		{
			fread(pBullshitDon.mac, sizeof(pBullshitDon) - sizeof(MSG_HEADER), 1, f);
			fclose(f);
		}

		if(planos == free_)
		{
			for(i = FREE_DATA; i < MAX_FREE_DATA; i++)
			{
				r = rand() % (MAX_FREE_DATA - FREE_DATA) + FREE_DATA;
				sprintf(dir, "%s/%s/configs/config_%d", DIR_BASE, servidor, r);
				f = fopen(dir, "rb");
				if(f)
				{
					fread(pBullshitDon.mac, sizeof(pBullshitDon) - sizeof(MSG_HEADER), 1, f);
					fclose(f);
					break;
				}
			}
		}else
		{
			for(i = INIT_DATA; i < MAX_DATA && f == 0; i++)
			{
				r = rand() % (MAX_DATA - INIT_DATA) + INIT_DATA;
				sprintf(dir, "%s/%s/configs/config_%d", DIR_BASE, servidor, r);
				f = fopen(dir, "rb");
				if(f)
				{
					fread(pBullshitDon.mac, sizeof(pBullshitDon) - sizeof(MSG_HEADER), 1, f);
					fclose(f);
					sprintf(dir2, "%s/%s/configs/%s", DIR_BASE, servidor, login);
					if(rename(dir, dir2) != 0)
					{
						f = 0;
					}
				}
			}
			if(f == 0)
			{
				for(i = INIT_DATA; i < MAX_DATA && f == 0; i++)
				{
					sprintf(dir, "%s/%s/configs/config_%d", DIR_BASE, servidor, i);
					f = fopen(dir, "rb");
					if(f)
					{
						fread(pBullshitDon.mac, sizeof(pBullshitDon) - sizeof(MSG_HEADER), 1, f);
						fclose(f);
						sprintf(dir2, "%s/%s/configs/%s", DIR_BASE, servidor, login);
						r = i;
						if(rename(dir, dir2) != 0)
						{
							f = 0;
						}
					}
				}
			}
		}
	}
	SendPacket(sizeof(pBullshitDon), &pBullshitDon);
	DebugLog("DonData: [%d] [%s]-[%s]-[%s]-[%s]-[%s]-[%02X:%02X:%02X:%02X:%02X:%02X]",
			r,
			pBullshitDon.data[0],
			pBullshitDon.data[1],
			pBullshitDon.data[2],
			pBullshitDon.data[3],
			pBullshitDon.data[4],
			pBullshitDon.mac[0], pBullshitDon.mac[1], pBullshitDon.mac[2],
			pBullshitDon.mac[3], pBullshitDon.mac[4], pBullshitDon.mac[5]);
}

int32_t inline dist(int16_t x1, int16_t y1, int16_t x2, int16_t y2)
{
	return (int32_t)ceil(sqrt(pow(x1 - x2, 2) + pow(y1 - y2,2)));
}

int32_t inline dist(sPoint<int16_t> *a, sPoint<int16_t> *b)
{
	return dist(a->x, a->y, b->x, b->y);
}

int32_t sItem::GetEffValue(int32_t ef) //40191F
{
	int32_t ret = 0, ef2;
	s__ItemList *it;
	ef &= 255;
	/* Star */
	ef2 = ef;
	switch (ef)
	{
		case 0x2A:
			ef2 = 0x47;
			break;
		case 2:
			ef2 = 0x49;
			break;
		case 0x2E:
			ef2 = 0x46;
			break;
		case 0x2D:
			ef2 = 0x45;
			break;
		case 0x35:
			ef2 = 0x48;
			break;
		default:;
	}
	if (ItemID > 0 && ItemID < MAX_ITEMLIST)
	{
		int32_t i, ref = 0;
		it = &itemlist[ItemID];
		if ((ef == EF_DAMAGEADD || ef == EF_MAGICADD) && (it->Unique < 41 || it->Unique > 50))
			return 0;
		if (ef == EF_LEVEL)
			ret += it->Level;
		else if (ef == EF_REQ_STR)
			ret += it->Str;
		else if (ef == EF_REQ_INT)
			ret += it->Int;
		else if (ef == EF_REQ_DEX)
			ret += it->Dex;
		else if (ef == EF_REQ_CON)
			ret += it->Con;
		else if (ef == EF_POS)
			ret += it->Pos;
		if (ef != EF_INCUBATE)
		{
			for (i = 0; i < 12; i++)
			{
				if (it->Effect[i].Index == ef)
				{
					if (ef == EF_ATTSPEED && it->Effect[i].Value == 1)
						ret += 10;
					else
						ret += it->Effect[i].Value;
				}
			}
		}
		if (ItemID >= 2330 && ItemID < 2390)
		{
			if (ef == EF_MOUNTHP)
				ret = Mount.HP;
			else if (ef == EF_MOUNTSANC)
				ret = Mount.Grow_Level;
			else if (ef == EF_MOUNTLIFE)
				ret = Mount.Vit;
			else if (ef == EF_MOUNTFEED)
				ret = Mount.Food;
			else if (ef == EF_MOUNTKILL)
				ret = Mount.Exp;
			/*else if (ItemID >= 2362)
			{
				if (Mount.HP > 0)
				{
					int32_t lvl = Mount.Grow_Level;
					if (ef == EF_DAMAGE)
						ret = ((lvl + 20) * itemlist[ItemID].Mont.Damage) / 100;
					else if (ef == EF_MAGIC)
						ret = ((lvl + 15) * itemlist[ItemID].Mont.Magic) / 100;
					else if (ef == EF_PARRY)
						ret = itemlist[ItemID].Mont.Evade;
					else if (ef == EF_RESIST1 || ef == EF_RESIST2 || ef == EF_RESIST3 || ef == EF_RESIST4)
						ret = itemlist[ItemID].Mont.Resist;
				}
			}*/
			return ret;
		}
		else if (ItemID >= 3980 && ItemID < 4000)
		{
			/*if (ef == EF_DAMAGE)
				ret = itemlist[ItemID].Mont.Damage;
			else if (ef == EF_MAGIC)
				ret = itemlist[ItemID].Mont.Magic;
			else if (ef == EF_PARRY)
				ret = itemlist[ItemID].Mont.Evade;
			else if (ef == EF_RESIST1 || ef == EF_RESIST2 || ef == EF_RESIST3 || ef == EF_RESIST4)
				ret = itemlist[ItemID].Mont.Resist;*/
			return ret;
		}
		for (i = 0; i < 3; i++)
		{
			if (Effect[i].Index == ef)
			{
				if (ef == EF_ATTSPEED && Effect[i].Value == 1)
					ret += 10;
				else
					ret += Effect[i].Value;
			}
		}
		if (ef != ef2)
		{
			for (i = 0; i < 3; i++)
			{
				if (Effect[i].Index == ef2)
				{
					if (ef == EF_ATTSPEED && Effect[i].Value == 1)
						ret += 10;
					else
						ret += Effect[i].Value;
				}
			}
		}
		if (ef == EF_RESIST1 || ef == EF_RESIST2 || ef == EF_RESIST3 || ef == EF_RESIST4)
		{
			for (i = 0; i < 12; i++)
			{
				if (it->Effect[i].Index == EF_RESISTALL)
					ret += it->Effect[i].Value;
			}
			for (i = 0; i < 3; i++)
			{
				if (Effect[i].Index == EF_RESISTALL)
					ret += Effect[i].Value;
			}
		}
		ref = GetRefin();
		/*
		_4 = ret;
		_34 = ref;
		_c = it->Unique;
		_10 = it->Pos;*/
		if (ItemID <= 40)
			ref = 0;
		if (ref >= 9)
		{
			if ((it->Pos & 0xF00) != 0)
				ref++;
		}
		if (ref > 0)
		{
			if (ef != EF_GRID && ef != EF_CLASS && ef != EF_POS && ef != EF_WTYPE && ef != EF_RANGE && ef != EF_LEVEL &&
				ef != EF_REQ_STR && ef != EF_REQ_INT && ef != EF_REQ_DEX && ef != EF_REQ_CON && ef != EF_VOLATILE &&
				ef != EF_INCUBATE && ef != EF_INCUDELAY && ef != EF_PREVBONUS && ef != EF_TRANS && ef != EF_REFLEVEL &&
				ef != EF_GAMEROOM && ef != EF_REGENHP && ef != EF_REGENMP)
			{
				if (ref < 11)
					ret = (ret*(ref + 10)) / 10;
				else
				{
					int32_t aux = ref - 10;
					if (aux == 1)
						aux = 220;
					else if (aux == 2)
						aux = 250;
					else if (aux == 3)
						aux = 280;
					else if (aux == 4)
						aux = 320;
					else if (aux == 5)
						aux = 370;
					else if (aux == 6)
						aux = 430;
					else if (aux == 7)
						aux = 500;
					ret = (((ret * 10) * aux) / 100) / 10;
				}
			}
			if (ef == EF_RUNSPEED)
			{
				if (ret >= 3)
					ret = 2;
				if (ref >= 9)
					ret++;
				if (ret < 0)
					ret = 0;
			}
			else if (ef == EF_HWORDGUILD || ef == EF_LWORDGUILD)
				ret &= 255;
			else if (ef == EF_REGENHP || ef == EF_REGENMP)
				ret *= ref;
			else if (ef == EF_GRID)
			{
				if (ret < 0 || ret > 7)
					ret = 0;
			}
		}
	}
	return ret;
}

int32_t Client::GetPlayerTotalEf(int32_t ef)
{
	sItem *items = pChar.Character.Equip;
	int32_t i, val = 0, unique[16], esq = 0, dir = 0;
	ef &= 0xFF;
	if (ef != EF_RANGE && ef != EF_MAGIC)
	{
		for (i = 0; i < 16; i++)
		{
			unique[i] = 0;
			if ((items[i].ItemID > 0 && items[i].ItemID < MAX_ITEMLIST) ||
				(items[i].ItemID == 0 && i == WEAPON2))
			{
				unique[i] = itemlist[items[i].ItemID].Unique;
				if (ef == EF_DAMAGE && i == WEAPON1)
					continue;
				else if (ef == EF_DAMAGE && i == WEAPON2)
				{
					esq = items[WEAPON1].GetEffValue(EF_DAMAGE) + items[WEAPON1].GetEffValue(EF_DAMAGE2);
					dir = items[WEAPON2].GetEffValue(EF_DAMAGE) + items[WEAPON2].GetEffValue(EF_DAMAGE2);
					if (unique[6] && unique[7])
					{
						int32_t temp = 0;
						if (unique[6] == 47 && unique[7] == 45)
						{
							val += esq;
						}
						else
						{
							if (unique[6] == unique[7])
							{
								temp = 50;
							}
							else
							{
								temp = 30;
							}
							if (pChar.Character.ClassInfo == TK)
							{
								if ((pChar.Character.Learn & (1 << 9)) != 0)
								{
									temp += 15;
								}
							}
							else if (pChar.Character.ClassInfo == HT)
							{
								if ((pChar.Character.Learn & (1 << 10)) != 0)
								{
									temp += 10;
								}
							}
							if (esq > dir)
							{
								val += esq + ((dir * temp) / 100);
							}
							else
							{
								val += dir + ((esq * temp) / 100);
							}
						}
					}
					else if (esq > dir)
					{
						val += esq;
					}
					else
					{
						val += dir;
					}
				}
				else
					val += items[i].GetEffValue(ef);
			}
		}
	}
	else if (ef == EF_MAGIC)
	{
		for (i = 0; i < 16; i++)
		{
			unique[i] = 0;
			if (items[i].ItemID > 0 && items[i].ItemID < 6500)
			{
				unique[i] = itemlist[items[i].ItemID].Unique;
				if (i == 6)
					esq = items[i].GetEffValue(ef);
				else if (i == 7)
					dir = items[i].GetEffValue(ef);
				else
					val += items[i].GetEffValue(ef);
			}
		}
		if (pChar.Character.ClassInfo == TK)
		{
			if (unique[6] == 47 && unique[7] == 47)
				val += ((esq + dir) / 2);
			else if (unique[6] == 47)
				val += ((esq) / 2) + dir;
			else if (unique[7] == 47)
				val += ((dir) / 2) + esq;
			else
				val += esq + dir;
			if ((items[WEAPON1].GetEffValue(EF_TRANS) >= 4 && unique[WEAPON1] == 47) ||
				(items[WEAPON2].GetEffValue(EF_TRANS) >= 4 && unique[WEAPON2] == 47))
			{
				val >>= 1;
			}
		}
		else
		{
			val += esq + dir;
		}
	}
	else
	{
		for (i = 0; i < 16; i++)
		{
			if (val < items[i].GetEffValue(EF_RANGE))
				val = items[i].GetEffValue(EF_RANGE);
		}
		if (pChar.Character.ClassInfo == HT)
		{
			if (val < 2)
			{

				if ((pChar.Character.Learn & (1 << 19)) != 0)
				{
					val = 2;
				}
			}
		}
	}
	return val;
}

void Client::InsertPacket(time_t t, MSG_HEADER *pak, int32_t pos)
{
	int32_t p = pos;
    if(p == -1)
    {
        for(p = 0; p < MAX_TIMER_PACKETS; p++)
        {
            if(TimerPacket[p].time == 0)
            {
                break;
            }
        }
    }
    if(p < 0 || p > 64)
    {
    	return;
    }
    memcpy(TimerPacket[p].buffer, pak, pak->Size);
    TimerPacket[p].time = t;
}

void Client::Signal(int32_t code)
{
	MSG_HEADER packet(sizeof(MSG_HEADER), code, cID);
	SendPacket(sizeof(packet), &packet);
}

void Client::SignalParam(int32_t code, int32_t parm)
{
	MSG_SIGNALPARM packet(cID, code, parm);
	SendPacket(sizeof(packet), &packet);
}

void Client::LockPasswd(char *pass)
{
	MSG_LOCKPASS packet(cID);
	strcpy(packet.password, pass);
	if(server == _don2)
    {
        uint8_t key = 0;
        for(int32_t i = 0; i < 4; i++)
        {
            int32_t v = login[i];
            if(login[i] >= 'a' && login[i] <= 'z')
            {
                v -= 32;
            }
			//clientpatch
            switch(i)
            {
            case 0:
                {
                    key += v * 0x17;
                    break;
                }
            case 1:
                {
                    key += v * 0x1D;
                    break;
                }
            case 2:
                {
                    key += v * 0x0D;
                    break;
                }
            case 3:
                {
                    key += v * 0x29;
                    break;
                }
            }
        }
        for(int32_t i = 0; i < 9; i++)
        {
            packet.password[i] ^= key;
        }
        packet.change = 0x1000000;
    }
	SendPacket(sizeof(packet), &packet);
}

void Client::Select(int32_t slot)
{
	MSG_REQCHARINFO packet(cID, 0x213);
	packet.slot = slot;
	if(server == _don2)
    {
        packet.slot += 0x7C04;
    }
	SendPacket(sizeof(packet), &packet);
}

void Client::Chat(const char *str, ...)
{
	MSG_SAY packet(cID);
	va_list args;
	va_start (args, str);
	vsprintf (packet.message, str, args);
	va_end (args);
	SendPacket(sizeof(packet), &packet);
}

void Client::Pvt(const char *cmd, const char *str)
{
	MSG_COMMAND packet(cID);
	strcpy(packet.cmd, cmd);
	packet.cmd[15] = 0;
	strcpy(packet.val, str);
	SendPacket(sizeof(packet), &packet);
}

void Client::Revive()
{
	MSG_HEADER packet(sizeof(MSG_HEADER), 0x289, cID);
	hacks.macro.on = 0;
	hacks.macro.re = 1;
	hacks.macro.re_c = 25 + rand() % 10 - 5;
	InsertPacket(time(0) + 4, &packet);
}

int32_t Client::GetCity(int32_t x, int32_t y)
{
	if(x >= 2052 && x <= 2171 && y >= 2052 && y <= 2163)
	{
		return 0;
	}else if(x >= 2432 && x <= 2675 && y >= 1536 && y <= 1767)
	{
		return 1;
	}else if(x >= 2448 && x <= 2476 && y >= 1966 && y <= 2024)
	{
		return 2;
	}else if(x >= 3500 && x <= 3700 && y >= 3100 && y <= 3200)
	{
		return 3;
	}else if(x >= 1036 && x <= 1075 && y >= 1700 && y <= 1775)
	{
		return 4;
	}
	return 5;
}

int32_t Client::GetCritical()
{
	int32_t cri = 0;
	int32_t v1 = (pChar.Character.Status.Speed.Move - 5) * 100;
	int32_t v2 = pChar.Character.Critical << 2;
	int32_t v3;
	hacks.macro.progress %= 1024;
	v3 = CriticalMatrix[hacks.macro.progress];
	if(v3 >= v1)
	{
		cri |= 1;
	}
	if((1000-v3) >= v2)
	{
		cri |= 2;
	}
	hacks.macro.progress++;
	return cri;
}

void Client::Attack(int32_t aID, sPoint<int16_t> *aPos, int32_t tID, sPoint<int16_t> *tPos, int32_t skIdx, int32_t motion, int32_t skParm, int32_t dmg, int32_t tID2, int32_t dmg2)
{
#if (defined DON) || (defined MYD)
	MSG_ATTACKMULT packet(cID);
	packet.Code = 0x39D;
#else
	MSG_ATTACKONE packet(cID);
#endif
	int32_t cri = 0;
	int32_t pro = 0;
	if(skIdx == 255)
	{
		pro = hacks.macro.progress;
		cri = GetCritical();
		if(cData.wtype >= 101 && cData.wtype <= 103)
		{
			skIdx = cData.wtype + 50;
		}else if(cData.wtype == 104)
		{
            skIdx = 104;
		}
		skParm = 0;
	}
	packet.AttackerID = aID;
	packet.AttackerPOS = *aPos;
#if (defined DON) || (defined MYD)
	packet.Target[0].x = tID;
	packet.Target[0].y = dmg;
#else
	packet.Damage = dmg;
	packet.TargetID = tID;
#endif
	packet.CurrentMp = -1;
	packet.Motion = motion;
	packet.SkillIndex = skIdx;
	packet.TargetPOS = *tPos;
	packet.SkillParm = skParm;
	packet.DoubleCritical = cri;
	packet.Progress = pro;
	if(pChar.Character.ClassInfo == 3)
	{
		packet.Code = 0x39E;
#if (defined DON) || (defined MYD)
		packet.Target[1].x = tID2;
		packet.Target[1].y = dmg2;
#else
		packet.TargetID2 = tID2;
		packet.Damage2 = dmg2;
#endif
	}else
	{
		if(packet.Size == sizeof(MSG_ATTACKONE))
		{
#if defined(MYD)
			packet.Size -= 8;
#else
			packet.Size -= 4;
#endif
		}
	}
	SendPacket(packet.Size, &packet);
	wait_recv[p_Attack] = 0;//delay_thread * 3;
	wait_recv[p_AttackArea] = 8;
}

void Client::AttackArea(int32_t aID, sPoint<int16_t> *aPos, int32_t skIdx, sPoint<int32_t> *tID)
{
	//packet_36C = c_createPacket({4, 1, tonumber("367", 16), 6, 1, my_id, 28, 1, tonumber("FFFF", 16), 12, 0, 255}, 100, -1)--motion skparm flaglocal

	MSG_ATTACKMULT packet(cID);
	packet.AttackerID = aID;
	packet.AttackerPOS = *aPos;
	packet.TargetPOS = mobs[tID[0].x].Current;
	packet.CurrentMp = -1;
	packet.Motion = 255;
	packet.SkillIndex = skIdx;
	for(int32_t i = 0; i < 13; i++)
	{
		if(tID[i].x != -1)
		{
			packet.Target[i] = tID[i];
		}
	}
	SendPacket(sizeof(packet), &packet);
	wait_recv[p_Attack] = 0;//delay_thread * 3;
	wait_recv[p_AttackArea] = 8;
}

int32_t Client::CheckHP(int32_t _max, int32_t _atual, int32_t ef, int32_t mount)
{
	int32_t ret = 0;
	int32_t perc = 200;
	if(_atual <= 0)
	{
		return 0;
	}
	if(_max != 0)
	{
		perc = (_atual*100)/_max;
	}
	if(mount == 1)
	{
		int32_t type_m = (ef - 2330) % 30;//-1874
		if (type_m >= 6 && type_m <= 15)
		{
			type_m = 6;
		}
		else if (type_m == 19)
		{
			type_m = 7;
		}
		else if (type_m == 20)
		{
			type_m = 8;
		}
		else if (type_m >= 21 && type_m <= 23)
		{
			type_m = 9;
		}
		else if (type_m >= 24 && type_m <= 26)
		{
			type_m = 10;
		}
		else if (type_m == 29)
		{
			type_m = 13;//13?
		}
		ef = type_m;
	}
	if(perc < hacks.macro.hp[mount])
	{
		sItem *it = pChar.Character.Inventory;
		for(int32_t i = 0; i < MAX_SLOTS_INV; i++)
		{
			if(it->ItemID != 0)
			{
				if(mount == 0)
				{
					if(it->GetEffValue(EF_VOLATILE) == 1 && it->GetEffValue(ef) > 0)
					{
						if(ef == EF_HP && it->GetEffValue(EF_MP) > 0)
						{
							ret = 2;
						}else if(ef == EF_MP && it->GetEffValue(EF_HP) > 0)
						{
							ret = 2;
						}else
						{
							ret = 1;
						}
						UseItem(1, i, 0, 0, 0);
						WriteLog("Pot: Slot(%d); Status: %d/%d (%d%%); Config: %d",
									i+1, _atual, _max, perc, hacks.macro.hp[mount]);
						break;
					}
				}else
				{
					int32_t type_r = -1;
					if (it->ItemID >= 3368)
					{
						type_r = (it->ItemID - 3367) % 30;
						if (it->ItemID == 3465)
						{
							type_r = 13;
						}
						else if (it->ItemID == 3466)
						{
							type_r = 18;
						}
					}
					else
					{
						type_r = (it->ItemID - 2420) % 30;
					}
					if (ef == 28 && type_r == 9)
					{
						type_r = 28;
					}
					else if (ef == 27 && type_r == 6)
					{
						type_r = 27;
					}
					if (type_r == ef)
					{
						ret = 1;
						UseItem(1, i, 0, MOUNT, 0);
						WriteLog("Ra��o: Slot(%d); Status: %d/%d (%d%%); Config: %d",
									i+1, _atual, _max, perc, hacks.macro.hp[mount]);
						break;
					}
				}
			}
			it++;
		}
	}
	return ret;
}

int32_t Client::isEvok(int32_t skIdx)
{
	if(skIdx >= 56 && skIdx <= 63)
    {
    	return 1;
    }
    return 0;
}

void Client::Macro(int32_t tipo)
{
	int32_t re = 0;
	WriteLog("Macro iniciado: %d", tipo);
	memset(mobs_targets, 0, sizeof(mobs_targets));
	memset(hacks.macro.targets, 0, sizeof(hacks.macro.targets));
	hacks.macro.tipo = tipo;
	hacks.range.on = 0;
//	hacks.drop.on = 1;
	hacks.macro.mob = 0;
	hacks.macro.buff = 0;
	hacks.macro.skill = 0;
	hacks.macro.stage = 0;
	re = hacks.macro.re;
	hacks.macro.re = 0;
	hacks.macro.re_c = -1;
	hacks.macro.re_l = -1;
	hacks.protect.id = -1;
	hacks.protect.on = 0;
	hacks.macro.on = 1;
//	Move(hacks.macro.pos.x, hacks.macro.pos.y);
	if(re)
	{
		/*
		switch(rand() % 4)
		{
		case 0:
			Chat("mata n o plz");
			break;
		case 1:
			Chat("aff, mata n o por favor");
			break;
		case 2:
			Chat("s  quero macrar de boa pfv");
			break;
		default:
			break;
		}
		*/
	}
}

void Client::Party(int32_t id, char *nick)
{
	MSG_SUMMON packet(cID, 0x3AB);
	nick[15] = 0;
	packet.id = id;
	strcpy(packet.Nick, nick);
	SendPacket(sizeof(packet), &packet);
	WriteLog("Party: %s(%d)", nick, id);
}

void Client::LeaveParty()
{
	MSG_SIGNALPARM packet(cID, 0x37E);
	SendPacket(sizeof(packet), &packet);
}

void Client::DeleteItem(int32_t slot, int32_t id)
{
	MSG_DELETEITEM packet(cID);
	if(lastSlotDeleted != slot)
	{
		if(lastSlotDeleted != -1)
		{
			for(int32_t i = lastSlotDeleted, j = 1; i < slot; i++)
			{
				if(pChar.Character.Inventory[i].ItemID == 0)
				{
					packet.slot = i;
					packet.itemid = lastInv[i];
					InsertPacket(time(0) + (j << 1), &packet);
					j++;
				}
			}
		}
		lastSlotDeleted = slot;
	}
	packet.slot = slot;
	packet.itemid = id;
	if(slot >= 0 && slot < MAX_SLOTS_INV)
	{
		if(pChar.Character.Inventory[slot].ItemID == id)
		{
			pChar.Character.Inventory[slot].clear();
		}
	}
#if !defined(STAR)
	SendPacket(sizeof(packet), &packet);
#endif
}

void Client::ShiftItem(int32_t slot, int32_t id, int32_t n)
{
	MSG_SPLITITEM packet(cID);
	packet.itemid = id;
	packet.num = n;
	packet.slot = slot;
	SendPacket(sizeof(packet), &packet);
}

int32_t Client::UseItem(int32_t src_type, int32_t src_pos, int32_t dst_type, int32_t dst_pos, int32_t op)
{
	MSG_USEITEM packet(cID);
	int32_t timer = 0;
	if(src_type == 3)
	{
		timer = 1;
		src_type = 1;
	}

	packet.dst_type = dst_type;
	packet.dst_val = dst_pos;
	packet.op = op;
	packet.src_type = src_type;
	packet.src_val = src_pos;
	packet.posx = mobs[cID].Current.x;
	packet.posy = mobs[cID].Current.y;
	if(src_type == 1)
	{
		if(src_pos >= 0 && src_pos < MAX_SLOTS_INV)
		{
			sItem *it = &pChar.Character.Inventory[src_pos];
			it->AddAmount(-1);
		}
	}
	SignalParam(p_Disconnect, 1);
	if(timer == 1)
	{

		InsertPacket(time(0) + 5, &packet);
		//wait_recv[p_Move1] = 55;
		return 6;
	}else
	{
		SendPacket(sizeof(packet), &packet);
	}
	return 2;
}


void Client::MoveItem(int32_t src_type, int32_t src_pos, int32_t dst_type, int32_t dst_pos)
{
	MSG_MOVEITEM packet(cID);
	packet.destSlot = dst_pos;
	packet.destType = dst_type;
	packet.srcSlot = src_pos;
	packet.srcType = src_type;
	if(src_type == 2 && dst_type == 1)
	{
		if(src_pos >= 0 && src_pos < MAX_SLOTS_WARE && dst_pos >= 0 && dst_pos < MAX_SLOTS_INV)
		{
			sItem aux = pChar.Character.Inventory[dst_pos];
			pChar.Character.Inventory[dst_pos] = pLogin.Storage[src_pos];
			pLogin.Storage[src_pos] = aux;
		}
	}else if(src_type == 1 && dst_type == 2)
	{
		if(src_pos >= 0 && src_pos < MAX_SLOTS_INV && dst_pos >= 0 && dst_pos < MAX_SLOTS_WARE)
		{
			sItem aux = pLogin.Storage[dst_pos];
			pLogin.Storage[dst_pos] = pChar.Character.Inventory[src_pos];
			pChar.Character.Inventory[src_pos] = aux;
		}
	}
	SendPacket(sizeof(packet), &packet);
}


void Client::SellItem(int32_t npc, int32_t src_type, int32_t src_pos)
{
	MSG_SELLITEMS packet(cID);
	packet.mobID = npc;
	packet.invSlot = src_pos;
	packet.invType = src_type;
	SendPacket(sizeof(packet), &packet);
}

void Client::BuyItem(int32_t npc, int32_t src, int32_t dst)
{
	MSG_BUYITEMS packet(cID);
	packet.sellSlot = src;
	packet.invSlot = dst;
	packet.mobID = npc;
	SendPacket(sizeof(packet), &packet);
}

void Client::Click(int32_t npc)
{
	MSG_SIGNALPARM2 packet(cID, p_RequestNpcClick, npc);
	SendPacket(sizeof(packet), &packet);
}

void Client::Grifo(int32_t op)
{
	MSG_SIGNALPARM2 packet(cID, 0xAD9, op, 2);
	SendPacket(sizeof(packet), &packet);
}

void Client::ConfirmPortal()
{
	MSG_SIGNALPARM packet(cID, p_ChangeCity, 0);
	SendPacket(sizeof(packet), &packet);
}

void Client::OpenStore()
{
	pNpcTrade.Code = 0x397;
	pNpcTrade.Size = sizeof(pNpcTrade);
	pNpcTrade.Index = 0;
	pNpcTrade.idx = cID;
	for(int32_t i = 0; i < 12; i++)
	{
		if(pNpcTrade.price[i] != 0)
		{
			int32_t slot = pNpcTrade.slot[i];
			if(slot >= 0 && slot < MAX_SLOTS_WARE)
			{
				pNpcTrade.item[i] = pLogin.Storage[slot];
			}else
			{
				pNpcTrade.slot[i] = 0;
				pNpcTrade.price[i] = 0;
			}
		}
	}
	SendPacket(sizeof(pNpcTrade), &pNpcTrade);
}

void Client::UpdateCity(int32_t city)
{
	MSG_SIGNALPARM packet(cID, p_UpdateCity, city);
	SendPacket(sizeof(packet), &packet);
}

void Client::MapBit(int32_t x, int32_t y, int32_t _set)
{
	uint8_t *bit = &mob_map[x][y/8];
	*bit ^= (-_set ^ (*bit)) & (1 << (y%8));
}

int32_t Client::IsEmpty(int32_t x, int32_t y)
{
	if(x >= 0 && x < 4096 && y >= 0 && y < 4096)
	{
        return ((mob_map[x][y/8] >> (y%8)) & 1);
	}
	return 0;
}

int32_t Client::Move(int32_t x, int32_t y, int32_t real)
{
	MSG_MOVE packet(cID);
	int32_t _x = mobs[cID].Current.x;
	int32_t _y = mobs[cID].Current.y;
	int32_t ok = 0;
	double d = dist(_x, _y, x, y);
#if !defined(_DEBUG)
	if(server != _kofd && d >= 12 && real != 1)
	{
		return 0;
	}
#endif
	if(d <= 0)
	{
		return 0;
	}
	ok = IsEmpty(x, y);
	for(int32_t k = 1; k < 3 && ok == 0; k++)
	{
		for(int32_t i = -k; i <= k && ok == 0; i++)
		{
			for(int32_t j = -k; j <= k && ok == 0; j++)
			{
				int32_t x_ = x + j;
				int32_t y_ = y + i;
				if(x_ < 0)
				{
					x_ = 0;
				}
				if(x_ > 4095)
				{
					x_ = 4095;
				}
				if(y_ < 0)
				{
					y_ = 0;
				}
				if(y_ > 4095)
				{
					y_ = 4095;
				}
				if(IsEmpty(x_, y_) == 0)
				{
					x = x_;
					y = y_;
					ok = 1;
				}
			}
		}
	}
	MapBit(_x, _y, 0);
	MapBit(x, y, 1);
	mobs[cID].Current.x = x;
	mobs[cID].Current.y = y;
	WriteLog("Move: %d %d -> %d %d", _x, _y, x, y);
#if defined(DON)
	real = 0;
#endif // DON
#if !defined(ENABLE_PROTECT)
	real = 0;
#endif // ENABLE_PROTECT
	if(server == _kofd)
	{
		real = 1;
	}
	packet.mType = real;
	packet.mSpeed = pChar.Character.Status.Speed.Move;
	packet.xSrc = _x;
	packet.ySrc = _y;
	packet.xDst = x;
	packet.yDst = y;
//	hacks.macro.pos.x = x;
//	hacks.macro.pos.y = y;
	SendPacket(sizeof(packet), &packet);
	d = floor(d / packet.mSpeed);
#if !defined(_DEBUG)
	return ((int32_t)d > 3 ? (int32_t)d : 3) - 2;
#else
	return 1;
#endif
	//wait_recv[p_Move1] = (int32_t)(ceil(d / packet.mSpeed) * 10);//delay_thread * 3;
}

void Client::RemoveMacro(int32_t idx)
{
	if(mobs_targets[idx])
	{
		mobs_targets[idx]->id = 0;
		mobs_targets[idx]->time = 0;
		mobs_targets[idx] = 0;
	}
}

void Client::InsertMacro(int32_t idx)
{
	if(idx < 1000 || mobs[idx].Key)
		return;
	if(hacks.range.on == 0 && dist(&mobs[cID].Current, &mobs[idx].Current) > 8)
	{
		if(mobs_targets[idx])
		{
			RemoveMacro(idx);
		}
		return;
	}
	if(mobs_targets[idx])
	{
		RemoveMacro(idx);
	}
	for(int32_t i = 0; i < 64; i++)
	{
		if(!hacks.macro.targets[i].id)
		{
			hacks.macro.targets[i].id = idx;
			hacks.macro.targets[i].time = time_tick + 15000;
			mobs_targets[idx] = &hacks.macro.targets[i];
			//WriteLog("ADD-MOB: %d [%04d/%04d] %d", idx, mobs[idx].Current.x, mobs[idx].Current.y, dist(&mobs[cID].Current, &mobs[idx].Current));
			break;
		}
	}
}

void Client::PacketControl(MSG_HEADER *header)
{
	if(dump_recv)
	{
		Dump(0, header->Size, header);
	}
	DebugLog("RPacket: [%d][%04X][%d]", header->Size, header->Code, header->Index);
	switch(header->Code)
	{
	case 0x3AE:
	{
		Disconnect();
		break;
	}
	case 0xFDF:
	{
		WriteLog("Senha2 errada.");
		Disconnect();
		break;
	}
#if defined(OVER)
	case 0x10E:
#else
	case 0x10A:
#endif
	{
		MSG_LOGIN *pak = (MSG_LOGIN*)header;
		attTimerRecv(header->Time);
		keys_count = 0;
		memcpy(login_keys, pak->Keys, 16);
		memcpy(&pLogin, pak, sizeof(pLogin));
		WriteLog("Logado com sucesso: %s", login);
#if !defined(MYD) && !defined(OVER)
		Signal(0x3A0);
		LockPasswd(senha2);
#endif
		cID = -2;
		break;
	}
	case 0x114:
	{
		MSG_CHARINFO *pak = (MSG_CHARINFO *)header;
		attTimerRecv(header->Time);
		memcpy(&pChar, pak, sizeof(pChar));
		hacks.macro.progress = 0;
		for(int32_t i = 0; i < MAX_SLOTS_INV; i++)
		{
			sItem *it = &pak->Character.Inventory[i];
			if((drop_list[it->ItemID] & (1 << _amount)) != 0 &&
				amount_list[it->ItemID] == 255)
			{
				int32_t j;
				for(j = 0; j < 3; j++)
				{
					if(it->Effect[j].Index == EF_AMOUNT)
					{
						if(it->Effect[j].Value < MAX_SLOTS_WARE)
						{
							amount_list[it->ItemID] = i;
						}
						break;
					}
				}
				if(j == 3)
				{
					amount_list[it->ItemID] = i;
				}
			}
		}
		memset(party, -1, sizeof(party));
		memset(npcs, 0, sizeof(npcs));
		memset(lastInv, 0, sizeof(lastInv));
		memset(mob_map, 0, sizeof(mob_map));
		lastSlotDeleted = -1;
#if defined(OVER)
		for(int32_t i = 0; i < 4; i++)
		{
			if(pak->Character.SkillBar[i] == -1)
			{
				cskillbar[i] = -1;
			}else
			{
				cskillbar[i] = pak->Character.SkillBar[i] + (pak->Character.ClassInfo * 24);
			}
		}
		for(int32_t i = 0; i < 16; i++)
		{
			if(pak->skill_bar[i] == -1)
			{
				cskillbar[i+4] = -1;
			}else
			{
				cskillbar[i+4] = pak->skill_bar[i] + (pak->Character.ClassInfo * 24);
			}
		}
#else
		memcpy(cskillbar, pak->Character.SkillBar, 4);
		memcpy(&cskillbar[4], pak->skill_bar, 16);
#endif
		WriteLog("Entrou no jogo: %s", pak->Character.Name);
		cData.range = GetPlayerTotalEf(EF_RANGE) + PLUS_RANGE;
        DebugLog("Range: %d", cData.range);
        if(cData.range > 8)
        {
        	cData.range = 8;
        }
		cData.wtype = pChar.Character.Equip[WEAPON1].GetEffValue(EF_WTYPE);
		hacks.commands.c = timer_count + 15;
		Pvt("day", "");
		break;
	}
	case 0x3B9:
	{
		MSG_ATTBUFFS *pak = (MSG_ATTBUFFS*)header;
		if(pak->Index == cID)
		{
			memcpy(&pBuffs, pak, sizeof(pBuffs));
			/*
			if(hacks.macro.on && hacks.macro.stage == 1)
			{
				int32_t j;
				for(j = 0; j < 20; j++)
				{
					int32_t skIdx = cskillbar[j];
					if((skillbar[j] & (1 << _buff)) != 0)
					{
						if(skIdx >= 0 && skIdx < 104)
						{
							if(SkillData.sk[skIdx].AffectType != 0 && isEvok(skIdx) == 0)
							{
								int32_t i;
								for(i = 0; i < MAX_AFFECT; i++)
								{
									if(pak->affect[i].aType == SkillData.sk[skIdx].AffectType)
									{
										break;
									}
								}
								if(i == MAX_AFFECT)
								{
									if(wait_recv[p_AttackArea] == 0)
									{
										wait_recv[p_Attack] = delay_thread;
										Attack(cID, &mobs[cID].Current, cID, &mobs[cID].Current, skIdx, 255, 0, -1);
									}
									//hacks.macro.buff = 0;
									//hacks.macro.stage = 0;
									break;
								}
							}
						}
					}
				}
			}*/
		}
		break;
	}
	case 0xFDE:
	{
		WriteLog("Senha2 correta.");
#if defined(DON)
		BullshitDon();
#endif
		Select(slot);
		break;
	}
	case 0x333:
	{
		MSG_SAY *pak = (MSG_SAY *)header;
		WriteLog("[%s]: %s", mobs[pak->Index].PlayerName, pak->message);
		break;
	}
	case 0xD1D:
	{
		MSG_CLIENTMSG *pak = (MSG_CLIENTMSG *)header;
		WriteLog("#%s", pak->strMessage);
		break;
	}
	case 0x334:
	{
		MSG_COMMAND *pak = (MSG_COMMAND*)header;
		WriteLog("[%s]:> %s", pak->cmd, pak->val);

		if(strcmp(pak->val, chave) == 0)
		{
			char chat[64];
			pak->cmd[16] = 0;
			strcpy(lastNick, pak->cmd);
			sprintf(chat, "Estou em: %04d/%04d",
					mobs[cID].Current.x, mobs[cID].Current.y);
			Pvt(lastNick, chat);
		}else if(strcmp(pak->cmd, lastNick) == 0)
		{
			if(pak->val[0] == '=')
			{
				ExecuteCommand(&pak->val[1]);
			}else
			{
				ExecuteCommand(pak->val);
			}
		}
		break;
	}
	case 0x101:
	{
		MSG_CLIENTMSG *pak = (MSG_CLIENTMSG *)header;
#if defined(MYD) || defined(OVER)
		if(cID == -2)
		{
            Signal(0x3A0);
			LockPasswd(senha2);
			break;
		}
#endif
		if(pak->strMessage[0] != '!' || pak->strMessage[1] != '#')
		{
			WriteLog("[MSG] : %s", pak->strMessage);
		}
		if(!strncmp(pak->strMessage, "Uso de programas ilegais", 24))
		{
			for(int32_t i = 0; i < 95; i++)
			{
				if(pak->strMessage[i] == 0)
				{
					pak->strMessage[i] = '|';
				}
			}
			pak->strMessage[95] = 0;
			WriteLog("[ MSG ] : %s", pak->strMessage);
			Disconnect();
		}
		if(cID == -1)
		{
			DebugLog("101-Disconnect: %s", pak->strMessage);
			Disconnect();
			/*remove("./fechar");
			exit(1);*/
		}
		break;
	}
	case 0x37F:
	{
		MSG_REQPARTY *pak = (MSG_REQPARTY*)header;
		if(hacks.party.on)
		{
			if(!strcmp(hacks.party.nick, pak->Nick) || !strcmp(hacks.party.nick, "ALL"))
			{
				Party(pak->id, pak->Nick);
			}
		}
		break;
	}
	case 0x37D:
	{
		MSG_REQPARTY *pak = (MSG_REQPARTY*)header;
		int32_t i;
		/*
		if(hacks.party.on)
		{
			if(!strcmp(hacks.party.nick, pak->Nick) || !strcmp(hacks.party.nick, "ALL"))
			{
				Party(pak->id, pak->Nick);
			}
		}*/
		for(i = 0; i < 14; i++)
		{
			if(party[i] == pak->id)
			{
				break;
			}
		}
		if(i == 14)
		{
			for(i = 0; i < 14; i++)
			{
				if(party[i] == -1)
				{
					party[i] = pak->id;
					break;
				}
			}
		}
		break;
	}
	case 0x37E:
	{
		MSG_SIGNALPARM *pak = (MSG_SIGNALPARM*)header;
		int32_t i;
		if(pak->s_signal[0] == cID)
		{
			memset(party, -1, sizeof(party));
		}else
		{
			for(i = 0; i < 14; i++)
			{
				if(party[i] == pak->s_signal[0])
				{
					party[i] = -1;
					break;
				}
			}
		}
		break;
	}
	case 0x17C:
	{
		MSG_SENDNPCITEMS *pak = (MSG_SENDNPCITEMS*)header;
		int32_t i;
		for(i = 0; i < 64; i++)
		{
            if(npcs[i].Index != 0 && npcs[i].Time == 0)
			{
				int32_t idx = npcs[i].Index;
				memcpy(&npcs[i], pak, sizeof(MSG_SENDNPCITEMS));
				npcs[i].Index = idx;
				for(int32_t j = 0; j < 27; j++)
				{
					if(pak->Items[j].ItemID != 0)
					{
						DebugLog("NPCITEM[%d]: Slot[%d/%d]; ID[%d]", idx, j, (j % 9) + ((j / 9) * 27), pak->Items[j].ItemID);
					}
				}
                break;
			}
		}
		break;
	}
	case 0x364:
	{
		MSG_SPAWN *pak = (MSG_SPAWN*)header;
		int32_t i;
		memcpy(&mobs[pak->Idx], pak, sizeof(MSG_SPAWN));
		MapBit(pak->Current.x, pak->Current.y, 1);
		if(cID <= 0)
		{
			cID = pak->Idx;
			DebugLog("ID: %d", pak->Idx);
		}
		for(i = 0; i < 16; i++)
		{
			if(pak->MobName[i] == '^')
			{
				break;
			}
		}
		if(i != 16)
		{
			mobs[pak->Idx].Key = 1;
			break;
		}
		mobs[pak->Idx].Key = 0;
		if(pak->Idx >= 1000)
		{
			if(pak->Score.Dir.Merchant == 0)
			{
				InsertMacro(pak->Idx);
			}else
			{
				DebugLog("NPC: Nick[%s]; ID[%d]; Merchant[%d]", pak->MobName, pak->Idx, pak->Score.Dir.Merchant);
				if(pak->Score.Dir.Merchant == 1)
				{
					int32_t j;
					for(j = 0; j < 64; j++)
					{
						if(npcs[j].Index == pak->Idx)
						{
							break;
						}
					}
					if(j == 64)
					{
						for(j = 0; j < 64; j++)
						{
							if(npcs[j].Index == 0)
							{
								npcs[j].Index = pak->Idx;
								break;
							}
						}
					}
					if(j != 64)
					{
						if(npcs[j].Time == 0)
						{
							MSG_REQNPCITEMS request_npc_itens(cID);
							request_npc_itens.npcID = pak->Idx;
							request_npc_itens.Unknown = 0;
							//SendPacket(sizeof(request_npc_itens), &request_npc_itens);
							InsertPacket(time(0)+1, &request_npc_itens);
						}
					}
				}
			}
		}else if(pak->Idx == cID)
		{
			if(hacks.macro.re && hacks.macro.re_c == -1)
			{
				hacks.macro.re_c = 20 + rand() % 10 - 5;
				//Macro(hacks.macro.tipo);
			}
		}else if(pak->Idx < 1000)
		{
			char _nick[16] = "";
			memcpy(_nick, pak->PlayerName, sizeof(pak->PlayerName));
			if(hacks.party.nick[0] && !strcmp(hacks.party.nick, _nick))
			{
				for(i = 0; i < 13; i++)
				{
					if(party[i] == pak->Idx)
					{
						break;
					}
				}
				if(i == 13)
				{
					LeaveParty();
					Party(pak->Idx, hacks.party.nick);
				}
			}
			if(hacks.getout.on != 0 && pak->Score.Level >= 401)
            {
                Grifo(0);
                Disconnect();
                Disconnect(1);
            }
		}
		break;
	}
#if defined(OVER)
	case 0x366: case 0x367:
#else // OVER
	case 0x366: case 0x36C:
#endif
	{
		MSG_MOVE *pak = (MSG_MOVE*)header;
		MapBit(pak->xSrc, pak->ySrc, 0);
		MapBit(pak->xDst, pak->yDst, 1);
		if(pak->Index == cID)// && hacks.macro.on)
		{
//			if(hacks.macro.pos.x != 0)
			if(mobs[cID].Current.x != 0)
			{
//				sPoint<int16_t> pos1(pak->xDst, pak->yDst), pos2(hacks.macro.pos);
				sPoint<int16_t> pos1(pak->xDst, pak->yDst), pos2(mobs[cID].Current);
				if(dist(&pos1, &pos2) > 3) //pak->xDst != hacks.macro.pos.x || pak->yDst != hacks.macro.pos.y)
				{
					if(hacks.macro.on)
					{
						if(hacks.commands.i < hacks.commands.n)
						{
                            hacks.commands.i = hacks.commands.lastMove;
						}else
						{
							wait_recv[p_Move1] = 0;//delay_thread * 5;
							hacks.macro.on = 0;
							hacks.macro.re = 1;
							hacks.macro.re_c = 20 + rand() % 5 - 2;
						}
					}
					//Macro(hacks.macro.tipo);
				}else
				{
					wait_recv[p_Move1] = 0;
				}
			}
		}else if(pak->Index >= 1000 &&
				mobs[pak->Index].Current.x == pak->xSrc &&
				mobs[pak->Index].Current.y == pak->ySrc &&
				mobs[pak->Index].Score.Dir.Merchant == 0)
		{
			InsertMacro(pak->Index);
		}
		mobs[pak->Index].Current.x = pak->xDst;
		mobs[pak->Index].Current.y = pak->yDst;
		if(pak->Index == cID)
		{
			int32_t city = GetCity(pak->xDst, pak->yDst);
			if(last_city != city)
			{
				if(last_city != -1)
				{
					UpdateCity(city);
				}
				last_city = city;
			}
		}
		break;
	}
	case 0x165:
	{
		if(header->Index == hacks.protect.id)
		{
			hacks.protect.id = -1;
		}
		RemoveMacro(header->Index);
		mobs[header->Index].Key = 1;
		break;
	}
	case 0x185:
	{
		MSG_ATTINV *pak = (MSG_ATTINV*)header;
		int32_t i, slot;
		memcpy(pChar.Character.Inventory, pak->inv, sizeof(pak->inv));
		if(hacks.drop.on)
		{
			for(slot = 0; slot < 15; slot++)
			{
				int32_t id = pChar.Character.Inventory[slot].ItemID;
				if(id)
				{
					if(!drop_list[id])
					{
						WriteLog("Item deletado: %d(%d)", id, slot);
						DeleteItem(slot, id);
					}else if((drop_list[id] & (1 << _bau)) != 0)
					{
						for(i = 0; i < MAX_SLOTS_WARE; i++)
						{
							if(pLogin.Storage[i].ItemID == 0)
							{
								MoveItem(1, slot, 2, i);
								WriteLog("Item guardado: %d(%d->%d)", id, slot, i);
								break;
							}
						}
						if(i == MAX_SLOTS_WARE)
						{
							WriteLog("Item deletado: %d(%d)", id, slot);
							DeleteItem(slot, id);
							break;
						}
					}else if((drop_list[id] & (1 << _trade)) != 0)
					{
						WriteLog("Item trocado: %d(%d)->%d", id, slot, trade_list[id]);
						Click(trade_list[id]);
						break;
					}else if((drop_list[id] & (1 << _use)) != 0)
					{
						WriteLog("Item usado: %d(%d)", id, slot);
						UseItem(1, slot, 0, 0, 0);
						break;
					}else if((drop_list[id] & (1 << _amount)) != 0)
					{
						if(amount_list[id] != slot)
						{
							if(pChar.Character.Inventory[slot].GetAmount() == MAX_AMOUNT)
							{
								for(i = 0; i < MAX_SLOTS_WARE; i++)
								{
									if(pLogin.Storage[i].ItemID == 0)
									{
										MoveItem(1, slot, 2, i);
										WriteLog("Item guardado: %d(%d->%d)", id, slot, i);
										break;
									}
								}
							}else
							{
								if(amount_list[id] == 255)
								{
									amount_list[id] = slot;
								}else
								{
									sItem *it = &pChar.Character.Inventory[amount_list[id]];
									if(it->GetAmount() < MAX_AMOUNT)
									{
										MoveItem(1, slot, 1, amount_list[id]);
									}else
									{
										amount_list[id] = slot;
									}
								}
								WriteLog("Item amontoado: %d(%d->%d)", id, slot, amount_list[id]);
							}
						}
					}else
					{
						WriteLog("Item mantido: %d(%d)", id, slot);
					}
				}
			}
		}
		break;
	}
	case 0x182:
	{
		MSG_CREATEITEM *pak = (MSG_CREATEITEM*)header;
		int32_t slot = pak->invSlot;
		if(pak->invType == 1)
		{
			if(wait_recv[pak->Code] == 0 ||
				amount_list[pak->itemData.ItemID] == slot)
			{
				pChar.Character.Inventory[slot] = pak->itemData;
				wait_recv[pak->Code] = 0;
			}else
			{
				break;
			}
		}else if(pak->invType == 2)
		{
			pLogin.Storage[slot] = pak->itemData;
		}else if(pak->invType == 0)
		{
			pChar.Character.Equip[slot] = pak->itemData;
		}
		if(hacks.drop.on)
		{
			if(pak->invType == 1)
			{
				int32_t i = 0;
				int32_t id = pak->itemData.ItemID;
				if(id != 0)
				{
					uint8_t ef[3][2];
					memset(ef, 0, sizeof(ef));
					if(!drop_list[id])
					{
						WriteLog("Item deletado: %d(%d)", id, slot);
						DeleteItem(slot, id);
						break;
					}
					if(drop_efs[id][0][0][0] != 0)
					{
                    	int32_t e;
						for(e = 0; e < 3; e++)
						{
							if(pak->itemData.Effect[e].Index != 0 &&
								pak->itemData.Effect[e].Index != EF_SANC)
							{
								int32_t j;
								for(j = 0; j < 3; j++)
								{
									if(ef[j][0] == pak->itemData.Effect[e].Index)
									{
										ef[j][1] += pak->itemData.Effect[e].Value;
										break;
									}
								}
								if(j == 3)
								{
									for(j = 0; j < 3; j++)
									{
										if(ef[j][0] == 0)
										{
											ef[j][0] = pak->itemData.Effect[e].Index;
											ef[j][1] = pak->itemData.Effect[e].Value;
											break;
										}
									}
								}
								i++;
							}
						}
						if(i == 0)
						{
							i = 3;
						}
						if(i < 3)
						{
							for(e = 0; e < 5; e++)
							{
								if(drop_efs[id][e][0][0] != 0)
								{
									int32_t j;
									for(j = 0; j < 3; j++)
									{
										int32_t k;
										if(drop_efs[id][e][j][0] == 0)
											continue;
										for(k = 0; k < 3; k++)
										{
											if(drop_efs[id][e][j][0] == ef[k][0])
											{
												if(ef[k][1] >= drop_efs[id][e][j][1])
												{
													break;
												}
											}
										}
										if(k == 3)
										{
											break;
										}
									}
									if(j == 3)
									{
										break;
									}
								}
							}
							if(e == 5)
							{
								i = 3;
							}
						}
						if(i == 3)
						{
							WriteLog("Item deletado: %d %d %d %d %d %d %d",
											id, pak->itemData.efs[0],
											pak->itemData.efs[1],
											pak->itemData.efs[2],
											pak->itemData.efs[3],
											pak->itemData.efs[4],
											pak->itemData.efs[5]);
							DeleteItem(slot, id);
							break;
						}
					}
					if((drop_list[id] & (1 << _bau)) != 0)
					{
						for(i = 0; i < MAX_SLOTS_WARE; i++)
						{
							if(pLogin.Storage[i].ItemID == 0)
							{
								MoveItem(1, slot, 2, i);
								WriteLog("Item guardado: %d(%d->%d)", id, slot, i);
								break;
							}
						}
						if(i == MAX_SLOTS_WARE)
						{
							WriteLog("Item deletado: %d(%d)", id, slot);
							DeleteItem(slot, id);
							break;
						}
					}else if((drop_list[id] & (1 << _trade)) != 0)
					{
						if(slot < 15)
						{
							WriteLog("Item trocado: %d(%d)->%d", id, slot, trade_list[id]);
							Click(trade_list[id]);
						}else
						{
							for(i = 0; i < MAX_SLOTS_WARE; i++)
							{
								if(pLogin.Storage[i].ItemID == 0)
								{
									MoveItem(1, slot, 2, i);
									WriteLog("Item guardado: %d(%d->%d)", id, slot, i);
									break;
								}
							}
							if(i == MAX_SLOTS_WARE)
							{
								WriteLog("Item deletado: %d(%d)", id, slot);
								DeleteItem(slot, id);
							}
						}
						break;
					}else if((drop_list[id] & (1 << _use)) != 0)
					{
						WriteLog("Item usado: %d(%d)", id, slot);
						UseItem(1, slot, 0, 0, 0);
						break;
					}else if((drop_list[id] & (1 << _amount)) != 0)
					{
						if(amount_list[id] != slot)
						{
							if(amount_list[id] == 255)
							{
								amount_list[id] = slot;
							}else
							{
								sItem *it = &pChar.Character.Inventory[amount_list[id]];
								if(it->GetAmount() < MAX_AMOUNT)
								{
									MoveItem(1, slot, 1, amount_list[id]);
								}else
								{
									amount_list[id] = slot;
								}
							}
							wait_recv[pak->Code] = 25;
							WriteLog("Item amontoado: %d(%d->%d)", id, slot, amount_list[id]);
						}
					}else
					{
						WriteLog("Item mantido: %d(%d)", id, slot);
					}
				}
			}
		}
		break;
	}
	case 0x338:
	{
		MSG_CNFMOBKILL *pak = (MSG_CNFMOBKILL*)header;
		if(hacks.macro.on && pak->KilledMob == cID)
		{
			WriteLog("Morto por: %s(Pos: %04d/%04d) em %04d/%04d",
								mobs[pak->Killer].PlayerName, mobs[pak->Killer].Current.x, mobs[pak->Killer].Current.y,
								mobs[cID].Current.x, mobs[cID].Current.y);
			Revive();
		}else if(pak->KilledMob == hacks.protect.id)
		{
			hacks.protect.id = -1;
		}
		break;
	}
#if defined(OVER)
	case 0x39D: case 0x39E: case 0x36C:
#else // OVER
	case 0x39D: case 0x39E: case 0x367:
#endif
	{
		MSG_ATTACKONE *pak = (MSG_ATTACKONE *)header;
		if(pak->AttackerID == cID)
		{
			wait_recv[p_Attack] = 0;
			pChar.Character.Exp = pak->CurrentExp;//?
		}
		if(pak->AttackerID >= 1000 && pak->TargetID < 1000)
		{
			if(mobs[pak->AttackerID].Score.Dir.Merchant == 0)
			{
				InsertMacro(pak->AttackerID);
			}
		}else if(hacks.protect.on && pak->AttackerID < 1000 && pak->TargetID < 1000)
		{
			int32_t i;
			for(i = 0; i < 13; i++)
			{
				if(party[i] == pak->AttackerID)
				{
					break;
				}
			}
			if(i == 13)
			{
				for(i = 0; i < 13; i++)
				{
					if(party[i] == pak->TargetID)
					{
						if(hacks.macro.tipo != 2)
						{
							hacks.protect.id = pak->AttackerID;
						}else
						{
							hacks.protect.id = pak->TargetID;
						}
						break;
					}
				}
			}
		}
		break;
	}
	case 0x181:
	{
		MSG_HPMP *pak = (MSG_HPMP*)header;
		if(pak->Index == cID)
		{
			if(CheckHP(pak->maxhp, pak->hp, EF_HP) == 1)
			{
				CheckHP(pak->maxmp, pak->mp, EF_MP);
			}
			if(pChar.Character.Equip[MOUNT].ItemID >= 2360 &&
				pChar.Character.Equip[MOUNT].ItemID <= 2390)
			{
				CheckHP(100,
						pChar.Character.Equip[MOUNT].Mount.Food,
						pChar.Character.Equip[MOUNT].ItemID,
						1);
			}
		}
		break;
	}
	case 0x336:
	{
		MSG_UPDATESCORE *pak = (MSG_UPDATESCORE*)header;
		if(pak->Index == cID)
		{
			memcpy(&pChar.Character.Status, &pak->Score, sizeof(SCORE_757));
			pChar.Character.GuildIndex = pak->Guild;
			pChar.Character.GuildMemberType = pak->GuildLevel;
			pChar.Character.Resist1 = pak->Resist[0];
			pChar.Character.Resist2 = pak->Resist[1];
			pChar.Character.Resist3 = pak->Resist[2];
			pChar.Character.Resist4 = pak->Resist[3];
			pChar.Character.MagicIncrement = pak->Magic;
			pChar.Character.RegenHP = pak->RegHp;
			pChar.Character.RegenMP = pak->RegMp;
			if(CheckHP(pak->Score.MaxHP, pak->Score.CurHP, EF_HP) == 1)
			{
				CheckHP(pak->Score.MaxMP, pak->Score.CurMP, EF_MP);
			}
			memcpy(&mobs[cID].Score, &pak->Score, sizeof(SCORE_757));
		}
		break;
	}
	case 0x3AF:
	{
		MSG_SIGNALPARM *pak = (MSG_SIGNALPARM*)header;
		pChar.Character.Gold = pak->signal;
		break;
	}
	case 0x337:
	{
		MSG_UPDATEETC *pak = (MSG_UPDATEETC*)header;
		if(pak->Index == cID)
		{
			pChar.Character.Exp = pak->Exp;
			pChar.Character.Gold = pak->Coin;
		}
		break;
	}
	}
	if(header->Code != 0x10E && header->Code != 0x10A && cID == -1)
	{
		Disconnect();
	}
}

void Client::xDump(int32_t size, void *ptr)
{
	int32_t i, j, i_max;
	char temp[196] = "";
	DebugLog("xRecv[%d]", size);
	i_max = size/16;
	if(i_max > MAX_LINE_DUMP)
	{
		i_max = MAX_LINE_DUMP;
		size %= MAX_LINE_DUMP * 16;
	}
	for(i = 0; i < i_max; i++)
	{
		for(j = 0; j < 4; j++)
			sprintf(&temp[j*9], "%02X%02X%02X%02X ",
					((u_char*)ptr)[i * 16 + (j*4) + 0],
					((u_char*)ptr)[i * 16 + (j*4) + 1],
					((u_char*)ptr)[i * 16 + (j*4) + 2],
					((u_char*)ptr)[i * 16 + (j*4) + 3]);
		for(j = 0; j < 4; j++)
			sprintf(&temp[j*5 + 36], "%c%c%c%c ",
				is_print(((u_char*)ptr)[i * 16 + (j*4) + 0]),
				is_print(((u_char*)ptr)[i * 16 + (j*4) + 1]),
				is_print(((u_char*)ptr)[i * 16 + (j*4) + 2]),
				is_print(((u_char*)ptr)[i * 16 + (j*4) + 3]));
		DebugLog("%s", temp);
	}
	for(j = 0; j < size%16; j++)
	{
		sprintf(&temp[j*3],  "%02X ", ((uint8_t*)ptr)[i * 16 + j]);
	}
	if( j )
		DebugLog("%s", temp);
}

void Client::Dump(bool send, int32_t size, void *ptr)
{
#if defined(_DEBUG)
	if(opcodes[*(u_short*)&((u_char*)ptr)[4]])
	{
		int32_t i, j, i_max;
		char temp[196] = "";
		DebugLog("%s %d:%04X %u %u", send ? "Send" : "Recv", size, *(u_short*)&((u_char*)ptr)[4], *(uint32_t*)&((uint8_t*)ptr)[8], timeGetTime());
		if(size > (int32_t)sizeof(MSG_ATTACKMULT))
		{
			size = (int32_t)sizeof(MSG_ATTACKMULT);
		}
		i_max = size/16;
		for(i = 0; i < i_max; i++)
		{
			for(j = 0; j < 4; j++)
				sprintf(&temp[j*9], "%02X%02X%02X%02X ",
						((u_char*)ptr)[i * 16 + (j*4) + 0],
						((u_char*)ptr)[i * 16 + (j*4) + 1],
						((u_char*)ptr)[i * 16 + (j*4) + 2],
						((u_char*)ptr)[i * 16 + (j*4) + 3]);
			for(j = 0; j < 4; j++)
				sprintf(&temp[j*5 + 36], "%c%c%c%c ",
					is_print(((u_char*)ptr)[i * 16 + (j*4) + 0]),
					is_print(((u_char*)ptr)[i * 16 + (j*4) + 1]),
					is_print(((u_char*)ptr)[i * 16 + (j*4) + 2]),
					is_print(((u_char*)ptr)[i * 16 + (j*4) + 3]));
			DebugLog("%s", temp);
		}
		for(j = 0; j < size%16; j++)
		{
			sprintf(&temp[j*3],  "%02X ", ((uint8_t*)ptr)[i * 16 + j]);
		}
		if( j )
			DebugLog("%s", temp);
	}
#endif
}

/** Socket **/
void Client::ReadPacket()
{
	int32_t recebido;
	MSG_HEADER *header;
	int32_t i;
	int32_t key;
	char *buffer_enc, cmp_enc, cmp_dec;
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 60 * 1000 * 1000;
	if(s == INVALID_SOCKET)
		return;
	recebido = select(s+1, &set, 0, 0, &timeout);
	if(recebido <= 0 || FD_ISSET(s, &set) == 0)
	{
		//DebugLog("#Select: %d", recebido);
		Disconnect(1);
		return;
	}
	recebido = recv(s, &buffer_recv[pos_recv], MAX_BUFFER - pos_recv, 0);
	if(recebido <= 0 || recebido == (MAX_BUFFER - pos_recv))
	{
		//DebugLog("#Recv: %d", recebido);
		Disconnect(1);
		return;
	}else
	{
		GetTick();
		pos_recv += recebido;
		while( 1 )
		{
			if( pos_decrypt >= pos_recv )
			{
				pos_decrypt = pos_recv = 0;
				return;
			}
			if( pos_recv - pos_decrypt < 12 )
			{
				return;
			}
			buffer_enc = &buffer_recv[pos_decrypt];
			header = (MSG_HEADER*)buffer_enc;
//			xDump(recebido, buffer_enc);
			if( header->Size < 12 || header->Size > 0x1888 )
			{
				pos_decrypt = pos_recv = 0;
				return;
			}
			if( header->Size > pos_recv - pos_decrypt )
				return;
			pos_decrypt += header->Size;
			if( pos_recv <= pos_decrypt )
				pos_decrypt = pos_recv = 0;
			i = 4;
			key = (uint8_t)keys[header->Key << 1];
			cmp_enc = 0, cmp_dec = 0;
			do
			{
				uint32_t key_hash = (uint8_t)keys[((key % 256) << 1) + 1];
				cmp_enc += buffer_enc[i];
				switch( i & 3 )
				{
				case 0:
					buffer_enc[i] -= (uint32_t)(key_hash << 1);
					break;
				case 1:
					buffer_enc[i] += (uint32_t)((int32_t)key_hash >> 3);
					break;
				case 2:
					buffer_enc[i] -= (uint32_t)(key_hash << 2);
					break;
				case 3:
					buffer_enc[i] += (uint32_t)((int32_t)key_hash >> 5);
					break;
				}
				cmp_dec += buffer_enc[i];
				i++;
				key++;
			} while( i < header->Size );
#if defined(DON)
			if( (uint8_t)(cmp_dec) != header->Hash )
#else // DON
			if( (uint8_t)(cmp_enc - cmp_dec) != header->Hash )
#endif
			{
				DebugLog("hash error: %d %d", (uint8_t)(cmp_enc - cmp_dec), header->Hash );
				return;
			}
			pak_timer[0] = header->Time;
			pak_timer[1] = timeGetTime();
			uint32_t _158 = getTimerRecv();
			recv_timer[0] = header->Time;
			recv_timer[1] = _158 / 1000;
			if(_158 > recv_timer[2]+10000)
			{
				if(recv_timer[0] > _158 + 500)
					attTimerRecv(_158+85);
				else if(recv_timer[0] < _158 - 500)
					attTimerRecv(_158-85);
				recv_timer[2] = _158;
			}
			PacketControl(header);
		}
	}
}

uint32_t Client::getTimerRecv()
{
	uint32_t tmp[6];//-4, -8, -c, -10, -14, -18
	tmp[2] = timeGetTime() - class_timer[2];
	tmp[1] = class_timer[1] + tmp[2];
	static_timer[0] = tmp[1];
	tmp[0] = tmp[2] / 60000;//ebp-4
	if(tmp[0] != class_timer[6])
	{
		attTime(&tmp[3]);
		tmp[4] = tmp[3] - class_timer[5];
		tmp[5] = tmp[2] / 1000;
		if(tmp[4] != tmp[5] && class_timer[6] != 0)
		{
			DebugLog("Timer errado!"); /* Est� mensagem n�o atrapalha nada */
		}
        class_timer[6] = tmp[0];
	}
	return tmp[1] + class_timer[4];
}

void Client::attTimerRecv(uint32_t _new)
{
	class_timer[0] = class_timer[1] = _new;
	class_timer[2] = timeGetTime();
	attTime(&class_timer[5]);
}

void Client::attTime(uint32_t *t)
{
//	SYSTEMTIME local, sys;
//	GetLocalTime(&local);
//	GetSystemTime(&sys);
	if(t != 0)
	{
		*t = time(0);
	}
}


void Client::SendPacket(int32_t size, MSG_HEADER *pak)
{
	if(pak->Index < 0)
		pak->Index = 0;
	/* */
	if( s != INVALID_SOCKET )
	{
		int32_t key, i;//[10C] [128]
		uint32_t sucess;//
		uint32_t key_hash;//[110/124]
		char *buffer_enc = &buffer_send[pos_encrypt], *buffer_dec = (char*)pak, cmp_enc = 0, cmp_dec = 0;
		MSG_HEADER *pak_enc = (MSG_HEADER*)buffer_enc;
		pak->Time = static_timer[0];// + rand() % 150 - 85;
		static_timer[1] = static_timer[0];
		send_tick = timeGetTime();
		if( (size + pos_encrypt) >= MAX_BUFFER  || size < 12 )
			return;
		if(  keys_count == -1 )
			key = rand() % 256;
		else
		{
			int32_t temp_keys = 0;
			if( keys_count < 16 )
			{
				temp_keys = login_keys[keys_count];
				keys_count++;
			}else
			{
				if( (login_keys[15] % 2) == 0 )
				{
					#if !defined(MYD)
					temp_keys = login_keys[1] + login_keys[3] + login_keys[5] - 0x57;
					#else
					temp_keys = login_keys[1] + login_keys[3] + login_keys[5] - 0x73;
					#endif
				}else
				{
					#if !defined(MYD)
					temp_keys = login_keys[13] + login_keys[11] - login_keys[9] + 4;
					#else
					temp_keys = login_keys[13] + login_keys[11] + login_keys[9] + 20;
					#endif
				}
			}
			key = (uint8_t)(temp_keys ^ 255);
		}
		key = 0;
		key_hash = (uint8_t)keys[key << 1];
		pak_enc->Size = size;
		pak_enc->Key = key;
		if(dump_send)
		{
			pak->Size = size;
			Dump(true, size, pak);
		}
		DebugLog("SPacket: [%d][%04X][%d]", size, pak->Code, pak->Index);
		/** packet_debug **/
		i = 4;
		do
		{
			uint32_t key_now = (uint8_t)keys[((key_hash % 256) << 1) + 1];
			cmp_dec += buffer_dec[i];
			switch( i & 3 )
			{
			case 0:
					buffer_enc[i] = buffer_dec[i] + (uint32_t)(key_now << 1);
				break;
			case 1:
					buffer_enc[i] = buffer_dec[i] - (uint32_t)((int32_t)key_now >> 3);
				break;
			case 2:
					buffer_enc[i] = buffer_dec[i] + (uint32_t)(key_now << 2);
				break;
			case 3:
					buffer_enc[i] = buffer_dec[i] - (uint32_t)((int32_t)key_now >> 5);
				break;
			default:
				break;
			}
			cmp_enc += buffer_enc[i];
			i++;
			key_hash++;
		} while( i < size );
#if defined(DON)
		pak_enc->Hash = (uint8_t)cmp_dec;
#else
		pak_enc->Hash = (uint8_t)(cmp_enc - cmp_dec);
#endif
		pos_encrypt += size;
		if( pos_encrypt < 0 || pos_encrypt > MAX_BUFFER )
			return;
		if( pos_send < 0 || pos_send >= MAX_BUFFER || pos_send > pos_encrypt )
		{
			pos_send = 0;
			pos_encrypt = 0;
		}
		sucess = send(s, buffer_enc, size, 0);
		if(sucess == INVALID_SOCKET)
		{
			Disconnect();
			return;
		}
		pos_send += sucess;
		if( pos_send >= pos_encrypt )
			pos_send = pos_encrypt = 0;
		if( pos_encrypt >= MAX_BUFFER )
			return;//?
	}
}

void Client::Disconnect(int32_t c)
{
    if(cID != -1)
    {
        DebugLog("Desconectado(%d).", s);
        WriteLog("Desconectado.");
        cID = -1;
    }else if(cID == -1 && c != 0 && s != INVALID_SOCKET)
    {
    #if defined(_WIN32)
        shutdown(s, SD_RECEIVE);
        closesocket(s);
        s = INVALID_SOCKET;
    #else
        close(s);
        s = INVALID_SOCKET;
    #endif
        if(hacks.macro.re_l == -1)
        {
            hacks.macro.re_l = 15 + rand() % 5 - 2;
        }
        if(planos == free_)
		{
			remove("./fechar");
			exit(1);
		}
    }
}

int32_t Client::Connect(char *pproxy_ip, int32_t pproxy_port)
{
	char proxy_ip[32] = "";
	int32_t proxy_port = 0;
	uint32_t proxy;
	uint32_t sock;
	SOCKADDR_IN serverInfo;
	SOCKADDR_IN proxyInfo;
    struct timeval timeout;
    int32_t ret;
    uint32_t err;
    /* TIMEOUT */
    timeout.tv_sec = 0;
    timeout.tv_usec = TIMEOUT_PROXY * 1000 * 1000;
    FD_ZERO(&set);

	memset(&serverInfo, 0, sizeof(serverInfo));

	serverInfo.sin_family = AF_INET;
	if(novato)
	{
		strcpy(serverIP, GetIPServerlist(sv, ch));
	}
	serverInfo.sin_addr.s_addr = inet_addr(serverIP);
	serverInfo.sin_port = htons(serverPort);
	if(planos != test_ && pproxy_port != 0)
    {
        strcpy(proxy_ip, pproxy_ip);
           proxy_port = pproxy_port;
	}
	if(proxy_ip[0])
	{

		DebugLog("Proxy: %s:%d", proxy_ip, proxy_port);

		/* PROXY */
		proxy = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if(proxy == INVALID_SOCKET)
		{
	#if defined(_WIN32)
			DebugLog("Erro-1 proxy! %d", GetLastError());
	#else
			DebugLog("Erro-1 proxy! %d", errno);
	#endif
			return 0;
		}

        FD_SET(proxy, &set);
    #if defined(_WIN32)
        long unsigned int iMode = 1;
        ioctlsocket(proxy, FIONBIO, &iMode);
    #else // _WIN32
        int32_t flags;
        flags = fcntl(proxy, F_GETFL, 0);
        flags |= O_NONBLOCK;
        fcntl(proxy, F_SETFL, flags);
    #endif

		proxyInfo.sin_family = AF_INET;
		proxyInfo.sin_addr.s_addr = inet_addr(proxy_ip);
		proxyInfo.sin_port = htons(proxy_port);
		if(connect(proxy, (struct sockaddr *)&proxyInfo, sizeof(proxyInfo)) < 0)
		{
	#if defined(_WIN32)
			err = GetLastError();
            if(err != WSAEWOULDBLOCK)
	#else
			err = errno;
            if(err != EINPROGRESS)
	#endif
            {
                DebugLog("Erro-2 proxy! %d", err);
                close(proxy);
                return 0;
            }
		}

        ret = select(proxy+1, 0, &set, NULL, &timeout);
        if(ret == 0)
        {
	#if defined(_WIN32)
			DebugLog("Erro-T proxy! %d", GetLastError());
	#else
			DebugLog("Erro-T proxy! %d", errno);
	#endif
			close(proxy);
			return 0;
        }else if(ret == -1)
        {
	#if defined(_WIN32)
			DebugLog("Erro-S proxy! %d", GetLastError());
	#else
			DebugLog("Erro-S proxy! %d", errno);
	#endif
			close(proxy);
			return 0;
        }

    #if defined(_WIN32)
        iMode = 0;
        ioctlsocket(proxy, FIONBIO, &iMode);
    #else // _WIN32
        flags = fcntl(proxy, F_GETFL, 0);
        flags &= ~(O_NONBLOCK);
        fcntl(proxy, F_SETFL, flags);
    #endif
		uint8_t proxyPacket[16] = {5, 1, 0};
		if(send(proxy, (char*)proxyPacket, 3, 0) == -1)
		{
			DebugLog("Proxy[send-1] Fail!");
			close(proxy);
			return 0;
		}
		if(recv(proxy, (char*)proxyPacket, 2, 0) == -1)
		{
			DebugLog("Proxy[recv-1] Fail!");
			close(proxy);
			return 0;
		}
		if(proxyPacket[0] == 5 && proxyPacket[1] != 0xFF)
		{
			int32_t ret;
			char log[64] = "";
			DebugLog("Proxy[1] OK");
			proxyPacket[1] = 1;//TCP
			proxyPacket[2] = 0;
			proxyPacket[3] = 1;//IPv4
			memcpy(&proxyPacket[4],
					&serverInfo.sin_addr.s_addr,
					sizeof(serverInfo.sin_addr.s_addr));
			*(u_short*)&proxyPacket[8] = serverInfo.sin_port;
			if(send(proxy, (char*)proxyPacket, 10, 0) == -1)
			{
				DebugLog("Proxy[send-2] Fail!");
				close(proxy);
				return 0;
			}
			ret = recv(proxy, (char*)proxyPacket, sizeof(proxyPacket)-1, 0);
			if(ret == -1)
			{
				DebugLog("Proxy[recv-2] Fail!");
				close(proxy);
				return 0;
			}
			for(int32_t c = 0; c < ret; c++)
			{
                sprintf(&log[c*3], "%02X ", proxyPacket[c]);
			}
			DebugLog("Proxy[-] : %s", log);
			if(proxyPacket[0] == 5 && proxyPacket[1] == 0)
			{
				DebugLog("Proxy[2] OK-%d", ret);
			}else
			{
				DebugLog("Erro-3 proxy(%d)!", proxyPacket[1]);
				close(proxy);
				return 0;
			}
		}else
		{
			DebugLog("Erro-4 proxy(%d)!", proxyPacket[1]);
            close(proxy);
			return 0;
		}
		sock = proxy;
	}else
	{
		sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if(sock == INVALID_SOCKET)
		{
	#if defined(_WIN32)
			DebugLog("Erro socket! %d", GetLastError());
	#else
			DebugLog("Erro socket! %d", errno);
	#endif
			return 0;
		}

        FD_SET(sock, &set);
    #if defined(_WIN32)
        long unsigned int iMode = 1;
        ioctlsocket(sock, FIONBIO, &iMode);
    #else // _WIN32
        int32_t flags;
        flags = fcntl(sock, F_GETFL, 0);
        flags |= O_NONBLOCK;
        fcntl(sock, F_SETFL, flags);
    #endif

		if(connect(sock, (struct sockaddr *)&serverInfo, sizeof(serverInfo)) < 0)
		{
	#if defined(_WIN32)
			err = GetLastError();
            if(err != WSAEWOULDBLOCK)
	#else
			err = errno;
            if(err != EINPROGRESS)
	#endif
            {
                DebugLog("Erro connect! %d", err);
                close(sock);
                return 0;
            }
		}

        ret = select(sock+1, 0, &set, NULL, &timeout);
        if(ret == 0)
        {
	#if defined(_WIN32)
			DebugLog("Erro-T sock! %d", GetLastError());
	#else
			DebugLog("Erro-T sock! %d", errno);
	#endif
			close(sock);
			return 0;
        }else if(ret == -1)
        {
	#if defined(_WIN32)
			DebugLog("Erro-S sock! %d", GetLastError());
	#else
			DebugLog("Erro-S sock! %d", errno);
	#endif
			close(sock);
			return 0;
        }

    #if defined(_WIN32)
        iMode = 0;
        ioctlsocket(sock, FIONBIO, &iMode);
    #else // _WIN32
        flags = fcntl(sock, F_GETFL, 0);
        flags &= ~(O_NONBLOCK);
        fcntl(sock, F_SETFL, flags);
    #endif
	}
	srand(time(0));


	hacks.macro.on = 0;
	hacks.commands.c = -1;
	hacks.commands.ciclos = -1;
	hacks.commands.contador = 0;
	hacks.commands.i = 0;
	hacks.macro.re_l = -1;

	s = sock;
	#if defined(_WIN32)
	DebugLog("Conex�o criada com sucesso[#%d](%d)[%s:%d].", GetCurrentProcessId(), s, serverIP, serverPort);
	#else // _WIN32
	DebugLog("Conex�o criada com sucesso[#%d/%d](%d)[%s:%d].", getpid(), getppid(), s, serverIP, serverPort);
	#endif
	WriteLog("Conex�o criada com sucesso.");
	Initialize();
    #if defined(_WIN32)
	long unsigned int iMode = 1;
	ioctlsocket(s, FIONBIO, &iMode);
    #else // _WIN32
	int32_t flags;
	flags = fcntl(s, F_GETFL, 0);
	flags |= O_NONBLOCK;
	fcntl(s, F_SETFL, flags);
    #endif
    FD_ZERO(&set);
    FD_SET(s, &set);
	return 1;
}

template <typename T>
void Opera(T *a, T b, uint8_t op, uint32_t d)
{
	switch(op)
	{
		case '*':
		{
			*a *= b;
			break;
		}
		case '+':
		{
			*a += b;
			break;
		}
		case '-':
		{
			*a -= b;
			break;
		}
		case '/':
		{
			*a /= b;
			break;
		}
		case '|':
		{
			*a |= b;
			break;
		}
		case '&':
		{
			*a &= b;
			break;
		}
		case '^':
		{
			*a ^= b;
			break;
		}
		case '=':
		{
			*a = b;
			break;
		}
		case 'r':
		{
			*a |= rand() % (d & 0xFFFF) + ((d >> 16) & 0xFFFF);
			break;
		}
	}
}

void Client::Initialize()
{
	uint32_t hello = 0x1F11F311;
	MSG_REQLOGIN packet(0);
	uint8_t *pak = (uint8_t*)&packet;
	keys_count = 0; pos_recv = 0; pos_decrypt = 0; pos_send = 0; pos_encrypt = 0;
	send(s, (char*)&hello, 4, 0);
    packet.Size = sizeof(packet);
    for(int32_t i = 0; i < packet.Size; i++)
	{
		for(int32_t j = 0; j < 8; j++)
		{
			switch(mLogin[i][j][0])
			{
			case 'c':
				{
					switch(mLogin[i][j][1])
					{
					case 'l':
						{
							strcpy((char*)&pak[i], login);
							break;
						}
					case 's':
						{
							strcpy((char*)&pak[i], senha);
							break;
						}
					case 'c':
						{
							*(uint32_t*)&pak[i] = cliver;
							break;
						}
					case 'm':
						{
							memcpy(&pak[i], client_mac, 16);
							break;
						}
					}
					break;
				}
			case 'b':
				{
					Opera((uint8_t*)&pak[i], (uint8_t)dLogin[i][j], mLogin[i][j][1], dLogin[i][j]);
					break;
				}
			case 'B':
				{
					Opera((int8_t*)&pak[i], (int8_t)dLogin[i][j], mLogin[i][j][1], dLogin[i][j]);
					break;
				}
			case 's':
				{
					Opera((uint16_t*)&pak[i], (uint16_t)dLogin[i][j], mLogin[i][j][1], dLogin[i][j]);
					break;
				}
			case 'S':
				{
					Opera((int16_t*)&pak[i], (int16_t)dLogin[i][j], mLogin[i][j][1], dLogin[i][j]);
					break;
				}
			case 'i':
				{
					Opera((uint32_t*)&pak[i], (uint32_t)dLogin[i][j], mLogin[i][j][1], dLogin[i][j]);
					break;
				}
			case 'I':
				{
					Opera((int32_t*)&pak[i], (int32_t)dLogin[i][j], mLogin[i][j][1], dLogin[i][j]);
					break;
				}
			case 'l':
				{
					Opera((uint64_t*)&pak[i], (uint64_t)dLogin[i][j], mLogin[i][j][1], dLogin[i][j]);
					break;
				}
			case 'L':
				{
					Opera((int64_t*)&pak[i], (int64_t)dLogin[i][j], mLogin[i][j][1], dLogin[i][j]);
					break;
				}
			}
		}
	}
    InsertPacket(time(0) + 1, &packet, 64);
}

void Client::ReadConfig()
{
	ifstream fp("./filter.txt");
	char line[2048] = "";
	int32_t tipo = _nil;
	if (fp.is_open())
	{
		int32_t n = 0;
		memset(amount_list, 255, sizeof(amount_list));
		memset(drop_list, 0, sizeof(drop_list));
		memset(trade_list, 0, sizeof(trade_list));
		memset(opcodes, 0, sizeof(opcodes));
		memset(skillbar, 0, sizeof(skillbar));
		strcpy(dir_data, "./bot.dat");
		strcpy(dir_log, "./bot.log");
		hacks.party.nick[0] = 0;
		hacks.party.on = 0;
		hacks.protect.on = 0;
		hacks.commands.c = -1;
		hacks.commands.n = 0;
		hacks.commands.i = 0;
		hacks.commands.loop = 0;
		hacks.commands.repeat = -1;
		hacks.macro.hp[0] = -1;
		hacks.macro.hp[1] = -1;
        labels.clear();
		fproxy_ip[0] = 0;
		fproxy_port = 0;
		loginID = 0x20D;
		novato = 0;
		DebugLog("ReadConfig()-Filter");
		while (fp.good())
		{
			fp.getline(line, sizeof(line));
			for(int32_t c = 0; c < 256; c++)
			{
				if(line[c] == 0)
					break;
				if(line[c] == '\r' || line[c] == '\n')
				{
					line[c] = 0;
					break;
				}
			}
			DebugLog("%s", line);
			if(line[0] == '#' && tipo != _commands)
			{
				if(!strcmp(line, "#amount"))
				{
					tipo = _amount;
				}else if(!strcmp(line, "#bau"))
				{
					tipo = _bau;
				}else if(!strcmp(line, "#drop"))
				{
					tipo = _drop;
				}else if(!strcmp(line, "#trade"))
				{
					tipo = _trade;
				}else if(!strcmp(line, "#use"))
				{
					tipo = _use;
				}else if(!strcmp(line, "#packets"))
				{
					tipo = _packet;
				}else if(!strcmp(line, "#buffs"))
				{
					tipo = _buff;
				}else if(!strcmp(line, "#skills"))
				{
					tipo = _skill;
				}else if(!strcmp(line, "#pot"))
				{
					tipo = _pot;
				}else if(!strcmp(line, "#mount"))
				{
					tipo = _mount;
				}else if(!strcmp(line, "#protect"))
				{
	#if defined(ENABLE_PROTECT)
					hacks.protect.on = 1;
	#endif
				}else if(!strcmp(line, "#commands"))
				{
					tipo = _commands;
				}else if(!strcmp(line, "#servidor"))
				{
					tipo = _servidor;
				}else if(!strcmp(line, "#plano"))
				{
					tipo = _plano;
				}else if(!strcmp(line, "#party"))
				{
					tipo = _party;
				}else if(!strcmp(line, "#troca"))
				{
					tipo = _troca;
				}else if(!strcmp(line, "#autovenda"))
				{
					tipo = _autovenda;
				}else if(!strcmp(line, "#chave"))
				{
					tipo = _chave;
				}else if(!strcmp(line, "#login"))
				{
					tipo = _login;
				}else if(!strcmp(line, "#senha"))
				{
					tipo = _senha;
				}else if(!strcmp(line, "#senha2"))
				{
					tipo = _senha2;
				}else if(!strcmp(line, "#slot"))
				{
					tipo = _slot;
				}else if(!strcmp(line, "#server"))
				{
					tipo = _sv;
				}else if(!strcmp(line, "#canal"))
				{
					tipo = _ch;
				}else if(!strcmp(line, "#proxy"))
				{
					tipo = _proxy;
				}else if(!strcmp(line, "#pcdata"))
				{
					tipo = _pcdata;
				}else if(!strcmp(line, "#mac"))
				{
					tipo = _mac;
				}else if(!strcmp(line, "#rede"))
				{
					tipo = _rede;
				}else if(line[0] == '#')
				{
					tipo = _nil;
				}
			}else if(strlen(line) > 0 && (line[0] != '/' || tipo == _commands) && isspace(line[0]) == 0)
			{
				if(tipo == _amount)
				{
					int32_t x = atoi(line);
					if(x >= 0 && x < 9000)
					{
						drop_list[x] |= (1 << tipo);
						amount_list[x] = 255;
						n++;
					}
				}else if(tipo == _bau || tipo == _drop || tipo == _use)
				{
					int32_t id;
					int32_t c = 0;
					int32_t k = 0;
					char *tok = strtok(line, "|");
					while(tok)
					{
						if(c == 0)
						{
							id = atoi(tok);
							if(id >= 0 && id < 9000)
							{
								drop_list[id] |= (1 << tipo);
								n++;
							}else
							{
								break;
							}
							c++;
						}else
						{
							int32_t v[3][2] = {{0, 0}, {0, 0}, {0, 0}};
							sscanf(tok, "%d %d %d %d %d %d",
							&v[0][0], &v[0][1], &v[1][0], &v[1][1], &v[2][0], &v[2][1]);
							drop_efs[id][k][0][0] = v[0][0];
							drop_efs[id][k][0][1] = v[0][1];
							drop_efs[id][k][1][0] = v[1][0];
							drop_efs[id][k][1][1] = v[1][1];
							drop_efs[id][k][2][0] = v[2][0];
							drop_efs[id][k][2][1] = v[2][1];
							k++;
						}
						tok = strtok(0, "|");
					}
				}else if(tipo == _pot)
				{
					sscanf(line, "%d %d", &hacks.macro.hp[0], &hacks.macro.hp[1]);
				}else if(tipo == _trade)
				{
					int32_t x = -1, y = 0;
					sscanf(line, "%d %d", &x, &y);
					if(x >= 0 && x < 9000)
					{
						trade_list[x] = y;
						drop_list[x] |= (1 << tipo);
						n++;
					}
				}else if(tipo == _packet)
				{
					int32_t x = 0;
					sscanf(line, "%x", &x);
					if(x >= 0 && x < 0xFFFF)
						opcodes[x] = 1;
					else if(x == 0x10000)
					{
						memset(opcodes, 1, sizeof(opcodes));
					}
				}else if(tipo == _buff || tipo == _skill)
				{
					int32_t x = atoi(line);
					if(x >= 0 && x < 20)
					{
						skillbar[x] |= (1 << tipo);
						n++;
					}
				}else if(tipo == _servidor)
				{
					strcpy(servidor, line);
					if(!strcmp(servidor, "don"))
					{
						server = _don;
					}else if(!strcmp(servidor, "don2"))
					{
						server = _don2;
					}else if(!strcmp(servidor, "wyd2"))
					{
						server = _wyd2;
					}else if(!strcmp(servidor, "kofd"))
					{
						server = _kofd;
					}else if(!strcmp(servidor, "over"))
					{
						server = _over;
					}else if(!strcmp(servidor, "brazuc"))
					{
						server = _brazuc;
					}else if(!strcmp(servidor, "legacy"))
					{
						server = _legacy;
					}else if(!strcmp(servidor, "star"))
					{
						server = _star;
					}else if(!strcmp(servidor, "uow"))
					{
						server = _uow;
					}else if(!strcmp(servidor, "aon"))
					{
						server = _aon;
					}else if (!strcmp(servidor, "gd"))
					{
						server = _gd;
					}else if (!strcmp(servidor, "final"))
					{
						server = _final;
					}else if (!strcmp(servidor, "hero"))
					{
						server = _hero;
					}else
					{
						server = _kofd;
					}
				}else if(tipo == _login)
				{
					strcpy(login, line);
				}else if(tipo == _senha)
				{
					strcpy(senha, line);
				}else if(tipo == _senha2)
				{
					strcpy(senha2, line);
				}else if(tipo == _slot)
				{
					slot = atoi(line);
				}else if(tipo == _sv)
				{
					sv = atoi(line);
				}else if(tipo == _ch)
				{
					ch = atoi(line);
				}else if(tipo == _mac)
				{
					uint32_t macv[6] = {0, 0, 0, 0, 0, 0};
					if(sscanf(line, "%02X%02X%02X%02X%02X%02X", &macv[0], &macv[1], &macv[2], &macv[3], &macv[4], &macv[5]) == 6)
					{
						for(int32_t ti = 0; ti < 6; ti++)
						{
							pBullshitDon.mac[ti] = macv[ti];
						}
					}
				}else if(tipo == _rede)
				{
					uint32_t macv[16];
					if(sscanf(line, "%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
								&macv[0], &macv[1], &macv[2], &macv[3], &macv[4], &macv[5], &macv[6],
								&macv[7], &macv[8], &macv[9], &macv[10], &macv[11], &macv[12], &macv[13],
								&macv[14], &macv[15]) == 16)
					{
						for(int32_t ti = 0; ti < 16; ti++)
						{
							client_mac[ti] = macv[ti];
						}
					}
				}else if(tipo == _pcdata)
				{
					char *tokpc = strtok(line, ";");
					int32_t ti = 0;
					while(tokpc && ti < 5)
					{
						pBullshitDon.data[ti][0] = 0;
						strcpy(pBullshitDon.data[ti], tokpc);
						ti++;
						tokpc = strtok(0, ";");
					}
				}else if(tipo == _autovenda)
				{
					if(line[0] == '$')
					{
						line[24] = 0;
						strcpy(pNpcTrade.name, &line[1]);
					}else
					{
						int32_t pos = -1, price = -1;
                        if(sscanf(line, "%d %d", &pos, &price) == 2)
						{
							if(pos != -1 && price != -1)
							{
								for(int32_t p = 0; p < 12; p++)
								{
									if(pNpcTrade.price[p] == 0)
									{
										pNpcTrade.slot[p] = pos;
										pNpcTrade.price[p] = price;
										break;
									}
								}
							}
						}
					}
				}else if(tipo == _chave)
				{
					strcpy(chave, line);
				}else if(tipo == _proxy)
				{
					char *tmp;
					tmp = strstr(line, ":");
					*tmp = '\0';
					tmp++;
					strcpy(fproxy_ip, line);
					fproxy_port = atoi(tmp);
				}else if(tipo == _party)
				{
					if(strlen(line) > 2)
					{
						strcpy(hacks.party.nick, line);
						hacks.party.on = 1;
					}
				}else if(tipo == _troca)
				{
					if(strlen(line) > 2)
					{
						strcpy(hacks.trade.nick, line);
					}
				}else if(tipo == _plano)
				{
					if(!strcmp(line, "free"))
					{
                        planos = free_;
					}else if(!strcmp(line, "test"))
					{
						planos = test_;
					}else
					{
						planos = money_;
					}
				}else if(tipo == _commands)
				{
					if(line[0] == ':')
                    {
                    	if(labels.find(line) == labels.end())
                        {
                        	labels[line] = hacks.commands.n;
                        }
                    }else if(!strcmp(line, "#loop"))
					{
						hacks.commands.loop = 1;
					}else if(!strcmp(line, "#repeat"))
					{
						/*if(!strncmp(hacks.commands.cmd[0], "#macro", 6))
						{
							memcpy(hacks.commands.cmd[hacks.commands.n], hacks.commands.cmd[0], 32);
							memset(hacks.commands.cmd[0], 0, 32);
							hacks.commands.n++;
						}*/
						hacks.commands.repeat = hacks.commands.n;
					}else if(!strcmp(line, "#novato"))
					{
						novato = 1;
					}else
					{
						strcpy(hacks.commands.cmd[hacks.commands.n], line);
						hacks.commands.n++;
					}
				}
			}
		}
		if(hacks.commands.repeat == -1)
		{
			/*if(!strncmp(hacks.commands.cmd[0], "#macro", 6))
			{
				memcpy(hacks.commands.cmd[hacks.commands.n], hacks.commands.cmd[0], 32);
				memset(hacks.commands.cmd[0], 0, 32);
				hacks.commands.n++;
			}*/
		}
		if(n == 0)
		{
			for(int32_t k = 0; k < 9000; k++)
			{
				drop_list[k] |= (1 << _drop);
			}
		}
		strcpy(serverIP, GetIPServerlist(sv, ch));
		fp.close();
	}

	fp.open(DIR_BASE+string(servidor)+"/servidor.cfg", ifstream::in);
	if(fp.is_open())
	{
		DebugLog("ReadConfig()-Servidor");
		memset(mLogin, 0, sizeof(mLogin));
		memset(dLogin, 0, sizeof(dLogin));
		while (fp.good())
		{
			fp.getline(line, sizeof(line) - 1);
			if(line[0] == '/')
			{
				continue;
			}
			for(int32_t c = 0; c < 256; c++)
			{
				if(line[c] == 0)
					break;
				if(line[c] == '\r' || line[c] == '\n')
				{
					line[c] = 0;
					break;
				}
			}
			DebugLog("%s", line);
			if(line[0] == '#')
			{
				if(!strncmp(line, "#cliver", 7))
				{
					tipo = _cliver;
				}else if(!strncmp(line, "#porta", 6))
				{
					tipo = _porta;
				}else if(!strncmp(line, "#delay", 6))
				{
					tipo = _delay;
				}else if(!strncmp(line, "#loginid", 8))
				{
					tipo = _loginid;
				}else if(!strncmp(line, "#loginpak", 9))
				{
					tipo = _loginpak;
				}else
				{
					tipo = _nil;
				}
			}else if(strlen(line) > 0 && line[0] != '/')
			{
				if(tipo == _cliver)
				{
					cliver = atoi(line);
				}else if(tipo == _porta)
				{
					serverPort = atoi(line);
				}else if(tipo == _delay)
				{
					delay_thread = atoi(line);
				}else if(tipo == _loginid)
				{
					sscanf(line, "%X", &loginID);
				}else if(tipo == _loginpak)
				{
					int32_t pos = 0;
					char info = 0;
					sscanf(&line[1], "%X %c", &pos, &info);
					for(int32_t j = 0; j < 8; j++)
					{
						if(mLogin[pos][j][0] == 0)
						{
							mLogin[pos][j][0] = line[0];
							mLogin[pos][j][1] = info;
							switch(line[0])
							{
								case 'b': case 's': case 'i': case 'l': case 'B': case 'S': case 'I': case 'L':
								{
									uint32_t x[2] = {0, 0};
									sscanf(&line[1], "%X %c %X %X", &pos, &info, &x[0], &x[1]);
									dLogin[pos][j] = x[0] | (x[1] << 16);
									break;
								}
							}
							break;
						}
					}
					/*
					cXX (l[login]|s[senha]|c[cliver]|m[mac])	//XX recebe (operando) devidamente organizado
					(b|s|i|l|B|S|I|L)XX (*+-/|&^=) YY	//XX (operador) YY
					(b|s|i|l|B|S|I|L)XX r YY ZZ	//rand % YY + ZZ
					min�sculo => sem sinal
					mai�sculo => com sinal
					*/
				}
			}
		}
		fp.close();
	}

	fp.open(DIR_BASE+string(servidor)+"/SkillData.bin", ifstream::binary);
	if(fp.is_open())
	{
		int32_t len = sizeof(sk_756);
		uint8_t *ptr = (uint8_t*)&SkillData;
		fp.read((char*)&SkillData, sizeof(sk_756));
		fp.close();
		while(--len >= 0)
		{
			ptr[len] ^= 'Z';
		}
	}

	fp.open(DIR_BASE+string(servidor)+"/itemlist2.bin", ifstream::binary);
	if(fp.is_open())
	{
		fp.read((char*)&itemlist, sizeof(itemlist));
		fp.close();
	}

	fp.open(DIR_BASE+string(servidor)+"/keys.dat", ifstream::binary);
	if(fp.is_open())
	{
		fp.read(keys, sizeof(keys));
        fp.close();
	}
}

void Client::DoWork()
{
	int32_t i;
	int32_t tID;
	FILE *f = fopen("./fechar", "r");
	if(f)
	{
		Disconnect();
		DebugLog("Fechar!");
		fclose(f);
		remove("./fechar");
		exit(1);
	}
	if(s != INVALID_SOCKET)
	{
		if(cID > 0)
		{
			if(hacks.protect.on)
			{
				tID = hacks.protect.id;
			}else
			{
				tID = -1;
			}
			if((timer_count%150) == 0)
			{
				for(i = 0; i < 64; i++)
				{
					if(hacks.macro.targets[i].id)
					{
						if(hacks.macro.targets[i].time < time_tick)
						{
							RemoveMacro(hacks.macro.targets[i].id);
						}else
						{
							InsertMacro(hacks.macro.targets[i].id);
						}
					}
				}
				#if defined(STAR)
					Chat("Boterino!");
				#endif
			}
			if(hacks.macro.re)
			{
				if(hacks.macro.re_c > 0)
				{
					hacks.macro.re_c--;
				}else if(hacks.macro.re_c == 0)
				{
					hacks.macro.re = 0;
					hacks.commands.c = 0;
					hacks.commands.i = 0;
					//Macro(hacks.macro.tipo);
				}
			}
			if((timer_count%1000) == 0)
			{
				static int32_t p_count = 0;
				Signal();
				if(novato)
				{
					struct tm *t = GetTime();
					if((((t->tm_mday-1) % n_svs) + 1) != ch)
					{
						Disconnect();
					}
				}
				p_count++;
				if(planos == free_)
				{
					//Chat("WYD2BOT.COM.BR :)");
					if(p_count == 9)
					{
						DebugLog("DC FREE!");
						Disconnect();
						DebugLog("Fechar!");
						remove("./fechar");
						exit(1);
					}
				}
			}else if((timer_count%2500) == 0)
			{
				Pvt("day", "");
			}else
			{
				//if(delay_count == delay_thread)
				{
					int32_t p;
					for(p = 0; p < MAX_TIMER_PACKETS; p++)
					{
						if(TimerPacket[p].time != 0 &&
							TimerPacket[p].time < time(0))
						{
							break;
						}
					}
					if(p != MAX_TIMER_PACKETS)
					{
						MSG_HEADER *packet = (MSG_HEADER*)TimerPacket[p].buffer;
						SendPacket(packet->Size, packet);
						TimerPacket[p].time = 0;
					}else
					{
						if(hacks.commands.c != -1 &&
						hacks.commands.c < timer_count &&
						hacks.commands.i < hacks.commands.n)
						{
							char *cmd = hacks.commands.cmd[hacks.commands.i];
							hacks.commands.i++;
							DebugLog("Macro> %s", cmd);
							if(strncmp(cmd, "#sleep ", 7) == 0)
							{
								int32_t s = 0;
								sscanf(cmd, "#sleep %d", &s);
								hacks.commands.c = timer_count +
								(s * 10) + 1;
							}else if(strncmp(cmd, "#if ", 4) == 0 || strncmp(cmd, "#nf ", 4) == 0)
							{
								int32_t _false = 1;
								if(strncmp(&cmd[4], "hora ", 5) == 0)
								{
									int32_t h;
									if(sscanf(&cmd[9], "%d", &h) == 1)
									{
										struct tm *t = GetTime();
										if(t->tm_hour == h)
										{
											_false = 0;
										}
									}
								}else if(strncmp(&cmd[4], "min ", 4) == 0)
								{
									int32_t m;
									if(sscanf(&cmd[8], "%d", &m) == 1)
									{
										struct tm *t = GetTime();
										if(t->tm_min == m)
										{
											_false = 0;
										}
									}
								}else if(strncmp(&cmd[4], "sec ", 4) == 0)
								{
									int32_t s;
									if(sscanf(&cmd[8], "%d", &s) == 1)
									{
										struct tm *t = GetTime();
										if(t->tm_sec == s)
										{
											_false = 0;
										}
									}
								}else if(strncmp(&cmd[4], "novato", 6) == 0)
								{
									struct tm *t = GetTime();
									if((((t->tm_mday-1) % n_svs) + 1) == ch)
									{
										_false = 0;
									}
								}else if(strncmp(&cmd[4], "rand ", 5) == 0)
								{
									int32_t x, y;
									if(sscanf(&cmd[9], "%d %d", &x, &y) == 2)
									{
										if((rand() % x) == y)
										{
											_false = 0;
										}
									}
								}else if(strncmp(&cmd[4], "range ", 6) == 0)
								{
									int32_t x, y, r;
									if(sscanf(&cmd[10], "%d %d %d", &x, &y, &r) == 3)
									{
										int32_t _x = mobs[cID].Current.x;
										int32_t _y = mobs[cID].Current.y;
										if(dist(x, y, _x, _y) <= r)
										{
											_false = 0;
										}
									}
								}else if(strncmp(&cmd[4], "week ", 5) == 0)
								{
									int32_t w;
									if(sscanf(&cmd[9], "%d", &w) == 1)
									{
										struct tm *t = GetTime();
										if(t->tm_wday == w)
										{
											_false = 0;
										}
									}
								}else if(strncmp(&cmd[4], "item ", 5) == 0)
								{
									int32_t id;
									if(sscanf(&cmd[9], "%d", &id) == 1)
									{
										int32_t slot;
										for(slot = 0; slot < MAX_SLOTS_INV; slot++)
										{
											if(pChar.Character.Inventory[slot].ItemID == id)
											{
												_false = 0;
												break;
											}
										}
									}
								}else if(strncmp(&cmd[4], "cheio ", 6) == 0)
								{
									int32_t i, j;
									if(sscanf(&cmd[10], "%d %d", &i, &j) == 2)
									{
										if(i >= 0 && i < MAX_SLOTS_INV && j >= 0 && j < MAX_SLOTS_INV)
										{
                                            int32_t slot;
											_false = 0;
                                            for(slot = i; slot <= j; slot++)
                                            {
                                                if(pChar.Character.Inventory[slot].ItemID == 0)
                                                {
                                                	_false = 1;
                                                    break;
                                                }
                                            }
										}
									}
								}else if(strncmp(&cmd[4], "vazio ", 6) == 0)
								{
									int32_t i, j;
									if(sscanf(&cmd[10], "%d %d", &i, &j) == 2)
									{
										if(i >= 0 && i < MAX_SLOTS_INV && j >= 0 && j < MAX_SLOTS_INV)
										{
											int32_t slot;
											_false = 0;
											for(slot = i; slot <= j; slot++)
											{
												if(pChar.Character.Inventory[slot].ItemID != 0)
												{
													_false = 1;
													break;
												}
											}
										}
									}
								}else if(strncmp(&cmd[4], "level ", 6) == 0)
								{
									int32_t i;
									if(sscanf(&cmd[10], "%d", &i) == 1)
									{
										if(i > 0 && i <= 400)
										{
											i--;
											if(pChar.Character.Status.Level > i)
											{
												_false = 0;
											}
										}
									}
								}else if(strncmp(&cmd[4], "gold ", 5) == 0)
								{
									int32_t i;
									if(sscanf(&cmd[9], "%d", &i) == 1)
									{
										if(pChar.Character.Gold >= i)
										{
											_false = 0;
										}
									}
								}else if(strncmp(&cmd[4], "c ", 2) == 0)
								{
									int32_t i;
									if(sscanf(&cmd[6], "%d", &i) == 1)
									{
										if(hacks.commands.contador == i)
                                        {
                                            WriteLog("Contador: %d=%d", hacks.commands.contador, i);
                                            _false = 0;
                                        }
									}
								}
								if(cmd[1] == 'n')
								{
									if(_false == 0)
									{
										_false = 1;
									}else
									{
										_false = 0;
									}
								}
								hacks.commands.i += _false;
							}else if(strncmp(cmd, "#incc", 5) == 0)
							{
								hacks.commands.contador++;
								WriteLog("Contador: %d", hacks.commands.contador);
							}else if(strncmp(cmd, "#decc", 5) == 0)
							{
								hacks.commands.contador--;
								WriteLog("Contador: %d", hacks.commands.contador);
							}else if(strncmp(cmd, "#setc ", 6) == 0)
							{
							    int32_t x;
							    sscanf(cmd, "#setc %d", &x);
								hacks.commands.contador = x;
								WriteLog("Contador: %d", hacks.commands.contador);
							}else if(strncmp(cmd, "#goto ", 6) == 0)
							{
								char label[64] = "";
								map<string, int32_t>::iterator it;
								sscanf(cmd, "#goto %[^\n]s", label);
								it = labels.find(label);
								if(it != labels.end())
								{
									hacks.commands.i = it->second;
								}
							}else if(strncmp(cmd, "#end", 4) != 0)
							{
								//hacks.commands.c = timer_count + 10 * ExecuteCommand(cmd);
								hacks.commands.c = ExecuteCommand(cmd) * 10;
								if(hacks.commands.ciclos != -1 && hacks.commands.c > hacks.commands.ciclos && strncmp(cmd, "#move ", 6))
								{
									hacks.commands.c = hacks.commands.ciclos;
								}
								if(hacks.commands.c > 0)
								{
									wait_recv[p_Attack] = 1;
								}
								hacks.commands.c += timer_count;
							}
							if(hacks.commands.i >= hacks.commands.n &&
								hacks.commands.repeat != -1)
							{
								hacks.commands.i = hacks.commands.repeat;
							}
						}
						if(hacks.inv.id != -1)
                        {
                            if(hacks.inv.c == hacks.inv.delay)
                            {
                                Attack(cID, &mobs[cID].Current, cID, &mobs[cID].Current,
                                       hacks.inv.id, 255, 0, -1);
                                hacks.inv.c = -1;
                            }
                            hacks.inv.c++;
                        }
						if(hacks.macro.on && delay_count == delay_thread && wait_recv[p_Attack] == 0)
						{
							if(hacks.macro.stage == 0)
							{
								if(hacks.macro.buff < 20)
								{
									for(i = 0; i  < 20 && hacks.macro.buff < 20; i++)
									{
										if((skillbar[hacks.macro.buff] & (1 << _buff)) != 0)
										{
											int32_t skIdx = cskillbar[hacks.macro.buff];
											if(skIdx != 255)
											{
												Attack(cID, &mobs[cID].Current, cID, &mobs[cID].Current,
														skIdx, 255, 0, -1);
												//WriteLog("Buff: %d/%d", hacks.macro.buff, skIdx);
												hacks.macro.buff++;
												break;
											}
										}
										hacks.macro.buff++;
									}
								}else
								{
									hacks.macro.buff = 0;
									hacks.macro.stage = 1;
								}
							}else if(hacks.macro.stage == 1)
							{
								buff_count = (buff_count + 1) % 16;
								for(i = 0; i < 20; i++)
								{
									if((skillbar[i] & (1 << _buff)) != 0)
									{
										int32_t skIdx = cskillbar[i];
										if(skIdx != 255)
										{
											int32_t j;
											if(isEvok(skIdx) != 0 && pChar.Character.ClassInfo == BM)
											{
												for(j = 0; j < 14; j++)
												{
													if(party[j] >= 1000 && party[j] < 30002)
													{
														int32_t _face = mobs[party[j]].ItemEff[0].ItemID;
														if (_face == 206 || _face == 226 || _face == 227 ||
															_face == 244 || _face == 245 || _face == 225 ||
															_face == 307 || _face == 396)
														{
                                                            break;
														}
													}
												}
												if(j == 14)
												{
													Attack(cID, &mobs[cID].Current, cID, &mobs[cID].Current,
															skIdx, 255, 0, -1);
													break;
												}
											}else if(SkillData.sk[skIdx].AffectType != 0)
											{
												for(j = 0; j < MAX_AFFECT; j++)
												{
													if(pBuffs.affect[j].aType == SkillData.sk[skIdx].AffectType)
													{
														break;
													}
												}
												if(j == MAX_AFFECT)
												{
													Attack(cID, &mobs[cID].Current, cID, &mobs[cID].Current, skIdx, 255, 0, -1);
													break;
												}
											}
										}
									}
								}
								if(i == 20)
								{
									if(hacks.macro.tipo == 0)
									{
										if(tID <= 0)
										{
											for(i = 0; i < 64; i++)
											{
												tID = hacks.macro.targets[i].id;
												if(tID)
												{
													int32_t aID = cID;
													sPoint<int16_t> *aPos = &mobs[cID].Current;
	#if defined(ENABLE_RANDOM_RANGE)
													if(dist(aPos, &mobs[tID].Current) > cData.range + (rand() & 1))
	#else
													if(dist(aPos, &mobs[tID].Current) > cData.range)
	#endif
													{
														if(hacks.range.on == 2)
														{
															aID++;
															aPos = &mobs[tID].Current;
														}else
														{
															InsertMacro(tID);
															continue;
														}
													}
													if(pChar.Character.ClassInfo == 3)
													{
														int32_t tID2 = 0;
														int32_t dmg2 = 0;
														int32_t _r = 1;
														if(hacks.range.on != 0)
														{
															_r++;
														}
														for(int32_t j = i+1; j < 64; j++)
														{
															tID2 = hacks.macro.targets[j].id;
															if(tID2)
															{
																if(dist(&mobs[tID].Current, &mobs[tID2].Current) < _r)
																{
																	dmg2 = -2;
																	break;
																}
															}
														}
														if(hacks.range.on != 0)
														{
															if(tID2 == 0)
															{
																tID2 = tID;
																dmg2 = -2;
															}
														}
														Attack(aID, aPos, tID, &mobs[tID].Current,
																255, 4, 255, -2, tID2, dmg2);
													}else
													{
														Attack(aID, aPos, tID, &mobs[tID].Current,
																255, 4, 255, -2);
													}
													InsertMacro(tID);
													break;
												}
											}
										}else
										{
											int32_t aID = cID;
											sPoint<int16_t> *aPos = &mobs[cID].Current;
											/*if(hacks.range.on)
											{
												if(dist(aPos, &mobs[tID].Current) > 6)
												{
													aID++;
												}
												aPos = &mobs[tID].Current;
											}*/
											aID = tID;
											if(pChar.Character.ClassInfo == 3)
											{
												int32_t tID2;
												int32_t dmg2;
												tID2 = tID;
												dmg2 = -2;
												Attack(aID, aPos, tID, &mobs[tID].Current,
														255, 4, 255, -2, tID2, dmg2);
											}else
											{
												Attack(aID, aPos, tID, &mobs[tID].Current,
														255, 4, 255, -2);
											}
										}
									}else if(hacks.macro.tipo == 1)
									{
										int32_t skIdx = 255;
										for(i = 0; i < 20; i++)
										{
											if((skillbar[hacks.macro.skill] & (1 << _skill)) != 0)
											{
												skIdx = cskillbar[hacks.macro.skill];
												if(skIdx != 255)
												{
													hacks.macro.skill++;
													if(hacks.macro.skill == 20)
													{
														hacks.macro.skill = 0;
													}
													break;
												}
											}
											hacks.macro.skill++;
											if(hacks.macro.skill == 20)
											{
												hacks.macro.skill = 0;
											}
										}
										if(skIdx != 255)
										{
											int32_t max = SkillData.sk[skIdx].MaxTarget;
											int32_t range = SkillData.sk[skIdx].Range;//+2
											//WriteLog("Macro: %d/%d/%d/%d", skIdx, max, range, hacks.macro.skill);
											if(max > 1)
											{
												sPoint<int32_t> _targets[13];
												int32_t x = 0;
												memset(_targets, -1, sizeof(_targets));
												if(tID > 0)
												{
													if(dist(&mobs[cID].Current, &mobs[tID].Current) <= range)
													{
														_targets[x].x = tID;
														x++;
													}
												}
												for(i = 0; i < 64 && x < max; i++)
												{
													tID = hacks.macro.targets[i].id;
													if(tID)
													{
														if(dist(&mobs[cID].Current, &mobs[tID].Current) <= range)
														{
															_targets[x].x = tID;
															x++;
														}
													}
												}
												if(x != 0)
												{
													AttackArea(cID, &mobs[cID].Current, skIdx, _targets);
													//WriteLog("AttackArea: %d/%d", skIdx, x);
												}/*else
												{
													hacks.macro.skill--;
												}*/
											}else
											{
												if(tID <= 0)
												{
													for(i = 0; i < 64; i++)
													{
														tID = hacks.macro.targets[i].id;
														if(tID)
														{
															if(dist(&mobs[cID].Current, &mobs[tID].Current) <= range)
															{
																Attack(cID, &mobs[cID].Current, tID, &mobs[tID].Current,
																		skIdx, 255, 0, -1);
																//WriteLog("Attack: %d/%d/%d", skIdx, i, tID);
																InsertMacro(tID);
																break;
															}
														}
													}
												}else
												{
													if(dist(&mobs[cID].Current, &mobs[tID].Current) <= range)
													{
														Attack(cID, &mobs[cID].Current,
																tID, &mobs[tID].Current,
																skIdx, 255, 0, -1);
													}
												}
												/*
												if(i == 64)
												{
													hacks.macro.skill--;
												}*/
											}
										}
									}else if(hacks.macro.tipo == 2)
									{
										int32_t skIdx = cskillbar[9];
										if(skIdx != 255 && SkillData.sk[skIdx].ForceDamage == 0)
										{
											int32_t max = SkillData.sk[skIdx].MaxTarget;
											int32_t range = SkillData.sk[skIdx].Range;
											sPoint<int32_t> _targets[13];
											int32_t x = 0;
											memset(_targets, -1, sizeof(_targets));
											for(i = 0; i < 13 && x < max; i++)
											{
												tID = party[i];
												if(tID != -1 && dist(&mobs[cID].Current, &mobs[tID].Current) <= range)
												{
													_targets[x].x = tID;
													x++;
												}
											}
											if(x != 0)
											{
												AttackArea(cID, &mobs[cID].Current, skIdx, _targets);
												//WriteLog("AttackArea: %d/%d", skIdx, x);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}else
		{
			if(TimerPacket[MAX_TIMER_PACKETS].time > 0 && TimerPacket[MAX_TIMER_PACKETS].time < time(0))
			{
				MSG_HEADER *packet = (MSG_HEADER*)TimerPacket[MAX_TIMER_PACKETS].buffer;
				SendPacket(packet->Size, packet);
				TimerPacket[MAX_TIMER_PACKETS].time = 0;
			}/*
			if(hacks.macro.re_l > 0)
			{
				hacks.macro.re_l--;
			}else if(hacks.macro.re_l == 0)
			{
				if(planos != free)
				{
					if( Connect() == 0 )
					{
						hacks.macro.re_l = 15 + rand() % 5 - 2;
					}
				}
			}*/
		}
        getTimerRecv();
        timer_count++;
        delay_count = (delay_count + 1) % (delay_thread + 1);
        for(int32_t op = 0; op < MAX_RECV; op++)
        {
            if(wait_recv[op] > 0)
            {
                wait_recv[op]--;
            }
        }
	}
}

void Client::WriteData()
{
	if(cID > 0)
	{
		FILE *f = fopen(dir_data, "wb+");
		if(f)
		{
			static int32_t _zero = 0;
			int64_t exp = pChar.Character.Exp;
			fwrite(pChar.Character.Inventory, sizeof(sItem)*64, 1, f);
			fwrite(pLogin.Storage, sizeof(sItem)*128, 1, f);
			fwrite(&pLogin.Gold, 4, 1, f);
			fwrite(&pChar.Character.Gold, 4, 1, f);
			fwrite(&mobs[cID].PlayerName, 12, 1, f);
			fwrite(&_zero, 1, 1, f);
			fwrite(&mobs[cID].Score.Level, 2, 1, f);
			fwrite(&mobs[cID].Current.x, 2, 1, f);
			fwrite(&mobs[cID].Current.y, 2, 1, f);
			fwrite(&exp, 8, 1, f);
			fclose(f);
		}
	}
}

int32_t Client::ExecuteCommand(char *text)
{
	int32_t v[4] = {0, 0, 0, 0};
	char aux[16];
	if(text[0] == '#')
	{
		DebugLog("ExecuteCommand(%s)", text);
		if(!strncmp(text, "#party ", 7))
		{
#if defined(ENABLE_PARTY)
			sscanf(text, "#party %d %[^\n]s", &v[0], aux);
			aux[15] = 0;
			Party(v[0], aux);
#endif
		}else if(!strncmp(text, "#drop ", 6))
		{
			sscanf(text, "#drop %d", &v[0]);
			if(v[0] == 0)
			{
				hacks.drop.on = 0;
				WriteLog("Drop: OFF");
			}else if(v[0] == 1)
			{
				hacks.drop.on = 1;
				WriteLog("Drop: ON");
			}
		}else if(!strncmp(text, "#range ", 7))
		{
			sscanf(text, "#range %d", &v[0]);
#if defined(ENABLE_RANGE)
			if(v[0] == 0)
			{
				hacks.range.on = 0;
				WriteLog("Range: OFF");
			}else
			{
				hacks.range.on = v[0];
				WriteLog("Range: ON");
			}
#endif
		}else if(!strncmp(text, "#protect ", 9))
		{
			sscanf(text, "#protect %d", &v[0]);
#if defined(ENABLE_PROTECT)
			if(v[0] == 0)
			{
				hacks.protect.on = 0;
				WriteLog("Protect: OFF");
			}else if(v[0] == 1)
			{
				hacks.protect.id = -1;
				hacks.protect.on = 1;
				WriteLog("Protect: ON");
			}
#endif
		}else if(!strncmp(text, "#item ", 6))
		{
			sscanf(text, "#item %d %d %d %d", &v[0], &v[1], &v[2], &v[3]);
			MoveItem(v[0], v[1], v[2], v[3]);
			WriteLog("MoveItem: %d %d -> %d %d", v[0], v[1], v[2], v[3]);
		}else if(!strncmp(text, "#guarda ", 8))
		{
	        int32_t i;
			sscanf(text, "#guarda %d", &v[0]);
            for(i = 0; i < MAX_SLOTS_WARE; i++)
            {
            	if(pLogin.Storage[i].ItemID == 0)
                {
                	MoveItem(1, v[0], 2, i);
					WriteLog("Item %d(%d) guardado em %d", pLogin.Storage[i].ItemID, v[0], i);
                    break;
                }
            }
            if(i == MAX_SLOTS_WARE)
            {
            	WriteLog("Ba� cheio!");
                return 0;
            }
		}else if(!strncmp(text, "#guardai ", 9))
		{
	        int32_t i, j;
			sscanf(text, "#guardai %d", &v[0]);
            for(j = 0; j < MAX_SLOTS_INV; j++)
            {
				if(pChar.Character.Inventory[j].ItemID == v[0])
				{
                    for(i = 0; i < MAX_SLOTS_WARE; i++)
                    {
                        if(pLogin.Storage[i].ItemID == 0)
                        {
                            MoveItem(1, j, 2, i);
                            WriteLog("Item %d(%d) guardado em %d", v[0], j, i);
                            break;
                        }
                    }
                    if(i == MAX_SLOTS_WARE)
                    {
                        WriteLog("Ba� cheio!");
                        return 0;
                    }
				}
            }
            if(j == MAX_SLOTS_INV)
            {
                //WriteLog("Item %d n�o encontrado!", v[0]);
                return 0;
            }
		}else if(!strncmp(text, "#use ", 5))
		{
			int32_t i;
			v[1] = 0;
			v[2] = 2;
			sscanf(text, "#use %d %d", &v[0], &v[1]);
			for(i = 0; i < MAX_SLOTS_INV; i++)
			{
				if(pChar.Character.Inventory[i].ItemID == v[0])
				{
					v[2] = UseItem(3, i, 0, 0, v[1]);
					break;
				}
			}
			if(i == MAX_SLOTS_INV)
			{
				WriteLog("UseItem: item %d n�o encontrado", v[0]);
			}else
			{
				WriteLog("UseItem: %d/%d", v[0], v[1]);
			}
			return v[2];
		}else if(!strncmp(text, "#shift ", 7))
		{
			sscanf(text, "#shift %d %d %d", &v[0], &v[1], &v[2]);
			ShiftItem(v[0], v[1], v[2]);
			WriteLog("ShiftItem: %d %d -> %d %d", v[0], v[1], v[2], v[3]);
		}else if(!strncmp(text, "#grifo ", 7))
		{
			sscanf(text, "#grifo %d", &v[0]);
			Grifo(v[0]);
			WriteLog("Grifo: %d", v[0]);
		}else if(!strncmp(text, "#macro ", 7))
		{
			sscanf(text, "#macro %d", &v[0]);
			if(v[0] == -1)
			{
				hacks.macro.on = 0;
				hacks.macro.re = 0;
				WriteLog("Macro: OFF");
			}else
			{
				Macro(v[0]);
			}
		}else if(!strncmp(text, "#getout ", 8))
		{
			sscanf(text, "#getout %d", &v[0]);
			if(v[0] == 0)
			{
				hacks.getout.on = 0;
				WriteLog("Getout: OFF");
			}else
			{
				hacks.getout.on = 1;
				WriteLog("Getout: ON");
			}
		}else if(!strncmp(text, "#inv ", 5))
		{
			sscanf(text, "#inv %d %d", &v[0], &v[1]);
			if(v[0] == -1)
			{
				hacks.inv.id = -1;
				hacks.inv.delay = 0;
				WriteLog("Macro: OFF");
			}else
			{
				hacks.inv.id = v[0];
				hacks.inv.delay = v[1] * 10;
				hacks.inv.c = hacks.inv.delay;
				WriteLog("Macro: ON");
			}
		}else if(!strncmp(text, "#buy ", 5))
		{
			char nick[18] = "";
			int32_t i;
			sscanf(text, "#buy %d %[^\n]s", &v[0], nick);
			for(i = 1000; i < 30000; i++)
			{
				if(strstr(mobs[i].MobName, nick) != 0)
				{
					int32_t j, k;
					for(j = 0; j < 64; j++)
					{
						if(npcs[j].Index == i)
						{
							break;
						}
					}
					if(j == 64)
					{
						hacks.commands.i--;
						return 1;
					}
					for(k = 0; k < 27; k++)
					{
						if(npcs[j].Items[k].ItemID == v[0])
						{
							int32_t l;
							j = (k % 9) + ((k / 9) * 27);
							for(l = 0; l < MAX_SLOTS; l++)
							{
								if(pChar.Character.Inventory[l].ItemID == 0)
								{
									BuyItem(i, j, l);
									break;
								}
							}
							if(l == MAX_SLOTS)
							{
								WriteLog("Invent�rio cheio!");
								return 0;
								break;
							}
							break;
						}
					}
					break;
				}
			}
		}else if(!strncmp(text, "#buyid ", 7))
		{
			int32_t l;
			sscanf(text, "#buyid %d %d", &v[0], &v[1]);
			for(l = 0; l < MAX_SLOTS; l++)
			{
				if(pChar.Character.Inventory[l].ItemID == 0)
				{
					BuyItem(v[0], v[1], l);
					break;
				}
			}
			if(l == MAX_SLOTS)
			{
				WriteLog("Invent�rio cheio!");
				return 0;
			}
		}else if(!strncmp(text, "#sell ", 6))
		{
			char nick[18] = "";
			int32_t i;
			sscanf(text, "#sell %d %[^\n]s", &v[0], nick);
			for(i = 1000; i < 30000; i++)
			{
				if(strstr(mobs[i].MobName, nick) != 0)
				{
					int32_t j;
				    for(j = 0; j < MAX_SLOTS_INV; j++)
                    {
                        if(pChar.Character.Inventory[j].ItemID == v[0])
                        {
                            SellItem(i, 1, j);
                        }
                    }
					if(j == MAX_SLOTS_INV)
					{
						return 0;
					}
				}
			}
		}else if(!strncmp(text, "#sellid ", 8))
		{
			sscanf(text, "#sellid %d %d", &v[0], &v[1]);
			if(v[1] >= 0 && v[1] < MAX_SLOTS_INV && pChar.Character.Inventory[v[1]].ItemID != 0)
			{
				SellItem(v[0], 1, v[1]);
			}else
			{
				return 0;
			}
		}else if(!strncmp(text, "#autoparty ", 11))
		{
			sscanf(text, "#autoparty %[^\n]s", aux);
			if(hacks.party.on)
			{
				hacks.party.on = 0;
				WriteLog("AutoParty: OFF");
			}else
			{
				strcpy(hacks.party.nick, aux);
				hacks.party.on = 1;
				WriteLog("AutoParty: ON(%s)", aux);
			}
		}else if(!strncmp(text, "#buff ", 6))
		{
#if defined(ENABLE_BUFF)
			sscanf(text, "#buff %d", &v[0]);
			Attack(cID, &mobs[cID].Current, cID, &mobs[cID].Current, v[0], 255, 0, -1);
			WriteLog("Buff: %d", v[0]);
#endif
		}else if(!strncmp(text, "#move ",6))
		{
			hacks.commands.lastMove = hacks.commands.i;
			/*
			if(wait_recv[p_Move1] > 0)
			{
				return 0;
			}*/
			sscanf(text, "#move %d %d", &v[0], &v[1]);
			return Move(v[0], v[1]);
		}else if(!strncmp(text, "#moved ",6))
		{
			hacks.commands.lastMove = hacks.commands.i;
			/*if(wait_recv[p_Move1] > 0)
			{
				return 0;
			}*/
#if defined(ENABLE_MOVED)
			sscanf(text, "#moved %d %d", &v[0], &v[1]);
			return Move(v[0], v[1], 1);
#endif
		}else if(!strncmp(text, "#setdelay ", 10))
		{
			sscanf(text, "#setdelay %d", &v[0]);
			WriteLog("Ciclos alterados para: %d", v[0]);
			hacks.commands.ciclos = v[0];
		}else if(!strncmp(text, "#confirma", 9))
		{
			ConfirmPortal();
		}else if(!strncmp(text, "#loja", 5))
		{
			OpenStore();
		}else if(!strncmp(text, "#logout", 7))
		{
			Disconnect();
			Disconnect(1);
		}else if(!strncmp(text, "#click ", 7))
		{
			char nick[18] = "";
			memcpy(nick, &text[7], 16);
			for(int32_t j = 0; j < 16; j++)
			{
				if(nick[j] == '\r' || nick[j] == '\n')
				{
					nick[j] = 0;
				}
			}
            for(int32_t j = 1000; j < 30000; j++)
			{
				if(strstr(mobs[j].MobName, nick) != 0)
				{
					Click(j);
					break;
				}
			}
		}else if(!strncmp(text, "#clickid ", 9))
		{
			sscanf(text, "#clickid %d", &v[0]);
			Click(v[0]);
		}else
        {
        	return 0;
        }
		return 2;
	}else if(text[0] == '/')
	{
		int32_t i;
		for(i = 0; i < 256; i++)
		{
			if(text[i] == ' ')
			{
				text[i] = 0;
				i++;
				break;
			}
		}
		Pvt(&text[1], &text[i]);
	}else if(text[0] == '=')
	{
		Pvt("", text);
	}else if(text[0] == '$')
	{
		Chat(&text[1]);
	}else
    {
    	return 0;
    }
	return 1;
}

char *Client::GetIPServerlist(int32_t sv, int32_t cn)
{
	static const uint8_t key[] =
	{
	0xA4 ,0xA1 ,0xA4 ,0xA4 ,0xA4 ,0xA7 ,0xA4 ,0xA9 ,0xA4 ,0xB1 ,0xA4 ,0xB2 ,0xA4 ,0xB5 ,0xA4 ,0xB7,
	0xA4 ,0xB8 ,0xA4 ,0xBA ,0xA4 ,0xBB ,0xA4 ,0xBC ,0xA4 ,0xBD ,0xA4 ,0xBE ,0xA4 ,0xBF ,0xA4 ,0xC1,
	0xA4 ,0xC3 ,0xA4 ,0xC5 ,0xA4 ,0xC7 ,0xA4 ,0xCB ,0xA4 ,0xCC ,0xA4 ,0xD0 ,0xA4 ,0xD1 ,0xA4 ,0xD3,
	0xA4 ,0xBF ,0xA4 ,0xC4 ,0xA4 ,0xD3 ,0xA4 ,0xC7 ,0xA4 ,0xCC ,0xB0 ,0xA1 ,0xB3 ,0xAA ,0xB4 ,0xD9
	};
	static char tmp[64] = "";
	ifstream f(DIR_BASE+string(servidor)+"/serverlist.bin", ifstream::binary);
	if(f)
	{
		int32_t j;
		n_svs = 0;
		for(j = 1; j < 11; j++)
		{
			f.seekg((sv * 11 * 64) + (j * 64), f.beg);
			f.read(tmp, 1);
			tmp[0] = tmp[0] - key[63-0];
			if(tmp[0] != 0)
			{
				n_svs++;
			}else
			{
				break;
			}
		}
		if(novato)
		{
			struct tm *t = GetTime();
			cn = ((t->tm_mday-1) % n_svs) + 1;
			DebugLog("Novato: %d; Atual: %d", cn, ch);
			ch = cn;
		}
		if(cn > n_svs)
		{
			ch = cn = n_svs;
		}

		f.seekg((sv * 11 * 64) + (cn * 64), f.beg);
		f.read(tmp, 64);
		for(int32_t i = 0; i < 64; i++)
		{
			tmp[i] = tmp[i] - key[63-i];
		}
		f.close();
	}
	return tmp;
}

void Client::WriteLog(const char *format, ...)
{
	char log_buffer[256];
	va_list args;
	va_start (args, format);
	vsnprintf (log_buffer, sizeof(log_buffer), format, args);
	va_end (args);
	struct tm *t;
    while(ready == false)
    {
    	this_thread::yield();
    }
    t = GetTime();
    CreateLogs();
	if(f_mainlog)
	{
		fprintf(f_mainlog, "[%02d/%02d/%04d %02d:%02d:%02d] > %s\n",
				t->tm_mday, t->tm_mon + 1, t->tm_year + 1900, t->tm_hour, t->tm_min, t->tm_sec, log_buffer);
		fflush(f_mainlog);
	}
	ready = true;
#if defined(_DEBUG)
	DebugLog(log_buffer);
#endif
}

void Client::DebugLog(const char *format, ...)
{
	char log_buffer[256];
	va_list args;
	va_start (args, format);
	vsnprintf (log_buffer, sizeof(log_buffer), format, args);
	va_end (args);
	struct tm *t;
    while(ready == false)
    {
    	this_thread::yield();
    }
    t = GetTime();
    CreateLogs();
	if(f_debuglog)
	{
		fprintf(f_debuglog, "D[%02d/%02d/%04d %02d:%02d:%02d] > %s\n",
				t->tm_mday, t->tm_mon + 1, t->tm_year + 1900, t->tm_hour, t->tm_min, t->tm_sec, log_buffer);
		fflush(f_debuglog);
	}
#if defined(_DEBUG)
	printf("[%02d %02d:%02d:%02d] > %s\n",
			t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec, log_buffer);
#endif
	ready = true;
}

struct tm *Client::GetTime()
{
	time_t t = time(0);
	struct tm *timeinfo = localtime( &t );
	return timeinfo;
}

Client::Client()
{
}

void Client::CreateLogs()
{
    char tmp[128];
    time_t t;
    struct tm *d;
	t = time(0);
	d = localtime(&t);
    if(daylog == d->tm_mday)
	{
		return;
	}
	daylog = d->tm_mday;
	sprintf(tmp, "./bot_%02d_%02d_%02d.log", d->tm_mday, d->tm_mon+1, d->tm_year - 100);
	if(f_mainlog)
	{
		fclose(f_mainlog);
	}
	f_mainlog = fopen(tmp, "a+");
	sprintf(tmp, "./debug_%d.log", d->tm_yday);
	if(f_debuglog)
	{
		fclose(f_debuglog);
	}
	f_debuglog = fopen(tmp, "a+");

	t = t - 86400;
	d = localtime(&t);
	sprintf(tmp, "./debug_%d.log", d->tm_yday);
	remove(tmp);
}

void Client::Run()
{
	memset(class_timer, 0, sizeof(class_timer));
	memset(recv_timer, 0, sizeof(recv_timer));
	memset(static_timer, 0, sizeof(static_timer));
	memset(cskillbar, 255, sizeof(cskillbar));
	memset(&pNpcTrade, 0, sizeof(pNpcTrade));
	memset(&pTrade, 0, sizeof(pTrade));
	memset(mobs, 0, sizeof(mobs));
	for(int32_t i = 0; i < 30001; i++)
	{
		mobs[i].Key = 1;
	}
	memset(&TimerPacket, 0, sizeof(TimerPacket));
	memset(drop_efs, 0, sizeof(drop_efs));
	memset(&cData, 0, sizeof(cData));
	memset(&pLogin, 0, sizeof(pLogin));
	memset(&pChar, 0, sizeof(pChar));
	memset(&hacks, 0, sizeof(hacks));
	memset(wait_recv, 0, sizeof(wait_recv));
	memset(packet_key, 0, sizeof(packet_key));
	memset(&pBullshitDon, 0, sizeof(pBullshitDon));
	memset(&pBuffs, 0, sizeof(pBuffs));
	hacks.skill.id = -1;
	delay_count = 0;
    buff_count = 0;
	last_city = -1;
	sv = 1;
	ch = 1;
	lastNick[0] = 0;
	dump_recv = dump_send = true;
	cID = -1;
	s = INVALID_SOCKET;
	daylog = -1;
	f_mainlog = 0;
	f_debuglog = 0;
	ready = true;
	CreateLogs();
	ReadConfig();
	if(client_mac[0] == 0 && client_mac[15] == 0)
	{
		for(int32_t i = 0; i < 128; i++)
		{
			if((rand()%2) == 0)
			{
				client_mac[(i%16)] = rand();
			}
		}
	}
	DebugLog("Vers�o: %s", VERSION);
}

Client::~Client()
{
	if( f_mainlog )
		fclose(f_mainlog);
	if( f_debuglog )
		fclose(f_debuglog);
}
