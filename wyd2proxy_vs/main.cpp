/***

The MIT License (MIT)

Copyright (c) 2017 Raphael Tom� Santana

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
***/

#include "main.h"

using namespace std;
Client client;
MD5 md5;

void HandleThread(int32_t func, int32_t timer)
{
	while (1)
	{
		switch (func)
		{
			case threadTimer:
			{
				client.DoWork();
				break;
			}
			case logTimer:
			{
				if (rand() % 2 == 0)
					srand(clock());
				else
					srand((uint32_t)timeGetTime());
				client.WriteData();
				break;
			}
		}
		this_thread::sleep_for(chrono::milliseconds(timer));
	}
}

int32_t main(int32_t argc, char **argv)
{
	char proxy__ip[32] = "";
	int32_t proxy__port = 0;
#if defined(_WIN32)
	WSADATA wsaData;
	WORD wsaVersion;
	wsaVersion = MAKEWORD(2, 2);
	if (WSAStartup(wsaVersion, &wsaData))
	{
		return 0;
	}
#else
	main_ts.tv_sec = 25 / 1000;
	main_ts.tv_nsec = (25 % 1000) * 1000000;
	rec_ts.tv_sec = 2000 / 1000;
	rec_ts.tv_nsec = (2000 % 1000) * 1000000;
	min_ts.tv_sec = 10 / 1000;
	min_ts.tv_nsec = (10 % 1000) * 1000000;
#endif
	if (argc > 1)
	{
		char *tmp;
		tmp = strstr(argv[1], ":");
		*tmp = '\0';
		tmp++;
		strcpy(proxy__ip, argv[1]);
		proxy__port = atoi(tmp);
	}
	client.Run();
	thread tLog(HandleThread, logTimer, 5000);
	thread tTimer(HandleThread, threadTimer, 100);
	while (1)
	{
		client.Connect(proxy__ip, proxy__port);
		while (client.s != INVALID_SOCKET)
		{
			client.ReadPacket();
#if defined(_WIN32)
			Sleep(25);
#else
			nanosleep(&main_ts, NULL);
#endif
		}
#if defined(_WIN32)
		Sleep(2000);
#else
		nanosleep(&rec_ts, NULL);
#endif
	}
#if defined(_WIN32)
	WSACleanup();
#endif
	return 0;
}
