/***

The MIT License (MIT)

Copyright (c) 2017 Raphael Tom้ Santana

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
***/

/* Nota sobre isto, se voc๊ abrir um itemeffect original como UNICODE pode conseguir ler algo dos comentแrios */

#ifndef ITEMEFFECT_H_INCLUDED
#define ITEMEFFECT_H_INCLUDED
/*
typedef struct
{
short Level;//[0]
short Defense;//[2]
short Attack;//[4]

struct
{
uchar Merchant : 4;
uchar Direction : 4;
} Dir;//[6]

struct
{
uchar Move : 4;
uchar Attack : 4;
} Speed;//[7]

short MaxHP, MaxMP;//[8]
short CurHP, CurMP;//[12]

short Str, Int;//[16]
short Dex, Con;//[20]

uchar wMaster, fMaster;//[24]
uchar sMaster, tMaster;//[26]
} SCORE_757;//[28]
*/
//#define EF_NONE     			  0	// * EFFECT Type์ด 0,์ด๋ฉด ์ดํํธ๊ฐ ์๋ค.  ITEM์์ Score[0]์ ๊ฐ์ ๋ํ ์ ์๋ค.
// Score ์์
#define EF_LEVEL     			  1	// ๋ ๋ฒจ * Score[1] - Score[15] Bonus Score๊ฐ์ ๊ทธ๋๋ก ๋ํด ๋ฒ๋ฆฐ๋ค.
#define EF_DAMAGE                 2 // ์ฆ๋
#define EF_AC       			  3	// ๋ฐฉ์ด
#define EF_HP        			  4	// ํผ์ฆ  HP,MP,๊ณต๊ฒฉ์๋,์ด๋์๋,๊ฐ์ข MAX๋ฑ๋ฑ
#define EF_MP       			  5	// ๋ง์ฆ
#define EF_EXP      			  6	// ๊ฒฝํ
#define EF_STR      		      7	// ํ
#define EF_INT      		      8	// ์ ์ 
#define EF_DEX       		      9	// ๋ฏผ์ฒฉ
#define EF_CON      		     10	// ์ฒด๊ฒฉ
#define EF_SPECIAL1    		     11	// ๋ฌด๋ง
#define EF_SPECIAL2    			 12	// ํน1
#define EF_SPECIAL3    			 13	// ํน2
#define EF_SPECIAL4    			 14	// ํน3
// Score ๋.
#define EF_SCORE14     			 15	//
#define EF_SCORE15     			 16	//
////////////////      REQUIREMENT        //////////////////////////////////////////
#define EF_POS                   17  // ์์น ์์์ ์๋ ์ฅ์ bit๊ฐ         - ๋ํดํธ NOWHERE (0๋นํธ ๊ฐ EQUIP 0)
#define EF_CLASS                 18  // ์ง์ ์์์ ์๋ (์ฑ๋ณ)ํด๋์ค bit๊ฐ - ๋ํดํธ YES
#define EF_R1SIDC                19  // ํ์ 1ํธ๋ ๋๋ ๊ฐ์ท์ ์์์ ์๋ STR INT DEC CON         - ๋ํดํธ 0
#define EF_R2SIDC                20  // ๋์ 2ํธ๋          ์ ์์์ ์๋ STR INT DEC CON         - ๋ํดํธ ๋ถ๊ฐ
////////////////   BONUS  ////////////////////////////////
#define EF_WTYPE         	     21	 // ๋ฌด๋ฅ ๋ฌด๊ธฐ๋ถ๋ฅ (์ ๋๋ฉ์ด์)
////////////////////////////////////////////////////////////////////////////
#define EF_REQ_STR               22  // ํ์ 
#define EF_REQ_INT               23  // ์ ์ 
#define EF_REQ_DEX               24  // ๋ฏผ์ 
#define EF_REQ_CON               25  // ์ฝ์ 
/////////////////////////////////////////////////////////////////////////
// * ์ฃผ ์๊ตฌ์น์ด๋, ์์ดํ์ ๊ธฐ๋ณธ SIDC์ ์ถ๊ฐ๋๋ ๊ฐ์ด๋ค.
/////////////////////////////////////////////////////////////////////////


#define EF_ATTSPEED  	       	 26	 // ๊ณต์ ๊ณต๊ฒฉ์๋
#define EF_RANGE                 27  // ์ฌ์  ์ฌ์ ๊ฑฐ๋ฆฌ. ์ ์ฒด์ค ์ต๋๊ฐ์ด ์ ์ฉ๋จ.
////#define EF_PRICE			 28	 // ๊ฐ๊ฒฉ
#define EF_CIDADANIA			 28	 // ๊ฐ๊ฒฉ
#define EF_RUNSPEED              29  // ๋ฐ๋ ๋ฌ๋ฆฌ๊ธฐ์๋.
#define EF_SPELL    	    	 30	 // ์คํ  Item์ ๋จน๊ฑฐ๋ ์์์๋ ๊ฑธ๋ฆฌ๋ ๋ง๋ฒ
#define EF_DURATION              31  // ์ง์ Item ํ๋ผ๋ฉํ1 - ๋จน์๋ ํจ๋ ฅ์ด ๋ฐ์ํ๋ ์๊ฐ, ์๋์์ดํ์ ํญ์
#define EF_PARM2     			 32	 // ํ๋ Item ํ๋ผ๋ฉํ2
#define EF_GRID                  33  // ํฌ๊ธฐ Carry๋ฅผ ์ฐจ์งํ๋ ์์ดํ๊ทธ๋ฆฌ๋
#define EF_GROUND                34  // ๋์นธ ์ฑ๋ฌธ๋ฑ์์ ๋์ ์ฐจ์งํ๋ ํฌ๊ธฐ
#define EF_CLAN                  35  // ์ข์กฑ
#define EF_HWORDCOIN             36  // ๋๋
#define EF_LWORDCOIN             37  // ์๋
#define EF_VOLATILE              38  // ์๋ชจ ๋จน์ผ๋ฉด ๋ฐ๋ก ์ฌ๋ผ์ง๋ ์์ดํ.  0:์ผ๋ฐ์ฅ๊ตฌ  1:๋ฌผ์ฝ๊ณํต  2:๋๊ณํต   3:์ด์  4:์ถ1  5:์ถ2  6:์ถ3
//                               6:์คํ +15   7:๋ ๋ฒจ+1
#define EF_KEYID                 39  // ํค๋ฒ
#define EF_PARRY                 40  // ํํผ ํํผ์จ ๋ณด๋์ค
#define EF_HITRATE               41  // ๋ฐ์ค ๋ช์ค๋ฅ  ๋ณด๋์ค
#define EF_CRITICAL              42  // ํฌ๋ฆฌ ํฌ๋ฆฌํฐ์ปฌ
#define EF_SANC                  43  // ์ ๋ จ Sanctuary ์ฃผ๋ก ๋ณด๋์ค ํฌ์ธํธ์ ํธ์ฑ๋๋ฉฐ
#define EF_SAVEMANA              44  // ๋ง์  1 -> 99%
#define EF_HPADD                 45  // ํผ๋ปฅ 1 -> 101%   MAX_MP
#define EF_MPADD                 46  // ๋ง๋ปฅ 1 -> 101%   MAX_MP
#define EF_REGENHP               47  // ํผํ
#define EF_REGENMP               48  // ๋งํ
#define EF_RESIST1               49  // ๋ถ์ 
#define EF_RESIST2               50  // ์ผ์ 
#define EF_RESIST3               51  // ์ ์ 
#define EF_RESIST4               52  // ๋ฒ์ 
#define EF_ACADD                 53  // ๋ฐฉ์ฆ
#define EF_RESISTALL             54  // ์ฌ์ 
#define EF_BONUS                 55  // ๋ณด๋
#define EF_HWORDGUILD            56  // ๊ธธ๋
#define EF_LWORDGUILD            57  // ๊ธธ๋
#define EF_QUEST                 58  // ์๋ฌด ๋ฐ๋์ 1๋ถํฐ ์์, ๋๋ถ๋ถ Volatile๊ณผ ํจ๊ฒ ์ธํ๋  ๊ฒ ์.
#define EF_UNIQUE                59  // ๊ณ ์ 
#define EF_MAGIC                 60  // ๋ง๊ณต 1 -> 1% ์ฆํญ
#define EF_AMOUNT                61  // ๊ฐฏ์
#define EF_HWORDINDEX            62  // ๋ฒํธ
#define EF_LWORDINDEX            63  // ๋ฒํธ
#define EF_INIT1                 64  // ์ด๊ธฐ
#define EF_INIT2                 65  // ์ด๊ธฐ
#define EF_INIT3                 66  // ์ด๊ธฐ
#define EF_DAMAGEADD             67  // ๋๋ฝ
#define EF_MAGICADD              68  // ๋ง๋ฝ
// ๋ค์ 2๋ค์ ๋ณ๋ก๋ ์ฟผ๋ฆฌ๋ฅผ ๋์ง์ง ์๊ณ  ๋ฒ ์ด์ค๋ฅผ ์ฟผ๋ฆฌ๋ฅผ ๋์ง๋ฉด ์ ๋ ์ต์์ ์๋ ์ฒดํนํ๋ ๋ถ๋ถ.
// ํด๋ผ์ด์ธํธ ํ๊ธฐ์๋ ์ด๊ฒ์ ๋จผ์  ๋์ ์ ์ด๊ฒ์ด ๋์ค๋ฉด ๋ฒ ์ด์ค๋ ๋์ง์ง ์๋๋ค.
#define EF_HPADD2                69  // ํผ2   ํผ๋ปฅ๊ณผ ๊ฐ์ผ๋ ๊ธฐ๋ณธ ์ต์ ํฌํจ๋์ด ํ์๋๋ค.
#define EF_MPADD2                70  // ํผ2   ํผ๋ปฅ๊ณผ ๊ฐ์ผ๋ ๊ธฐ๋ณธ ์ต์ ํฌํจ๋์ด ํ์๋๋ค.
#define EF_CRITICAL2             71  // ํฌ๋ฆฌ ํฌ๋ฆฌํฐ์ปฌ
#define EF_ACADD2                72  // ๋ฐฉ์ฆ
#define EF_DAMAGE2               73  //
#define EF_SPECIALALL            74  // ๋ง์คํฐ๋ฆฌ ๋ฌด๋ง์ ์ธ ์ฆ๊ฐ

#define	EF_CURKILL				 75  // not used
#define EF_LTOTKILL				 76  // not used
#define EF_HTOTKILL				 77  // not used
#define EF_INCUBATE				 78  // ๋ถํ์๊ณ์น


#define EF_MOUNTLIFE			 79  // ๋ง ์ด ์๋ช
#define EF_MOUNTHP				 80  // ๋ง ํ ํผ
#define EF_MOUNTSANC			 81  // ๋ง ์ฑ์ฅ๋
#define EF_MOUNTFEED			 82  // ๋ง ๋ฐฐ๋ถ๋ฆ ์ ๋
#define EF_MOUNTKILL			 83  // ๋ง์ด ์ฃฝ์ธ ๋ชฌ์คํฐ ๊ฐฏ์?

#define EF_INCUDELAY             84
#define EF_SUBGUILD				 85	// SUB๊ธธ๋, 0์ด๋ฉด ์ต๋๊ธธ๋ 1,2,3๊น์ง ์๋ค
#define EF_PREVBONUS			 86	// ์ด์ ์บ๋ํฐ ๋ ๋ฒจ๋ณด๋์ค

#define EF_REFLEVEL              87 // ์ ๋ จ ํ๋ฅ ์ด ์ ์ฉ๋๋ level
#define EF_GAMEROOM	             88

#define EF_ABSDAM                89 // ์ ๋ ๋ฐ๋ฏธ์ง, ๋ฐฉ์ด๋ ฅ ๋ฌด์
#define EF_ABSAC                 90 // ์ ๋ ๋ฐฉ์ด๋ ฅ

#define EF_RESET				 98
#define EF_EVO					 99

#define EF_GRADE0				 100 // 0๊ธ
#define EF_GRADE1				 101 // 1๊ธ
#define EF_GRADE2				 102 // 2๊ธ
#define EF_GRADE3				 103 // 3๊ธ
#define EF_GRADE4				 104 // 4๊ธ
#define EF_GRADE5				 105 // 5๊ธ

#define EF_DATE                  106 // ๋ ์ง
#define EF_HOUR                  107 // ์๊ฐ
#define EF_MIN                   108 // ๋ถ
#define EF_YEAR                  109 // ์ฐ๋
#define EF_MONTH                 110 // ์
#define EF_NOTRADE               111 // ๊ฑฐ๋๋ถ๊ฐ
#define EF_TRANS				 112 // ์ ์  ์์ดํ 0์ด๋ฉด ์ ์ฒด 1์ด๋ฉด ์ ์  ์ ์ฉ 2์ด๋ฉด ์ผ๋ฐ ์ ์ฉ

#define EF_STARTCOL              115
#define EF_COLOR0                116
#define EF_COLOR1                117
#define EF_COLOR2                118
#define EF_COLOR3                119
#define EF_COLOR4                120
#define EF_COLOR5                121
#define EF_COLOR6                122
#define EF_COLOR7                123
#define EF_COLOR8                124
#define EF_COLOR9                125
#define EF_MAXCOL                126

#endif // ITEMEFFECT_H_INCLUDED
