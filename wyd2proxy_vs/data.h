/***

The MIT License (MIT)

Copyright (c) 2017 Raphael Tom� Santana

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
***/

#ifndef DATA_H_INCLUDED
#define DATA_H_INCLUDED

#define GREEN_SAY				0xFF00CD00
#define PINK_SAY				0xFFFF00A0
#define WYDRGB(r,g,b)			((uint32_t)(0xFF000000 | (b & 0xFF) | ((g << 8) & 0xFF00) | ((r << 16) & 0xFF0000)))
#define IniInt					GetPrivateProfileIntA
#define IniString				GetPrivateProfileStringA
#define sim						1
#define nao						0
#define TIMER_MAIN				0x001
#define TIMER_TM_250			0x002
#define TIMER_TM_10000			0x003
#define WM_SOCKET_TM			WM_USER+0x101 //0x403
#define WM_SOCKET_DB			WM_USER+0x100 //0x402
#define WM_SOCKET_CLIENT		0x9000 //0x402
#define MAX_TM					10
#define MAX_NPKO				10
#define MAX_ITEMLIST			9000
#define MAX_SKILLDATA			256
#define MAX_MONTSTATS			128
#define MAX_MOBS				0x4000
#define MAX_CLIENTS				1000
#define MAX_INITITEM			128
#define MAX_GENERS				8192
#define MAX_DROPITEM			6500
#define MAX_GUILDS				0x10000
#define MAX_FIXED_MOBS			64
#ifndef MAX_SLOTS
	#define MAX_SLOTS				60
#endif
#ifndef MAX_SLOTS_INV
	#define MAX_SLOTS_INV			MAX_SLOTS
#endif
#ifndef MAX_SLOTS_WARE
	#define MAX_SLOTS_WARE			120
#endif
#define MAX_RECV				0x400
#define MAX_BUFFER				0x10000

#define FREE_DATA				2000
#define MAX_FREE_DATA			2200
#if defined(_DEBUG)
	#define INIT_DATA				FREE_DATA
	#define MAX_DATA				MAX_FREE_DATA
#else
	#define INIT_DATA				0
	#define MAX_DATA				2000
#endif

#define LIMIT_FRAGS				65000
#define LIMIT_STATUS			64000
#define LIMIT_HP_MP				64000
#define LIMIT_ATK_DEF			64000
#define LIMIT_MAGIC				1000
#define LIMIT_DAMAGE			32000

#define SET_ZERO				0x7FFFFFFF


#define GUILD_SLOT_ONLY 62
#define GUILD_SLOT_FULL (16+62)

enum PacketID
{
	//DB PACKET
	p_UpdateLeaders = 0x80D,
	p_SetFameNoatun = 0xC23,
	p_Disconnect = 0x3AE,
	p_DisconnectAccount = 0x40A,
	p_DisconnectAccountTM = 0x40B,
	p_StartTM = 0xC19,
	p_StartTMOK = 0x424,
	p_AttGuild = 0xC17,
	p_SetLeaderCity = 0xC18,
	p_SetKefraGuild = 0xC15,
	p_Celestial = 0x0C34,
	p_Trombeta = 0xD1D,
	p_Auxiliar = 0xD1E,
	p_InitGuild = 0x0C1B,
	p_ChangeChannel = 0x814,
	p_RequestWar = 0xE0E,
	p_RoomTimer = 0x3A1,
	p_RoomCounter = 0x3B0,
	p_RequestAlly = 0xE12,
	p_SendLeaders = 0x427,
	p_RecvSapphire = 0x80A,
	p_SendSapphire = 0x423,
	p_WarChannel = 0xED7,
	p_BuyItemCash = 0xFDC,
	p_LockPassSuccess = 0xFDE,
	p_LockPassFail = 0xFDF,
	p_ClientLogin = 0x803,
	p_ClientReLogin = 0x808, //new packet login email
	p_LoginInexistent = 0x421,
	p_LoginBlocked = 0x425,
	p_LoginOtherComp = 0x406,
	p_LoginOnline = 0x420,
	p_LoginOnlineS = 0x41F,
	p_PasswordIncorrect = 0x422,
	p_Message = 0x404,
	p_DisconnectAcc = 0x805,
	p_CreateChar = 0x802,
	p_CreateCharSuccess = 0x418,
	p_CreateCharFail = 0x41D,
	p_DeleteChar = 0x809,
	p_DeleteCharFail = 0x041E,
	p_DeleteCharSuccess = 0x419,
	p_SubGuild = 0x3C1C,
	//p_UnkGuild = 0x3C1A,
	p_UpdateGuild = 0x3C16,
	p_MessageNPKO = 0x3409,
	p_SelectCharDB = 0x804,
	p_SaveChar = 0x807,
	p_SaveCharQuit = 0x806,
	p_OutSealFail = 0x431,
	p_OutSealComplete = 0xC30,
	p_PutSealFail = 0x42F,
	p_PutSealComplete = 0xC2E,
	p_SendCommand = 0xC2F,
	p_DonateResp = 0x405,
	p_SealInfo = 0xC32,
	p_ItemSend = 0xC0F,
	p_ChangeChannelInfo = 0x52A,
	p_CmdSend = 0xC0B,
	p_SelectCharFail = 0x41C,
	p_LoginSuccess = 0x416,
	p_SelectCharTM = 0x417,
	p_Quiz = 0x7B1,
	/* TM*/
	p_MobKilled = 0x165, //401884
	p_Spawn = 0x364,
	p_SpawnTrade = 0x363,
	p_Attack = 0x39D,
	p_AttackE = 0x39E,
	p_AttackArea = 0x367,
	p_ConquistandoAltar = 0x3AD,
	p_NpcCount = 0x3BB,
	p_Gold = 0x3AF,
	p_CNFMobKill = 0x338,
	p_Tick = 0x3A0,
	p_RequestLogin = 0x20D,
	p_Trade = 0x383,
	p_CreateTrade = 0x397,
	p_BuyItem = 0x398,
	p_SellItem = 0x37A,
	p_DropItem = 0x272,
	p_RequestCreateChar = 0x20F,
	p_RequestDeleteChar = 0x211,
	p_RequestSelectChar = 0x213,
	p_CreateCharSuccessTM = 0x110,
	p_CreateCharFailTM = 0x11A,
	p_DeleteCharFailTM = 0x11B,
	p_RequestBackToCharlist = 0x215,
	p_SendCharlistTM = 0x116,
	p_RequestNpcItems = 0x27B,
	p_SendNpcItems = 0x17C,
	p_RequestRevive = 0x289,
	p_RequestNpcClick = 0x28B,
	p_LoginSuccessTM = 0x10A,
	p_CharInfo = 0x114,
	p_CharInfoFailTM = 0x119,
	p_LoginOnlineTM = 0x11D,
	p_Move1 = 0x36C,
	p_Move2 = 0x366,
	p_DeleteCharSuccessTM = 0x112,
	p_SealInfoTM = 0xDC3,
	p_Weather = 0x18B,
	p_ChangeCity = 0x290,
	p_UpdateCity = 0x291,
	p_UpdateScore = 0x336,
	p_Summon1 = 0x3B3
};

enum MOB_CLASS
{
	TK,
	FM,
	BM,
	HT
};

enum EQUIP_SLOT
{
	FACE,
	HELM,
	ARMOR,
	PANTS,
	GLOVES,
	BOOTS,
	WEAPON1,
	WEAPON2,
	RING,
	BRACE,
	ORB,
	GEM,
	SET,
	PET,
	MOUNT,
	CAPE
};

enum Buffs
{
	Lentidao = 1,
	Velocidade_P,
	Resistencia_N,
	Ataque_Bonus,
	Evasao_N,
	Evasao_P,
	Velocidade_N,
	Joia,
	Dano_P,
	Ataque_N,
	Escudo_Magico,
	Defesa_N,
	Assalto,
	Possuido,
	Tecnica_P,
	TransformacaoBM,
	Aura_da_Vida,
	Controle_de_Mana,
	Imunidade,
	VenenoFM,
	Meditacao,
	Trovao,
	Aura_Bestial,
	Samaritano,
	Protecao_Elemental,
	Evasao_HT,
	Congelamento,
	Invisibilidade,
	Limite_da_Alma,
	Bonus_PvM,
	Escudo_Dourado,
	Cancelamento,
	Transformacao,
	Comida,
	Bonus_HP_MP,
	Veneno_HT_TK,
	Ligacao_Espectral,
	Troca_de_Espirito,
	Bonus_EXP,
	Atordoado,
	Esquiva_N,
	Magia_Misteriosa,
	Anti_Magia,
	Movimento_Zero,
	Congelar,
	Chama_Resistente,
	Sangrar,
	Ultima_Resistencia
};

struct s_TransBM
{
	/*
	int32_t HP[6]; //[0]
	int32_t MoveSpeed;//[28]
	int32_t AtkSpeed;//[32]
	int32_t CriticalSum;//[40]
	int32_t CriticalFactor;//[44]
	int32_t Resist;//[48]
	int32_t RefinFace;//[60]
	*/
	int32_t v[17];
};

//static int32_t MinXY[] = {0, 0};//8C762A0 8C762A4
//static int32_t MaxXY[] = {4096, 4096};//5A1034 5A1038

extern int32_t CriticalMatrix[];// = {512, 384, 448, 640, 480, 896, 576, 129, 496, 383, 960, 641, 544, 895, 65, 130, 504, 382, 447, 642, 992, 894, 577, 131, 528, 381, 959, 643, 33, 893, 66, 132, 508, 380, 446, 644, 479, 892, 578, 133, 999, 379, 958, 645, 545, 891, 67, 134, 520, 378, 445, 646, 991, 890, 579, 135, 17, 377, 957, 647, 34, 889, 68, 136, 510, 376, 444, 648, 478, 888, 580, 137, 495, 375, 956, 649, 546, 887, 69, 138, 999, 374, 443, 650, 990, 886, 581, 139, 529, 373, 955, 651, 35, 885, 70, 140, 516, 372,442, 652, 477, 884, 582, 141, 999, 371, 954, 653, 547, 883, 71, 142, 9, 370, 441, 654, 989, 882, 583, 143, 18, 369, 953, 655, 36, 881, 72, 144, 511, 368, 440, 656, 476, 880, 584, 145, 494, 367, 952, 657, 548, 879, 73, 146, 503, 366, 439, 658, 988, 878, 585, 147, 530, 365, 951, 659, 37, 877, 74, 148, 999, 364, 438, 660, 475, 876, 586, 149, 999, 363, 950, 661, 549, 875, 75, 150, 521, 362, 437, 662,987, 874, 587, 151, 19, 361, 949, 663, 38, 873, 76, 152, 514, 360, 436, 664, 474, 872, 588, 153, 493, 359, 948, 665, 550, 871, 77, 154, 999, 358, 435, 666, 986, 870, 589, 155, 531, 357, 947, 667, 39, 869, 78, 156, 5, 356, 434, 668, 473, 868, 590, 157, 999, 355, 946, 669, 551, 867, 79, 158, 10, 354, 433, 670, 985, 866,591, 159, 20, 353, 945, 671, 40, 865, 80, 160, 999, 352, 432, 672, 472, 864, 592, 161, 492, 351, 944, 673, 552, 863, 81, 162, 502, 350, 431, 674, 984, 862, 593, 163, 532, 349, 943, 675, 41, 861, 82, 164, 507, 348, 430, 676, 471, 860, 594, 165, 999, 347, 942, 677, 553, 859, 83, 166, 522, 346, 429, 678, 983, 858, 595, 167, 21, 345, 941, 679, 42, 857, 84, 168, 999, 344, 428, 680, 470, 856, 596, 169,491, 343, 940, 681, 554, 855, 85, 170, 999, 342, 427, 682, 982, 854, 597, 171, 533, 341, 939, 683, 43, 853, 86, 172, 517, 340, 426, 684, 469, 852, 598, 173, 999, 339, 938, 685, 555, 851, 87, 174, 11, 338, 425, 686, 981, 850, 599, 175, 22, 337, 937, 687, 44, 849, 88, 176, 513, 336, 424, 688, 468, 848, 600, 177, 490, 335, 936, 689, 556, 847, 89, 178, 501, 334, 423, 690, 980, 846, 601, 179, 534, 333, 935, 691, 45, 845, 90, 180, 999, 332, 422, 692, 467, 844, 602, 181, 999, 331, 934, 693, 557, 843, 91, 182, 523, 330, 421, 694, 979, 842, 603, 183, 23, 329, 933, 695, 46, 841, 92, 184, 3, 328, 420, 696, 466, 840, 604, 185, 489, 327, 932, 697, 558, 839, 93, 186, 999, 326, 419, 698, 978, 838, 605, 187, 535, 325, 931, 699, 47, 837, 94, 188, 6, 324, 418, 700, 465, 836, 606, 189, 999, 323, 930, 701, 559, 835, 95, 190, 12, 322, 417, 702, 977, 834, 607, 191, 24, 321, 929, 703, 48, 833, 96, 192, 512, 320, 416, 704, 464, 832, 608, 193, 488, 319, 928, 705, 560, 831, 97, 194, 500, 318, 415, 706, 976, 830, 609, 195, 536, 317, 927, 707, 49, 829, 98, 196, 506, 316, 414, 708, 463, 828, 610, 197, 999, 315, 926, 709, 561, 827,99, 198, 524, 314, 413, 710, 975, 826, 611, 199, 25, 313, 925, 711, 50, 825, 100, 200, 509, 312, 412, 712, 462, 824, 612, 201, 487, 311, 924, 713, 562, 823, 101, 202, 999, 310, 411, 714, 974, 822, 613, 203, 537, 309, 923, 715, 51, 821, 102, 204, 518, 308, 410, 716, 461, 820, 614, 205, 999, 307, 922, 717, 563, 819, 103, 206, 13, 306, 409, 718, 973, 818, 615, 207, 26, 305, 921, 719, 52, 817, 104, 208, 999, 304, 408, 720, 460, 816, 616, 209, 486, 303, 920, 721, 564, 815, 105, 210, 499, 302, 407, 722, 972, 814, 617, 211, 538, 301, 919, 723, 53, 813, 106, 212, 999, 300, 406, 724, 459, 812, 618, 213, 998, 299, 918, 725, 565, 811, 107, 214, 525, 298, 405, 726, 971, 810, 619, 215, 27, 297, 917, 727, 54, 809, 108, 216,515, 296, 404, 728, 458, 808, 620, 217, 485, 295, 916, 729, 566, 807, 109, 218,999, 294, 403, 730, 970, 806, 621, 219, 539, 293, 915, 731, 55, 805, 110, 220, 7, 292, 402, 732, 457, 804, 622, 221, 997, 291, 914, 733, 567, 803, 111, 222, 14, 290, 401, 734, 969, 802, 623, 223, 28, 289, 913, 735, 56, 801, 112, 224, 1, 288, 400, 736, 456, 800, 624, 225, 484, 287, 912, 737, 568, 799, 113, 226, 498, 286, 399, 738, 968, 798, 625, 227, 540, 285, 911, 739, 57, 797, 114, 228, 505, 284, 398, 740, 455, 796, 626, 229, 996, 283, 910, 741, 569, 795, 115, 230, 526, 282, 397, 742, 967, 794, 627, 231, 29, 281, 909, 743, 58, 793, 116, 232, 999, 280, 396, 744, 454, 792, 628, 233, 483, 279, 908, 745, 570, 791, 117, 234, 999, 278, 395, 746, 966, 790, 629, 235, 541, 277, 907, 747, 59, 789, 118, 236, 519, 276, 394, 748, 453, 788, 630, 237, 995, 275, 906, 749, 571, 787, 119, 238, 15, 274, 393, 750, 965, 786, 631, 239, 30, 273, 905, 751, 60, 785, 120, 240, 2, 272, 392, 752, 452, 784, 632, 241, 482, 271, 904, 753, 572, 783, 121, 242, 497, 270, 391, 754, 964, 782, 633, 243, 542, 269, 903, 755, 61, 781, 122, 244, 999, 268, 390, 756, 451, 780, 634, 245, 994, 267, 902, 757, 573, 779, 123, 246, 527, 266, 389, 758, 963, 778, 635, 247, 31, 265, 901, 759, 62, 777, 124, 248, 4, 264, 388, 760, 450, 776, 636, 249, 481, 263, 900, 761, 574, 775, 125, 250, 999, 262, 387, 762, 962, 774, 637, 251, 543, 261, 899, 763, 63, 773, 126, 252, 8, 260, 386, 764, 449,772, 638, 253, 993, 259, 898, 765, 575, 771, 127, 254, 16, 258, 385, 766, 961, 770, 639, 255, 32, 257, 897, 767, 64, 769, 128, 256};


struct s_MontStats //597720 _ mont_table_status
{
	int32_t Damage;//[00]
	int32_t Magic;//[04]
	int32_t Evade;//[08]
	int32_t Resist;//[12]
	int32_t Speed;//[16]
	int32_t Abs;//[20]
};

struct s_ItemList//9CCCXX 9CCC40
{
	char Name[64];//[040]
	int16_t Mesh1;//[080]
	int32_t Mesh2;//[082]
	int16_t Level;//[086]
	int16_t Str;//[088]
	int16_t Int;//[08A]
	int16_t Dex;//[08C]
	int16_t Con;//[08E]
	struct
	{
		int16_t Index;
		int16_t Value;
	} Effect[12];//[090]
	int32_t Price;//[0C0]
	int16_t Unique;//[0C4]
	uint16_t Pos;//[0C6]
	int16_t Grade;//[0C8]
	int16_t Extreme;//[0CA]
	s_MontStats Mont;//[0CC] //GetMontIndex
	char CapeInfo;//[0E4]//401870
};

#pragma pack(push, 1)
struct s__ItemList//9CCCXX 9CCC40
{
	char Name[64];//[000]
	int16_t Mesh1;//[040]
	int32_t Mesh2;//[042]
	int16_t Level;//[046]
	int16_t Str;//[048]
	int16_t Int;//[04A]
	int16_t Dex;//[04C]
	int16_t Con;//[04E]
	struct
	{
		int16_t Index;
		int16_t Value;
	} Effect[12];//[050]
	int32_t Price;//[080]
	int16_t Unique;//[084]
	uint16_t Pos;//[086]
	int16_t Extreme;//[088]
	int16_t Grade;//[08A]
};
#pragma pack(pop)

struct s_SkillData//8B61C8
{
	uint32_t SkillPoint;		//[00]
	uint32_t TargetType;		//[04]
	uint32_t ManaSpent;		//[08]
	uint32_t Delay;			//[0C]
	uint32_t Range;			//[10]
	uint32_t InstanceType;		//[14]
	uint32_t InstanceValue;	//[18]
	uint32_t TickType;			//[1C]
	uint32_t TickValue;		//[20]
	uint32_t AffectType;		//[24]
	uint32_t AffectValue;		//[28]
	uint32_t AffectTime;		//[2C]
	struct
	{
		char Man[8];//[-468]
		char Woman[8];//[-470]
	} Action;				//[30]
	uint32_t InstanceAttribute;//[40]
	uint32_t TickAttribute;	//[44]
	uint32_t Aggressive;		//[48]
	uint32_t MaxTarget;		//[4C]
	uint32_t PartyCheck;		//[50]
	uint32_t AffectResist;		//[54]
	uint32_t PassiveCheck;		//[58]
	uint32_t ForceDamage;		//[5C]
};//[60]

struct s__SkillData//8B61C8
{	//[4A0]
	uint16_t SkillPoint;		//[00][01]
	uint8_t TargetType;		//[02]
	uint16_t ManaSpent;		//[03][04]
	uint16_t Delay;			//[05][06]
	uint8_t Range;			//[07]
	uint8_t InstanceType;	//[08]
	uint8_t InstanceValue;	//[09]
	uint8_t TickType;		//[0A]
	uint8_t TickValue;		//[0B]
	uint8_t AffectType;		//[0C]
	uint16_t AffectValue;	//[0D][0E]
	uint32_t AffectTime;		//[0F][10][11][12]
	//-438/-420
	struct
	{
		char Man[6];//[-468]
		char Woman[6];//[-470]
	} Action;//[13]
	uint8_t InstanceAttribute;//[1F]
	uint8_t TickAttribute;//[20]
	uint8_t Aggressive;//[21]
	uint8_t MaxTarget;//[22]
	uint8_t PartyCheck;//[23]
	uint8_t AffectResist;//[24]
	uint8_t PassiveCheck;//[25]
	uint8_t ForceDamage;//[26]
};//[27]

struct s_InitItem
{
	int16_t PosX;//8B88D0
	int16_t PosY;//8B88D2
	int16_t nIndex;//8B88D4
	int16_t Rotate;//8B88D6
};

struct _sItem
{
	int16_t ItemID;

	union
	{
		uint8_t efs[6];
		struct
		{
			uint8_t Index;
			uint8_t Value;
		} Effect[3];
		struct
		{
			int16_t HP;//[02]
			uint8_t Grow_Level;//[04] 2330_2360
			uint8_t Vit;//[05]
			uint8_t Food;//[06]
			uint8_t Exp;//[07]
		} Mount; //2330
		struct
		{
			uint8_t PKPoint;//[02]
			uint8_t CurKill;//[03]
			uint8_t Guilty;//[04]
			uint8_t LowFrag;//[05]
			uint8_t Index;//[06]
			uint8_t HighFrag;//[07]
		} Ass; //2360
	};
};

struct sItem : _sItem
{

	void clear() /* 40173A */
	{
		memset(this, 0, sizeof(sItem));
	}

	void set(int32_t vet[7])
	{
		ItemID = vet[0];
		efs[0] = vet[1];
		efs[1] = vet[2];
		efs[2] = vet[3];
		efs[3] = vet[4];
		efs[4] = vet[5];
		efs[5] = vet[6];
	}

	void set(int32_t id, int32_t ef1 = 0, int32_t efv1 = 0, int32_t ef2 = 0, int32_t efv2 = 0, int32_t ef3 = 0, int32_t efv3 = 0)
	{
		ItemID = id;
		efs[0] = ef1;
		efs[1] = efv1;
		efs[2] = ef2;
		efs[3] = efv2;
		efs[4] = ef3;
		efs[5] = efv3;
	}

	void toString(char(&dst)[64]) /* 401B1D */
	{
		sprintf(dst, " (%d %d %d %d %d %d %d)", ItemID, efs[0], efs[1], efs[2], efs[3], efs[4], efs[5]);
	}

	void operator() ()
	{
		clear();
	}

	friend bool operator== (const sItem &lhs, const  sItem &rhs)
	{
		return memcmp(&lhs, &rhs, sizeof(sItem)) ? false : true;
	}

	friend bool operator== (const sItem &lhs, const  int32_t &rhs)
	{
		return lhs.ItemID == rhs;
	}

	friend bool operator== (const int32_t &lhs, const  sItem &rhs)
	{
		return lhs == rhs.ItemID;
	}

	friend bool operator!= (const sItem &lhs, const  sItem &rhs)
	{
		return !(lhs == rhs);
	}

	friend bool operator!= (const sItem &lhs, const  int32_t &rhs)
	{
		return !(lhs == rhs);
	}

	friend bool operator!= (const int32_t &lhs, const  sItem &rhs)
	{
		return !(lhs == rhs);
	}

	int32_t GetEffValue(int32_t EF);

	void CalcBaseStats(int32_t mobLevel, int32_t arg2, int32_t bonusDrop, int32_t arg4); /* 4015CD */

	int32_t CanSell();/* 401C49 */

	void SetTimeCash(int32_t days); /* 40136B */

	int32_t GetTimeCash(); /* 401A0F */

	int32_t GetGuildId() /* 401672 */
	{
		int32_t gIndex = 0;
		for (int32_t i = 0; i < 3; i++)
		{
			if (Effect[i].Index == EF_HWORDGUILD)
			{
				gIndex += (Effect[i].Value << 8);
			}
			else if (Effect[i].Index == EF_LWORDGUILD)
			{
				gIndex += Effect[i].Value;
			}
		}
		return gIndex;
	}

	int32_t GetRawRefin() /* 40103C */
	{
		int32_t ref = 0;
		if (ItemID >= 2330 && ItemID < 2390)
			return 0;
		/*
			if (ItemID >= 3200 && ItemID < 3300)
				return 0;
		*/
		if (ItemID >= 3980 && ItemID < 4000)
			return 0;
		for (int32_t i = 0; i < 3; i++)
		{
			if (Effect[i].Index == EF_SANC || (Effect[i].Index >= EF_STARTCOL || Effect[i].Index <= EF_MAXCOL))
			{
				ref = Effect[i].Value;
				break;
			}
		}
		if (ref < 230)
			ref = ref % 10;
		else
			ref -= 220;
		return ref;
	}

	int32_t GetRefin() /* 4014E2 */
	{
		int32_t ref = 0;
		if (ItemID >= 2330 && ItemID < 2390)
			return 0;
		/*
			if (ItemID >= 3200 && ItemID < 3300)
				return 0;
		*/
		for (int32_t i = 0; i < 3; i++)
		{
			if (Effect[i].Index == EF_SANC || (Effect[i].Index >= EF_STARTCOL || Effect[i].Index <= EF_MAXCOL))
			{
				ref = Effect[i].Value;
				break;
			}
		}
		if (ItemID != 786 && ItemID != 1936 && ItemID != 1937)
		{
			if (ref < 230)
				ref = ref % 10;
			else
				ref -= 220;
			if (ref >= 10 && ref <= 35)
				ref = ((ref - 10) / 4) + 10;
		}
		return ref;
	}

	int32_t PutRefin(int32_t v, int32_t perc) /* 401109 */
	{
		int32_t i;
		if (v < 0)
			v = 0;
		else if (v > 33)
			return 0;
		if (perc < 0)
			perc = 0;
		else if (perc > 20)
			perc = 20;
		int32_t ref;//-4, -8
		/*
		if(v > 9)
		{
			aux1 = v - 9;
			v = 9;
		}
		if(aux1 <= 0)
			aux2 = perc * 10 + v;
		else
			aux2 = aux1 + 229;*/
		if (v > 9)
			ref = v + 220;
		else
			ref = perc * 10 + v;
		for (i = 0; i < 3; i++)
		{
			if (Effect[i].Index == EF_SANC || (Effect[i].Index >= EF_STARTCOL || Effect[i].Index <= EF_MAXCOL))
			{
				Effect[i].Value = ref;
				return 1;
			}
		}
		for (i = 0; i < 3; i++)
		{
			if (Effect[i].Index == 0)
			{
				Effect[i].Index = EF_SANC;
				Effect[i].Value = ref;
				return 1;
			}
		}
		return 0;
	}

	void AddRefin(int32_t v)
	{
		int32_t i;
		if (ItemID == 0)
			return;
		for (i = 0; i < 3; i++)
		{
			if (Effect[i].Index == EF_SANC || (Effect[i].Index >= EF_STARTCOL || Effect[i].Index <= EF_MAXCOL))
			{
				if (v == SET_ZERO)
					Effect[i].Value = 0;
				else
				{
					int32_t tmp = Effect[i].Value + v;
					if (tmp < 0)
						tmp = 0;
					else if (tmp > 255)
						tmp = 255;
					Effect[i].Value = tmp;
				}
				return;
			}
		}
		if (v > 0 && v != SET_ZERO)
		{
			for (i = 0; i < 3; i++)
			{
				if (Effect[i].Index == 0)
				{
					Effect[i].Index = EF_SANC;
					Effect[i].Value = v;
					return;
				}
			}
		}
	}

	int32_t GetAmount() /* 4013D9 */
	{
		int32_t r = 1;
		if (Effect[0].Index == EF_AMOUNT)
			r = Effect[0].Value;
		else if (Effect[1].Index == EF_AMOUNT)
			r = Effect[1].Value;
		else if (Effect[2].Index == EF_AMOUNT)
			r = Effect[2].Value;
		return ItemID != 0 ? r : 0;
	}

	void AddAmount(int32_t _v) /* 40187F */
	{
		int32_t p;
		if (ItemID == 0)
		{
			clear();
			return;
		}
		if (ItemID == 419 || ItemID == 420 || ItemID == 412 || ItemID == 413 ||
			(ItemID >= 2390 && ItemID < 2418))
		{
			if (Effect[0].Index == 59)
				Effect[0].Index = Effect[0].Value = 0;
			if (Effect[1].Index == 59)
				Effect[1].Index = Effect[1].Value = 0;
			if (Effect[2].Index == 59)
				Effect[2].Index = Effect[2].Value = 0;
		}
		for (p = 0; p < 3; p++)
		{
			if (Effect[p].Index == EF_AMOUNT)
			{
				int32_t v = Effect[p].Value + _v;
				if (v == 0)
					Effect[p].Index = 0;
				else if (v == 1 && ItemID != 493 && ItemID != 494)
				{
					Effect[p].Index = 0;
					v = 0;
				}
				else if (v > 120)
					v = 120;
				Effect[p].Value = v;
				return;
			}
		}
		if (_v > 1 || (ItemID != 493 && ItemID != 494))
		{
			for (p = 0; p < 3; p++)
			{
				if (Effect[p].Index == 0)
				{
					Effect[p].Index = EF_AMOUNT;
					if (_v > 120)
						_v = 120;
					Effect[p].Value = _v;
					return;
				}
			}
		}
	}

	sItem()
	{
		ItemID = 0;
		memset(efs, 0, 6);
	}

	sItem(sItem &old)
	{
		ItemID = old.ItemID;
		memcpy(efs, old.efs, 6);
	}

	sItem(int32_t _ints[7])
	{
		ItemID = _ints[0];
		for (int32_t i = 0; i < 6; i++)
			efs[i] = _ints[i + 1];
	}

	sItem(int32_t ID)
	{
		ItemID = ID;
		memset(efs, 0, 6);
	}

	sItem(int32_t ID,
		uint8_t ef1, uint8_t efv1 = 0,
		uint8_t ef2 = 0, uint8_t efv2 = 0,
		uint8_t ef3 = 0, uint8_t efv3 = 0)
	{
		ItemID = ID;
		efs[0] = ef1;
		efs[1] = efv1;
		efs[2] = ef2;
		efs[3] = efv2;
		efs[4] = ef3;
		efs[5] = efv3;
	}
};

struct s_DropItem
{
	sItem ItemData;//8863698
	int32_t Status;//88636A0
	int32_t PosX;//88636A4
	int32_t PosY;//88636A8
	int32_t Ground;//88636AC
	int32_t Enable;//88636B0
	int32_t Rotate;//88636B4
	int32_t Free;//88636B8 ?
	int32_t Free2;//88636BC ?
	int32_t Height;//88636C0
	int32_t TmCount;//88636C4
	int32_t Key1;//88636C8
};

#endif // DATA_H_INCLUDED
