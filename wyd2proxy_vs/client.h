/***

The MIT License (MIT)

Copyright (c) 2017 Raphael Tom� Santana

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
***/

#ifndef CLIENT_HPP_INCLUDED
#define CLIENT_HPP_INCLUDED

enum e_cfg
{
	_nil = -1,
	_amount,
	_bau,
	_drop,
	_trade,
	_buff,
	_skill,
	_login,
	_use,
	_commands,
	_over,
	_packet,
	_pot,
	_mount,
	_plano,
	_servidor,
	_chave,
	_party,
	_senha,
	_senha2,
	_slot,
	_sv,
	_ch,
	_cliver,
	_porta,
	_delay,
	_loop,
	_repeat,
	_sleep_,
	_proxy,
	_autovenda,
	_loginid,
	_pcdata,
	_mac,
	_rede,
	_troca,
	_loginpak
};
/*
#define _nil -1
#define _amount 0
#define _bau 1
#define _drop 2
#define _trade 3
#define _buff 4
#define _skill 5
#define _login 6
#define _use 7
#define _commands 8
#define _over 9
#define _packet 10
#define _hp 11
#define _mount 12
#define _plano 13
#define _servidor 14
#define _chave 15
#define _party 16
#define _senha 17
#define _senha2 18
#define _slot 19
#define _sv 20
#define _ch 21
#define _cliver 22
#define _porta 23
#define _tickdiff 24
#define _delay 25
#define _loop 26
*/

struct sk_756
{
	s_SkillData sk[103];
    char unk[100];
} extern SkillData;

typedef struct
{
	int32_t id;
	uint32_t time;
} s_targets;

class Client
{
public:
	enum log_types
	{
		no_log = 0,
		log_all,
		log_filter
	};

	enum e_planos
	{
		free_ = 0,
		money_,
		test_
	} planos;

	enum login_types
	{
		autologin = 0,
		hook
	};

	enum e_server
	{
		_don,
		_don2,
		_kofd,
		_wyd2,
		_over,
		_myd,
		_brazuc,
		_legacy,
		_uow,
		_star,
		_aon,
		_gd,
		_final,
		_hero
	} server;

	struct log_struct
	{
		uint8_t *packet;
		int32_t size;
		int32_t send;
		bool show;
		bool file;
	};

	static uint32_t class_timer[7];//[50][54][58][5C][60][64][68]
    static uint32_t static_timer[2];//[96F210][96F214]
    static uint32_t pak_timer[2];
    static uint32_t recv_timer[3];//[13FAA00][13FAA04][13FAA08]
    /*
     * 429126
     * [ECX+8] = [96F210]
     * [96F214] = [96F210]
     *
     * 4C6BF0
     * [96F210] = timeGetTime() - [58] + [54]
     */
//private:
	/* Socket */
	int32_t ch, sv;
	char serverIP[32];
	int32_t serverPort;
	int32_t loginID;
	uint32_t s;
    fd_set set;
	/* Buffer */
	char buffer_recv[MAX_BUFFER+1];
	char buffer_send[MAX_BUFFER+1];
	struct sTimerPacket
	{
		time_t time;
		uint8_t buffer[MAX_BUFFER_TIMER];
	} TimerPacket[MAX_TIMER_PACKETS+1];
	int32_t pos_recv;
	int32_t pos_decrypt;
	int32_t pos_send;
	int32_t pos_encrypt;
	/* Packet */
	char keys[512];
	uint32_t min_tick;
	uint32_t time_tick;
	uint32_t send_tick;
	login_types login_type;
	char login_keys[16];
	int32_t keys_count;
	uint8_t client_mac[16];
	char servidor[16];
	char chave[48];
	char lastNick[18];
	char login[18];
	char senha[18];
	char senha2[8];
	char fproxy_ip[32];
	int32_t fproxy_port;
	int32_t slot;
	int32_t cliver;
	int32_t cID;
	int32_t novato;
	int32_t n_svs;
	bool dump_send;
	bool dump_recv;
	/* Log */
	FILE *f_mainlog;
	FILE *f_debuglog;
	int32_t fd;
	/* Timers */
	int32_t delay_thread;
	int64_t timer_count;
	uint8_t wait_recv[MAX_RECV];
	uint8_t packet_key[MAX_RECV];
	int32_t delay_count;
    int32_t buff_count;
	int32_t last_city;
	/* Client */
	MSG_SPAWN mobs[30002];
	uint8_t mob_map[4096][512];
	MSG_SENDNPCITEMS npcs[64];
	s_targets *mobs_targets[30002];
	struct s_hacks
	{
		char hash[64];
		struct
		{
			int32_t on;
		} showid;
		struct
		{
			int32_t id;
		} skill;
		struct
		{
			int32_t on;
		} drop;
		struct
		{
			int32_t on;
		} getout;
		struct
		{
			int32_t id;
		} chat;
		struct
		{
			int32_t on;
		} range;
		struct
		{
			int32_t id;
			int32_t delay;
			int32_t c;
		} inv;
		struct
		{
			int32_t on;
			char nick[16];
		} party;
		struct
		{
			char nick[16];
		} trade;
		struct
		{
			int32_t on;
			int32_t id;
		} protect;
		struct
		{
			int32_t on;
			int32_t progress;
			int32_t tipo;
			int32_t buff;
			int32_t mob;
			int32_t skill;
			int32_t stage;
			int32_t re;
			int32_t re_c;
			int32_t re_l;
			char pt[16];
			sPoint<int32_t> pos;
			s_targets targets[64];
			int32_t hp[2];
		} macro;
		struct
		{
			int32_t loop;
			int32_t repeat;
			char cmd[MAX_COMMANDS][32];
			int32_t n;
			int32_t i;
			int32_t lastMove;
			int32_t c;
			int32_t ciclos;
			int32_t contador;
		} commands;
	} hacks;
	map<string, int32_t> labels;
	struct scData
	{
		int32_t range;
		int32_t wtype;
	} cData;
	MSG_BULLSHITDON pBullshitDon;
	MSG_CREATETRADE pNpcTrade;
	MSG_TRADE pTrade;
	MSG_LOGIN pLogin;
	uint8_t mLogin[sizeof(MSG_LOGIN)][8][2];
	uint32_t dLogin[sizeof(MSG_LOGIN)][8];
	MSG_CHARINFO pChar;
	MSG_ATTBUFFS pBuffs;
	uint8_t skillbar[20];
	uint8_t cskillbar[20];
	int32_t party[14];
	uint8_t drop_list[MAX_ITEMLIST];
	uint8_t drop_efs[MAX_ITEMLIST][5][3][2];
	uint16_t amount_list[MAX_ITEMLIST];
	uint16_t trade_list[MAX_ITEMLIST];
	uint8_t opcodes[0xFFFF];
	char dir_data[128];
	char dir_log[128];
	char dir_check[128];
	char names[2][128];
	uint8_t lastSlotDeleted;
	uint16_t lastInv[MAX_SLOTS_INV];
	int32_t daylog;

//public:
	int32_t GetPlayerTotalEf(int32_t ef);
	int32_t GetThreadDelay() { return delay_thread; }
	int32_t IsEmpty(int32_t x, int32_t y);
	void MapBit(int32_t x, int32_t y, int32_t _set);
	/* Timers */
	uint32_t getTimerRecv();
	void attTimerRecv(uint32_t _new);
	void attTime(uint32_t *t);
	/* Thread */
	void DoWork();
	void CreateLogs();
	/* Packet */
	void ConfirmPortal();
	void UpdateCity(int32_t city);
	void SignalParam(int32_t code, int32_t parm);
	void Signal(int32_t code = 0x3A0);
	void InsertPacket(time_t t, MSG_HEADER *pak, int32_t pos = -1);
	void BullshitDon();
	void OpenStore();
	void LockPasswd(char *pass);
	void Select(int32_t slot);
	void Chat(const char *str, ...);
	void Pvt(const char *cmd, const char *str);
	void Revive();
	int32_t GetCity(int32_t x, int32_t y);
	int32_t GetCritical();
	void Attack(int32_t aID, sPoint<int16_t> *aPos, int32_t tID, sPoint<int16_t> *tPos, int32_t skIdx, int32_t motion, int32_t skParm, int32_t dmg, int32_t tID2 = 0, int32_t dmg2 = 0);
	void AttackArea(int32_t aID, sPoint<int16_t> *aPos, int32_t skIdx, sPoint<int32_t> *tID);
	int32_t CheckHP(int32_t _max, int32_t _atual, int32_t ef, int32_t mount = 0);
	void Macro(int32_t tipo);
    int32_t isEvok(int32_t skIdx);
	void InsertMacro(int32_t idx);
	void RemoveMacro(int32_t idx);
	void LeaveParty();
	void Party(int32_t id, char *nick);
	void DeleteItem(int32_t slot, int32_t id);
	void ShiftItem(int32_t slot, int32_t id, int32_t n);
	int32_t UseItem(int32_t src_type, int32_t src_pos, int32_t dst_type, int32_t dst_pos, int32_t op);
	void MoveItem(int32_t src_type, int32_t src_pos, int32_t dst_type, int32_t dst_pos);
	void SellItem(int32_t npc, int32_t src_type, int32_t src_pos);
	void BuyItem(int32_t npc, int32_t src, int32_t dst);
	void Click(int32_t npc);
	void Grifo(int32_t op);
	int32_t Move(int32_t x, int32_t y, int32_t real = 0);
	void *GetInfo(int32_t type);
	void PacketControl(MSG_HEADER *header);
	void Dump(bool send, int32_t size, void *ptr);
	void xDump(int32_t size, void *ptr);
	int32_t ExecuteCommand(char *text);
	/* Socket */
	void ReadPacket();
	void SendPacket(int32_t size, MSG_HEADER *pak);
	void GetTick() { time_tick = timeGetTime(); }
	int32_t Connect(char *pproxy_ip = 0, int32_t pproxy_port = 0);
	void Disconnect(int32_t c = 0);
	/* Class */
	struct tm *GetTime();
	void WriteData();
	char *GetIPServerlist(int32_t sv, int32_t cn);
	void ReadConfig();
	void WriteLog(const char *format, ...);
	void DebugLog(const char *format, ...);
	void Initialize();
	void Run();
	Client();
	~Client();
};

#endif // CLIENT_HPP_INCLUDED
